import prompt from '@system.prompt';
import router from '@system.router';
import * as info from '../../common/city.js'
export default {
    data: {
        valueName: '',
        namelist:[
            {
                      "id" : "110101",
                      "name" : "东城",
                      "pinyin" : "DongCheng"
                  },
                  {
                      "id" : "110102",
                      "name" : "西城",
                      "pinyin" : "XiCheng"
                  },
                  {
                      "id" : "110105",
                      "name" : "朝阳",
                      "pinyin" : "ChaoYang"
                  },
                  {
                      "id" : "110106",
                      "name" : "丰台",
                      "pinyin" : "FengTai"
                  },
                  {
                      "id" : "110107",
                      "name" : "石景山",
                      "pinyin" : "ShiJingShan"
                  },
                  {
                      "id" : "110108",
                      "name" : "海淀",
                      "pinyin" : "HaiDian"
                  },
                  {
                      "id" : "110109",
                      "name" : "门头沟",
                      "pinyin" : "MenTouGou"
                  },
                  {
                      "id" : "110111",
                      "name" : "房山",
                      "pinyin" : "FangShan"
                  },
                  {
                      "id" : "110112",
                      "name" : "通州",
                      "pinyin" : "TongZhou"
                  },
                  {
                      "id" : "110113",
                      "name" : "顺义",
                      "pinyin" : "ShunYi"
                  },
                  {
                      "id" : "110114",
                      "name" : "昌平",
                      "pinyin" : "ChangPing"
                  },
                  {
                      "id" : "110115",
                      "name" : "大兴",
                      "pinyin" : "DaXing"
                  },
                  {
                      "id" : "110116",
                      "name" : "怀柔",
                      "pinyin" : "HuaiRou"
                  },
                  {
                      "id" : "110117",
                      "name" : "平谷",
                      "pinyin" : "PingGu"
                  },
                  {
                      "id" : "110118",
                      "name" : "密云",
                      "pinyin" : "MiYun"
                  },
                  {
                      "id" : "110119",
                      "name" : "延庆",
                      "pinyin" : "YanQing"
                  },{
            "id" : "120101",
            "name" : "和平",
            "pinyin" : "HePing"
          },
          {
            "id" : "120102",
            "name" : "河东",
            "pinyin" : "HeDong"
          },
          {
            "id" : "120103",
            "name" : "河西",
            "pinyin" : "HeXi"
          },
          {
            "id" : "120104",
            "name" : "南开",
            "pinyin" : "NanKai"
          },
          {
            "id" : "120105",
            "name" : "河北",
            "pinyin" : "HeBei"
          },
          {
            "id" : "120106",
            "name" : "红桥",
            "pinyin" : "HongQiao"
          },
          {
            "id" : "120110",
            "name" : "东丽",
            "pinyin" : "DongLi"
          },
          {
            "id" : "120111",
            "name" : "西青",
            "pinyin" : "XiQing"
          },
          {
            "id" : "120112",
            "name" : "津南",
            "pinyin" : "JinNan"
          },
          {
            "id" : "120113",
            "name" : "北辰",
            "pinyin" : "BeiChen"
          },
          {
            "id" : "120114",
            "name" : "武清",
            "pinyin" : "WuQing"
          },
          {
            "id" : "120115",
            "name" : "宝坻",
            "pinyin" : "BaoDi"
          },
          {
            "id" : "120116",
            "name" : "滨海",
            "pinyin" : "BinHaiXin"
          },
          {
            "id" : "120117",
            "name" : "宁河",
            "pinyin" : "NingHe"
          },
          {
            "id" : "120118",
            "name" : "静海",
            "pinyin" : "JingHai"
          },
          {
            "id" : "120119",
            "name" : "蓟州",
            "pinyin" : "JiZhou"
          },{
            "id" : "120100",
            "name" : "天津",
            "pinyin" : "Tianjin"
          },{
                      "id" : "130102",
                      "name" : "长安",
                      "pinyin" : "ChangAn"
                  },
                  {
                      "id" : "130104",
                      "name" : "桥西",
                      "pinyin" : "QiaoXi"
                  },
                  {
                      "id" : "130105",
                      "name" : "新华",
                      "pinyin" : "XinHua"
                  },
                  {
                      "id" : "130107",
                      "name" : "井陉矿区",
                      "pinyin" : "JingXingKuangQu"
                  },
                  {
                      "id" : "130108",
                      "name" : "裕华",
                      "pinyin" : "YuHua"
                  },
                  {
                      "id" : "130109",
                      "name" : "藁城",
                      "pinyin" : "GaoCheng"
                  },
                  {
                      "id" : "130110",
                      "name" : "鹿泉",
                      "pinyin" : "LuQuan"
                  },
                  {
                      "id" : "130111",
                      "name" : "栾城",
                      "pinyin" : "LuanCheng"
                  },
                  {
                      "id" : "130121",
                      "name" : "井陉",
                      "pinyin" : "JingXing"
                  },
                  {
                      "id" : "130123",
                      "name" : "正定",
                      "pinyin" : "ZhengDing"
                  },
                  {
                      "id" : "130125",
                      "name" : "行唐",
                      "pinyin" : "XingTang"
                  },
                  {
                      "id" : "130126",
                      "name" : "灵寿",
                      "pinyin" : "LingShou"
                  },
                  {
                      "id" : "130127",
                      "name" : "高邑",
                      "pinyin" : "GaoYi"
                  },
                  {
                      "id" : "130128",
                      "name" : "深泽",
                      "pinyin" : "ShenZe"
                  },
                  {
                      "id" : "130129",
                      "name" : "赞皇",
                      "pinyin" : "ZanHuang"
                  },
                  {
                      "id" : "130130",
                      "name" : "无极",
                      "pinyin" : "WuJi"
                  },
                  {
                      "id" : "130131",
                      "name" : "平山",
                      "pinyin" : "PingShan"
                  },
                  {
                      "id" : "130132",
                      "name" : "元氏",
                      "pinyin" : "YuanShi"
                  },
                  {
                      "id" : "130133",
                      "name" : "赵县",
                      "pinyin" : "ZhaoXian"
                  },
                  {
                      "id" : "130181",
                      "name" : "辛集",
                      "pinyin" : "XinJi"
                  },
                  {
                      "id" : "130183",
                      "name" : "晋州",
                      "pinyin" : "JinZhou"
                  },
                  {
                      "id" : "130184",
                      "name" : "新乐",
                      "pinyin" : "XinLe"
                  },{
                      "id" : "130100",
                      "name" : "石家庄",
                      "pinyin" : "ShiJiaZhuang"
                  }
        ],
        listData:[],
    },
    onInit() {

        let cityData = info.default.city;
        let citylist = [];
        cityData.forEach(item => {
            item.list.forEach(itemChild => {
                let obj = {
                    name: itemChild.name,
                    pinyin: itemChild.pinyin,
                    id: itemChild.id,

                }
                citylist.push(obj)
            });
        });
        console.log('数据'+JSON.stringify(citylist))
        this.listData = citylist;
    },
    chageModel(val_obj){
        this.valueName = val_obj.text;
    },
    clearInput(){
        this.valueName = "";
        this.listData = this.namelist;
    },
    // 搜索功能
    handleInputConfirm(str) {
        if(str === '') return prompt.showToast({
            message: '请输入搜索内容'
        });
        let filterList =  this.listData.filter(item => (item.name).indexOf(str) > -1)
        console.log('filterList',filterList)
        this.listData = filterList;
    },
    nextPage(){
        router.push({
            uri: 'pages/listGroup/listGroup',
        })
    }

}