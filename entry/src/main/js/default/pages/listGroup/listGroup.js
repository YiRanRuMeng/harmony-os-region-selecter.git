import prompt from '@system.prompt';
import router from '@system.router';
import * as info from '../../common/city.js'
export default {
    data: {
        direction: 'column',

        list: [],
        last:true
    },
    onInit() {
        console.log("城市数据"+ info.default.city)
        this.list = info.default.city;
    },
    returnBack(){
        router.back();
    },
    collapse(e) {
        prompt.showToast({
            message: 'Close ' + e.groupid
        })
    },
    expand(e) {
        prompt.showToast({
            message: 'Open ' + e.groupid
        })
    }

}