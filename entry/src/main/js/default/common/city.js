let city =[
    {
        "id" : 110000,
        "list" : [
            {
                "id" : 110100,
                "list" : [
                    {
                        "id" : "110101",
                        "name" : "东城",
                        "pinyin" : "DongCheng"
                    },
                    {
                        "id" : "110102",
                        "name" : "西城",
                        "pinyin" : "XiCheng"
                    },
                    {
                        "id" : "110105",
                        "name" : "朝阳",
                        "pinyin" : "ChaoYang"
                    },
                    {
                        "id" : "110106",
                        "name" : "丰台",
                        "pinyin" : "FengTai"
                    },
                    {
                        "id" : "110107",
                        "name" : "石景山",
                        "pinyin" : "ShiJingShan"
                    },
                    {
                        "id" : "110108",
                        "name" : "海淀",
                        "pinyin" : "HaiDian"
                    },
                    {
                        "id" : "110109",
                        "name" : "门头沟",
                        "pinyin" : "MenTouGou"
                    },
                    {
                        "id" : "110111",
                        "name" : "房山",
                        "pinyin" : "FangShan"
                    },
                    {
                        "id" : "110112",
                        "name" : "通州",
                        "pinyin" : "TongZhou"
                    },
                    {
                        "id" : "110113",
                        "name" : "顺义",
                        "pinyin" : "ShunYi"
                    },
                    {
                        "id" : "110114",
                        "name" : "昌平",
                        "pinyin" : "ChangPing"
                    },
                    {
                        "id" : "110115",
                        "name" : "大兴",
                        "pinyin" : "DaXing"
                    },
                    {
                        "id" : "110116",
                        "name" : "怀柔",
                        "pinyin" : "HuaiRou"
                    },
                    {
                        "id" : "110117",
                        "name" : "平谷",
                        "pinyin" : "PingGu"
                    },
                    {
                        "id" : "110118",
                        "name" : "密云",
                        "pinyin" : "MiYun"
                    },
                    {
                        "id" : "110119",
                        "name" : "延庆",
                        "pinyin" : "YanQing"
                    }
                ],
                "name" : "北京",
                "pinyin" : "BeiJing"
            }
        ],
        "name" : "北京",
        "pinyin" : "BeiJing"
    },
    {
        "id" : 120000,
        "list" : [
            {
                "id" : 120100,
                "list" : [
                    {
                        "id" : "120101",
                        "name" : "和平",
                        "pinyin" : "HePing"
                    },
                    {
                        "id" : "120102",
                        "name" : "河东",
                        "pinyin" : "HeDong"
                    },
                    {
                        "id" : "120103",
                        "name" : "河西",
                        "pinyin" : "HeXi"
                    },
                    {
                        "id" : "120104",
                        "name" : "南开",
                        "pinyin" : "NanKai"
                    },
                    {
                        "id" : "120105",
                        "name" : "河北",
                        "pinyin" : "HeBei"
                    },
                    {
                        "id" : "120106",
                        "name" : "红桥",
                        "pinyin" : "HongQiao"
                    },
                    {
                        "id" : "120110",
                        "name" : "东丽",
                        "pinyin" : "DongLi"
                    },
                    {
                        "id" : "120111",
                        "name" : "西青",
                        "pinyin" : "XiQing"
                    },
                    {
                        "id" : "120112",
                        "name" : "津南",
                        "pinyin" : "JinNan"
                    },
                    {
                        "id" : "120113",
                        "name" : "北辰",
                        "pinyin" : "BeiChen"
                    },
                    {
                        "id" : "120114",
                        "name" : "武清",
                        "pinyin" : "WuQing"
                    },
                    {
                        "id" : "120115",
                        "name" : "宝坻",
                        "pinyin" : "BaoDi"
                    },
                    {
                        "id" : "120116",
                        "name" : "滨海",
                        "pinyin" : "BinHaiXin"
                    },
                    {
                        "id" : "120117",
                        "name" : "宁河",
                        "pinyin" : "NingHe"
                    },
                    {
                        "id" : "120118",
                        "name" : "静海",
                        "pinyin" : "JingHai"
                    },
                    {
                        "id" : "120119",
                        "name" : "蓟州",
                        "pinyin" : "JiZhou"
                    }
                ],
                "name" : "天津",
                "pinyin" : "TianJin"
            }
        ],
        "name" : "天津",
        "pinyin" : "TianJin"
    },
    {
        "id" : 130000,
        "list" : [
            {
                "id" : 130100,
                "list" : [
                    {
                        "id" : "130102",
                        "name" : "长安",
                        "pinyin" : "ChangAn"
                    },
                    {
                        "id" : "130104",
                        "name" : "桥西",
                        "pinyin" : "QiaoXi"
                    },
                    {
                        "id" : "130105",
                        "name" : "新华",
                        "pinyin" : "XinHua"
                    },
                    {
                        "id" : "130107",
                        "name" : "井陉矿区",
                        "pinyin" : "JingXingKuangQu"
                    },
                    {
                        "id" : "130108",
                        "name" : "裕华",
                        "pinyin" : "YuHua"
                    },
                    {
                        "id" : "130109",
                        "name" : "藁城",
                        "pinyin" : "GaoCheng"
                    },
                    {
                        "id" : "130110",
                        "name" : "鹿泉",
                        "pinyin" : "LuQuan"
                    },
                    {
                        "id" : "130111",
                        "name" : "栾城",
                        "pinyin" : "LuanCheng"
                    },
                    {
                        "id" : "130121",
                        "name" : "井陉",
                        "pinyin" : "JingXing"
                    },
                    {
                        "id" : "130123",
                        "name" : "正定",
                        "pinyin" : "ZhengDing"
                    },
                    {
                        "id" : "130125",
                        "name" : "行唐",
                        "pinyin" : "XingTang"
                    },
                    {
                        "id" : "130126",
                        "name" : "灵寿",
                        "pinyin" : "LingShou"
                    },
                    {
                        "id" : "130127",
                        "name" : "高邑",
                        "pinyin" : "GaoYi"
                    },
                    {
                        "id" : "130128",
                        "name" : "深泽",
                        "pinyin" : "ShenZe"
                    },
                    {
                        "id" : "130129",
                        "name" : "赞皇",
                        "pinyin" : "ZanHuang"
                    },
                    {
                        "id" : "130130",
                        "name" : "无极",
                        "pinyin" : "WuJi"
                    },
                    {
                        "id" : "130131",
                        "name" : "平山",
                        "pinyin" : "PingShan"
                    },
                    {
                        "id" : "130132",
                        "name" : "元氏",
                        "pinyin" : "YuanShi"
                    },
                    {
                        "id" : "130133",
                        "name" : "赵县",
                        "pinyin" : "ZhaoXian"
                    },
                    {
                        "id" : "130181",
                        "name" : "辛集",
                        "pinyin" : "XinJi"
                    },
                    {
                        "id" : "130183",
                        "name" : "晋州",
                        "pinyin" : "JinZhou"
                    },
                    {
                        "id" : "130184",
                        "name" : "新乐",
                        "pinyin" : "XinLe"
                    }
                ],
                "name" : "石家庄",
                "pinyin" : "ShiJiaZhuang"
            },
            {
                "id" : 130200,
                "list" : [
                    {
                        "id" : "130202",
                        "name" : "路南",
                        "pinyin" : "LuNan"
                    },
                    {
                        "id" : "130203",
                        "name" : "路北",
                        "pinyin" : "LuBei"
                    },
                    {
                        "id" : "130204",
                        "name" : "古冶",
                        "pinyin" : "GuYe"
                    },
                    {
                        "id" : "130205",
                        "name" : "开平",
                        "pinyin" : "KaiPing"
                    },
                    {
                        "id" : "130207",
                        "name" : "丰南",
                        "pinyin" : "FengNan"
                    },
                    {
                        "id" : "130208",
                        "name" : "丰润",
                        "pinyin" : "FengRun"
                    },
                    {
                        "id" : "130209",
                        "name" : "曹妃甸",
                        "pinyin" : "CaoFeiDian"
                    },
                    {
                        "id" : "130224",
                        "name" : "滦南",
                        "pinyin" : "LuanNan"
                    },
                    {
                        "id" : "130225",
                        "name" : "乐亭",
                        "pinyin" : "LeTing"
                    },
                    {
                        "id" : "130227",
                        "name" : "迁西",
                        "pinyin" : "QianXi"
                    },
                    {
                        "id" : "130229",
                        "name" : "玉田",
                        "pinyin" : "YuTian"
                    },
                    {
                        "id" : "130281",
                        "name" : "遵化",
                        "pinyin" : "ZunHua"
                    },
                    {
                        "id" : "130283",
                        "name" : "迁安",
                        "pinyin" : "QianAn"
                    },
                    {
                        "id" : "130284",
                        "name" : "滦州",
                        "pinyin" : "LuanZhou"
                    }
                ],
                "name" : "唐山",
                "pinyin" : "TangShan"
            },
            {
                "id" : 130300,
                "list" : [
                    {
                        "id" : "130302",
                        "name" : "海港",
                        "pinyin" : "HaiGang"
                    },
                    {
                        "id" : "130303",
                        "name" : "山海关",
                        "pinyin" : "ShanHaiGuan"
                    },
                    {
                        "id" : "130304",
                        "name" : "北戴河",
                        "pinyin" : "BeiDaiHe"
                    },
                    {
                        "id" : "130306",
                        "name" : "抚宁",
                        "pinyin" : "FuNing"
                    },
                    {
                        "id" : "130321",
                        "name" : "青龙",
                        "pinyin" : "QingLong"
                    },
                    {
                        "id" : "130322",
                        "name" : "昌黎",
                        "pinyin" : "ChangLi"
                    },
                    {
                        "id" : "130324",
                        "name" : "卢龙",
                        "pinyin" : "LuLong"
                    }
                ],
                "name" : "秦皇岛",
                "pinyin" : "QinHuangDao"
            },
            {
                "id" : 130400,
                "list" : [
                    {
                        "id" : "130402",
                        "name" : "邯山",
                        "pinyin" : "HanShan"
                    },
                    {
                        "id" : "130403",
                        "name" : "丛台",
                        "pinyin" : "CongTai"
                    },
                    {
                        "id" : "130404",
                        "name" : "复兴",
                        "pinyin" : "FuXing"
                    },
                    {
                        "id" : "130406",
                        "name" : "峰峰",
                        "pinyin" : "FengFeng"
                    },
                    {
                        "id" : "130407",
                        "name" : "肥乡",
                        "pinyin" : "FeiXiang"
                    },
                    {
                        "id" : "130408",
                        "name" : "永年",
                        "pinyin" : "YongNian"
                    },
                    {
                        "id" : "130423",
                        "name" : "临漳",
                        "pinyin" : "LinZhang"
                    },
                    {
                        "id" : "130424",
                        "name" : "成安",
                        "pinyin" : "ChengAn"
                    },
                    {
                        "id" : "130425",
                        "name" : "大名",
                        "pinyin" : "DaMing"
                    },
                    {
                        "id" : "130426",
                        "name" : "涉县",
                        "pinyin" : "SheXian"
                    },
                    {
                        "id" : "130427",
                        "name" : "磁县",
                        "pinyin" : "CiXian"
                    },
                    {
                        "id" : "130430",
                        "name" : "邱县",
                        "pinyin" : "QiuXian"
                    },
                    {
                        "id" : "130431",
                        "name" : "鸡泽",
                        "pinyin" : "JiZe"
                    },
                    {
                        "id" : "130432",
                        "name" : "广平",
                        "pinyin" : "GuangPing"
                    },
                    {
                        "id" : "130433",
                        "name" : "馆陶",
                        "pinyin" : "GuanTao"
                    },
                    {
                        "id" : "130434",
                        "name" : "魏县",
                        "pinyin" : "WeiXian"
                    },
                    {
                        "id" : "130435",
                        "name" : "曲周",
                        "pinyin" : "QuZhou"
                    },
                    {
                        "id" : "130481",
                        "name" : "武安",
                        "pinyin" : "WuAn"
                    }
                ],
                "name" : "邯郸",
                "pinyin" : "HanDan"
            },
            {
                "id" : 130500,
                "list" : [
                    {
                        "id" : "130502",
                        "name" : "桥东",
                        "pinyin" : "QiaoDong"
                    },
                    {
                        "id" : "130503",
                        "name" : "桥西",
                        "pinyin" : "QiaoXi"
                    },
                    {
                        "id" : "130521",
                        "name" : "邢台",
                        "pinyin" : "XingTai"
                    },
                    {
                        "id" : "130522",
                        "name" : "临城",
                        "pinyin" : "LinCheng"
                    },
                    {
                        "id" : "130523",
                        "name" : "内丘",
                        "pinyin" : "NaQiu"
                    },
                    {
                        "id" : "130524",
                        "name" : "柏乡",
                        "pinyin" : "BaiXiang"
                    },
                    {
                        "id" : "130525",
                        "name" : "隆尧",
                        "pinyin" : "LongYao"
                    },
                    {
                        "id" : "130526",
                        "name" : "任县",
                        "pinyin" : "RenXian"
                    },
                    {
                        "id" : "130527",
                        "name" : "南和",
                        "pinyin" : "NanHe"
                    },
                    {
                        "id" : "130528",
                        "name" : "宁晋",
                        "pinyin" : "NingJin"
                    },
                    {
                        "id" : "130529",
                        "name" : "巨鹿",
                        "pinyin" : "JuLu"
                    },
                    {
                        "id" : "130530",
                        "name" : "新河",
                        "pinyin" : "XinHe"
                    },
                    {
                        "id" : "130531",
                        "name" : "广宗",
                        "pinyin" : "GuangZong"
                    },
                    {
                        "id" : "130532",
                        "name" : "平乡",
                        "pinyin" : "PingXiang"
                    },
                    {
                        "id" : "130533",
                        "name" : "威县",
                        "pinyin" : "WeiXian"
                    },
                    {
                        "id" : "130534",
                        "name" : "清河",
                        "pinyin" : "QingHe"
                    },
                    {
                        "id" : "130535",
                        "name" : "临西",
                        "pinyin" : "LinXi"
                    },
                    {
                        "id" : "130581",
                        "name" : "南宫",
                        "pinyin" : "NanGong"
                    },
                    {
                        "id" : "130582",
                        "name" : "沙河",
                        "pinyin" : "ShaHe"
                    }
                ],
                "name" : "邢台",
                "pinyin" : "XingTai"
            },
            {
                "id" : 130600,
                "list" : [
                    {
                        "id" : "130602",
                        "name" : "竞秀",
                        "pinyin" : "JingXiu"
                    },
                    {
                        "id" : "130606",
                        "name" : "莲池",
                        "pinyin" : "LianChi"
                    },
                    {
                        "id" : "130607",
                        "name" : "满城",
                        "pinyin" : "ManCheng"
                    },
                    {
                        "id" : "130608",
                        "name" : "清苑",
                        "pinyin" : "QingYuan"
                    },
                    {
                        "id" : "130609",
                        "name" : "徐水",
                        "pinyin" : "XuShui"
                    },
                    {
                        "id" : "130623",
                        "name" : "涞水",
                        "pinyin" : "LaiShui"
                    },
                    {
                        "id" : "130624",
                        "name" : "阜平",
                        "pinyin" : "FuPing"
                    },
                    {
                        "id" : "130626",
                        "name" : "定兴",
                        "pinyin" : "DingXing"
                    },
                    {
                        "id" : "130627",
                        "name" : "唐县",
                        "pinyin" : "TangXian"
                    },
                    {
                        "id" : "130628",
                        "name" : "高阳",
                        "pinyin" : "GaoYang"
                    },
                    {
                        "id" : "130629",
                        "name" : "容城",
                        "pinyin" : "RongCheng"
                    },
                    {
                        "id" : "130630",
                        "name" : "涞源",
                        "pinyin" : "LaiYuan"
                    },
                    {
                        "id" : "130631",
                        "name" : "望都",
                        "pinyin" : "WangDu"
                    },
                    {
                        "id" : "130632",
                        "name" : "安新",
                        "pinyin" : "AnXin"
                    },
                    {
                        "id" : "130633",
                        "name" : "易县",
                        "pinyin" : "YiXian"
                    },
                    {
                        "id" : "130634",
                        "name" : "曲阳",
                        "pinyin" : "QuYang"
                    },
                    {
                        "id" : "130635",
                        "name" : "蠡县",
                        "pinyin" : "LiXian"
                    },
                    {
                        "id" : "130636",
                        "name" : "顺平",
                        "pinyin" : "ShunPing"
                    },
                    {
                        "id" : "130637",
                        "name" : "博野",
                        "pinyin" : "BoYe"
                    },
                    {
                        "id" : "130638",
                        "name" : "雄县",
                        "pinyin" : "XiongXian"
                    },
                    {
                        "id" : "130681",
                        "name" : "涿州",
                        "pinyin" : "ZhuoZhou"
                    },
                    {
                        "id" : "130682",
                        "name" : "定州",
                        "pinyin" : "DingZhou"
                    },
                    {
                        "id" : "130683",
                        "name" : "安国",
                        "pinyin" : "AnGuo"
                    },
                    {
                        "id" : "130684",
                        "name" : "高碑店",
                        "pinyin" : "GaoBeiDian"
                    }
                ],
                "name" : "保定",
                "pinyin" : "BaoDing"
            },
            {
                "id" : 130700,
                "list" : [
                    {
                        "id" : "130702",
                        "name" : "桥东",
                        "pinyin" : "QiaoDong"
                    },
                    {
                        "id" : "130703",
                        "name" : "桥西",
                        "pinyin" : "QiaoXi"
                    },
                    {
                        "id" : "130705",
                        "name" : "宣化",
                        "pinyin" : "XuanHua"
                    },
                    {
                        "id" : "130706",
                        "name" : "下花园",
                        "pinyin" : "XiaHuaYuan"
                    },
                    {
                        "id" : "130708",
                        "name" : "万全",
                        "pinyin" : "WanQuan"
                    },
                    {
                        "id" : "130709",
                        "name" : "崇礼",
                        "pinyin" : "ChongLi"
                    },
                    {
                        "id" : "130722",
                        "name" : "张北",
                        "pinyin" : "ZhangBei"
                    },
                    {
                        "id" : "130723",
                        "name" : "康保",
                        "pinyin" : "KangBao"
                    },
                    {
                        "id" : "130724",
                        "name" : "沽源",
                        "pinyin" : "GuYuan"
                    },
                    {
                        "id" : "130725",
                        "name" : "尚义",
                        "pinyin" : "ShangYi"
                    },
                    {
                        "id" : "130726",
                        "name" : "蔚县",
                        "pinyin" : "YuXian"
                    },
                    {
                        "id" : "130727",
                        "name" : "阳原",
                        "pinyin" : "YangYuan"
                    },
                    {
                        "id" : "130728",
                        "name" : "怀安",
                        "pinyin" : "HuaiAn"
                    },
                    {
                        "id" : "130730",
                        "name" : "怀来",
                        "pinyin" : "HuaiLai"
                    },
                    {
                        "id" : "130731",
                        "name" : "涿鹿",
                        "pinyin" : "ZhuoLu"
                    },
                    {
                        "id" : "130732",
                        "name" : "赤城",
                        "pinyin" : "ChiCheng"
                    }
                ],
                "name" : "张家口",
                "pinyin" : "ZhangJiaKou"
            },
            {
                "id" : 130800,
                "list" : [
                    {
                        "id" : "130802",
                        "name" : "双桥",
                        "pinyin" : "ShuangQiao"
                    },
                    {
                        "id" : "130803",
                        "name" : "双滦",
                        "pinyin" : "ShuangLuan"
                    },
                    {
                        "id" : "130804",
                        "name" : "鹰手营子",
                        "pinyin" : "YingShouYingZi"
                    },
                    {
                        "id" : "130821",
                        "name" : "承德",
                        "pinyin" : "ChengDe"
                    },
                    {
                        "id" : "130822",
                        "name" : "兴隆",
                        "pinyin" : "XingLong"
                    },
                    {
                        "id" : "130824",
                        "name" : "滦平",
                        "pinyin" : "LuanPing"
                    },
                    {
                        "id" : "130825",
                        "name" : "隆化",
                        "pinyin" : "LongHua"
                    },
                    {
                        "id" : "130826",
                        "name" : "丰宁",
                        "pinyin" : "FengNing"
                    },
                    {
                        "id" : "130827",
                        "name" : "宽城",
                        "pinyin" : "KuanCheng"
                    },
                    {
                        "id" : "130828",
                        "name" : "围场",
                        "pinyin" : "WeiChang"
                    },
                    {
                        "id" : "130881",
                        "name" : "平泉",
                        "pinyin" : "PingQuan"
                    }
                ],
                "name" : "承德",
                "pinyin" : "ChengDe"
            },
            {
                "id" : 130900,
                "list" : [
                    {
                        "id" : "130902",
                        "name" : "新华",
                        "pinyin" : "XinHua"
                    },
                    {
                        "id" : "130903",
                        "name" : "运河",
                        "pinyin" : "YunHe"
                    },
                    {
                        "id" : "130921",
                        "name" : "沧县",
                        "pinyin" : "CangXian"
                    },
                    {
                        "id" : "130922",
                        "name" : "青县",
                        "pinyin" : "QingXian"
                    },
                    {
                        "id" : "130923",
                        "name" : "东光",
                        "pinyin" : "DongGuang"
                    },
                    {
                        "id" : "130924",
                        "name" : "海兴",
                        "pinyin" : "HaiXing"
                    },
                    {
                        "id" : "130925",
                        "name" : "盐山",
                        "pinyin" : "YanShan"
                    },
                    {
                        "id" : "130926",
                        "name" : "肃宁",
                        "pinyin" : "SuNing"
                    },
                    {
                        "id" : "130927",
                        "name" : "南皮",
                        "pinyin" : "NanPi"
                    },
                    {
                        "id" : "130928",
                        "name" : "吴桥",
                        "pinyin" : "WuQiao"
                    },
                    {
                        "id" : "130929",
                        "name" : "献县",
                        "pinyin" : "XianXian"
                    },
                    {
                        "id" : "130930",
                        "name" : "孟村",
                        "pinyin" : "MengCun"
                    },
                    {
                        "id" : "130981",
                        "name" : "泊头",
                        "pinyin" : "BoTou"
                    },
                    {
                        "id" : "130982",
                        "name" : "任丘",
                        "pinyin" : "RenQiu"
                    },
                    {
                        "id" : "130983",
                        "name" : "黄骅",
                        "pinyin" : "HuangHua"
                    },
                    {
                        "id" : "130984",
                        "name" : "河间",
                        "pinyin" : "HeJian"
                    }
                ],
                "name" : "沧州",
                "pinyin" : "CangZhou"
            },
            {
                "id" : 131000,
                "list" : [
                    {
                        "id" : "131002",
                        "name" : "安次",
                        "pinyin" : "AnCi"
                    },
                    {
                        "id" : "131003",
                        "name" : "广阳",
                        "pinyin" : "GuangYang"
                    },
                    {
                        "id" : "131022",
                        "name" : "固安",
                        "pinyin" : "GuAn"
                    },
                    {
                        "id" : "131023",
                        "name" : "永清",
                        "pinyin" : "YongQing"
                    },
                    {
                        "id" : "131024",
                        "name" : "香河",
                        "pinyin" : "XiangHe"
                    },
                    {
                        "id" : "131025",
                        "name" : "大城",
                        "pinyin" : "DaCheng"
                    },
                    {
                        "id" : "131026",
                        "name" : "文安",
                        "pinyin" : "WenAn"
                    },
                    {
                        "id" : "131028",
                        "name" : "大厂",
                        "pinyin" : "DaChang"
                    },
                    {
                        "id" : "131081",
                        "name" : "霸州",
                        "pinyin" : "BaZhou"
                    },
                    {
                        "id" : "131082",
                        "name" : "三河",
                        "pinyin" : "SanHe"
                    }
                ],
                "name" : "廊坊",
                "pinyin" : "LangFang"
            },
            {
                "id" : 131100,
                "list" : [
                    {
                        "id" : "131102",
                        "name" : "桃城",
                        "pinyin" : "TaoCheng"
                    },
                    {
                        "id" : "131103",
                        "name" : "冀州",
                        "pinyin" : "JiZhou"
                    },
                    {
                        "id" : "131121",
                        "name" : "枣强",
                        "pinyin" : "ZaoQiang"
                    },
                    {
                        "id" : "131122",
                        "name" : "武邑",
                        "pinyin" : "WuYi"
                    },
                    {
                        "id" : "131123",
                        "name" : "武强",
                        "pinyin" : "WuQiang"
                    },
                    {
                        "id" : "131124",
                        "name" : "饶阳",
                        "pinyin" : "RaoYang"
                    },
                    {
                        "id" : "131125",
                        "name" : "安平",
                        "pinyin" : "AnPing"
                    },
                    {
                        "id" : "131126",
                        "name" : "故城",
                        "pinyin" : "GuCheng"
                    },
                    {
                        "id" : "131127",
                        "name" : "景县",
                        "pinyin" : "JingXian"
                    },
                    {
                        "id" : "131128",
                        "name" : "阜城",
                        "pinyin" : "FuCheng"
                    },
                    {
                        "id" : "131182",
                        "name" : "深州",
                        "pinyin" : "ShenZhou"
                    }
                ],
                "name" : "衡水",
                "pinyin" : "HengShui"
            }
        ],
        "name" : "河北",
        "pinyin" : "HeBei"
    },
    {
        "id" : 140000,
        "list" : [
            {
                "id" : 140100,
                "list" : [
                    {
                        "id" : "140105",
                        "name" : "小店",
                        "pinyin" : "XiaoDian"
                    },
                    {
                        "id" : "140106",
                        "name" : "迎泽",
                        "pinyin" : "YingZe"
                    },
                    {
                        "id" : "140107",
                        "name" : "杏花岭",
                        "pinyin" : "XingHuaLing"
                    },
                    {
                        "id" : "140108",
                        "name" : "尖草坪",
                        "pinyin" : "JianCaoPing"
                    },
                    {
                        "id" : "140109",
                        "name" : "万柏林",
                        "pinyin" : "WanBaiLin"
                    },
                    {
                        "id" : "140110",
                        "name" : "晋源",
                        "pinyin" : "JinYuan"
                    },
                    {
                        "id" : "140121",
                        "name" : "清徐",
                        "pinyin" : "QingXu"
                    },
                    {
                        "id" : "140122",
                        "name" : "阳曲",
                        "pinyin" : "YangQu"
                    },
                    {
                        "id" : "140123",
                        "name" : "娄烦",
                        "pinyin" : "LouFan"
                    },
                    {
                        "id" : "140181",
                        "name" : "古交",
                        "pinyin" : "GuJiao"
                    }
                ],
                "name" : "太原",
                "pinyin" : "TaiYuan"
            },
            {
                "id" : 140200,
                "list" : [
                    {
                        "id" : "140212",
                        "name" : "新荣",
                        "pinyin" : "XinRong"
                    },
                    {
                        "id" : "140213",
                        "name" : "平城",
                        "pinyin" : "PingCheng"
                    },
                    {
                        "id" : "140214",
                        "name" : "云冈",
                        "pinyin" : "YunGang"
                    },
                    {
                        "id" : "140215",
                        "name" : "云州",
                        "pinyin" : "YunZhou"
                    },
                    {
                        "id" : "140221",
                        "name" : "阳高",
                        "pinyin" : "YangGao"
                    },
                    {
                        "id" : "140222",
                        "name" : "天镇",
                        "pinyin" : "TianZhen"
                    },
                    {
                        "id" : "140223",
                        "name" : "广灵",
                        "pinyin" : "GuangLing"
                    },
                    {
                        "id" : "140224",
                        "name" : "灵丘",
                        "pinyin" : "LingQiu"
                    },
                    {
                        "id" : "140225",
                        "name" : "浑源",
                        "pinyin" : "HunYuan"
                    },
                    {
                        "id" : "140226",
                        "name" : "左云",
                        "pinyin" : "ZuoYun"
                    }
                ],
                "name" : "大同",
                "pinyin" : "DaTong"
            },
            {
                "id" : 140300,
                "list" : [
                    {
                        "id" : "140302",
                        "name" : "城区",
                        "pinyin" : "ChengQu"
                    },
                    {
                        "id" : "140303",
                        "name" : "矿区",
                        "pinyin" : "KuangQu"
                    },
                    {
                        "id" : "140311",
                        "name" : "郊区",
                        "pinyin" : "JiaoQu"
                    },
                    {
                        "id" : "140321",
                        "name" : "平定",
                        "pinyin" : "PingDing"
                    },
                    {
                        "id" : "140322",
                        "name" : "盂县",
                        "pinyin" : "YuXian"
                    }
                ],
                "name" : "阳泉",
                "pinyin" : "YangQuan"
            },
            {
                "id" : 140400,
                "list" : [
                    {
                        "id" : "140403",
                        "name" : "潞州",
                        "pinyin" : "LuZhou"
                    },
                    {
                        "id" : "140404",
                        "name" : "上党",
                        "pinyin" : "ShangDang"
                    },
                    {
                        "id" : "140405",
                        "name" : "屯留",
                        "pinyin" : "TunLiu"
                    },
                    {
                        "id" : "140406",
                        "name" : "潞城",
                        "pinyin" : "LuCheng"
                    },
                    {
                        "id" : "140423",
                        "name" : "襄垣",
                        "pinyin" : "XiangYuan"
                    },
                    {
                        "id" : "140425",
                        "name" : "平顺",
                        "pinyin" : "PingShun"
                    },
                    {
                        "id" : "140426",
                        "name" : "黎城",
                        "pinyin" : "LiCheng"
                    },
                    {
                        "id" : "140427",
                        "name" : "壶关",
                        "pinyin" : "HuGuan"
                    },
                    {
                        "id" : "140428",
                        "name" : "长子",
                        "pinyin" : "ChangZi"
                    },
                    {
                        "id" : "140429",
                        "name" : "武乡",
                        "pinyin" : "WuXiang"
                    },
                    {
                        "id" : "140430",
                        "name" : "沁县",
                        "pinyin" : "QinXian"
                    },
                    {
                        "id" : "140431",
                        "name" : "沁源",
                        "pinyin" : "QinYuan"
                    }
                ],
                "name" : "长治",
                "pinyin" : "ChangZhi"
            },
            {
                "id" : 140500,
                "list" : [
                    {
                        "id" : "140502",
                        "name" : "城区",
                        "pinyin" : "ChengQu"
                    },
                    {
                        "id" : "140521",
                        "name" : "沁水",
                        "pinyin" : "QinShui"
                    },
                    {
                        "id" : "140522",
                        "name" : "阳城",
                        "pinyin" : "YangCheng"
                    },
                    {
                        "id" : "140524",
                        "name" : "陵川",
                        "pinyin" : "LingChuan"
                    },
                    {
                        "id" : "140525",
                        "name" : "泽州",
                        "pinyin" : "ZeZhou"
                    },
                    {
                        "id" : "140581",
                        "name" : "高平",
                        "pinyin" : "GaoPing"
                    }
                ],
                "name" : "晋城",
                "pinyin" : "JinCheng"
            },
            {
                "id" : 140600,
                "list" : [
                    {
                        "id" : "140602",
                        "name" : "朔城",
                        "pinyin" : "ShuoCheng"
                    },
                    {
                        "id" : "140603",
                        "name" : "平鲁",
                        "pinyin" : "PingLu"
                    },
                    {
                        "id" : "140621",
                        "name" : "山阴",
                        "pinyin" : "ShanYin"
                    },
                    {
                        "id" : "140622",
                        "name" : "应县",
                        "pinyin" : "YingXian"
                    },
                    {
                        "id" : "140623",
                        "name" : "右玉",
                        "pinyin" : "YouYu"
                    },
                    {
                        "id" : "140681",
                        "name" : "怀仁",
                        "pinyin" : "HuaiRen"
                    }
                ],
                "name" : "朔州",
                "pinyin" : "ShuoZhou"
            },
            {
                "id" : 140700,
                "list" : [
                    {
                        "id" : "140702",
                        "name" : "榆次",
                        "pinyin" : "YuCi"
                    },
                    {
                        "id" : "140721",
                        "name" : "榆社",
                        "pinyin" : "YuShe"
                    },
                    {
                        "id" : "140722",
                        "name" : "左权",
                        "pinyin" : "ZuoQuan"
                    },
                    {
                        "id" : "140723",
                        "name" : "和顺",
                        "pinyin" : "HeShun"
                    },
                    {
                        "id" : "140724",
                        "name" : "昔阳",
                        "pinyin" : "XiYang"
                    },
                    {
                        "id" : "140725",
                        "name" : "寿阳",
                        "pinyin" : "ShouYang"
                    },
                    {
                        "id" : "140726",
                        "name" : "太谷",
                        "pinyin" : "TaiGu"
                    },
                    {
                        "id" : "140727",
                        "name" : "祁县",
                        "pinyin" : "QiXian"
                    },
                    {
                        "id" : "140728",
                        "name" : "平遥",
                        "pinyin" : "PingYao"
                    },
                    {
                        "id" : "140729",
                        "name" : "灵石",
                        "pinyin" : "LingShi"
                    },
                    {
                        "id" : "140781",
                        "name" : "介休",
                        "pinyin" : "JieXiu"
                    }
                ],
                "name" : "晋中",
                "pinyin" : "JinZhong"
            },
            {
                "id" : 140800,
                "list" : [
                    {
                        "id" : "140802",
                        "name" : "盐湖",
                        "pinyin" : "YanHu"
                    },
                    {
                        "id" : "140821",
                        "name" : "临猗",
                        "pinyin" : "LinYi"
                    },
                    {
                        "id" : "140822",
                        "name" : "万荣",
                        "pinyin" : "WanRong"
                    },
                    {
                        "id" : "140823",
                        "name" : "闻喜",
                        "pinyin" : "WenXi"
                    },
                    {
                        "id" : "140824",
                        "name" : "稷山",
                        "pinyin" : "JiShan"
                    },
                    {
                        "id" : "140825",
                        "name" : "新绛",
                        "pinyin" : "XinJiang"
                    },
                    {
                        "id" : "140826",
                        "name" : "绛县",
                        "pinyin" : "JiangXian"
                    },
                    {
                        "id" : "140827",
                        "name" : "垣曲",
                        "pinyin" : "YuanQu"
                    },
                    {
                        "id" : "140828",
                        "name" : "夏县",
                        "pinyin" : "XiaXian"
                    },
                    {
                        "id" : "140829",
                        "name" : "平陆",
                        "pinyin" : "PingLu"
                    },
                    {
                        "id" : "140830",
                        "name" : "芮城",
                        "pinyin" : "RuiCheng"
                    },
                    {
                        "id" : "140881",
                        "name" : "永济",
                        "pinyin" : "YongJi"
                    },
                    {
                        "id" : "140882",
                        "name" : "河津",
                        "pinyin" : "HeJin"
                    }
                ],
                "name" : "运城",
                "pinyin" : "YunCheng"
            },
            {
                "id" : 140900,
                "list" : [
                    {
                        "id" : "140902",
                        "name" : "忻府",
                        "pinyin" : "XinFu"
                    },
                    {
                        "id" : "140921",
                        "name" : "定襄",
                        "pinyin" : "DingXiang"
                    },
                    {
                        "id" : "140922",
                        "name" : "五台",
                        "pinyin" : "WuTai"
                    },
                    {
                        "id" : "140923",
                        "name" : "代县",
                        "pinyin" : "DaiXian"
                    },
                    {
                        "id" : "140924",
                        "name" : "繁峙",
                        "pinyin" : "FanShi"
                    },
                    {
                        "id" : "140925",
                        "name" : "宁武",
                        "pinyin" : "NingWu"
                    },
                    {
                        "id" : "140926",
                        "name" : "静乐",
                        "pinyin" : "JingLe"
                    },
                    {
                        "id" : "140927",
                        "name" : "神池",
                        "pinyin" : "ShenChi"
                    },
                    {
                        "id" : "140928",
                        "name" : "五寨",
                        "pinyin" : "WuZhai"
                    },
                    {
                        "id" : "140929",
                        "name" : "岢岚",
                        "pinyin" : "KeLan"
                    },
                    {
                        "id" : "140930",
                        "name" : "河曲",
                        "pinyin" : "HeQu"
                    },
                    {
                        "id" : "140931",
                        "name" : "保德",
                        "pinyin" : "BaoDe"
                    },
                    {
                        "id" : "140932",
                        "name" : "偏关",
                        "pinyin" : "PianGuan"
                    },
                    {
                        "id" : "140981",
                        "name" : "原平",
                        "pinyin" : "YuanPing"
                    }
                ],
                "name" : "忻州",
                "pinyin" : "XinZhou"
            },
            {
                "id" : 141000,
                "list" : [
                    {
                        "id" : "141002",
                        "name" : "尧都",
                        "pinyin" : "YaoDu"
                    },
                    {
                        "id" : "141021",
                        "name" : "曲沃",
                        "pinyin" : "QuWo"
                    },
                    {
                        "id" : "141022",
                        "name" : "翼城",
                        "pinyin" : "YiCheng"
                    },
                    {
                        "id" : "141023",
                        "name" : "襄汾",
                        "pinyin" : "XiangFen"
                    },
                    {
                        "id" : "141024",
                        "name" : "洪洞",
                        "pinyin" : "HongDong"
                    },
                    {
                        "id" : "141025",
                        "name" : "古县",
                        "pinyin" : "GuXian"
                    },
                    {
                        "id" : "141026",
                        "name" : "安泽",
                        "pinyin" : "AnZe"
                    },
                    {
                        "id" : "141027",
                        "name" : "浮山",
                        "pinyin" : "FuShan"
                    },
                    {
                        "id" : "141028",
                        "name" : "吉县",
                        "pinyin" : "JiXian"
                    },
                    {
                        "id" : "141029",
                        "name" : "乡宁",
                        "pinyin" : "XiangNing"
                    },
                    {
                        "id" : "141030",
                        "name" : "大宁",
                        "pinyin" : "DaNing"
                    },
                    {
                        "id" : "141031",
                        "name" : "隰县",
                        "pinyin" : "XiXian"
                    },
                    {
                        "id" : "141032",
                        "name" : "永和",
                        "pinyin" : "YongHe"
                    },
                    {
                        "id" : "141033",
                        "name" : "蒲县",
                        "pinyin" : "PuXian"
                    },
                    {
                        "id" : "141034",
                        "name" : "汾西",
                        "pinyin" : "FenXi"
                    },
                    {
                        "id" : "141081",
                        "name" : "侯马",
                        "pinyin" : "HouMa"
                    },
                    {
                        "id" : "141082",
                        "name" : "霍州",
                        "pinyin" : "HuoZhou"
                    }
                ],
                "name" : "临汾",
                "pinyin" : "LinFen"
            },
            {
                "id" : 141100,
                "list" : [
                    {
                        "id" : "141102",
                        "name" : "离石",
                        "pinyin" : "LiShi"
                    },
                    {
                        "id" : "141121",
                        "name" : "文水",
                        "pinyin" : "WenShui"
                    },
                    {
                        "id" : "141122",
                        "name" : "交城",
                        "pinyin" : "JiaoCheng"
                    },
                    {
                        "id" : "141123",
                        "name" : "兴县",
                        "pinyin" : "XingXian"
                    },
                    {
                        "id" : "141124",
                        "name" : "临县",
                        "pinyin" : "LinXian"
                    },
                    {
                        "id" : "141125",
                        "name" : "柳林",
                        "pinyin" : "LiuLin"
                    },
                    {
                        "id" : "141126",
                        "name" : "石楼",
                        "pinyin" : "ShiLou"
                    },
                    {
                        "id" : "141127",
                        "name" : "岚县",
                        "pinyin" : "LanXian"
                    },
                    {
                        "id" : "141128",
                        "name" : "方山",
                        "pinyin" : "FangShan"
                    },
                    {
                        "id" : "141129",
                        "name" : "中阳",
                        "pinyin" : "ZhongYang"
                    },
                    {
                        "id" : "141130",
                        "name" : "交口",
                        "pinyin" : "JiaoKou"
                    },
                    {
                        "id" : "141181",
                        "name" : "孝义",
                        "pinyin" : "XiaoYi"
                    },
                    {
                        "id" : "141182",
                        "name" : "汾阳",
                        "pinyin" : "FenYang"
                    }
                ],
                "name" : "吕梁",
                "pinyin" : "LvLiang"
            }
        ],
        "name" : "山西",
        "pinyin" : "ShanXi"
    },
    {
        "id" : 150000,
        "list" : [
            {
                "id" : 150100,
                "list" : [
                    {
                        "id" : "150102",
                        "name" : "新城",
                        "pinyin" : "XinCheng"
                    },
                    {
                        "id" : "150103",
                        "name" : "回民",
                        "pinyin" : "HuiMin"
                    },
                    {
                        "id" : "150104",
                        "name" : "玉泉",
                        "pinyin" : "YuQuan"
                    },
                    {
                        "id" : "150105",
                        "name" : "赛罕",
                        "pinyin" : "SaiHan"
                    },
                    {
                        "id" : "150121",
                        "name" : "土默特左旗",
                        "pinyin" : "TuMoTeZuoQi"
                    },
                    {
                        "id" : "150122",
                        "name" : "托克托",
                        "pinyin" : "TuoKeTuo"
                    },
                    {
                        "id" : "150123",
                        "name" : "和林格尔",
                        "pinyin" : "HeLinGeEr"
                    },
                    {
                        "id" : "150124",
                        "name" : "清水河",
                        "pinyin" : "QingShuiHe"
                    },
                    {
                        "id" : "150125",
                        "name" : "武川",
                        "pinyin" : "WuChuan"
                    }
                ],
                "name" : "呼和浩特",
                "pinyin" : "HuHeHaoTe"
            },
            {
                "id" : 150200,
                "list" : [
                    {
                        "id" : "150202",
                        "name" : "东河",
                        "pinyin" : "DongHe"
                    },
                    {
                        "id" : "150203",
                        "name" : "昆都仑",
                        "pinyin" : "KunDuLun"
                    },
                    {
                        "id" : "150204",
                        "name" : "青山",
                        "pinyin" : "QingShan"
                    },
                    {
                        "id" : "150205",
                        "name" : "石拐",
                        "pinyin" : "ShiGuai"
                    },
                    {
                        "id" : "150206",
                        "name" : "白云鄂博",
                        "pinyin" : "BaiYunEBo"
                    },
                    {
                        "id" : "150207",
                        "name" : "九原",
                        "pinyin" : "JiuYuan"
                    },
                    {
                        "id" : "150221",
                        "name" : "土默特右旗",
                        "pinyin" : "TuMoTeYouQi"
                    },
                    {
                        "id" : "150222",
                        "name" : "固阳",
                        "pinyin" : "GuYang"
                    },
                    {
                        "id" : "150223",
                        "name" : "达茂旗",
                        "pinyin" : "DaMaoQi"
                    }
                ],
                "name" : "包头",
                "pinyin" : "BaoTou"
            },
            {
                "id" : 150300,
                "list" : [
                    {
                        "id" : "150302",
                        "name" : "海勃湾",
                        "pinyin" : "HaiBoWan"
                    },
                    {
                        "id" : "150303",
                        "name" : "海南",
                        "pinyin" : "HaiNan"
                    },
                    {
                        "id" : "150304",
                        "name" : "乌达",
                        "pinyin" : "WuDa"
                    }
                ],
                "name" : "乌海",
                "pinyin" : "WuHai"
            },
            {
                "id" : 150400,
                "list" : [
                    {
                        "id" : "150402",
                        "name" : "红山",
                        "pinyin" : "HongShan"
                    },
                    {
                        "id" : "150403",
                        "name" : "元宝山",
                        "pinyin" : "YuanBaoShan"
                    },
                    {
                        "id" : "150404",
                        "name" : "松山",
                        "pinyin" : "SongShan"
                    },
                    {
                        "id" : "150421",
                        "name" : "阿鲁科尔沁旗",
                        "pinyin" : "ALuKeErQinQi"
                    },
                    {
                        "id" : "150422",
                        "name" : "巴林左旗",
                        "pinyin" : "BaLinZuoQi"
                    },
                    {
                        "id" : "150423",
                        "name" : "巴林右旗",
                        "pinyin" : "BaLinYouQi"
                    },
                    {
                        "id" : "150424",
                        "name" : "林西",
                        "pinyin" : "LinXi"
                    },
                    {
                        "id" : "150425",
                        "name" : "克什克腾旗",
                        "pinyin" : "KeKeTengQi"
                    },
                    {
                        "id" : "150426",
                        "name" : "翁牛特旗",
                        "pinyin" : "WengNiuTeQi"
                    },
                    {
                        "id" : "150428",
                        "name" : "喀喇沁旗",
                        "pinyin" : "KaLaQinQi"
                    },
                    {
                        "id" : "150429",
                        "name" : "宁城",
                        "pinyin" : "NingCheng"
                    },
                    {
                        "id" : "150430",
                        "name" : "敖汉旗",
                        "pinyin" : "AoHanQi"
                    }
                ],
                "name" : "赤峰",
                "pinyin" : "ChiFeng"
            },
            {
                "id" : 150500,
                "list" : [
                    {
                        "id" : "150502",
                        "name" : "科尔沁",
                        "pinyin" : "KeErQin"
                    },
                    {
                        "id" : "150521",
                        "name" : "科尔沁左翼中旗",
                        "pinyin" : "KeErQinZuoYiZhongQi"
                    },
                    {
                        "id" : "150522",
                        "name" : "科尔沁左翼后旗",
                        "pinyin" : "KeErQinZuoYiHouQi"
                    },
                    {
                        "id" : "150523",
                        "name" : "开鲁",
                        "pinyin" : "KaiLu"
                    },
                    {
                        "id" : "150524",
                        "name" : "库伦旗",
                        "pinyin" : "KuLunQi"
                    },
                    {
                        "id" : "150525",
                        "name" : "奈曼旗",
                        "pinyin" : "NaiManQi"
                    },
                    {
                        "id" : "150526",
                        "name" : "扎鲁特旗",
                        "pinyin" : "ZhaLuTeQi"
                    },
                    {
                        "id" : "150581",
                        "name" : "霍林郭勒",
                        "pinyin" : "HuoLinGuoLe"
                    }
                ],
                "name" : "通辽",
                "pinyin" : "TongLiao"
            },
            {
                "id" : 150600,
                "list" : [
                    {
                        "id" : "150602",
                        "name" : "东胜",
                        "pinyin" : "DongSheng"
                    },
                    {
                        "id" : "150603",
                        "name" : "康巴什",
                        "pinyin" : "KangBaShi"
                    },
                    {
                        "id" : "150621",
                        "name" : "达拉特旗",
                        "pinyin" : "DaLaTeQi"
                    },
                    {
                        "id" : "150622",
                        "name" : "准格尔旗",
                        "pinyin" : "ZhunGeErQi"
                    },
                    {
                        "id" : "150623",
                        "name" : "鄂托克前旗",
                        "pinyin" : "ETuoKeQianQi"
                    },
                    {
                        "id" : "150624",
                        "name" : "鄂托克旗",
                        "pinyin" : "ETuoKeQi"
                    },
                    {
                        "id" : "150625",
                        "name" : "杭锦旗",
                        "pinyin" : "HangJinQi"
                    },
                    {
                        "id" : "150626",
                        "name" : "乌审旗",
                        "pinyin" : "WuShenQi"
                    },
                    {
                        "id" : "150627",
                        "name" : "伊金霍洛旗",
                        "pinyin" : "YiJinHuoLuoQi"
                    }
                ],
                "name" : "鄂尔多斯",
                "pinyin" : "EErDuoSi"
            },
            {
                "id" : 150700,
                "list" : [
                    {
                        "id" : "150702",
                        "name" : "海拉尔",
                        "pinyin" : "HaiLaEr"
                    },
                    {
                        "id" : "150703",
                        "name" : "扎赉诺尔",
                        "pinyin" : "ZhaLaiNuoEr"
                    },
                    {
                        "id" : "150721",
                        "name" : "阿荣旗",
                        "pinyin" : "ARongQi"
                    },
                    {
                        "id" : "150722",
                        "name" : "莫力达瓦",
                        "pinyin" : "MoLiDaWa"
                    },
                    {
                        "id" : "150723",
                        "name" : "鄂伦春",
                        "pinyin" : "ELunChun"
                    },
                    {
                        "id" : "150724",
                        "name" : "鄂温克族",
                        "pinyin" : "EWenKeZu"
                    },
                    {
                        "id" : "150725",
                        "name" : "陈巴尔虎旗",
                        "pinyin" : "ChenBaErHuQi"
                    },
                    {
                        "id" : "150726",
                        "name" : "新巴尔虎左旗",
                        "pinyin" : "XinBaErHuZuoQi"
                    },
                    {
                        "id" : "150727",
                        "name" : "新巴尔虎右旗",
                        "pinyin" : "XinBaErHuYouQi"
                    },
                    {
                        "id" : "150781",
                        "name" : "满洲里",
                        "pinyin" : "ManZhouLi"
                    },
                    {
                        "id" : "150782",
                        "name" : "牙克石",
                        "pinyin" : "YaKeShi"
                    },
                    {
                        "id" : "150783",
                        "name" : "扎兰屯",
                        "pinyin" : "ZhaLanTun"
                    },
                    {
                        "id" : "150784",
                        "name" : "额尔古纳",
                        "pinyin" : "EErGuNa"
                    },
                    {
                        "id" : "150785",
                        "name" : "根河",
                        "pinyin" : "GenHe"
                    }
                ],
                "name" : "呼伦贝尔",
                "pinyin" : "HuLunBeiEr"
            },
            {
                "id" : 150800,
                "list" : [
                    {
                        "id" : "150802",
                        "name" : "临河",
                        "pinyin" : "LinHe"
                    },
                    {
                        "id" : "150821",
                        "name" : "五原",
                        "pinyin" : "WuYuan"
                    },
                    {
                        "id" : "150822",
                        "name" : "磴口",
                        "pinyin" : "DengKou"
                    },
                    {
                        "id" : "150823",
                        "name" : "乌拉特前旗",
                        "pinyin" : "WuLaTeQianQi"
                    },
                    {
                        "id" : "150824",
                        "name" : "乌拉特中旗",
                        "pinyin" : "WuLaTeZhongQi"
                    },
                    {
                        "id" : "150825",
                        "name" : "乌拉特后旗",
                        "pinyin" : "WuLaTeHouQi"
                    },
                    {
                        "id" : "150826",
                        "name" : "杭锦后旗",
                        "pinyin" : "HangJinHouQi"
                    }
                ],
                "name" : "巴彦淖尔",
                "pinyin" : "BaYanNeEr"
            },
            {
                "id" : 150900,
                "list" : [
                    {
                        "id" : "150902",
                        "name" : "集宁",
                        "pinyin" : "JiNing"
                    },
                    {
                        "id" : "150921",
                        "name" : "卓资",
                        "pinyin" : "ZhuoZi"
                    },
                    {
                        "id" : "150922",
                        "name" : "化德",
                        "pinyin" : "HuaDe"
                    },
                    {
                        "id" : "150923",
                        "name" : "商都",
                        "pinyin" : "ShangDu"
                    },
                    {
                        "id" : "150924",
                        "name" : "兴和",
                        "pinyin" : "XingHe"
                    },
                    {
                        "id" : "150925",
                        "name" : "凉城",
                        "pinyin" : "LiangCheng"
                    },
                    {
                        "id" : "150926",
                        "name" : "察哈尔右翼前旗",
                        "pinyin" : "ChaHaErYouYiQianQi"
                    },
                    {
                        "id" : "150927",
                        "name" : "察哈尔右翼中旗",
                        "pinyin" : "ChaHaErYouYiZhongQi"
                    },
                    {
                        "id" : "150928",
                        "name" : "察哈尔右翼后旗",
                        "pinyin" : "ChaHaErYouYiHouQi"
                    },
                    {
                        "id" : "150929",
                        "name" : "四子王旗",
                        "pinyin" : "SiZiWangQi"
                    },
                    {
                        "id" : "150981",
                        "name" : "丰镇",
                        "pinyin" : "FengZhen"
                    }
                ],
                "name" : "乌兰察布",
                "pinyin" : "WuLanChaBu"
            },
            {
                "id" : 152200,
                "list" : [
                    {
                        "id" : "152201",
                        "name" : "乌兰浩特",
                        "pinyin" : "WuLanHaoTe"
                    },
                    {
                        "id" : "152202",
                        "name" : "阿尔山",
                        "pinyin" : "AErShan"
                    },
                    {
                        "id" : "152221",
                        "name" : "科尔沁右翼前旗",
                        "pinyin" : "KeErQinYouYiQianQi"
                    },
                    {
                        "id" : "152222",
                        "name" : "科尔沁右翼中旗",
                        "pinyin" : "KeErQinYouYiZhongQi"
                    },
                    {
                        "id" : "152223",
                        "name" : "扎赉特旗",
                        "pinyin" : "ZhaLaiTeQi"
                    },
                    {
                        "id" : "152224",
                        "name" : "突泉",
                        "pinyin" : "TuQuan"
                    }
                ],
                "name" : "兴安盟",
                "pinyin" : "XingAnMeng"
            },
            {
                "id" : 152500,
                "list" : [
                    {
                        "id" : "152501",
                        "name" : "二连浩特",
                        "pinyin" : "ErLianHaoTe"
                    },
                    {
                        "id" : "152502",
                        "name" : "锡林浩特",
                        "pinyin" : "XiLinHaoTe"
                    },
                    {
                        "id" : "152522",
                        "name" : "阿巴嘎旗",
                        "pinyin" : "ABaGaQi"
                    },
                    {
                        "id" : "152523",
                        "name" : "苏尼特左旗",
                        "pinyin" : "SuNiTeZuoQi"
                    },
                    {
                        "id" : "152524",
                        "name" : "苏尼特右旗",
                        "pinyin" : "SuNiTeYouQi"
                    },
                    {
                        "id" : "152525",
                        "name" : "东乌珠穆沁旗",
                        "pinyin" : "DongWuZhuMuQinQi"
                    },
                    {
                        "id" : "152526",
                        "name" : "西乌珠穆沁旗",
                        "pinyin" : "XiWuZhuMuQinQi"
                    },
                    {
                        "id" : "152527",
                        "name" : "太仆寺旗",
                        "pinyin" : "TaiPuSiQi"
                    },
                    {
                        "id" : "152528",
                        "name" : "镶黄旗",
                        "pinyin" : "XiangHuangQi"
                    },
                    {
                        "id" : "152529",
                        "name" : "正镶白旗",
                        "pinyin" : "ZhengXiangBaiQi"
                    },
                    {
                        "id" : "152530",
                        "name" : "正蓝旗",
                        "pinyin" : "ZhengLanQi"
                    },
                    {
                        "id" : "152531",
                        "name" : "多伦",
                        "pinyin" : "DuoLun"
                    }
                ],
                "name" : "锡林郭勒",
                "pinyin" : "XiLinGuoLe"
            },
            {
                "id" : 152900,
                "list" : [
                    {
                        "id" : "152921",
                        "name" : "阿拉善左旗",
                        "pinyin" : "ALaShanZuoQi"
                    },
                    {
                        "id" : "152922",
                        "name" : "阿拉善右旗",
                        "pinyin" : "ALaShanYouQi"
                    },
                    {
                        "id" : "152923",
                        "name" : "额济纳旗",
                        "pinyin" : "EJiNaQi"
                    }
                ],
                "name" : "阿拉善",
                "pinyin" : "ALaShan"
            }
        ],
        "name" : "内蒙古",
        "pinyin" : "NaMengGu"
    },
    {
        "id" : 210000,
        "list" : [
            {
                "id" : 210100,
                "list" : [
                    {
                        "id" : "210102",
                        "name" : "和平",
                        "pinyin" : "HePing"
                    },
                    {
                        "id" : "210103",
                        "name" : "沈河",
                        "pinyin" : "ShenHe"
                    },
                    {
                        "id" : "210104",
                        "name" : "大东",
                        "pinyin" : "DaDong"
                    },
                    {
                        "id" : "210105",
                        "name" : "皇姑",
                        "pinyin" : "HuangGu"
                    },
                    {
                        "id" : "210106",
                        "name" : "铁西",
                        "pinyin" : "TieXi"
                    },
                    {
                        "id" : "210111",
                        "name" : "苏家屯",
                        "pinyin" : "SuJiaTun"
                    },
                    {
                        "id" : "210112",
                        "name" : "浑南",
                        "pinyin" : "HunNan"
                    },
                    {
                        "id" : "210113",
                        "name" : "沈北新区",
                        "pinyin" : "ShenBeiXin"
                    },
                    {
                        "id" : "210114",
                        "name" : "于洪",
                        "pinyin" : "YuHong"
                    },
                    {
                        "id" : "210115",
                        "name" : "辽中",
                        "pinyin" : "LiaoZhong"
                    },
                    {
                        "id" : "210123",
                        "name" : "康平",
                        "pinyin" : "KangPing"
                    },
                    {
                        "id" : "210124",
                        "name" : "法库",
                        "pinyin" : "FaKu"
                    },
                    {
                        "id" : "210181",
                        "name" : "新民",
                        "pinyin" : "XinMin"
                    }
                ],
                "name" : "沈阳",
                "pinyin" : "ShenYang"
            },
            {
                "id" : 210200,
                "list" : [
                    {
                        "id" : "210202",
                        "name" : "中山",
                        "pinyin" : "ZhongShan"
                    },
                    {
                        "id" : "210203",
                        "name" : "西岗",
                        "pinyin" : "XiGang"
                    },
                    {
                        "id" : "210204",
                        "name" : "沙河口",
                        "pinyin" : "ShaHeKou"
                    },
                    {
                        "id" : "210211",
                        "name" : "甘井子",
                        "pinyin" : "GanJingZi"
                    },
                    {
                        "id" : "210212",
                        "name" : "旅顺口",
                        "pinyin" : "LvShunKou"
                    },
                    {
                        "id" : "210213",
                        "name" : "金州",
                        "pinyin" : "JinZhou"
                    },
                    {
                        "id" : "210214",
                        "name" : "普兰店",
                        "pinyin" : "PuLanDian"
                    },
                    {
                        "id" : "210224",
                        "name" : "长海",
                        "pinyin" : "ChangHai"
                    },
                    {
                        "id" : "210281",
                        "name" : "瓦房店",
                        "pinyin" : "WaFangDian"
                    },
                    {
                        "id" : "210283",
                        "name" : "庄河",
                        "pinyin" : "ZhuangHe"
                    }
                ],
                "name" : "大连",
                "pinyin" : "DaLian"
            },
            {
                "id" : 210300,
                "list" : [
                    {
                        "id" : "210302",
                        "name" : "铁东",
                        "pinyin" : "TieDong"
                    },
                    {
                        "id" : "210303",
                        "name" : "铁西",
                        "pinyin" : "TieXi"
                    },
                    {
                        "id" : "210304",
                        "name" : "立山",
                        "pinyin" : "LiShan"
                    },
                    {
                        "id" : "210311",
                        "name" : "千山",
                        "pinyin" : "QianShan"
                    },
                    {
                        "id" : "210321",
                        "name" : "台安",
                        "pinyin" : "TaiAn"
                    },
                    {
                        "id" : "210323",
                        "name" : "岫岩",
                        "pinyin" : "XiuYan"
                    },
                    {
                        "id" : "210381",
                        "name" : "海城",
                        "pinyin" : "HaiCheng"
                    }
                ],
                "name" : "鞍山",
                "pinyin" : "AnShan"
            },
            {
                "id" : 210400,
                "list" : [
                    {
                        "id" : "210402",
                        "name" : "新抚",
                        "pinyin" : "XinFu"
                    },
                    {
                        "id" : "210403",
                        "name" : "东洲",
                        "pinyin" : "DongZhou"
                    },
                    {
                        "id" : "210404",
                        "name" : "望花",
                        "pinyin" : "WangHua"
                    },
                    {
                        "id" : "210411",
                        "name" : "顺城",
                        "pinyin" : "ShunCheng"
                    },
                    {
                        "id" : "210421",
                        "name" : "抚顺",
                        "pinyin" : "FuShun"
                    },
                    {
                        "id" : "210422",
                        "name" : "新宾",
                        "pinyin" : "XinBin"
                    },
                    {
                        "id" : "210423",
                        "name" : "清原",
                        "pinyin" : "QingYuan"
                    }
                ],
                "name" : "抚顺",
                "pinyin" : "FuShun"
            },
            {
                "id" : 210500,
                "list" : [
                    {
                        "id" : "210502",
                        "name" : "平山",
                        "pinyin" : "PingShan"
                    },
                    {
                        "id" : "210503",
                        "name" : "溪湖",
                        "pinyin" : "XiHu"
                    },
                    {
                        "id" : "210504",
                        "name" : "明山",
                        "pinyin" : "MingShan"
                    },
                    {
                        "id" : "210505",
                        "name" : "南芬",
                        "pinyin" : "NanFen"
                    },
                    {
                        "id" : "210521",
                        "name" : "本溪",
                        "pinyin" : "BenXi"
                    },
                    {
                        "id" : "210522",
                        "name" : "桓仁",
                        "pinyin" : "HuanRen"
                    }
                ],
                "name" : "本溪",
                "pinyin" : "BenXi"
            },
            {
                "id" : 210600,
                "list" : [
                    {
                        "id" : "210602",
                        "name" : "元宝",
                        "pinyin" : "YuanBao"
                    },
                    {
                        "id" : "210603",
                        "name" : "振兴",
                        "pinyin" : "ZhenXing"
                    },
                    {
                        "id" : "210604",
                        "name" : "振安",
                        "pinyin" : "ZhenAn"
                    },
                    {
                        "id" : "210624",
                        "name" : "宽甸",
                        "pinyin" : "KuanDian"
                    },
                    {
                        "id" : "210681",
                        "name" : "东港",
                        "pinyin" : "DongGang"
                    },
                    {
                        "id" : "210682",
                        "name" : "凤城",
                        "pinyin" : "FengCheng"
                    }
                ],
                "name" : "丹东",
                "pinyin" : "DanDong"
            },
            {
                "id" : 210700,
                "list" : [
                    {
                        "id" : "210702",
                        "name" : "古塔",
                        "pinyin" : "GuTa"
                    },
                    {
                        "id" : "210703",
                        "name" : "凌河",
                        "pinyin" : "LingHe"
                    },
                    {
                        "id" : "210711",
                        "name" : "太和",
                        "pinyin" : "TaiHe"
                    },
                    {
                        "id" : "210726",
                        "name" : "黑山",
                        "pinyin" : "HeiShan"
                    },
                    {
                        "id" : "210727",
                        "name" : "义县",
                        "pinyin" : "YiXian"
                    },
                    {
                        "id" : "210781",
                        "name" : "凌海",
                        "pinyin" : "LingHai"
                    },
                    {
                        "id" : "210782",
                        "name" : "北镇",
                        "pinyin" : "BeiZhen"
                    }
                ],
                "name" : "锦州",
                "pinyin" : "JinZhou"
            },
            {
                "id" : 210800,
                "list" : [
                    {
                        "id" : "210802",
                        "name" : "站前",
                        "pinyin" : "ZhanQian"
                    },
                    {
                        "id" : "210803",
                        "name" : "西市",
                        "pinyin" : "XiShi"
                    },
                    {
                        "id" : "210804",
                        "name" : "鲅鱼圈",
                        "pinyin" : "BaYuQuan"
                    },
                    {
                        "id" : "210811",
                        "name" : "老边",
                        "pinyin" : "LaoBian"
                    },
                    {
                        "id" : "210881",
                        "name" : "盖州",
                        "pinyin" : "GaiZhou"
                    },
                    {
                        "id" : "210882",
                        "name" : "大石桥",
                        "pinyin" : "DaShiQiao"
                    }
                ],
                "name" : "营口",
                "pinyin" : "YingKou"
            },
            {
                "id" : 210900,
                "list" : [
                    {
                        "id" : "210902",
                        "name" : "海州",
                        "pinyin" : "HaiZhou"
                    },
                    {
                        "id" : "210903",
                        "name" : "新邱",
                        "pinyin" : "XinQiu"
                    },
                    {
                        "id" : "210904",
                        "name" : "太平",
                        "pinyin" : "TaiPing"
                    },
                    {
                        "id" : "210905",
                        "name" : "清河门",
                        "pinyin" : "QingHeMen"
                    },
                    {
                        "id" : "210911",
                        "name" : "细河",
                        "pinyin" : "XiHe"
                    },
                    {
                        "id" : "210921",
                        "name" : "阜新",
                        "pinyin" : "FuXin"
                    },
                    {
                        "id" : "210922",
                        "name" : "彰武",
                        "pinyin" : "ZhangWu"
                    }
                ],
                "name" : "阜新",
                "pinyin" : "FuXin"
            },
            {
                "id" : 211000,
                "list" : [
                    {
                        "id" : "211002",
                        "name" : "白塔",
                        "pinyin" : "BaiTa"
                    },
                    {
                        "id" : "211003",
                        "name" : "文圣",
                        "pinyin" : "WenSheng"
                    },
                    {
                        "id" : "211004",
                        "name" : "宏伟",
                        "pinyin" : "HongWei"
                    },
                    {
                        "id" : "211005",
                        "name" : "弓长岭",
                        "pinyin" : "GongChangLing"
                    },
                    {
                        "id" : "211011",
                        "name" : "太子河",
                        "pinyin" : "TaiZiHe"
                    },
                    {
                        "id" : "211021",
                        "name" : "辽阳",
                        "pinyin" : "LiaoYang"
                    },
                    {
                        "id" : "211081",
                        "name" : "灯塔",
                        "pinyin" : "DengTa"
                    }
                ],
                "name" : "辽阳",
                "pinyin" : "LiaoYang"
            },
            {
                "id" : 211100,
                "list" : [
                    {
                        "id" : "211102",
                        "name" : "双台子",
                        "pinyin" : "ShuangTaiZi"
                    },
                    {
                        "id" : "211103",
                        "name" : "兴隆台",
                        "pinyin" : "XingLongTai"
                    },
                    {
                        "id" : "211104",
                        "name" : "大洼",
                        "pinyin" : "DaWa"
                    },
                    {
                        "id" : "211122",
                        "name" : "盘山",
                        "pinyin" : "PanShan"
                    }
                ],
                "name" : "盘锦",
                "pinyin" : "PanJin"
            },
            {
                "id" : 211200,
                "list" : [
                    {
                        "id" : "211202",
                        "name" : "银州",
                        "pinyin" : "YinZhou"
                    },
                    {
                        "id" : "211204",
                        "name" : "清河",
                        "pinyin" : "QingHe"
                    },
                    {
                        "id" : "211221",
                        "name" : "铁岭",
                        "pinyin" : "TieLing"
                    },
                    {
                        "id" : "211223",
                        "name" : "西丰",
                        "pinyin" : "XiFeng"
                    },
                    {
                        "id" : "211224",
                        "name" : "昌图",
                        "pinyin" : "ChangTu"
                    },
                    {
                        "id" : "211281",
                        "name" : "调兵山",
                        "pinyin" : "DiaoBingShan"
                    },
                    {
                        "id" : "211282",
                        "name" : "开原",
                        "pinyin" : "KaiYuan"
                    }
                ],
                "name" : "铁岭",
                "pinyin" : "TieLing"
            },
            {
                "id" : 211300,
                "list" : [
                    {
                        "id" : "211302",
                        "name" : "双塔",
                        "pinyin" : "ShuangTa"
                    },
                    {
                        "id" : "211303",
                        "name" : "龙城",
                        "pinyin" : "LongCheng"
                    },
                    {
                        "id" : "211321",
                        "name" : "朝阳",
                        "pinyin" : "ChaoYang"
                    },
                    {
                        "id" : "211322",
                        "name" : "建平",
                        "pinyin" : "JianPing"
                    },
                    {
                        "id" : "211324",
                        "name" : "喀喇沁左翼",
                        "pinyin" : "KaLaQinZuoYi"
                    },
                    {
                        "id" : "211381",
                        "name" : "北票",
                        "pinyin" : "BeiPiao"
                    },
                    {
                        "id" : "211382",
                        "name" : "凌源",
                        "pinyin" : "LingYuan"
                    }
                ],
                "name" : "朝阳",
                "pinyin" : "ChaoYang"
            },
            {
                "id" : 211400,
                "list" : [
                    {
                        "id" : "211402",
                        "name" : "连山",
                        "pinyin" : "LianShan"
                    },
                    {
                        "id" : "211403",
                        "name" : "龙港",
                        "pinyin" : "LongGang"
                    },
                    {
                        "id" : "211404",
                        "name" : "南票",
                        "pinyin" : "NanPiao"
                    },
                    {
                        "id" : "211421",
                        "name" : "绥中",
                        "pinyin" : "SuiZhong"
                    },
                    {
                        "id" : "211422",
                        "name" : "建昌",
                        "pinyin" : "JianChang"
                    },
                    {
                        "id" : "211481",
                        "name" : "兴城",
                        "pinyin" : "XingCheng"
                    }
                ],
                "name" : "葫芦岛",
                "pinyin" : "HuLuDao"
            }
        ],
        "name" : "辽宁",
        "pinyin" : "LiaoNing"
    },
    {
        "id" : 220000,
        "list" : [
            {
                "id" : 220100,
                "list" : [
                    {
                        "id" : "220102",
                        "name" : "南关",
                        "pinyin" : "NanGuan"
                    },
                    {
                        "id" : "220103",
                        "name" : "宽城",
                        "pinyin" : "KuanCheng"
                    },
                    {
                        "id" : "220104",
                        "name" : "朝阳",
                        "pinyin" : "ChaoYang"
                    },
                    {
                        "id" : "220105",
                        "name" : "二道",
                        "pinyin" : "ErDao"
                    },
                    {
                        "id" : "220106",
                        "name" : "绿园",
                        "pinyin" : "LvYuan"
                    },
                    {
                        "id" : "220112",
                        "name" : "双阳",
                        "pinyin" : "ShuangYang"
                    },
                    {
                        "id" : "220113",
                        "name" : "九台",
                        "pinyin" : "JiuTai"
                    },
                    {
                        "id" : "220122",
                        "name" : "农安",
                        "pinyin" : "NongAn"
                    },
                    {
                        "id" : "220182",
                        "name" : "榆树",
                        "pinyin" : "YuShu"
                    },
                    {
                        "id" : "220183",
                        "name" : "德惠",
                        "pinyin" : "DeHui"
                    }
                ],
                "name" : "长春",
                "pinyin" : "ChangChun"
            },
            {
                "id" : 220200,
                "list" : [
                    {
                        "id" : "220202",
                        "name" : "昌邑",
                        "pinyin" : "ChangYi"
                    },
                    {
                        "id" : "220203",
                        "name" : "龙潭",
                        "pinyin" : "LongTan"
                    },
                    {
                        "id" : "220204",
                        "name" : "船营",
                        "pinyin" : "ChuanYing"
                    },
                    {
                        "id" : "220211",
                        "name" : "丰满",
                        "pinyin" : "FengMan"
                    },
                    {
                        "id" : "220221",
                        "name" : "永吉",
                        "pinyin" : "YongJi"
                    },
                    {
                        "id" : "220281",
                        "name" : "蛟河",
                        "pinyin" : "JiaoHe"
                    },
                    {
                        "id" : "220282",
                        "name" : "桦甸",
                        "pinyin" : "HuaDian"
                    },
                    {
                        "id" : "220283",
                        "name" : "舒兰",
                        "pinyin" : "ShuLan"
                    },
                    {
                        "id" : "220284",
                        "name" : "磐石",
                        "pinyin" : "PanShi"
                    }
                ],
                "name" : "吉林",
                "pinyin" : "JiLin"
            },
            {
                "id" : 220300,
                "list" : [
                    {
                        "id" : "220302",
                        "name" : "铁西",
                        "pinyin" : "TieXi"
                    },
                    {
                        "id" : "220303",
                        "name" : "铁东",
                        "pinyin" : "TieDong"
                    },
                    {
                        "id" : "220322",
                        "name" : "梨树",
                        "pinyin" : "LiShu"
                    },
                    {
                        "id" : "220323",
                        "name" : "伊通",
                        "pinyin" : "YiTong"
                    },
                    {
                        "id" : "220381",
                        "name" : "公主岭",
                        "pinyin" : "GongZhuLing"
                    },
                    {
                        "id" : "220382",
                        "name" : "双辽",
                        "pinyin" : "ShuangLiao"
                    }
                ],
                "name" : "四平",
                "pinyin" : "SiPing"
            },
            {
                "id" : 220400,
                "list" : [
                    {
                        "id" : "220402",
                        "name" : "龙山",
                        "pinyin" : "LongShan"
                    },
                    {
                        "id" : "220403",
                        "name" : "西安",
                        "pinyin" : "XiAn"
                    },
                    {
                        "id" : "220421",
                        "name" : "东丰",
                        "pinyin" : "DongFeng"
                    },
                    {
                        "id" : "220422",
                        "name" : "东辽",
                        "pinyin" : "DongLiao"
                    }
                ],
                "name" : "辽源",
                "pinyin" : "LiaoYuan"
            },
            {
                "id" : 220500,
                "list" : [
                    {
                        "id" : "220502",
                        "name" : "东昌",
                        "pinyin" : "DongChang"
                    },
                    {
                        "id" : "220503",
                        "name" : "二道江",
                        "pinyin" : "ErDaoJiang"
                    },
                    {
                        "id" : "220521",
                        "name" : "通化",
                        "pinyin" : "TongHua"
                    },
                    {
                        "id" : "220523",
                        "name" : "辉南",
                        "pinyin" : "HuiNan"
                    },
                    {
                        "id" : "220524",
                        "name" : "柳河",
                        "pinyin" : "LiuHe"
                    },
                    {
                        "id" : "220581",
                        "name" : "梅河口",
                        "pinyin" : "MeiHeKou"
                    },
                    {
                        "id" : "220582",
                        "name" : "集安",
                        "pinyin" : "JiAn"
                    }
                ],
                "name" : "通化",
                "pinyin" : "TongHua"
            },
            {
                "id" : 220600,
                "list" : [
                    {
                        "id" : "220602",
                        "name" : "浑江",
                        "pinyin" : "HunJiang"
                    },
                    {
                        "id" : "220605",
                        "name" : "江源",
                        "pinyin" : "JiangYuan"
                    },
                    {
                        "id" : "220621",
                        "name" : "抚松",
                        "pinyin" : "FuSong"
                    },
                    {
                        "id" : "220622",
                        "name" : "靖宇",
                        "pinyin" : "JingYu"
                    },
                    {
                        "id" : "220623",
                        "name" : "长白",
                        "pinyin" : "ChangBai"
                    },
                    {
                        "id" : "220681",
                        "name" : "临江",
                        "pinyin" : "LinJiang"
                    }
                ],
                "name" : "白山",
                "pinyin" : "BaiShan"
            },
            {
                "id" : 220700,
                "list" : [
                    {
                        "id" : "220702",
                        "name" : "宁江",
                        "pinyin" : "NingJiang"
                    },
                    {
                        "id" : "220721",
                        "name" : "前郭尔罗斯",
                        "pinyin" : "QianGuoErLuoSi"
                    },
                    {
                        "id" : "220722",
                        "name" : "长岭",
                        "pinyin" : "ChangLing"
                    },
                    {
                        "id" : "220723",
                        "name" : "乾安",
                        "pinyin" : "QianAn"
                    },
                    {
                        "id" : "220781",
                        "name" : "扶余",
                        "pinyin" : "FuYu"
                    }
                ],
                "name" : "松原",
                "pinyin" : "SongYuan"
            },
            {
                "id" : 220800,
                "list" : [
                    {
                        "id" : "220802",
                        "name" : "洮北",
                        "pinyin" : "DaoBeiQu"
                    },
                    {
                        "id" : "220821",
                        "name" : "镇赉",
                        "pinyin" : "ZhenLai"
                    },
                    {
                        "id" : "220822",
                        "name" : "通榆",
                        "pinyin" : "TongYu"
                    },
                    {
                        "id" : "220881",
                        "name" : "洮南",
                        "pinyin" : "DaoNan"
                    },
                    {
                        "id" : "220882",
                        "name" : "大安",
                        "pinyin" : "DaAn"
                    }
                ],
                "name" : "白城",
                "pinyin" : "BaiCheng"
            },
            {
                "id" : 222400,
                "list" : [
                    {
                        "id" : "222401",
                        "name" : "延吉",
                        "pinyin" : "YanJi"
                    },
                    {
                        "id" : "222402",
                        "name" : "图们",
                        "pinyin" : "TuMen"
                    },
                    {
                        "id" : "222403",
                        "name" : "敦化",
                        "pinyin" : "DunHua"
                    },
                    {
                        "id" : "222404",
                        "name" : "珲春",
                        "pinyin" : "HuiChun"
                    },
                    {
                        "id" : "222405",
                        "name" : "龙井",
                        "pinyin" : "LongJing"
                    },
                    {
                        "id" : "222406",
                        "name" : "和龙",
                        "pinyin" : "HeLong"
                    },
                    {
                        "id" : "222424",
                        "name" : "汪清",
                        "pinyin" : "WangQing"
                    },
                    {
                        "id" : "222426",
                        "name" : "安图",
                        "pinyin" : "AnTu"
                    }
                ],
                "name" : "延边",
                "pinyin" : "YanBian"
            }
        ],
        "name" : "吉林",
        "pinyin" : "JiLin"
    },
    {
        "id" : 230000,
        "list" : [
            {
                "id" : 230100,
                "list" : [
                    {
                        "id" : "230102",
                        "name" : "道里",
                        "pinyin" : "DaoLi"
                    },
                    {
                        "id" : "230103",
                        "name" : "南岗",
                        "pinyin" : "NanGang"
                    },
                    {
                        "id" : "230104",
                        "name" : "道外",
                        "pinyin" : "DaoWai"
                    },
                    {
                        "id" : "230108",
                        "name" : "平房",
                        "pinyin" : "PingFang"
                    },
                    {
                        "id" : "230109",
                        "name" : "松北",
                        "pinyin" : "SongBei"
                    },
                    {
                        "id" : "230110",
                        "name" : "香坊",
                        "pinyin" : "XiangFang"
                    },
                    {
                        "id" : "230111",
                        "name" : "呼兰",
                        "pinyin" : "HuLan"
                    },
                    {
                        "id" : "230112",
                        "name" : "阿城",
                        "pinyin" : "ACheng"
                    },
                    {
                        "id" : "230113",
                        "name" : "双城",
                        "pinyin" : "ShuangCheng"
                    },
                    {
                        "id" : "230123",
                        "name" : "依兰",
                        "pinyin" : "YiLan"
                    },
                    {
                        "id" : "230124",
                        "name" : "方正",
                        "pinyin" : "FangZheng"
                    },
                    {
                        "id" : "230125",
                        "name" : "宾县",
                        "pinyin" : "BinXian"
                    },
                    {
                        "id" : "230126",
                        "name" : "巴彦",
                        "pinyin" : "BaYan"
                    },
                    {
                        "id" : "230127",
                        "name" : "木兰",
                        "pinyin" : "MuLan"
                    },
                    {
                        "id" : "230128",
                        "name" : "通河",
                        "pinyin" : "TongHe"
                    },
                    {
                        "id" : "230129",
                        "name" : "延寿",
                        "pinyin" : "YanShou"
                    },
                    {
                        "id" : "230183",
                        "name" : "尚志",
                        "pinyin" : "ShangZhi"
                    },
                    {
                        "id" : "230184",
                        "name" : "五常",
                        "pinyin" : "WuChang"
                    }
                ],
                "name" : "哈尔滨",
                "pinyin" : "HaErBin"
            },
            {
                "id" : 230200,
                "list" : [
                    {
                        "id" : "230202",
                        "name" : "龙沙",
                        "pinyin" : "LongSha"
                    },
                    {
                        "id" : "230203",
                        "name" : "建华",
                        "pinyin" : "JianHua"
                    },
                    {
                        "id" : "230204",
                        "name" : "铁锋",
                        "pinyin" : "TieFeng"
                    },
                    {
                        "id" : "230205",
                        "name" : "昂昂溪",
                        "pinyin" : "AngAngXi"
                    },
                    {
                        "id" : "230206",
                        "name" : "富拉尔基",
                        "pinyin" : "FuLaErJi"
                    },
                    {
                        "id" : "230207",
                        "name" : "碾子山",
                        "pinyin" : "NianZiShan"
                    },
                    {
                        "id" : "230208",
                        "name" : "梅里斯",
                        "pinyin" : "MeiLiSi"
                    },
                    {
                        "id" : "230221",
                        "name" : "龙江",
                        "pinyin" : "LongJiang"
                    },
                    {
                        "id" : "230223",
                        "name" : "依安",
                        "pinyin" : "YiAn"
                    },
                    {
                        "id" : "230224",
                        "name" : "泰来",
                        "pinyin" : "TaiLai"
                    },
                    {
                        "id" : "230225",
                        "name" : "甘南",
                        "pinyin" : "GanNan"
                    },
                    {
                        "id" : "230227",
                        "name" : "富裕",
                        "pinyin" : "FuYu"
                    },
                    {
                        "id" : "230229",
                        "name" : "克山",
                        "pinyin" : "KeShan"
                    },
                    {
                        "id" : "230230",
                        "name" : "克东",
                        "pinyin" : "KeDong"
                    },
                    {
                        "id" : "230231",
                        "name" : "拜泉",
                        "pinyin" : "BaiQuan"
                    },
                    {
                        "id" : "230281",
                        "name" : "讷河",
                        "pinyin" : "NeHe"
                    }
                ],
                "name" : "齐齐哈尔",
                "pinyin" : "QiQiHaEr"
            },
            {
                "id" : 230300,
                "list" : [
                    {
                        "id" : "230302",
                        "name" : "鸡冠",
                        "pinyin" : "JiGuan"
                    },
                    {
                        "id" : "230303",
                        "name" : "恒山",
                        "pinyin" : "HengShan"
                    },
                    {
                        "id" : "230304",
                        "name" : "滴道",
                        "pinyin" : "DiDao"
                    },
                    {
                        "id" : "230305",
                        "name" : "梨树",
                        "pinyin" : "LiShu"
                    },
                    {
                        "id" : "230306",
                        "name" : "城子河",
                        "pinyin" : "ChengZiHe"
                    },
                    {
                        "id" : "230307",
                        "name" : "麻山",
                        "pinyin" : "MaShan"
                    },
                    {
                        "id" : "230321",
                        "name" : "鸡东",
                        "pinyin" : "JiDong"
                    },
                    {
                        "id" : "230381",
                        "name" : "虎林",
                        "pinyin" : "HuLin"
                    },
                    {
                        "id" : "230382",
                        "name" : "密山",
                        "pinyin" : "MiShan"
                    }
                ],
                "name" : "鸡西",
                "pinyin" : "JiXi"
            },
            {
                "id" : 230400,
                "list" : [
                    {
                        "id" : "230402",
                        "name" : "向阳",
                        "pinyin" : "XiangYang"
                    },
                    {
                        "id" : "230403",
                        "name" : "工农",
                        "pinyin" : "GongNong"
                    },
                    {
                        "id" : "230404",
                        "name" : "南山",
                        "pinyin" : "NanShan"
                    },
                    {
                        "id" : "230405",
                        "name" : "兴安",
                        "pinyin" : "XingAn"
                    },
                    {
                        "id" : "230406",
                        "name" : "东山",
                        "pinyin" : "DongShan"
                    },
                    {
                        "id" : "230407",
                        "name" : "兴山",
                        "pinyin" : "XingShan"
                    },
                    {
                        "id" : "230421",
                        "name" : "萝北",
                        "pinyin" : "LuoBei"
                    },
                    {
                        "id" : "230422",
                        "name" : "绥滨",
                        "pinyin" : "SuiBin"
                    }
                ],
                "name" : "鹤岗",
                "pinyin" : "HeGang"
            },
            {
                "id" : 230500,
                "list" : [
                    {
                        "id" : "230502",
                        "name" : "尖山",
                        "pinyin" : "JianShan"
                    },
                    {
                        "id" : "230503",
                        "name" : "岭东",
                        "pinyin" : "LingDong"
                    },
                    {
                        "id" : "230505",
                        "name" : "四方台",
                        "pinyin" : "SiFangTai"
                    },
                    {
                        "id" : "230506",
                        "name" : "宝山",
                        "pinyin" : "BaoShan"
                    },
                    {
                        "id" : "230521",
                        "name" : "集贤",
                        "pinyin" : "JiXian"
                    },
                    {
                        "id" : "230522",
                        "name" : "友谊",
                        "pinyin" : "YouYi"
                    },
                    {
                        "id" : "230523",
                        "name" : "宝清",
                        "pinyin" : "BaoQing"
                    },
                    {
                        "id" : "230524",
                        "name" : "饶河",
                        "pinyin" : "RaoHe"
                    }
                ],
                "name" : "双鸭山",
                "pinyin" : "ShuangYaShan"
            },
            {
                "id" : 230600,
                "list" : [
                    {
                        "id" : "230602",
                        "name" : "萨尔图",
                        "pinyin" : "SaErTu"
                    },
                    {
                        "id" : "230603",
                        "name" : "龙凤",
                        "pinyin" : "LongFeng"
                    },
                    {
                        "id" : "230604",
                        "name" : "让胡路",
                        "pinyin" : "RangHuLu"
                    },
                    {
                        "id" : "230605",
                        "name" : "红岗",
                        "pinyin" : "HongGang"
                    },
                    {
                        "id" : "230606",
                        "name" : "大同",
                        "pinyin" : "DaTong"
                    },
                    {
                        "id" : "230621",
                        "name" : "肇州",
                        "pinyin" : "ZhaoZhou"
                    },
                    {
                        "id" : "230622",
                        "name" : "肇源",
                        "pinyin" : "ZhaoYuan"
                    },
                    {
                        "id" : "230623",
                        "name" : "林甸",
                        "pinyin" : "LinDian"
                    },
                    {
                        "id" : "230624",
                        "name" : "杜尔伯特",
                        "pinyin" : "DuErBoTe"
                    }
                ],
                "name" : "大庆",
                "pinyin" : "DaQing"
            },
            {
                "id" : 230700,
                "list" : [
                    {
                        "id" : "230702",
                        "name" : "伊春",
                        "pinyin" : "YiChun"
                    },
                    {
                        "id" : "230703",
                        "name" : "南岔",
                        "pinyin" : "NanCha"
                    },
                    {
                        "id" : "230704",
                        "name" : "友好",
                        "pinyin" : "YouHao"
                    },
                    {
                        "id" : "230705",
                        "name" : "西林",
                        "pinyin" : "XiLin"
                    },
                    {
                        "id" : "230706",
                        "name" : "翠峦",
                        "pinyin" : "CuiLuan"
                    },
                    {
                        "id" : "230707",
                        "name" : "新青",
                        "pinyin" : "XinQing"
                    },
                    {
                        "id" : "230708",
                        "name" : "美溪",
                        "pinyin" : "MeiXi"
                    },
                    {
                        "id" : "230709",
                        "name" : "金山屯",
                        "pinyin" : "JinShanTun"
                    },
                    {
                        "id" : "230710",
                        "name" : "五营",
                        "pinyin" : "WuYing"
                    },
                    {
                        "id" : "230711",
                        "name" : "乌马河",
                        "pinyin" : "WuMaHe"
                    },
                    {
                        "id" : "230712",
                        "name" : "汤旺河",
                        "pinyin" : "TangWangHe"
                    },
                    {
                        "id" : "230713",
                        "name" : "带岭",
                        "pinyin" : "DaiLing"
                    },
                    {
                        "id" : "230714",
                        "name" : "乌伊岭",
                        "pinyin" : "WuYiLing"
                    },
                    {
                        "id" : "230715",
                        "name" : "红星",
                        "pinyin" : "HongXing"
                    },
                    {
                        "id" : "230716",
                        "name" : "上甘岭",
                        "pinyin" : "ShangGanLing"
                    },
                    {
                        "id" : "230722",
                        "name" : "嘉荫",
                        "pinyin" : "JiaYin"
                    },
                    {
                        "id" : "230781",
                        "name" : "铁力",
                        "pinyin" : "TieLi"
                    }
                ],
                "name" : "伊春",
                "pinyin" : "YiChun"
            },
            {
                "id" : 230800,
                "list" : [
                    {
                        "id" : "230803",
                        "name" : "向阳",
                        "pinyin" : "XiangYang"
                    },
                    {
                        "id" : "230804",
                        "name" : "前进",
                        "pinyin" : "QianJin"
                    },
                    {
                        "id" : "230805",
                        "name" : "东风",
                        "pinyin" : "DongFeng"
                    },
                    {
                        "id" : "230811",
                        "name" : "郊区",
                        "pinyin" : "JiaoQu"
                    },
                    {
                        "id" : "230822",
                        "name" : "桦南",
                        "pinyin" : "HuaNan"
                    },
                    {
                        "id" : "230826",
                        "name" : "桦川",
                        "pinyin" : "HuaChuan"
                    },
                    {
                        "id" : "230828",
                        "name" : "汤原",
                        "pinyin" : "TangYuan"
                    },
                    {
                        "id" : "230881",
                        "name" : "同江",
                        "pinyin" : "TongJiang"
                    },
                    {
                        "id" : "230882",
                        "name" : "富锦",
                        "pinyin" : "FuJin"
                    },
                    {
                        "id" : "230883",
                        "name" : "抚远",
                        "pinyin" : "FuYuan"
                    }
                ],
                "name" : "佳木斯",
                "pinyin" : "JiaMuSi"
            },
            {
                "id" : 230900,
                "list" : [
                    {
                        "id" : "230902",
                        "name" : "新兴",
                        "pinyin" : "XinXing"
                    },
                    {
                        "id" : "230903",
                        "name" : "桃山",
                        "pinyin" : "TaoShan"
                    },
                    {
                        "id" : "230904",
                        "name" : "茄子河",
                        "pinyin" : "QieZiHe"
                    },
                    {
                        "id" : "230921",
                        "name" : "勃利",
                        "pinyin" : "BoLi"
                    }
                ],
                "name" : "七台河",
                "pinyin" : "QiTaiHe"
            },
            {
                "id" : 231000,
                "list" : [
                    {
                        "id" : "231002",
                        "name" : "东安",
                        "pinyin" : "DongAn"
                    },
                    {
                        "id" : "231003",
                        "name" : "阳明",
                        "pinyin" : "YangMing"
                    },
                    {
                        "id" : "231004",
                        "name" : "爱民",
                        "pinyin" : "AiMin"
                    },
                    {
                        "id" : "231005",
                        "name" : "西安",
                        "pinyin" : "XiAn"
                    },
                    {
                        "id" : "231025",
                        "name" : "林口",
                        "pinyin" : "LinKou"
                    },
                    {
                        "id" : "231081",
                        "name" : "绥芬河",
                        "pinyin" : "SuiFenHe"
                    },
                    {
                        "id" : "231083",
                        "name" : "海林",
                        "pinyin" : "HaiLin"
                    },
                    {
                        "id" : "231084",
                        "name" : "宁安",
                        "pinyin" : "NingAn"
                    },
                    {
                        "id" : "231085",
                        "name" : "穆棱",
                        "pinyin" : "MuLing"
                    },
                    {
                        "id" : "231086",
                        "name" : "东宁",
                        "pinyin" : "DongNing"
                    }
                ],
                "name" : "牡丹江",
                "pinyin" : "MuDanJiang"
            },
            {
                "id" : 231100,
                "list" : [
                    {
                        "id" : "231102",
                        "name" : "爱辉",
                        "pinyin" : "AiHui"
                    },
                    {
                        "id" : "231121",
                        "name" : "嫩江",
                        "pinyin" : "NenJiang"
                    },
                    {
                        "id" : "231123",
                        "name" : "逊克",
                        "pinyin" : "XunKe"
                    },
                    {
                        "id" : "231124",
                        "name" : "孙吴",
                        "pinyin" : "SunWu"
                    },
                    {
                        "id" : "231181",
                        "name" : "北安",
                        "pinyin" : "BeiAn"
                    },
                    {
                        "id" : "231182",
                        "name" : "五大连池",
                        "pinyin" : "WuDaLianChi"
                    }
                ],
                "name" : "黑河",
                "pinyin" : "HeiHe"
            },
            {
                "id" : 231200,
                "list" : [
                    {
                        "id" : "231202",
                        "name" : "北林",
                        "pinyin" : "BeiLin"
                    },
                    {
                        "id" : "231221",
                        "name" : "望奎",
                        "pinyin" : "WangKui"
                    },
                    {
                        "id" : "231222",
                        "name" : "兰西",
                        "pinyin" : "LanXi"
                    },
                    {
                        "id" : "231223",
                        "name" : "青冈",
                        "pinyin" : "QingGang"
                    },
                    {
                        "id" : "231224",
                        "name" : "庆安",
                        "pinyin" : "QingAn"
                    },
                    {
                        "id" : "231225",
                        "name" : "明水",
                        "pinyin" : "MingShui"
                    },
                    {
                        "id" : "231226",
                        "name" : "绥棱",
                        "pinyin" : "SuiLing"
                    },
                    {
                        "id" : "231281",
                        "name" : "安达",
                        "pinyin" : "AnDa"
                    },
                    {
                        "id" : "231282",
                        "name" : "肇东",
                        "pinyin" : "ZhaoDong"
                    },
                    {
                        "id" : "231283",
                        "name" : "海伦",
                        "pinyin" : "HaiLun"
                    }
                ],
                "name" : "绥化",
                "pinyin" : "SuiHua"
            },
            {
                "id" : 232700,
                "list" : [
                    {
                        "id" : "232701",
                        "name" : "漠河",
                        "pinyin" : "MoHe"
                    },
                    {
                        "id" : "232721",
                        "name" : "呼玛",
                        "pinyin" : "HuMa"
                    },
                    {
                        "id" : "232722",
                        "name" : "塔河",
                        "pinyin" : "TaHe"
                    },
                    {
                        "id" : "232723",
                        "name" : "大兴安岭",
                        "pinyin" : "DaXingAnLing"
                    }
                ],
                "name" : "大兴安岭",
                "pinyin" : "DaXingAnLing"
            }
        ],
        "name" : "黑龙江",
        "pinyin" : "HeiLongJiang"
    },
    {
        "id" : 310000,
        "list" : [
            {
                "id" : 310100,
                "list" : [
                    {
                        "id" : "310101",
                        "name" : "黄浦",
                        "pinyin" : "HuangPu"
                    },
                    {
                        "id" : "310104",
                        "name" : "徐汇",
                        "pinyin" : "XuHui"
                    },
                    {
                        "id" : "310105",
                        "name" : "长宁",
                        "pinyin" : "ChangNing"
                    },
                    {
                        "id" : "310106",
                        "name" : "静安",
                        "pinyin" : "JingAn"
                    },
                    {
                        "id" : "310107",
                        "name" : "普陀",
                        "pinyin" : "PuTuo"
                    },
                    {
                        "id" : "310109",
                        "name" : "虹口",
                        "pinyin" : "HongKou"
                    },
                    {
                        "id" : "310110",
                        "name" : "杨浦",
                        "pinyin" : "YangPu"
                    },
                    {
                        "id" : "310112",
                        "name" : "闵行",
                        "pinyin" : "MinXing"
                    },
                    {
                        "id" : "310113",
                        "name" : "宝山",
                        "pinyin" : "BaoShan"
                    },
                    {
                        "id" : "310114",
                        "name" : "嘉定",
                        "pinyin" : "JiaDing"
                    },
                    {
                        "id" : "310115",
                        "name" : "浦东新区",
                        "pinyin" : "PuDongXin"
                    },
                    {
                        "id" : "310116",
                        "name" : "金山",
                        "pinyin" : "JinShan"
                    },
                    {
                        "id" : "310117",
                        "name" : "松江",
                        "pinyin" : "SongJiang"
                    },
                    {
                        "id" : "310118",
                        "name" : "青浦",
                        "pinyin" : "QingPu"
                    },
                    {
                        "id" : "310120",
                        "name" : "奉贤",
                        "pinyin" : "FengXian"
                    },
                    {
                        "id" : "310151",
                        "name" : "崇明",
                        "pinyin" : "ChongMing"
                    }
                ],
                "name" : "上海",
                "pinyin" : "ShangHai"
            }
        ],
        "name" : "上海",
        "pinyin" : "ShangHai"
    },
    {
        "id" : 320000,
        "list" : [
            {
                "id" : 320100,
                "list" : [
                    {
                        "id" : "320102",
                        "name" : "玄武",
                        "pinyin" : "XuanWu"
                    },
                    {
                        "id" : "320104",
                        "name" : "秦淮",
                        "pinyin" : "QinHuai"
                    },
                    {
                        "id" : "320105",
                        "name" : "建邺",
                        "pinyin" : "JianYe"
                    },
                    {
                        "id" : "320106",
                        "name" : "鼓楼",
                        "pinyin" : "GuLou"
                    },
                    {
                        "id" : "320111",
                        "name" : "浦口",
                        "pinyin" : "PuKou"
                    },
                    {
                        "id" : "320113",
                        "name" : "栖霞",
                        "pinyin" : "QiXia"
                    },
                    {
                        "id" : "320114",
                        "name" : "雨花台",
                        "pinyin" : "YuHuaTai"
                    },
                    {
                        "id" : "320115",
                        "name" : "江宁",
                        "pinyin" : "JiangNing"
                    },
                    {
                        "id" : "320116",
                        "name" : "六合",
                        "pinyin" : "LiuHe"
                    },
                    {
                        "id" : "320117",
                        "name" : "溧水",
                        "pinyin" : "LiShui"
                    },
                    {
                        "id" : "320118",
                        "name" : "高淳",
                        "pinyin" : "GaoChun"
                    }
                ],
                "name" : "南京",
                "pinyin" : "NanJing"
            },
            {
                "id" : 320200,
                "list" : [
                    {
                        "id" : "320205",
                        "name" : "锡山",
                        "pinyin" : "XiShan"
                    },
                    {
                        "id" : "320206",
                        "name" : "惠山",
                        "pinyin" : "HuiShan"
                    },
                    {
                        "id" : "320211",
                        "name" : "滨湖",
                        "pinyin" : "BinHu"
                    },
                    {
                        "id" : "320213",
                        "name" : "梁溪",
                        "pinyin" : "LiangXi"
                    },
                    {
                        "id" : "320214",
                        "name" : "新吴",
                        "pinyin" : "XinWu"
                    },
                    {
                        "id" : "320281",
                        "name" : "江阴",
                        "pinyin" : "JiangYin"
                    },
                    {
                        "id" : "320282",
                        "name" : "宜兴",
                        "pinyin" : "YiXing"
                    }
                ],
                "name" : "无锡",
                "pinyin" : "WuXi"
            },
            {
                "id" : 320300,
                "list" : [
                    {
                        "id" : "320302",
                        "name" : "鼓楼",
                        "pinyin" : "GuLou"
                    },
                    {
                        "id" : "320303",
                        "name" : "云龙",
                        "pinyin" : "YunLong"
                    },
                    {
                        "id" : "320305",
                        "name" : "贾汪",
                        "pinyin" : "JiaWang"
                    },
                    {
                        "id" : "320311",
                        "name" : "泉山",
                        "pinyin" : "QuanShan"
                    },
                    {
                        "id" : "320312",
                        "name" : "铜山",
                        "pinyin" : "TongShan"
                    },
                    {
                        "id" : "320321",
                        "name" : "丰县",
                        "pinyin" : "FengXian"
                    },
                    {
                        "id" : "320322",
                        "name" : "沛县",
                        "pinyin" : "PeiXian"
                    },
                    {
                        "id" : "320324",
                        "name" : "睢宁",
                        "pinyin" : "SuiNing"
                    },
                    {
                        "id" : "320381",
                        "name" : "新沂",
                        "pinyin" : "XinYi"
                    },
                    {
                        "id" : "320382",
                        "name" : "邳州",
                        "pinyin" : "PiZhou"
                    }
                ],
                "name" : "徐州",
                "pinyin" : "XuZhou"
            },
            {
                "id" : 320400,
                "list" : [
                    {
                        "id" : "320402",
                        "name" : "天宁",
                        "pinyin" : "TianNing"
                    },
                    {
                        "id" : "320404",
                        "name" : "钟楼",
                        "pinyin" : "ZhongLou"
                    },
                    {
                        "id" : "320411",
                        "name" : "新北",
                        "pinyin" : "XinBei"
                    },
                    {
                        "id" : "320412",
                        "name" : "武进",
                        "pinyin" : "WuJin"
                    },
                    {
                        "id" : "320413",
                        "name" : "金坛",
                        "pinyin" : "JinTan"
                    },
                    {
                        "id" : "320481",
                        "name" : "溧阳",
                        "pinyin" : "LiYang"
                    }
                ],
                "name" : "常州",
                "pinyin" : "ChangZhou"
            },
            {
                "id" : 320500,
                "list" : [
                    {
                        "id" : "320505",
                        "name" : "虎丘",
                        "pinyin" : "HuQiu"
                    },
                    {
                        "id" : "320506",
                        "name" : "吴中",
                        "pinyin" : "WuZhong"
                    },
                    {
                        "id" : "320507",
                        "name" : "相城",
                        "pinyin" : "XiangCheng"
                    },
                    {
                        "id" : "320508",
                        "name" : "姑苏",
                        "pinyin" : "GuSu"
                    },
                    {
                        "id" : "320509",
                        "name" : "吴江",
                        "pinyin" : "WuJiang"
                    },
                    {
                        "id" : "320571",
                        "name" : "苏州工业园区",
                        "pinyin" : "SuZhouGongYeYuanQu"
                    },
                    {
                        "id" : "320581",
                        "name" : "常熟",
                        "pinyin" : "ChangShu"
                    },
                    {
                        "id" : "320582",
                        "name" : "张家港",
                        "pinyin" : "ZhangJiaGang"
                    },
                    {
                        "id" : "320583",
                        "name" : "昆山",
                        "pinyin" : "KunShan"
                    },
                    {
                        "id" : "320585",
                        "name" : "太仓",
                        "pinyin" : "TaiCang"
                    }
                ],
                "name" : "苏州",
                "pinyin" : "SuZhou"
            },
            {
                "id" : 320600,
                "list" : [
                    {
                        "id" : "320602",
                        "name" : "崇川",
                        "pinyin" : "ChongChuan"
                    },
                    {
                        "id" : "320611",
                        "name" : "港闸",
                        "pinyin" : "GangZha"
                    },
                    {
                        "id" : "320612",
                        "name" : "通州",
                        "pinyin" : "TongZhou"
                    },
                    {
                        "id" : "320623",
                        "name" : "如东",
                        "pinyin" : "RuDong"
                    },
                    {
                        "id" : "320681",
                        "name" : "启东",
                        "pinyin" : "QiDong"
                    },
                    {
                        "id" : "320682",
                        "name" : "如皋",
                        "pinyin" : "RuGao"
                    },
                    {
                        "id" : "320684",
                        "name" : "海门",
                        "pinyin" : "HaiMen"
                    },
                    {
                        "id" : "320685",
                        "name" : "海安",
                        "pinyin" : "HaiAn"
                    }
                ],
                "name" : "南通",
                "pinyin" : "NanTong"
            },
            {
                "id" : 320700,
                "list" : [
                    {
                        "id" : "320703",
                        "name" : "连云",
                        "pinyin" : "LianYun"
                    },
                    {
                        "id" : "320706",
                        "name" : "海州",
                        "pinyin" : "HaiZhou"
                    },
                    {
                        "id" : "320707",
                        "name" : "赣榆",
                        "pinyin" : "GanYu"
                    },
                    {
                        "id" : "320722",
                        "name" : "东海",
                        "pinyin" : "DongHai"
                    },
                    {
                        "id" : "320723",
                        "name" : "灌云",
                        "pinyin" : "GuanYun"
                    },
                    {
                        "id" : "320724",
                        "name" : "灌南",
                        "pinyin" : "GuanNan"
                    }
                ],
                "name" : "连云港",
                "pinyin" : "LianYunGang"
            },
            {
                "id" : 320800,
                "list" : [
                    {
                        "id" : "320803",
                        "name" : "淮安",
                        "pinyin" : "HuaiAn"
                    },
                    {
                        "id" : "320804",
                        "name" : "淮阴",
                        "pinyin" : "HuaiYin"
                    },
                    {
                        "id" : "320812",
                        "name" : "清江浦",
                        "pinyin" : "QingJiangPu"
                    },
                    {
                        "id" : "320813",
                        "name" : "洪泽",
                        "pinyin" : "HongZe"
                    },
                    {
                        "id" : "320826",
                        "name" : "涟水",
                        "pinyin" : "LianShui"
                    },
                    {
                        "id" : "320830",
                        "name" : "盱眙",
                        "pinyin" : "Xuyi"
                    },
                    {
                        "id" : "320831",
                        "name" : "金湖",
                        "pinyin" : "JinHu"
                    }
                ],
                "name" : "淮安",
                "pinyin" : "HuaiAn"
            },
            {
                "id" : 320900,
                "list" : [
                    {
                        "id" : "320902",
                        "name" : "亭湖",
                        "pinyin" : "TingHu"
                    },
                    {
                        "id" : "320903",
                        "name" : "盐都",
                        "pinyin" : "YanDu"
                    },
                    {
                        "id" : "320904",
                        "name" : "大丰",
                        "pinyin" : "DaFeng"
                    },
                    {
                        "id" : "320921",
                        "name" : "响水",
                        "pinyin" : "XiangShui"
                    },
                    {
                        "id" : "320922",
                        "name" : "滨海",
                        "pinyin" : "BinHai"
                    },
                    {
                        "id" : "320923",
                        "name" : "阜宁",
                        "pinyin" : "FuNing"
                    },
                    {
                        "id" : "320924",
                        "name" : "射阳",
                        "pinyin" : "SheYang"
                    },
                    {
                        "id" : "320925",
                        "name" : "建湖",
                        "pinyin" : "JianHu"
                    },
                    {
                        "id" : "320981",
                        "name" : "东台",
                        "pinyin" : "DongTai"
                    }
                ],
                "name" : "盐城",
                "pinyin" : "YanCheng"
            },
            {
                "id" : 321000,
                "list" : [
                    {
                        "id" : "321002",
                        "name" : "广陵",
                        "pinyin" : "GuangLing"
                    },
                    {
                        "id" : "321003",
                        "name" : "邗江",
                        "pinyin" : "HanJiang"
                    },
                    {
                        "id" : "321012",
                        "name" : "江都",
                        "pinyin" : "JiangDu"
                    },
                    {
                        "id" : "321023",
                        "name" : "宝应",
                        "pinyin" : "BaoYing"
                    },
                    {
                        "id" : "321081",
                        "name" : "仪征",
                        "pinyin" : "YiZheng"
                    },
                    {
                        "id" : "321084",
                        "name" : "高邮",
                        "pinyin" : "GaoYou"
                    }
                ],
                "name" : "扬州",
                "pinyin" : "YangZhou"
            },
            {
                "id" : 321100,
                "list" : [
                    {
                        "id" : "321102",
                        "name" : "京口",
                        "pinyin" : "JingKou"
                    },
                    {
                        "id" : "321111",
                        "name" : "润州",
                        "pinyin" : "RunZhou"
                    },
                    {
                        "id" : "321112",
                        "name" : "丹徒",
                        "pinyin" : "DanTu"
                    },
                    {
                        "id" : "321181",
                        "name" : "丹阳",
                        "pinyin" : "DanYang"
                    },
                    {
                        "id" : "321182",
                        "name" : "扬中",
                        "pinyin" : "YangZhong"
                    },
                    {
                        "id" : "321183",
                        "name" : "句容",
                        "pinyin" : "JuRong"
                    }
                ],
                "name" : "镇江",
                "pinyin" : "ZhenJiang"
            },
            {
                "id" : 321200,
                "list" : [
                    {
                        "id" : "321202",
                        "name" : "海陵",
                        "pinyin" : "HaiLing"
                    },
                    {
                        "id" : "321203",
                        "name" : "高港",
                        "pinyin" : "GaoGang"
                    },
                    {
                        "id" : "321204",
                        "name" : "姜堰",
                        "pinyin" : "JiangYan"
                    },
                    {
                        "id" : "321281",
                        "name" : "兴化",
                        "pinyin" : "XingHua"
                    },
                    {
                        "id" : "321282",
                        "name" : "靖江",
                        "pinyin" : "JingJiang"
                    },
                    {
                        "id" : "321283",
                        "name" : "泰兴",
                        "pinyin" : "TaiXing"
                    }
                ],
                "name" : "泰州",
                "pinyin" : "TaiZhou"
            },
            {
                "id" : 321300,
                "list" : [
                    {
                        "id" : "321302",
                        "name" : "宿城",
                        "pinyin" : "SuCheng"
                    },
                    {
                        "id" : "321311",
                        "name" : "宿豫",
                        "pinyin" : "SuYu"
                    },
                    {
                        "id" : "321322",
                        "name" : "沭阳",
                        "pinyin" : "ShuYang"
                    },
                    {
                        "id" : "321323",
                        "name" : "泗阳",
                        "pinyin" : "SiYang"
                    },
                    {
                        "id" : "321324",
                        "name" : "泗洪",
                        "pinyin" : "SiHong"
                    }
                ],
                "name" : "宿迁",
                "pinyin" : "SuQian"
            }
        ],
        "name" : "江苏",
        "pinyin" : "JiangSu"
    },
    {
        "id" : 330000,
        "list" : [
            {
                "id" : 330100,
                "list" : [
                    {
                        "id" : "330102",
                        "name" : "上城",
                        "pinyin" : "ShangCheng"
                    },
                    {
                        "id" : "330103",
                        "name" : "下城",
                        "pinyin" : "XiaCheng"
                    },
                    {
                        "id" : "330104",
                        "name" : "江干",
                        "pinyin" : "JiangGan"
                    },
                    {
                        "id" : "330105",
                        "name" : "拱墅",
                        "pinyin" : "GongShu"
                    },
                    {
                        "id" : "330106",
                        "name" : "西湖",
                        "pinyin" : "XiHu"
                    },
                    {
                        "id" : "330108",
                        "name" : "滨江",
                        "pinyin" : "BinJiang"
                    },
                    {
                        "id" : "330109",
                        "name" : "萧山",
                        "pinyin" : "XiaoShan"
                    },
                    {
                        "id" : "330110",
                        "name" : "余杭",
                        "pinyin" : "YuHang"
                    },
                    {
                        "id" : "330111",
                        "name" : "富阳",
                        "pinyin" : "FuYang"
                    },
                    {
                        "id" : "330112",
                        "name" : "临安",
                        "pinyin" : "LinAn"
                    },
                    {
                        "id" : "330122",
                        "name" : "桐庐",
                        "pinyin" : "TongLu"
                    },
                    {
                        "id" : "330127",
                        "name" : "淳安",
                        "pinyin" : "ChunAn"
                    },
                    {
                        "id" : "330182",
                        "name" : "建德",
                        "pinyin" : "JianDe"
                    }
                ],
                "name" : "杭州",
                "pinyin" : "HangZhou"
            },
            {
                "id" : 330200,
                "list" : [
                    {
                        "id" : "330203",
                        "name" : "海曙",
                        "pinyin" : "HaiShu"
                    },
                    {
                        "id" : "330205",
                        "name" : "江北",
                        "pinyin" : "JiangBei"
                    },
                    {
                        "id" : "330206",
                        "name" : "北仑",
                        "pinyin" : "BeiLun"
                    },
                    {
                        "id" : "330211",
                        "name" : "镇海",
                        "pinyin" : "ZhenHai"
                    },
                    {
                        "id" : "330212",
                        "name" : "鄞州",
                        "pinyin" : "YinZhou"
                    },
                    {
                        "id" : "330213",
                        "name" : "奉化",
                        "pinyin" : "FengHua"
                    },
                    {
                        "id" : "330225",
                        "name" : "象山",
                        "pinyin" : "XiangShan"
                    },
                    {
                        "id" : "330226",
                        "name" : "宁海",
                        "pinyin" : "NingHai"
                    },
                    {
                        "id" : "330281",
                        "name" : "余姚",
                        "pinyin" : "YuYao"
                    },
                    {
                        "id" : "330282",
                        "name" : "慈溪",
                        "pinyin" : "CiXi"
                    }
                ],
                "name" : "宁波",
                "pinyin" : "NingBo"
            },
            {
                "id" : 330300,
                "list" : [
                    {
                        "id" : "330302",
                        "name" : "鹿城",
                        "pinyin" : "LuCheng"
                    },
                    {
                        "id" : "330303",
                        "name" : "龙湾",
                        "pinyin" : "LongWan"
                    },
                    {
                        "id" : "330304",
                        "name" : "瓯海",
                        "pinyin" : "OuHai"
                    },
                    {
                        "id" : "330305",
                        "name" : "洞头",
                        "pinyin" : "DongTou"
                    },
                    {
                        "id" : "330324",
                        "name" : "永嘉",
                        "pinyin" : "YongJia"
                    },
                    {
                        "id" : "330326",
                        "name" : "平阳",
                        "pinyin" : "PingYang"
                    },
                    {
                        "id" : "330327",
                        "name" : "苍南",
                        "pinyin" : "CangNan"
                    },
                    {
                        "id" : "330328",
                        "name" : "文成",
                        "pinyin" : "WenCheng"
                    },
                    {
                        "id" : "330329",
                        "name" : "泰顺",
                        "pinyin" : "TaiShun"
                    },
                    {
                        "id" : "330381",
                        "name" : "瑞安",
                        "pinyin" : "RuiAn"
                    },
                    {
                        "id" : "330382",
                        "name" : "乐清",
                        "pinyin" : "LeQing"
                    }
                ],
                "name" : "温州",
                "pinyin" : "WenZhou"
            },
            {
                "id" : 330400,
                "list" : [
                    {
                        "id" : "330402",
                        "name" : "南湖",
                        "pinyin" : "NanHu"
                    },
                    {
                        "id" : "330411",
                        "name" : "秀洲",
                        "pinyin" : "XiuZhou"
                    },
                    {
                        "id" : "330421",
                        "name" : "嘉善",
                        "pinyin" : "JiaShan"
                    },
                    {
                        "id" : "330424",
                        "name" : "海盐",
                        "pinyin" : "HaiYan"
                    },
                    {
                        "id" : "330481",
                        "name" : "海宁",
                        "pinyin" : "HaiNing"
                    },
                    {
                        "id" : "330482",
                        "name" : "平湖",
                        "pinyin" : "PingHu"
                    },
                    {
                        "id" : "330483",
                        "name" : "桐乡",
                        "pinyin" : "TongXiang"
                    }
                ],
                "name" : "嘉兴",
                "pinyin" : "JiaXing"
            },
            {
                "id" : 330500,
                "list" : [
                    {
                        "id" : "330502",
                        "name" : "吴兴",
                        "pinyin" : "WuXing"
                    },
                    {
                        "id" : "330503",
                        "name" : "南浔",
                        "pinyin" : "NanXun"
                    },
                    {
                        "id" : "330521",
                        "name" : "德清",
                        "pinyin" : "DeQing"
                    },
                    {
                        "id" : "330522",
                        "name" : "长兴",
                        "pinyin" : "ChangXing"
                    },
                    {
                        "id" : "330523",
                        "name" : "安吉",
                        "pinyin" : "AnJi"
                    }
                ],
                "name" : "湖州",
                "pinyin" : "HuZhou"
            },
            {
                "id" : 330600,
                "list" : [
                    {
                        "id" : "330602",
                        "name" : "越城",
                        "pinyin" : "YueCheng"
                    },
                    {
                        "id" : "330603",
                        "name" : "柯桥",
                        "pinyin" : "KeQiao"
                    },
                    {
                        "id" : "330604",
                        "name" : "上虞",
                        "pinyin" : "ShangYu"
                    },
                    {
                        "id" : "330624",
                        "name" : "新昌",
                        "pinyin" : "XinChang"
                    },
                    {
                        "id" : "330681",
                        "name" : "诸暨",
                        "pinyin" : "ZhuJi"
                    },
                    {
                        "id" : "330683",
                        "name" : "嵊州",
                        "pinyin" : "ShengZhou"
                    }
                ],
                "name" : "绍兴",
                "pinyin" : "ShaoXing"
            },
            {
                "id" : 330700,
                "list" : [
                    {
                        "id" : "330702",
                        "name" : "婺城",
                        "pinyin" : "WuCheng"
                    },
                    {
                        "id" : "330703",
                        "name" : "金东",
                        "pinyin" : "JinDong"
                    },
                    {
                        "id" : "330723",
                        "name" : "武义",
                        "pinyin" : "WuYi"
                    },
                    {
                        "id" : "330726",
                        "name" : "浦江",
                        "pinyin" : "PuJiang"
                    },
                    {
                        "id" : "330727",
                        "name" : "磐安",
                        "pinyin" : "PanAn"
                    },
                    {
                        "id" : "330781",
                        "name" : "兰溪",
                        "pinyin" : "LanXi"
                    },
                    {
                        "id" : "330782",
                        "name" : "义乌",
                        "pinyin" : "YiWu"
                    },
                    {
                        "id" : "330783",
                        "name" : "东阳",
                        "pinyin" : "DongYang"
                    },
                    {
                        "id" : "330784",
                        "name" : "永康",
                        "pinyin" : "YongKang"
                    }
                ],
                "name" : "金华",
                "pinyin" : "JinHua"
            },
            {
                "id" : 330800,
                "list" : [
                    {
                        "id" : "330802",
                        "name" : "柯城",
                        "pinyin" : "KeCheng"
                    },
                    {
                        "id" : "330803",
                        "name" : "衢江",
                        "pinyin" : "QuJiang"
                    },
                    {
                        "id" : "330822",
                        "name" : "常山",
                        "pinyin" : "ChangShan"
                    },
                    {
                        "id" : "330824",
                        "name" : "开化",
                        "pinyin" : "KaiHua"
                    },
                    {
                        "id" : "330825",
                        "name" : "龙游",
                        "pinyin" : "LongYou"
                    },
                    {
                        "id" : "330881",
                        "name" : "江山",
                        "pinyin" : "JiangShan"
                    }
                ],
                "name" : "衢州",
                "pinyin" : "QuZhou"
            },
            {
                "id" : 330900,
                "list" : [
                    {
                        "id" : "330902",
                        "name" : "定海",
                        "pinyin" : "DingHai"
                    },
                    {
                        "id" : "330903",
                        "name" : "普陀",
                        "pinyin" : "PuTuo"
                    },
                    {
                        "id" : "330921",
                        "name" : "岱山",
                        "pinyin" : "DaiShan"
                    },
                    {
                        "id" : "330922",
                        "name" : "嵊泗",
                        "pinyin" : "ChengSi"
                    }
                ],
                "name" : "舟山",
                "pinyin" : "ZhouShan"
            },
            {
                "id" : 331000,
                "list" : [
                    {
                        "id" : "331002",
                        "name" : "椒江",
                        "pinyin" : "JiaoJiang"
                    },
                    {
                        "id" : "331003",
                        "name" : "黄岩",
                        "pinyin" : "HuangYan"
                    },
                    {
                        "id" : "331004",
                        "name" : "路桥",
                        "pinyin" : "LuQiao"
                    },
                    {
                        "id" : "331022",
                        "name" : "三门",
                        "pinyin" : "SanMen"
                    },
                    {
                        "id" : "331023",
                        "name" : "天台",
                        "pinyin" : "TianTai"
                    },
                    {
                        "id" : "331024",
                        "name" : "仙居",
                        "pinyin" : "XianJi"
                    },
                    {
                        "id" : "331081",
                        "name" : "温岭",
                        "pinyin" : "WenLing"
                    },
                    {
                        "id" : "331082",
                        "name" : "临海",
                        "pinyin" : "LinHai"
                    },
                    {
                        "id" : "331083",
                        "name" : "玉环",
                        "pinyin" : "YuHuan"
                    }
                ],
                "name" : "台州",
                "pinyin" : "TaiZhou"
            },
            {
                "id" : 331100,
                "list" : [
                    {
                        "id" : "331102",
                        "name" : "莲都",
                        "pinyin" : "LianDu"
                    },
                    {
                        "id" : "331121",
                        "name" : "青田",
                        "pinyin" : "QingTian"
                    },
                    {
                        "id" : "331122",
                        "name" : "缙云",
                        "pinyin" : "JinYun"
                    },
                    {
                        "id" : "331123",
                        "name" : "遂昌",
                        "pinyin" : "SuiChang"
                    },
                    {
                        "id" : "331124",
                        "name" : "松阳",
                        "pinyin" : "SongYang"
                    },
                    {
                        "id" : "331125",
                        "name" : "云和",
                        "pinyin" : "YunHe"
                    },
                    {
                        "id" : "331126",
                        "name" : "庆元",
                        "pinyin" : "QingYuan"
                    },
                    {
                        "id" : "331127",
                        "name" : "景宁",
                        "pinyin" : "JingNing"
                    },
                    {
                        "id" : "331181",
                        "name" : "龙泉",
                        "pinyin" : "LongQuan"
                    }
                ],
                "name" : "丽水",
                "pinyin" : "LiShui"
            }
        ],
        "name" : "浙江",
        "pinyin" : "ZheJiang"
    },
    {
        "id" : 340000,
        "list" : [
            {
                "id" : 340100,
                "list" : [
                    {
                        "id" : "340102",
                        "name" : "瑶海",
                        "pinyin" : "YaoHai"
                    },
                    {
                        "id" : "340103",
                        "name" : "庐阳",
                        "pinyin" : "LuYang"
                    },
                    {
                        "id" : "340104",
                        "name" : "蜀山",
                        "pinyin" : "ShuShan"
                    },
                    {
                        "id" : "340111",
                        "name" : "包河",
                        "pinyin" : "BaoHe"
                    },
                    {
                        "id" : "340121",
                        "name" : "长丰",
                        "pinyin" : "ChangFeng"
                    },
                    {
                        "id" : "340122",
                        "name" : "肥东",
                        "pinyin" : "FeiDong"
                    },
                    {
                        "id" : "340123",
                        "name" : "肥西",
                        "pinyin" : "FeiXi"
                    },
                    {
                        "id" : "340124",
                        "name" : "庐江",
                        "pinyin" : "LuJiang"
                    },
                    {
                        "id" : "340181",
                        "name" : "巢湖",
                        "pinyin" : "ChaoHu"
                    }
                ],
                "name" : "合肥",
                "pinyin" : "HeFei"
            },
            {
                "id" : 340200,
                "list" : [
                    {
                        "id" : "340202",
                        "name" : "镜湖",
                        "pinyin" : "JingHu"
                    },
                    {
                        "id" : "340203",
                        "name" : "弋江",
                        "pinyin" : "YiJiang"
                    },
                    {
                        "id" : "340207",
                        "name" : "鸠江",
                        "pinyin" : "QiuJiang"
                    },
                    {
                        "id" : "340208",
                        "name" : "三山",
                        "pinyin" : "SanShan"
                    },
                    {
                        "id" : "340221",
                        "name" : "芜湖",
                        "pinyin" : "WuHu"
                    },
                    {
                        "id" : "340222",
                        "name" : "繁昌",
                        "pinyin" : "FanChang"
                    },
                    {
                        "id" : "340223",
                        "name" : "南陵",
                        "pinyin" : "NanLing"
                    },
                    {
                        "id" : "340225",
                        "name" : "无为",
                        "pinyin" : "WuWei"
                    }
                ],
                "name" : "芜湖",
                "pinyin" : "WuHu"
            },
            {
                "id" : 340300,
                "list" : [
                    {
                        "id" : "340302",
                        "name" : "龙子湖",
                        "pinyin" : "LongZiHu"
                    },
                    {
                        "id" : "340303",
                        "name" : "蚌山",
                        "pinyin" : "BangShan"
                    },
                    {
                        "id" : "340304",
                        "name" : "禹会",
                        "pinyin" : "YuHui"
                    },
                    {
                        "id" : "340311",
                        "name" : "淮上",
                        "pinyin" : "HuaiShang"
                    },
                    {
                        "id" : "340321",
                        "name" : "怀远",
                        "pinyin" : "HuaiYuan"
                    },
                    {
                        "id" : "340322",
                        "name" : "五河",
                        "pinyin" : "WuHe"
                    },
                    {
                        "id" : "340323",
                        "name" : "固镇",
                        "pinyin" : "GuZhen"
                    }
                ],
                "name" : "蚌埠",
                "pinyin" : "BangBu"
            },
            {
                "id" : 340400,
                "list" : [
                    {
                        "id" : "340402",
                        "name" : "大通",
                        "pinyin" : "DaTong"
                    },
                    {
                        "id" : "340403",
                        "name" : "田家庵",
                        "pinyin" : "TianJiaAn"
                    },
                    {
                        "id" : "340404",
                        "name" : "谢家集",
                        "pinyin" : "XieJiaJi"
                    },
                    {
                        "id" : "340405",
                        "name" : "八公山",
                        "pinyin" : "BaGongShan"
                    },
                    {
                        "id" : "340406",
                        "name" : "潘集",
                        "pinyin" : "PanJi"
                    },
                    {
                        "id" : "340421",
                        "name" : "凤台",
                        "pinyin" : "FengTai"
                    },
                    {
                        "id" : "340422",
                        "name" : "寿县",
                        "pinyin" : "Shou"
                    }
                ],
                "name" : "淮南",
                "pinyin" : "HuaiNan"
            },
            {
                "id" : 340500,
                "list" : [
                    {
                        "id" : "340503",
                        "name" : "花山",
                        "pinyin" : "HuaShan"
                    },
                    {
                        "id" : "340504",
                        "name" : "雨山",
                        "pinyin" : "YuShan"
                    },
                    {
                        "id" : "340506",
                        "name" : "博望",
                        "pinyin" : "BoWang"
                    },
                    {
                        "id" : "340521",
                        "name" : "当涂",
                        "pinyin" : "DangTu"
                    },
                    {
                        "id" : "340522",
                        "name" : "含山",
                        "pinyin" : "HanShan"
                    },
                    {
                        "id" : "340523",
                        "name" : "和县",
                        "pinyin" : "HeXian"
                    }
                ],
                "name" : "马鞍山",
                "pinyin" : "MaAnShan"
            },
            {
                "id" : 340600,
                "list" : [
                    {
                        "id" : "340602",
                        "name" : "杜集",
                        "pinyin" : "DuJi"
                    },
                    {
                        "id" : "340603",
                        "name" : "相山",
                        "pinyin" : "XiangShan"
                    },
                    {
                        "id" : "340604",
                        "name" : "烈山",
                        "pinyin" : "LieShan"
                    },
                    {
                        "id" : "340621",
                        "name" : "濉溪",
                        "pinyin" : "SuiXi"
                    }
                ],
                "name" : "淮北",
                "pinyin" : "HuaiBei"
            },
            {
                "id" : 340700,
                "list" : [
                    {
                        "id" : "340705",
                        "name" : "铜官",
                        "pinyin" : "TongGuan"
                    },
                    {
                        "id" : "340706",
                        "name" : "义安",
                        "pinyin" : "YiAn"
                    },
                    {
                        "id" : "340711",
                        "name" : "郊区",
                        "pinyin" : "JiaoQu"
                    },
                    {
                        "id" : "340722",
                        "name" : "枞阳",
                        "pinyin" : "ZongYang"
                    }
                ],
                "name" : "铜陵",
                "pinyin" : "TongLing"
            },
            {
                "id" : 340800,
                "list" : [
                    {
                        "id" : "340802",
                        "name" : "迎江",
                        "pinyin" : "YingJiang"
                    },
                    {
                        "id" : "340803",
                        "name" : "大观",
                        "pinyin" : "DaGuan"
                    },
                    {
                        "id" : "340811",
                        "name" : "宜秀",
                        "pinyin" : "YiXiu"
                    },
                    {
                        "id" : "340822",
                        "name" : "怀宁",
                        "pinyin" : "HuaiNing"
                    },
                    {
                        "id" : "340825",
                        "name" : "太湖",
                        "pinyin" : "TaiHu"
                    },
                    {
                        "id" : "340826",
                        "name" : "宿松",
                        "pinyin" : "SuSong"
                    },
                    {
                        "id" : "340827",
                        "name" : "望江",
                        "pinyin" : "WangJiang"
                    },
                    {
                        "id" : "340828",
                        "name" : "岳西",
                        "pinyin" : "YueXi"
                    },
                    {
                        "id" : "340881",
                        "name" : "桐城",
                        "pinyin" : "TongCheng"
                    },
                    {
                        "id" : "340882",
                        "name" : "潜山",
                        "pinyin" : "QianShan"
                    }
                ],
                "name" : "安庆",
                "pinyin" : "AnQing"
            },
            {
                "id" : 341000,
                "list" : [
                    {
                        "id" : "341002",
                        "name" : "屯溪",
                        "pinyin" : "TunXi"
                    },
                    {
                        "id" : "341003",
                        "name" : "黄山",
                        "pinyin" : "HuangShan"
                    },
                    {
                        "id" : "341004",
                        "name" : "徽州",
                        "pinyin" : "HuiZhou"
                    },
                    {
                        "id" : "341021",
                        "name" : "歙县",
                        "pinyin" : "SheXian"
                    },
                    {
                        "id" : "341022",
                        "name" : "休宁",
                        "pinyin" : "XiuNing"
                    },
                    {
                        "id" : "341023",
                        "name" : "黟县",
                        "pinyin" : "YiXian"
                    },
                    {
                        "id" : "341024",
                        "name" : "祁门",
                        "pinyin" : "QiMen"
                    }
                ],
                "name" : "黄山",
                "pinyin" : "HuangShan"
            },
            {
                "id" : 341100,
                "list" : [
                    {
                        "id" : "341102",
                        "name" : "琅琊",
                        "pinyin" : "LangYa"
                    },
                    {
                        "id" : "341103",
                        "name" : "南谯",
                        "pinyin" : "NanQiao"
                    },
                    {
                        "id" : "341122",
                        "name" : "来安",
                        "pinyin" : "LaiAn"
                    },
                    {
                        "id" : "341124",
                        "name" : "全椒",
                        "pinyin" : "QuanJiao"
                    },
                    {
                        "id" : "341125",
                        "name" : "定远",
                        "pinyin" : "DingYuan"
                    },
                    {
                        "id" : "341126",
                        "name" : "凤阳",
                        "pinyin" : "FengYang"
                    },
                    {
                        "id" : "341181",
                        "name" : "天长",
                        "pinyin" : "TianChang"
                    },
                    {
                        "id" : "341182",
                        "name" : "明光",
                        "pinyin" : "MingGuang"
                    }
                ],
                "name" : "滁州",
                "pinyin" : "ChuZhou"
            },
            {
                "id" : 341200,
                "list" : [
                    {
                        "id" : "341202",
                        "name" : "颍州",
                        "pinyin" : "YingZhou"
                    },
                    {
                        "id" : "341203",
                        "name" : "颍东",
                        "pinyin" : "YingDong"
                    },
                    {
                        "id" : "341204",
                        "name" : "颍泉",
                        "pinyin" : "YingQuan"
                    },
                    {
                        "id" : "341221",
                        "name" : "临泉",
                        "pinyin" : "LinQuan"
                    },
                    {
                        "id" : "341222",
                        "name" : "太和",
                        "pinyin" : "TaiHe"
                    },
                    {
                        "id" : "341225",
                        "name" : "阜南",
                        "pinyin" : "FuNan"
                    },
                    {
                        "id" : "341226",
                        "name" : "颍上",
                        "pinyin" : "YingShang"
                    },
                    {
                        "id" : "341282",
                        "name" : "界首",
                        "pinyin" : "JieShou"
                    }
                ],
                "name" : "阜阳",
                "pinyin" : "FuYang"
            },
            {
                "id" : 341300,
                "list" : [
                    {
                        "id" : "341302",
                        "name" : "埇桥",
                        "pinyin" : "YongQiao"
                    },
                    {
                        "id" : "341321",
                        "name" : "砀山",
                        "pinyin" : "DangShan"
                    },
                    {
                        "id" : "341322",
                        "name" : "萧县",
                        "pinyin" : "XiaoXian"
                    },
                    {
                        "id" : "341323",
                        "name" : "灵璧",
                        "pinyin" : "LingBi"
                    },
                    {
                        "id" : "341324",
                        "name" : "泗县",
                        "pinyin" : "SiXian"
                    }
                ],
                "name" : "宿州",
                "pinyin" : "SuZhou"
            },
            {
                "id" : 341500,
                "list" : [
                    {
                        "id" : "341502",
                        "name" : "金安",
                        "pinyin" : "JinAn"
                    },
                    {
                        "id" : "341503",
                        "name" : "裕安",
                        "pinyin" : "YuAn"
                    },
                    {
                        "id" : "341504",
                        "name" : "叶集",
                        "pinyin" : "YeJi"
                    },
                    {
                        "id" : "341522",
                        "name" : "霍邱",
                        "pinyin" : "HuoQiu"
                    },
                    {
                        "id" : "341523",
                        "name" : "舒城",
                        "pinyin" : "ShuCheng"
                    },
                    {
                        "id" : "341524",
                        "name" : "金寨",
                        "pinyin" : "JinZhai"
                    },
                    {
                        "id" : "341525",
                        "name" : "霍山",
                        "pinyin" : "HuoShan"
                    }
                ],
                "name" : "六安",
                "pinyin" : "LuAn"
            },
            {
                "id" : 341600,
                "list" : [
                    {
                        "id" : "341602",
                        "name" : "谯城",
                        "pinyin" : "QiaoCheng"
                    },
                    {
                        "id" : "341621",
                        "name" : "涡阳",
                        "pinyin" : "WoYang"
                    },
                    {
                        "id" : "341622",
                        "name" : "蒙城",
                        "pinyin" : "MengCheng"
                    },
                    {
                        "id" : "341623",
                        "name" : "利辛",
                        "pinyin" : "LiXin"
                    }
                ],
                "name" : "亳州",
                "pinyin" : "BoZhou"
            },
            {
                "id" : 341700,
                "list" : [
                    {
                        "id" : "341702",
                        "name" : "贵池",
                        "pinyin" : "GuiChi"
                    },
                    {
                        "id" : "341721",
                        "name" : "东至",
                        "pinyin" : "DongZhi"
                    },
                    {
                        "id" : "341722",
                        "name" : "石台",
                        "pinyin" : "ShiTai"
                    },
                    {
                        "id" : "341723",
                        "name" : "青阳",
                        "pinyin" : "QingYang"
                    }
                ],
                "name" : "池州",
                "pinyin" : "ChiZhou"
            },
            {
                "id" : 341800,
                "list" : [
                    {
                        "id" : "341802",
                        "name" : "宣州",
                        "pinyin" : "XuanZhou"
                    },
                    {
                        "id" : "341821",
                        "name" : "郎溪",
                        "pinyin" : "LangXi"
                    },
                    {
                        "id" : "341822",
                        "name" : "广德",
                        "pinyin" : "GuangDe"
                    },
                    {
                        "id" : "341823",
                        "name" : "泾县",
                        "pinyin" : "JingXian"
                    },
                    {
                        "id" : "341824",
                        "name" : "绩溪",
                        "pinyin" : "JiXi"
                    },
                    {
                        "id" : "341825",
                        "name" : "旌德",
                        "pinyin" : "JingDe"
                    },
                    {
                        "id" : "341881",
                        "name" : "宁国",
                        "pinyin" : "NingGuo"
                    }
                ],
                "name" : "宣城",
                "pinyin" : "XuanCheng"
            }
        ],
        "name" : "安徽",
        "pinyin" : "AnHui"
    },
    {
        "id" : 350000,
        "list" : [
            {
                "id" : 350100,
                "list" : [
                    {
                        "id" : "350102",
                        "name" : "鼓楼",
                        "pinyin" : "GuLou"
                    },
                    {
                        "id" : "350103",
                        "name" : "台江",
                        "pinyin" : "TaiJiang"
                    },
                    {
                        "id" : "350104",
                        "name" : "仓山",
                        "pinyin" : "CangShan"
                    },
                    {
                        "id" : "350105",
                        "name" : "马尾",
                        "pinyin" : "MaWei"
                    },
                    {
                        "id" : "350111",
                        "name" : "晋安",
                        "pinyin" : "JinAn"
                    },
                    {
                        "id" : "350112",
                        "name" : "长乐",
                        "pinyin" : "ChangLe"
                    },
                    {
                        "id" : "350121",
                        "name" : "闽侯",
                        "pinyin" : "MinHou"
                    },
                    {
                        "id" : "350122",
                        "name" : "连江",
                        "pinyin" : "LianJiang"
                    },
                    {
                        "id" : "350123",
                        "name" : "罗源",
                        "pinyin" : "LuoYuan"
                    },
                    {
                        "id" : "350124",
                        "name" : "闽清",
                        "pinyin" : "MinQing"
                    },
                    {
                        "id" : "350125",
                        "name" : "永泰",
                        "pinyin" : "YongTai"
                    },
                    {
                        "id" : "350128",
                        "name" : "平潭",
                        "pinyin" : "PingTan"
                    },
                    {
                        "id" : "350181",
                        "name" : "福清",
                        "pinyin" : "FuQing"
                    }
                ],
                "name" : "福州",
                "pinyin" : "FuZhou"
            },
            {
                "id" : 350200,
                "list" : [
                    {
                        "id" : "350203",
                        "name" : "思明",
                        "pinyin" : "SiMing"
                    },
                    {
                        "id" : "350205",
                        "name" : "海沧",
                        "pinyin" : "HaiCang"
                    },
                    {
                        "id" : "350206",
                        "name" : "湖里",
                        "pinyin" : "HuLi"
                    },
                    {
                        "id" : "350211",
                        "name" : "集美",
                        "pinyin" : "JiMei"
                    },
                    {
                        "id" : "350212",
                        "name" : "同安",
                        "pinyin" : "TongAn"
                    },
                    {
                        "id" : "350213",
                        "name" : "翔安",
                        "pinyin" : "XiangAn"
                    }
                ],
                "name" : "厦门",
                "pinyin" : "XiaMen"
            },
            {
                "id" : 350300,
                "list" : [
                    {
                        "id" : "350302",
                        "name" : "城厢",
                        "pinyin" : "ChengXiang"
                    },
                    {
                        "id" : "350303",
                        "name" : "涵江",
                        "pinyin" : "HanJiang"
                    },
                    {
                        "id" : "350304",
                        "name" : "荔城",
                        "pinyin" : "LiCheng"
                    },
                    {
                        "id" : "350305",
                        "name" : "秀屿",
                        "pinyin" : "XiuYu"
                    },
                    {
                        "id" : "350322",
                        "name" : "仙游",
                        "pinyin" : "XianYou"
                    }
                ],
                "name" : "莆田",
                "pinyin" : "PuTian"
            },
            {
                "id" : 350400,
                "list" : [
                    {
                        "id" : "350402",
                        "name" : "梅列",
                        "pinyin" : "MeiLie"
                    },
                    {
                        "id" : "350403",
                        "name" : "三元",
                        "pinyin" : "SanYuan"
                    },
                    {
                        "id" : "350421",
                        "name" : "明溪",
                        "pinyin" : "MingXi"
                    },
                    {
                        "id" : "350423",
                        "name" : "清流",
                        "pinyin" : "QingLiu"
                    },
                    {
                        "id" : "350424",
                        "name" : "宁化",
                        "pinyin" : "NingHua"
                    },
                    {
                        "id" : "350425",
                        "name" : "大田",
                        "pinyin" : "DaTian"
                    },
                    {
                        "id" : "350426",
                        "name" : "尤溪",
                        "pinyin" : "YouXi"
                    },
                    {
                        "id" : "350427",
                        "name" : "沙县",
                        "pinyin" : "ShaXian"
                    },
                    {
                        "id" : "350428",
                        "name" : "将乐",
                        "pinyin" : "JiangLe"
                    },
                    {
                        "id" : "350429",
                        "name" : "泰宁",
                        "pinyin" : "TaiNing"
                    },
                    {
                        "id" : "350430",
                        "name" : "建宁",
                        "pinyin" : "JianNing"
                    },
                    {
                        "id" : "350481",
                        "name" : "永安",
                        "pinyin" : "YongAn"
                    }
                ],
                "name" : "三明",
                "pinyin" : "SanMing"
            },
            {
                "id" : 350500,
                "list" : [
                    {
                        "id" : "350502",
                        "name" : "鲤城",
                        "pinyin" : "LiCheng"
                    },
                    {
                        "id" : "350503",
                        "name" : "丰泽",
                        "pinyin" : "FengZe"
                    },
                    {
                        "id" : "350504",
                        "name" : "洛江",
                        "pinyin" : "LuoJiang"
                    },
                    {
                        "id" : "350505",
                        "name" : "泉港",
                        "pinyin" : "QuanGang"
                    },
                    {
                        "id" : "350521",
                        "name" : "惠安",
                        "pinyin" : "HuiAn"
                    },
                    {
                        "id" : "350524",
                        "name" : "安溪",
                        "pinyin" : "AnXi"
                    },
                    {
                        "id" : "350525",
                        "name" : "永春",
                        "pinyin" : "YongChun"
                    },
                    {
                        "id" : "350526",
                        "name" : "德化",
                        "pinyin" : "DeHua"
                    },
                    {
                        "id" : "350527",
                        "name" : "金门",
                        "pinyin" : "JinMen"
                    },
                    {
                        "id" : "350581",
                        "name" : "石狮",
                        "pinyin" : "ShiShi"
                    },
                    {
                        "id" : "350582",
                        "name" : "晋江",
                        "pinyin" : "JinJiang"
                    },
                    {
                        "id" : "350583",
                        "name" : "南安",
                        "pinyin" : "NanAn"
                    }
                ],
                "name" : "泉州",
                "pinyin" : "QuanZhou"
            },
            {
                "id" : 350600,
                "list" : [
                    {
                        "id" : "350602",
                        "name" : "芗城",
                        "pinyin" : "XiangCheng"
                    },
                    {
                        "id" : "350603",
                        "name" : "龙文",
                        "pinyin" : "LongWen"
                    },
                    {
                        "id" : "350622",
                        "name" : "云霄",
                        "pinyin" : "YunXiao"
                    },
                    {
                        "id" : "350623",
                        "name" : "漳浦",
                        "pinyin" : "ZhangPu"
                    },
                    {
                        "id" : "350624",
                        "name" : "诏安",
                        "pinyin" : "ZhaoAn"
                    },
                    {
                        "id" : "350625",
                        "name" : "长泰",
                        "pinyin" : "ChangTai"
                    },
                    {
                        "id" : "350626",
                        "name" : "东山",
                        "pinyin" : "DongShan"
                    },
                    {
                        "id" : "350627",
                        "name" : "南靖",
                        "pinyin" : "NanJing"
                    },
                    {
                        "id" : "350628",
                        "name" : "平和",
                        "pinyin" : "PingHe"
                    },
                    {
                        "id" : "350629",
                        "name" : "华安",
                        "pinyin" : "HuaAn"
                    },
                    {
                        "id" : "350681",
                        "name" : "龙海",
                        "pinyin" : "LongHai"
                    }
                ],
                "name" : "漳州",
                "pinyin" : "ZhangZhou"
            },
            {
                "id" : 350700,
                "list" : [
                    {
                        "id" : "350702",
                        "name" : "延平",
                        "pinyin" : "YanPing"
                    },
                    {
                        "id" : "350703",
                        "name" : "建阳",
                        "pinyin" : "JianYang"
                    },
                    {
                        "id" : "350721",
                        "name" : "顺昌",
                        "pinyin" : "ShunChang"
                    },
                    {
                        "id" : "350722",
                        "name" : "浦城",
                        "pinyin" : "PuCheng"
                    },
                    {
                        "id" : "350723",
                        "name" : "光泽",
                        "pinyin" : "GuangZe"
                    },
                    {
                        "id" : "350724",
                        "name" : "松溪",
                        "pinyin" : "SongXi"
                    },
                    {
                        "id" : "350725",
                        "name" : "政和",
                        "pinyin" : "ZhengHe"
                    },
                    {
                        "id" : "350781",
                        "name" : "邵武",
                        "pinyin" : "ShaoWu"
                    },
                    {
                        "id" : "350782",
                        "name" : "武夷山",
                        "pinyin" : "WuYiShan"
                    },
                    {
                        "id" : "350783",
                        "name" : "建瓯",
                        "pinyin" : "JianOu"
                    }
                ],
                "name" : "南平",
                "pinyin" : "NanPing"
            },
            {
                "id" : 350800,
                "list" : [
                    {
                        "id" : "350802",
                        "name" : "新罗",
                        "pinyin" : "XinLuo"
                    },
                    {
                        "id" : "350803",
                        "name" : "永定",
                        "pinyin" : "YongDing"
                    },
                    {
                        "id" : "350821",
                        "name" : "长汀",
                        "pinyin" : "ChangTing"
                    },
                    {
                        "id" : "350823",
                        "name" : "上杭",
                        "pinyin" : "ShangHang"
                    },
                    {
                        "id" : "350824",
                        "name" : "武平",
                        "pinyin" : "WuPing"
                    },
                    {
                        "id" : "350825",
                        "name" : "连城",
                        "pinyin" : "LianCheng"
                    },
                    {
                        "id" : "350881",
                        "name" : "漳平",
                        "pinyin" : "ZhangPing"
                    }
                ],
                "name" : "龙岩",
                "pinyin" : "LongYan"
            },
            {
                "id" : 350900,
                "list" : [
                    {
                        "id" : "350902",
                        "name" : "蕉城",
                        "pinyin" : "JiaoCheng"
                    },
                    {
                        "id" : "350921",
                        "name" : "霞浦",
                        "pinyin" : "XiaPu"
                    },
                    {
                        "id" : "350922",
                        "name" : "古田",
                        "pinyin" : "GuTian"
                    },
                    {
                        "id" : "350923",
                        "name" : "屏南",
                        "pinyin" : "PingNan"
                    },
                    {
                        "id" : "350924",
                        "name" : "寿宁",
                        "pinyin" : "ShouNing"
                    },
                    {
                        "id" : "350925",
                        "name" : "周宁",
                        "pinyin" : "ZhouNing"
                    },
                    {
                        "id" : "350926",
                        "name" : "柘荣",
                        "pinyin" : "ZheRong"
                    },
                    {
                        "id" : "350981",
                        "name" : "福安",
                        "pinyin" : "FuAn"
                    },
                    {
                        "id" : "350982",
                        "name" : "福鼎",
                        "pinyin" : "FuDing"
                    }
                ],
                "name" : "宁德",
                "pinyin" : "NingDe"
            }
        ],
        "name" : "福建",
        "pinyin" : "FuJian"
    },
    {
        "id" : 360000,
        "list" : [
            {
                "id" : 360100,
                "list" : [
                    {
                        "id" : "360102",
                        "name" : "东湖",
                        "pinyin" : "DongHu"
                    },
                    {
                        "id" : "360103",
                        "name" : "西湖",
                        "pinyin" : "XiHu"
                    },
                    {
                        "id" : "360104",
                        "name" : "青云谱",
                        "pinyin" : "QingYunPu"
                    },
                    {
                        "id" : "360105",
                        "name" : "湾里",
                        "pinyin" : "WanLi"
                    },
                    {
                        "id" : "360111",
                        "name" : "青山湖",
                        "pinyin" : "QingShanHu"
                    },
                    {
                        "id" : "360112",
                        "name" : "新建",
                        "pinyin" : "XinJian"
                    },
                    {
                        "id" : "360121",
                        "name" : "南昌",
                        "pinyin" : "NanChang"
                    },
                    {
                        "id" : "360123",
                        "name" : "安义",
                        "pinyin" : "AnYi"
                    },
                    {
                        "id" : "360124",
                        "name" : "进贤",
                        "pinyin" : "JinXian"
                    }
                ],
                "name" : "南昌",
                "pinyin" : "NanChang"
            },
            {
                "id" : 360200,
                "list" : [
                    {
                        "id" : "360202",
                        "name" : "昌江",
                        "pinyin" : "ChangJiang"
                    },
                    {
                        "id" : "360203",
                        "name" : "珠山",
                        "pinyin" : "ZhuShan"
                    },
                    {
                        "id" : "360222",
                        "name" : "浮梁",
                        "pinyin" : "FuLiang"
                    },
                    {
                        "id" : "360281",
                        "name" : "乐平",
                        "pinyin" : "LePing"
                    }
                ],
                "name" : "景德镇",
                "pinyin" : "JingDeZhen"
            },
            {
                "id" : 360300,
                "list" : [
                    {
                        "id" : "360302",
                        "name" : "安源",
                        "pinyin" : "AnYuan"
                    },
                    {
                        "id" : "360313",
                        "name" : "湘东",
                        "pinyin" : "XiangDong"
                    },
                    {
                        "id" : "360321",
                        "name" : "莲花",
                        "pinyin" : "LianHua"
                    },
                    {
                        "id" : "360322",
                        "name" : "上栗",
                        "pinyin" : "ShangLi"
                    },
                    {
                        "id" : "360323",
                        "name" : "芦溪",
                        "pinyin" : "LuXi"
                    }
                ],
                "name" : "萍乡",
                "pinyin" : "PingXiang"
            },
            {
                "id" : 360400,
                "list" : [
                    {
                        "id" : "360402",
                        "name" : "濂溪",
                        "pinyin" : "LianXi"
                    },
                    {
                        "id" : "360403",
                        "name" : "浔阳",
                        "pinyin" : "XunYang"
                    },
                    {
                        "id" : "360404",
                        "name" : "柴桑",
                        "pinyin" : "ChaiSang"
                    },
                    {
                        "id" : "360423",
                        "name" : "武宁",
                        "pinyin" : "WuNing"
                    },
                    {
                        "id" : "360424",
                        "name" : "修水",
                        "pinyin" : "XiuShui"
                    },
                    {
                        "id" : "360425",
                        "name" : "永修",
                        "pinyin" : "YongXiu"
                    },
                    {
                        "id" : "360426",
                        "name" : "德安",
                        "pinyin" : "DeAn"
                    },
                    {
                        "id" : "360428",
                        "name" : "都昌",
                        "pinyin" : "DuChang"
                    },
                    {
                        "id" : "360429",
                        "name" : "湖口",
                        "pinyin" : "HuKou"
                    },
                    {
                        "id" : "360430",
                        "name" : "彭泽",
                        "pinyin" : "PengZe"
                    },
                    {
                        "id" : "360481",
                        "name" : "瑞昌",
                        "pinyin" : "RuiChang"
                    },
                    {
                        "id" : "360482",
                        "name" : "共青城",
                        "pinyin" : "GongQingCheng"
                    },
                    {
                        "id" : "360483",
                        "name" : "庐山",
                        "pinyin" : "LuShan"
                    }
                ],
                "name" : "九江",
                "pinyin" : "JiuJiang"
            },
            {
                "id" : 360500,
                "list" : [
                    {
                        "id" : "360502",
                        "name" : "渝水",
                        "pinyin" : "YuShui"
                    },
                    {
                        "id" : "360521",
                        "name" : "分宜",
                        "pinyin" : "FenYi"
                    }
                ],
                "name" : "新余",
                "pinyin" : "XinYu"
            },
            {
                "id" : 360600,
                "list" : [
                    {
                        "id" : "360602",
                        "name" : "月湖",
                        "pinyin" : "YueHu"
                    },
                    {
                        "id" : "360603",
                        "name" : "余江",
                        "pinyin" : "YuJiang"
                    },
                    {
                        "id" : "360681",
                        "name" : "贵溪",
                        "pinyin" : "GuiXi"
                    }
                ],
                "name" : "鹰潭",
                "pinyin" : "YingTan"
            },
            {
                "id" : 360700,
                "list" : [
                    {
                        "id" : "360702",
                        "name" : "章贡",
                        "pinyin" : "ZhangGong"
                    },
                    {
                        "id" : "360703",
                        "name" : "南康",
                        "pinyin" : "NanKang"
                    },
                    {
                        "id" : "360704",
                        "name" : "赣县",
                        "pinyin" : "GanXian"
                    },
                    {
                        "id" : "360722",
                        "name" : "信丰",
                        "pinyin" : "XinFeng"
                    },
                    {
                        "id" : "360723",
                        "name" : "大余",
                        "pinyin" : "DaYu"
                    },
                    {
                        "id" : "360724",
                        "name" : "上犹",
                        "pinyin" : "ShangYou"
                    },
                    {
                        "id" : "360725",
                        "name" : "崇义",
                        "pinyin" : "ChongYi"
                    },
                    {
                        "id" : "360726",
                        "name" : "安远",
                        "pinyin" : "AnYuan"
                    },
                    {
                        "id" : "360727",
                        "name" : "龙南",
                        "pinyin" : "LongNan"
                    },
                    {
                        "id" : "360728",
                        "name" : "定南",
                        "pinyin" : "DingNan"
                    },
                    {
                        "id" : "360729",
                        "name" : "全南",
                        "pinyin" : "QuanNan"
                    },
                    {
                        "id" : "360730",
                        "name" : "宁都",
                        "pinyin" : "NingDu"
                    },
                    {
                        "id" : "360731",
                        "name" : "于都",
                        "pinyin" : "YuDu"
                    },
                    {
                        "id" : "360732",
                        "name" : "兴国",
                        "pinyin" : "XingGuo"
                    },
                    {
                        "id" : "360733",
                        "name" : "会昌",
                        "pinyin" : "HuiChang"
                    },
                    {
                        "id" : "360734",
                        "name" : "寻乌",
                        "pinyin" : "XunWu"
                    },
                    {
                        "id" : "360735",
                        "name" : "石城",
                        "pinyin" : "ShiCheng"
                    },
                    {
                        "id" : "360781",
                        "name" : "瑞金",
                        "pinyin" : "RuiJin"
                    }
                ],
                "name" : "赣州",
                "pinyin" : "GanZhou"
            },
            {
                "id" : 360800,
                "list" : [
                    {
                        "id" : "360802",
                        "name" : "吉州",
                        "pinyin" : "JiZhou"
                    },
                    {
                        "id" : "360803",
                        "name" : "青原",
                        "pinyin" : "QingYuan"
                    },
                    {
                        "id" : "360821",
                        "name" : "吉安",
                        "pinyin" : "JiAn"
                    },
                    {
                        "id" : "360822",
                        "name" : "吉水",
                        "pinyin" : "JiShui"
                    },
                    {
                        "id" : "360823",
                        "name" : "峡江",
                        "pinyin" : "XiaJiang"
                    },
                    {
                        "id" : "360824",
                        "name" : "新干",
                        "pinyin" : "XinGan"
                    },
                    {
                        "id" : "360825",
                        "name" : "永丰",
                        "pinyin" : "YongFeng"
                    },
                    {
                        "id" : "360826",
                        "name" : "泰和",
                        "pinyin" : "TaiHe"
                    },
                    {
                        "id" : "360827",
                        "name" : "遂川",
                        "pinyin" : "SuiChuan"
                    },
                    {
                        "id" : "360828",
                        "name" : "万安",
                        "pinyin" : "WanAn"
                    },
                    {
                        "id" : "360829",
                        "name" : "安福",
                        "pinyin" : "AnFu"
                    },
                    {
                        "id" : "360830",
                        "name" : "永新",
                        "pinyin" : "YongXin"
                    },
                    {
                        "id" : "360881",
                        "name" : "井冈山",
                        "pinyin" : "JingGangShan"
                    }
                ],
                "name" : "吉安",
                "pinyin" : "JiAn"
            },
            {
                "id" : 360900,
                "list" : [
                    {
                        "id" : "360902",
                        "name" : "袁州",
                        "pinyin" : "YuanZhou"
                    },
                    {
                        "id" : "360921",
                        "name" : "奉新",
                        "pinyin" : "FengXin"
                    },
                    {
                        "id" : "360922",
                        "name" : "万载",
                        "pinyin" : "WanZai"
                    },
                    {
                        "id" : "360923",
                        "name" : "上高",
                        "pinyin" : "ShangGao"
                    },
                    {
                        "id" : "360924",
                        "name" : "宜丰",
                        "pinyin" : "YiFeng"
                    },
                    {
                        "id" : "360925",
                        "name" : "靖安",
                        "pinyin" : "JingAn"
                    },
                    {
                        "id" : "360926",
                        "name" : "铜鼓",
                        "pinyin" : "TongGu"
                    },
                    {
                        "id" : "360981",
                        "name" : "丰城",
                        "pinyin" : "FengCheng"
                    },
                    {
                        "id" : "360982",
                        "name" : "樟树",
                        "pinyin" : "ZhangShu"
                    },
                    {
                        "id" : "360983",
                        "name" : "高安",
                        "pinyin" : "GaoAn"
                    }
                ],
                "name" : "宜春",
                "pinyin" : "YiChun"
            },
            {
                "id" : 361000,
                "list" : [
                    {
                        "id" : "361002",
                        "name" : "临川",
                        "pinyin" : "LinChuan"
                    },
                    {
                        "id" : "361003",
                        "name" : "东乡",
                        "pinyin" : "DongXiang"
                    },
                    {
                        "id" : "361021",
                        "name" : "南城",
                        "pinyin" : "NanCheng"
                    },
                    {
                        "id" : "361022",
                        "name" : "黎川",
                        "pinyin" : "LiChuan"
                    },
                    {
                        "id" : "361023",
                        "name" : "南丰",
                        "pinyin" : "NanFeng"
                    },
                    {
                        "id" : "361024",
                        "name" : "崇仁",
                        "pinyin" : "ChongRen"
                    },
                    {
                        "id" : "361025",
                        "name" : "乐安",
                        "pinyin" : "LeAn"
                    },
                    {
                        "id" : "361026",
                        "name" : "宜黄",
                        "pinyin" : "YiHuang"
                    },
                    {
                        "id" : "361027",
                        "name" : "金溪",
                        "pinyin" : "JinXi"
                    },
                    {
                        "id" : "361028",
                        "name" : "资溪",
                        "pinyin" : "ZiXi"
                    },
                    {
                        "id" : "361030",
                        "name" : "广昌",
                        "pinyin" : "GuangChang"
                    }
                ],
                "name" : "抚州",
                "pinyin" : "FuZhou"
            },
            {
                "id" : 361100,
                "list" : [
                    {
                        "id" : "361102",
                        "name" : "信州",
                        "pinyin" : "XinZhou"
                    },
                    {
                        "id" : "361103",
                        "name" : "广丰",
                        "pinyin" : "GuangFeng"
                    },
                    {
                        "id" : "361121",
                        "name" : "上饶",
                        "pinyin" : "ShangRao"
                    },
                    {
                        "id" : "361123",
                        "name" : "玉山",
                        "pinyin" : "YuShan"
                    },
                    {
                        "id" : "361124",
                        "name" : "铅山",
                        "pinyin" : "YanShan"
                    },
                    {
                        "id" : "361125",
                        "name" : "横峰",
                        "pinyin" : "HengFeng"
                    },
                    {
                        "id" : "361126",
                        "name" : "弋阳",
                        "pinyin" : "YiYang"
                    },
                    {
                        "id" : "361127",
                        "name" : "余干",
                        "pinyin" : "YuGan"
                    },
                    {
                        "id" : "361128",
                        "name" : "鄱阳",
                        "pinyin" : "PoYang"
                    },
                    {
                        "id" : "361129",
                        "name" : "万年",
                        "pinyin" : "WanNian"
                    },
                    {
                        "id" : "361130",
                        "name" : "婺源",
                        "pinyin" : "WuYuan"
                    },
                    {
                        "id" : "361181",
                        "name" : "德兴",
                        "pinyin" : "DeXing"
                    }
                ],
                "name" : "上饶",
                "pinyin" : "ShangRao"
            }
        ],
        "name" : "江西",
        "pinyin" : "JiangXi"
    },
    {
        "id" : 370000,
        "list" : [
            {
                "id" : 370100,
                "list" : [
                    {
                        "id" : "370102",
                        "name" : "历下",
                        "pinyin" : "LiXia"
                    },
                    {
                        "id" : "370103",
                        "name" : "市中",
                        "pinyin" : "ShiZhong"
                    },
                    {
                        "id" : "370104",
                        "name" : "槐荫",
                        "pinyin" : "HuaiYin"
                    },
                    {
                        "id" : "370105",
                        "name" : "天桥",
                        "pinyin" : "TianQiao"
                    },
                    {
                        "id" : "370112",
                        "name" : "历城",
                        "pinyin" : "LiCheng"
                    },
                    {
                        "id" : "370113",
                        "name" : "长清",
                        "pinyin" : "ChangQing"
                    },
                    {
                        "id" : "370114",
                        "name" : "章丘",
                        "pinyin" : "ZhangQiu"
                    },
                    {
                        "id" : "370115",
                        "name" : "济阳",
                        "pinyin" : "JiYang"
                    },
                    {
                        "id" : "370116",
                        "name" : "莱芜",
                        "pinyin" : "LaiWu"
                    },
                    {
                        "id" : "370117",
                        "name" : "钢城",
                        "pinyin" : "GangCheng"
                    },
                    {
                        "id" : "370124",
                        "name" : "平阴",
                        "pinyin" : "PingYin"
                    },
                    {
                        "id" : "370126",
                        "name" : "商河",
                        "pinyin" : "ShangHe"
                    }
                ],
                "name" : "济南",
                "pinyin" : "JiNan"
            },
            {
                "id" : 370200,
                "list" : [
                    {
                        "id" : "370202",
                        "name" : "市南",
                        "pinyin" : "ShiNan"
                    },
                    {
                        "id" : "370203",
                        "name" : "市北",
                        "pinyin" : "ShiBei"
                    },
                    {
                        "id" : "370211",
                        "name" : "黄岛",
                        "pinyin" : "HuangDao"
                    },
                    {
                        "id" : "370212",
                        "name" : "崂山",
                        "pinyin" : "LaoShan"
                    },
                    {
                        "id" : "370213",
                        "name" : "李沧",
                        "pinyin" : "LiCang"
                    },
                    {
                        "id" : "370214",
                        "name" : "城阳",
                        "pinyin" : "ChengYang"
                    },
                    {
                        "id" : "370215",
                        "name" : "即墨",
                        "pinyin" : "JiMo"
                    },
                    {
                        "id" : "370281",
                        "name" : "胶州",
                        "pinyin" : "JiaoZhou"
                    },
                    {
                        "id" : "370283",
                        "name" : "平度",
                        "pinyin" : "PingDu"
                    },
                    {
                        "id" : "370285",
                        "name" : "莱西",
                        "pinyin" : "LaiXi"
                    }
                ],
                "name" : "青岛",
                "pinyin" : "QingDao"
            },
            {
                "id" : 370300,
                "list" : [
                    {
                        "id" : "370302",
                        "name" : "淄川",
                        "pinyin" : "ZiChuan"
                    },
                    {
                        "id" : "370303",
                        "name" : "张店",
                        "pinyin" : "ZhangDian"
                    },
                    {
                        "id" : "370304",
                        "name" : "博山",
                        "pinyin" : "BoShan"
                    },
                    {
                        "id" : "370305",
                        "name" : "临淄",
                        "pinyin" : "LinZi"
                    },
                    {
                        "id" : "370306",
                        "name" : "周村",
                        "pinyin" : "ZhouCun"
                    },
                    {
                        "id" : "370321",
                        "name" : "桓台",
                        "pinyin" : "HuanTai"
                    },
                    {
                        "id" : "370322",
                        "name" : "高青",
                        "pinyin" : "GaoQing"
                    },
                    {
                        "id" : "370323",
                        "name" : "沂源",
                        "pinyin" : "YiYuan"
                    }
                ],
                "name" : "淄博",
                "pinyin" : "ZiBo"
            },
            {
                "id" : 370400,
                "list" : [
                    {
                        "id" : "370402",
                        "name" : "市中",
                        "pinyin" : "ShiZhong"
                    },
                    {
                        "id" : "370403",
                        "name" : "薛城",
                        "pinyin" : "XueCheng"
                    },
                    {
                        "id" : "370404",
                        "name" : "峄城",
                        "pinyin" : "YiCheng"
                    },
                    {
                        "id" : "370405",
                        "name" : "台儿庄",
                        "pinyin" : "TaiErZhuang"
                    },
                    {
                        "id" : "370406",
                        "name" : "山亭",
                        "pinyin" : "ShanTing"
                    },
                    {
                        "id" : "370481",
                        "name" : "滕州",
                        "pinyin" : "TengZhou"
                    }
                ],
                "name" : "枣庄",
                "pinyin" : "ZaoZhuang"
            },
            {
                "id" : 370500,
                "list" : [
                    {
                        "id" : "370502",
                        "name" : "东营",
                        "pinyin" : "DongYing"
                    },
                    {
                        "id" : "370503",
                        "name" : "河口",
                        "pinyin" : "HeKou"
                    },
                    {
                        "id" : "370505",
                        "name" : "垦利",
                        "pinyin" : "KenLi"
                    },
                    {
                        "id" : "370522",
                        "name" : "利津",
                        "pinyin" : "LiJin"
                    },
                    {
                        "id" : "370523",
                        "name" : "广饶",
                        "pinyin" : "GuangRao"
                    }
                ],
                "name" : "东营",
                "pinyin" : "DongYing"
            },
            {
                "id" : 370600,
                "list" : [
                    {
                        "id" : "370602",
                        "name" : "芝罘",
                        "pinyin" : "ZhiFu"
                    },
                    {
                        "id" : "370611",
                        "name" : "福山",
                        "pinyin" : "FuShan"
                    },
                    {
                        "id" : "370612",
                        "name" : "牟平",
                        "pinyin" : "MouPing"
                    },
                    {
                        "id" : "370613",
                        "name" : "莱山",
                        "pinyin" : "LaiShan"
                    },
                    {
                        "id" : "370634",
                        "name" : "长岛",
                        "pinyin" : "ChangDao"
                    },
                    {
                        "id" : "370681",
                        "name" : "龙口",
                        "pinyin" : "LongKou"
                    },
                    {
                        "id" : "370682",
                        "name" : "莱阳",
                        "pinyin" : "LaiYang"
                    },
                    {
                        "id" : "370683",
                        "name" : "莱州",
                        "pinyin" : "LaiZhou"
                    },
                    {
                        "id" : "370684",
                        "name" : "蓬莱",
                        "pinyin" : "PengLai"
                    },
                    {
                        "id" : "370685",
                        "name" : "招远",
                        "pinyin" : "ZhaoYuan"
                    },
                    {
                        "id" : "370686",
                        "name" : "栖霞",
                        "pinyin" : "QiXia"
                    },
                    {
                        "id" : "370687",
                        "name" : "海阳",
                        "pinyin" : "HaiYang"
                    }
                ],
                "name" : "烟台",
                "pinyin" : "YanTai"
            },
            {
                "id" : 370700,
                "list" : [
                    {
                        "id" : "370702",
                        "name" : "潍城",
                        "pinyin" : "WeiCheng"
                    },
                    {
                        "id" : "370703",
                        "name" : "寒亭",
                        "pinyin" : "HanTing"
                    },
                    {
                        "id" : "370704",
                        "name" : "坊子",
                        "pinyin" : "FangZi"
                    },
                    {
                        "id" : "370705",
                        "name" : "奎文",
                        "pinyin" : "KuiWen"
                    },
                    {
                        "id" : "370724",
                        "name" : "临朐",
                        "pinyin" : "LinChun"
                    },
                    {
                        "id" : "370725",
                        "name" : "昌乐",
                        "pinyin" : "ChangLe"
                    },
                    {
                        "id" : "370781",
                        "name" : "青州",
                        "pinyin" : "QingZhou"
                    },
                    {
                        "id" : "370782",
                        "name" : "诸城",
                        "pinyin" : "ZhuCheng"
                    },
                    {
                        "id" : "370783",
                        "name" : "寿光",
                        "pinyin" : "ShouGuang"
                    },
                    {
                        "id" : "370784",
                        "name" : "安丘",
                        "pinyin" : "AnQiu"
                    },
                    {
                        "id" : "370785",
                        "name" : "高密",
                        "pinyin" : "GaoMi"
                    },
                    {
                        "id" : "370786",
                        "name" : "昌邑",
                        "pinyin" : "ChangYi"
                    }
                ],
                "name" : "潍坊",
                "pinyin" : "WeiFang"
            },
            {
                "id" : 370800,
                "list" : [
                    {
                        "id" : "370811",
                        "name" : "任城",
                        "pinyin" : "RenCheng"
                    },
                    {
                        "id" : "370812",
                        "name" : "兖州",
                        "pinyin" : "YanZhou"
                    },
                    {
                        "id" : "370826",
                        "name" : "微山",
                        "pinyin" : "WeiShan"
                    },
                    {
                        "id" : "370827",
                        "name" : "鱼台",
                        "pinyin" : "YuTai"
                    },
                    {
                        "id" : "370828",
                        "name" : "金乡",
                        "pinyin" : "JinXiang"
                    },
                    {
                        "id" : "370829",
                        "name" : "嘉祥",
                        "pinyin" : "JiaXiang"
                    },
                    {
                        "id" : "370830",
                        "name" : "汶上",
                        "pinyin" : "WenShang"
                    },
                    {
                        "id" : "370831",
                        "name" : "泗水",
                        "pinyin" : "SiShui"
                    },
                    {
                        "id" : "370832",
                        "name" : "梁山",
                        "pinyin" : "LiangShan"
                    },
                    {
                        "id" : "370881",
                        "name" : "曲阜",
                        "pinyin" : "QuFu"
                    },
                    {
                        "id" : "370883",
                        "name" : "邹城",
                        "pinyin" : "ZouCheng"
                    }
                ],
                "name" : "济宁",
                "pinyin" : "JiNing"
            },
            {
                "id" : 370900,
                "list" : [
                    {
                        "id" : "370902",
                        "name" : "泰山",
                        "pinyin" : "TaiShan"
                    },
                    {
                        "id" : "370911",
                        "name" : "岱岳",
                        "pinyin" : "DaiYue"
                    },
                    {
                        "id" : "370921",
                        "name" : "宁阳",
                        "pinyin" : "NingYang"
                    },
                    {
                        "id" : "370923",
                        "name" : "东平",
                        "pinyin" : "DongPing"
                    },
                    {
                        "id" : "370982",
                        "name" : "新泰",
                        "pinyin" : "XinTai"
                    },
                    {
                        "id" : "370983",
                        "name" : "肥城",
                        "pinyin" : "FeiCheng"
                    }
                ],
                "name" : "泰安",
                "pinyin" : "TaiAn"
            },
            {
                "id" : 371000,
                "list" : [
                    {
                        "id" : "371002",
                        "name" : "环翠",
                        "pinyin" : "HuanCui"
                    },
                    {
                        "id" : "371003",
                        "name" : "文登",
                        "pinyin" : "WenDeng"
                    },
                    {
                        "id" : "371082",
                        "name" : "荣成",
                        "pinyin" : "RongCheng"
                    },
                    {
                        "id" : "371083",
                        "name" : "乳山",
                        "pinyin" : "RuShan"
                    }
                ],
                "name" : "威海",
                "pinyin" : "WeiHai"
            },
            {
                "id" : 371100,
                "list" : [
                    {
                        "id" : "371102",
                        "name" : "东港",
                        "pinyin" : "DongGang"
                    },
                    {
                        "id" : "371103",
                        "name" : "岚山",
                        "pinyin" : "LanShan"
                    },
                    {
                        "id" : "371121",
                        "name" : "五莲",
                        "pinyin" : "WuLian"
                    },
                    {
                        "id" : "371122",
                        "name" : "莒县",
                        "pinyin" : "JuXian"
                    }
                ],
                "name" : "日照",
                "pinyin" : "RiZhao"
            },
            {
                "id" : 371300,
                "list" : [
                    {
                        "id" : "371302",
                        "name" : "兰山",
                        "pinyin" : "LanShan"
                    },
                    {
                        "id" : "371311",
                        "name" : "罗庄",
                        "pinyin" : "LuoZhuang"
                    },
                    {
                        "id" : "371312",
                        "name" : "河东",
                        "pinyin" : "HeDong"
                    },
                    {
                        "id" : "371321",
                        "name" : "沂南",
                        "pinyin" : "YiNan"
                    },
                    {
                        "id" : "371322",
                        "name" : "郯城",
                        "pinyin" : "TanCheng"
                    },
                    {
                        "id" : "371323",
                        "name" : "沂水",
                        "pinyin" : "YiShui"
                    },
                    {
                        "id" : "371324",
                        "name" : "兰陵",
                        "pinyin" : "LanLing"
                    },
                    {
                        "id" : "371325",
                        "name" : "费县",
                        "pinyin" : "FeiXian"
                    },
                    {
                        "id" : "371326",
                        "name" : "平邑",
                        "pinyin" : "PingYi"
                    },
                    {
                        "id" : "371327",
                        "name" : "莒南",
                        "pinyin" : "JuNan"
                    },
                    {
                        "id" : "371328",
                        "name" : "蒙阴",
                        "pinyin" : "MengYin"
                    },
                    {
                        "id" : "371329",
                        "name" : "临沭",
                        "pinyin" : "LinShu"
                    }
                ],
                "name" : "临沂",
                "pinyin" : "LinYi"
            },
            {
                "id" : 371400,
                "list" : [
                    {
                        "id" : "371402",
                        "name" : "德城",
                        "pinyin" : "DeCheng"
                    },
                    {
                        "id" : "371403",
                        "name" : "陵城",
                        "pinyin" : "LingCheng"
                    },
                    {
                        "id" : "371422",
                        "name" : "宁津",
                        "pinyin" : "NingJin"
                    },
                    {
                        "id" : "371423",
                        "name" : "庆云",
                        "pinyin" : "QingYun"
                    },
                    {
                        "id" : "371424",
                        "name" : "临邑",
                        "pinyin" : "LinYi"
                    },
                    {
                        "id" : "371425",
                        "name" : "齐河",
                        "pinyin" : "QiHe"
                    },
                    {
                        "id" : "371426",
                        "name" : "平原",
                        "pinyin" : "PingYuan"
                    },
                    {
                        "id" : "371427",
                        "name" : "夏津",
                        "pinyin" : "XiaJin"
                    },
                    {
                        "id" : "371428",
                        "name" : "武城",
                        "pinyin" : "WuCheng"
                    },
                    {
                        "id" : "371481",
                        "name" : "乐陵",
                        "pinyin" : "LeLing"
                    },
                    {
                        "id" : "371482",
                        "name" : "禹城",
                        "pinyin" : "YuCheng"
                    }
                ],
                "name" : "德州",
                "pinyin" : "DeZhou"
            },
            {
                "id" : 371500,
                "list" : [
                    {
                        "id" : "371502",
                        "name" : "东昌府",
                        "pinyin" : "DongChangFu"
                    },
                    {
                        "id" : "371521",
                        "name" : "阳谷",
                        "pinyin" : "YangGu"
                    },
                    {
                        "id" : "371522",
                        "name" : "莘县",
                        "pinyin" : "ShenXian"
                    },
                    {
                        "id" : "371523",
                        "name" : "茌平",
                        "pinyin" : "ChiPing"
                    },
                    {
                        "id" : "371524",
                        "name" : "东阿",
                        "pinyin" : "DongA"
                    },
                    {
                        "id" : "371525",
                        "name" : "冠县",
                        "pinyin" : "GuanXian"
                    },
                    {
                        "id" : "371526",
                        "name" : "高唐",
                        "pinyin" : "GaoTang"
                    },
                    {
                        "id" : "371581",
                        "name" : "临清",
                        "pinyin" : "LinQing"
                    }
                ],
                "name" : "聊城",
                "pinyin" : "LiaoCheng"
            },
            {
                "id" : 371600,
                "list" : [
                    {
                        "id" : "371602",
                        "name" : "滨城",
                        "pinyin" : "BinCheng"
                    },
                    {
                        "id" : "371603",
                        "name" : "沾化",
                        "pinyin" : "ZhanHua"
                    },
                    {
                        "id" : "371621",
                        "name" : "惠民",
                        "pinyin" : "HuiMin"
                    },
                    {
                        "id" : "371622",
                        "name" : "阳信",
                        "pinyin" : "YangXin"
                    },
                    {
                        "id" : "371623",
                        "name" : "无棣",
                        "pinyin" : "WuDi"
                    },
                    {
                        "id" : "371625",
                        "name" : "博兴",
                        "pinyin" : "BoXing"
                    },
                    {
                        "id" : "371681",
                        "name" : "邹平",
                        "pinyin" : "ZouPing"
                    }
                ],
                "name" : "滨州",
                "pinyin" : "BinZhou"
            },
            {
                "id" : 371700,
                "list" : [
                    {
                        "id" : "371702",
                        "name" : "牡丹",
                        "pinyin" : "MuDan"
                    },
                    {
                        "id" : "371703",
                        "name" : "定陶",
                        "pinyin" : "DingTao"
                    },
                    {
                        "id" : "371721",
                        "name" : "曹县",
                        "pinyin" : "CaoXian"
                    },
                    {
                        "id" : "371722",
                        "name" : "单县",
                        "pinyin" : "ShanXian"
                    },
                    {
                        "id" : "371723",
                        "name" : "成武",
                        "pinyin" : "ChengWu"
                    },
                    {
                        "id" : "371724",
                        "name" : "巨野",
                        "pinyin" : "JuYe"
                    },
                    {
                        "id" : "371725",
                        "name" : "郓城",
                        "pinyin" : "YunCheng"
                    },
                    {
                        "id" : "371726",
                        "name" : "鄄城",
                        "pinyin" : "JuanCheng"
                    },
                    {
                        "id" : "371728",
                        "name" : "东明",
                        "pinyin" : "DongMing"
                    }
                ],
                "name" : "菏泽",
                "pinyin" : "HeZe"
            }
        ],
        "name" : "山东",
        "pinyin" : "ShanDong"
    },
    {
        "id" : 410000,
        "list" : [
            {
                "id" : 410100,
                "list" : [
                    {
                        "id" : "410102",
                        "name" : "中原",
                        "pinyin" : "ZhongYuan"
                    },
                    {
                        "id" : "410103",
                        "name" : "二七",
                        "pinyin" : "ErQi"
                    },
                    {
                        "id" : "410104",
                        "name" : "管城",
                        "pinyin" : "GuanCheng"
                    },
                    {
                        "id" : "410105",
                        "name" : "金水",
                        "pinyin" : "JinShui"
                    },
                    {
                        "id" : "410106",
                        "name" : "上街",
                        "pinyin" : "ShangJie"
                    },
                    {
                        "id" : "410108",
                        "name" : "惠济",
                        "pinyin" : "HuiJi"
                    },
                    {
                        "id" : "410122",
                        "name" : "中牟",
                        "pinyin" : "ZhongMou"
                    },
                    {
                        "id" : "410181",
                        "name" : "巩义",
                        "pinyin" : "GongYi"
                    },
                    {
                        "id" : "410182",
                        "name" : "荥阳",
                        "pinyin" : "XingYang"
                    },
                    {
                        "id" : "410183",
                        "name" : "新密",
                        "pinyin" : "XinMi"
                    },
                    {
                        "id" : "410184",
                        "name" : "新郑",
                        "pinyin" : "XinZheng"
                    },
                    {
                        "id" : "410185",
                        "name" : "登封",
                        "pinyin" : "DengFeng"
                    }
                ],
                "name" : "郑州",
                "pinyin" : "ZhengZhou"
            },
            {
                "id" : 410200,
                "list" : [
                    {
                        "id" : "410202",
                        "name" : "龙亭",
                        "pinyin" : "LongTing"
                    },
                    {
                        "id" : "410203",
                        "name" : "顺河",
                        "pinyin" : "ShunHe"
                    },
                    {
                        "id" : "410204",
                        "name" : "鼓楼",
                        "pinyin" : "GuLou"
                    },
                    {
                        "id" : "410205",
                        "name" : "禹王台",
                        "pinyin" : "YuWangTai"
                    },
                    {
                        "id" : "410212",
                        "name" : "祥符",
                        "pinyin" : "XiangFu"
                    },
                    {
                        "id" : "410221",
                        "name" : "杞县",
                        "pinyin" : "QiXian"
                    },
                    {
                        "id" : "410222",
                        "name" : "通许",
                        "pinyin" : "TongXu"
                    },
                    {
                        "id" : "410223",
                        "name" : "尉氏",
                        "pinyin" : "WeiShi"
                    },
                    {
                        "id" : "410225",
                        "name" : "兰考",
                        "pinyin" : "LanKao"
                    }
                ],
                "name" : "开封",
                "pinyin" : "KaiFeng"
            },
            {
                "id" : 410300,
                "list" : [
                    {
                        "id" : "410302",
                        "name" : "老城",
                        "pinyin" : "LaoCheng"
                    },
                    {
                        "id" : "410303",
                        "name" : "西工",
                        "pinyin" : "XiGong"
                    },
                    {
                        "id" : "410304",
                        "name" : "瀍河",
                        "pinyin" : "ChanHe"
                    },
                    {
                        "id" : "410305",
                        "name" : "涧西",
                        "pinyin" : "JianXi"
                    },
                    {
                        "id" : "410306",
                        "name" : "吉利",
                        "pinyin" : "JiLi"
                    },
                    {
                        "id" : "410311",
                        "name" : "洛龙",
                        "pinyin" : "LuoLong"
                    },
                    {
                        "id" : "410322",
                        "name" : "孟津",
                        "pinyin" : "MengJin"
                    },
                    {
                        "id" : "410323",
                        "name" : "新安",
                        "pinyin" : "XinAn"
                    },
                    {
                        "id" : "410324",
                        "name" : "栾川",
                        "pinyin" : "LuanChuan"
                    },
                    {
                        "id" : "410325",
                        "name" : "嵩县",
                        "pinyin" : "SongXian"
                    },
                    {
                        "id" : "410326",
                        "name" : "汝阳",
                        "pinyin" : "RuYang"
                    },
                    {
                        "id" : "410327",
                        "name" : "宜阳",
                        "pinyin" : "YiYang"
                    },
                    {
                        "id" : "410328",
                        "name" : "洛宁",
                        "pinyin" : "LuoNing"
                    },
                    {
                        "id" : "410329",
                        "name" : "伊川",
                        "pinyin" : "YiChuan"
                    },
                    {
                        "id" : "410381",
                        "name" : "偃师",
                        "pinyin" : "YanShi"
                    }
                ],
                "name" : "洛阳",
                "pinyin" : "LuoYang"
            },
            {
                "id" : 410400,
                "list" : [
                    {
                        "id" : "410402",
                        "name" : "新华",
                        "pinyin" : "XinHua"
                    },
                    {
                        "id" : "410403",
                        "name" : "卫东",
                        "pinyin" : "WeiDong"
                    },
                    {
                        "id" : "410404",
                        "name" : "石龙",
                        "pinyin" : "ShiLong"
                    },
                    {
                        "id" : "410411",
                        "name" : "湛河",
                        "pinyin" : "ZhanHe"
                    },
                    {
                        "id" : "410421",
                        "name" : "宝丰",
                        "pinyin" : "BaoFeng"
                    },
                    {
                        "id" : "410422",
                        "name" : "叶县",
                        "pinyin" : "YeXian"
                    },
                    {
                        "id" : "410423",
                        "name" : "鲁山",
                        "pinyin" : "LuShan"
                    },
                    {
                        "id" : "410425",
                        "name" : "郏县",
                        "pinyin" : "JiaXian"
                    },
                    {
                        "id" : "410481",
                        "name" : "舞钢",
                        "pinyin" : "WuGang"
                    },
                    {
                        "id" : "410482",
                        "name" : "汝州",
                        "pinyin" : "RuZhou"
                    }
                ],
                "name" : "平顶山",
                "pinyin" : "PingDingShan"
            },
            {
                "id" : 410500,
                "list" : [
                    {
                        "id" : "410502",
                        "name" : "文峰",
                        "pinyin" : "WenFeng"
                    },
                    {
                        "id" : "410503",
                        "name" : "北关",
                        "pinyin" : "BeiGuan"
                    },
                    {
                        "id" : "410505",
                        "name" : "殷都",
                        "pinyin" : "YinDu"
                    },
                    {
                        "id" : "410506",
                        "name" : "龙安",
                        "pinyin" : "LongAn"
                    },
                    {
                        "id" : "410522",
                        "name" : "安阳",
                        "pinyin" : "AnYang"
                    },
                    {
                        "id" : "410523",
                        "name" : "汤阴",
                        "pinyin" : "TangYin"
                    },
                    {
                        "id" : "410526",
                        "name" : "滑县",
                        "pinyin" : "HuaXian"
                    },
                    {
                        "id" : "410527",
                        "name" : "内黄",
                        "pinyin" : "NaHuang"
                    },
                    {
                        "id" : "410581",
                        "name" : "林州",
                        "pinyin" : "LinZhou"
                    }
                ],
                "name" : "安阳",
                "pinyin" : "AnYang"
            },
            {
                "id" : 410600,
                "list" : [
                    {
                        "id" : "410602",
                        "name" : "鹤山",
                        "pinyin" : "HeShan"
                    },
                    {
                        "id" : "410603",
                        "name" : "山城",
                        "pinyin" : "ShanCheng"
                    },
                    {
                        "id" : "410611",
                        "name" : "淇滨",
                        "pinyin" : "QiBin"
                    },
                    {
                        "id" : "410621",
                        "name" : "浚县",
                        "pinyin" : "XunXian"
                    },
                    {
                        "id" : "410622",
                        "name" : "淇县",
                        "pinyin" : "QiXian"
                    }
                ],
                "name" : "鹤壁",
                "pinyin" : "HeBi"
            },
            {
                "id" : 410700,
                "list" : [
                    {
                        "id" : "410702",
                        "name" : "红旗",
                        "pinyin" : "HongQi"
                    },
                    {
                        "id" : "410703",
                        "name" : "卫滨",
                        "pinyin" : "WeiBin"
                    },
                    {
                        "id" : "410704",
                        "name" : "凤泉",
                        "pinyin" : "FengQuan"
                    },
                    {
                        "id" : "410711",
                        "name" : "牧野",
                        "pinyin" : "MuYe"
                    },
                    {
                        "id" : "410721",
                        "name" : "新乡",
                        "pinyin" : "XinXiang"
                    },
                    {
                        "id" : "410724",
                        "name" : "获嘉",
                        "pinyin" : "HuoJia"
                    },
                    {
                        "id" : "410725",
                        "name" : "原阳",
                        "pinyin" : "YuanYang"
                    },
                    {
                        "id" : "410726",
                        "name" : "延津",
                        "pinyin" : "YanJin"
                    },
                    {
                        "id" : "410727",
                        "name" : "封丘",
                        "pinyin" : "FengQiu"
                    },
                    {
                        "id" : "410728",
                        "name" : "长垣",
                        "pinyin" : "ChangYuan"
                    },
                    {
                        "id" : "410781",
                        "name" : "卫辉",
                        "pinyin" : "WeiHui"
                    },
                    {
                        "id" : "410782",
                        "name" : "辉县",
                        "pinyin" : "HuiXian"
                    }
                ],
                "name" : "新乡",
                "pinyin" : "XinXiang"
            },
            {
                "id" : 410800,
                "list" : [
                    {
                        "id" : "410802",
                        "name" : "解放",
                        "pinyin" : "JieFang"
                    },
                    {
                        "id" : "410803",
                        "name" : "中站",
                        "pinyin" : "ZhongZhan"
                    },
                    {
                        "id" : "410804",
                        "name" : "马村",
                        "pinyin" : "MaCun"
                    },
                    {
                        "id" : "410811",
                        "name" : "山阳",
                        "pinyin" : "ShanYang"
                    },
                    {
                        "id" : "410821",
                        "name" : "修武",
                        "pinyin" : "XiuWu"
                    },
                    {
                        "id" : "410822",
                        "name" : "博爱",
                        "pinyin" : "BoAi"
                    },
                    {
                        "id" : "410823",
                        "name" : "武陟",
                        "pinyin" : "WuZhi"
                    },
                    {
                        "id" : "410825",
                        "name" : "温县",
                        "pinyin" : "WenXian"
                    },
                    {
                        "id" : "410882",
                        "name" : "沁阳",
                        "pinyin" : "QinYang"
                    },
                    {
                        "id" : "410883",
                        "name" : "孟州",
                        "pinyin" : "MengZhou"
                    }
                ],
                "name" : "焦作",
                "pinyin" : "JiaoZuo"
            },
            {
                "id" : 410900,
                "list" : [
                    {
                        "id" : "410902",
                        "name" : "华龙",
                        "pinyin" : "HuaLong"
                    },
                    {
                        "id" : "410922",
                        "name" : "清丰",
                        "pinyin" : "QingFeng"
                    },
                    {
                        "id" : "410923",
                        "name" : "南乐",
                        "pinyin" : "NanLe"
                    },
                    {
                        "id" : "410926",
                        "name" : "范县",
                        "pinyin" : "FanXian"
                    },
                    {
                        "id" : "410927",
                        "name" : "台前",
                        "pinyin" : "TaiQian"
                    },
                    {
                        "id" : "410928",
                        "name" : "濮阳",
                        "pinyin" : "PuYang"
                    }
                ],
                "name" : "濮阳",
                "pinyin" : "PuYang"
            },
            {
                "id" : 411000,
                "list" : [
                    {
                        "id" : "411002",
                        "name" : "魏都",
                        "pinyin" : "WeiDu"
                    },
                    {
                        "id" : "411003",
                        "name" : "建安",
                        "pinyin" : "JianAn"
                    },
                    {
                        "id" : "411024",
                        "name" : "鄢陵",
                        "pinyin" : "YanLing"
                    },
                    {
                        "id" : "411025",
                        "name" : "襄城",
                        "pinyin" : "XiangCheng"
                    },
                    {
                        "id" : "411081",
                        "name" : "禹州",
                        "pinyin" : "YuZhou"
                    },
                    {
                        "id" : "411082",
                        "name" : "长葛",
                        "pinyin" : "ChangGe"
                    }
                ],
                "name" : "许昌",
                "pinyin" : "XuChang"
            },
            {
                "id" : 411100,
                "list" : [
                    {
                        "id" : "411102",
                        "name" : "源汇",
                        "pinyin" : "YuanHui"
                    },
                    {
                        "id" : "411103",
                        "name" : "郾城",
                        "pinyin" : "YanCheng"
                    },
                    {
                        "id" : "411104",
                        "name" : "召陵",
                        "pinyin" : "ZhaoLing"
                    },
                    {
                        "id" : "411121",
                        "name" : "舞阳",
                        "pinyin" : "WuYang"
                    },
                    {
                        "id" : "411122",
                        "name" : "临颍",
                        "pinyin" : "LinYing"
                    }
                ],
                "name" : "漯河",
                "pinyin" : "LeiHe"
            },
            {
                "id" : 411200,
                "list" : [
                    {
                        "id" : "411202",
                        "name" : "湖滨",
                        "pinyin" : "HuBin"
                    },
                    {
                        "id" : "411203",
                        "name" : "陕州",
                        "pinyin" : "ShanZhou"
                    },
                    {
                        "id" : "411221",
                        "name" : "渑池",
                        "pinyin" : "ShengChi"
                    },
                    {
                        "id" : "411224",
                        "name" : "卢氏",
                        "pinyin" : "LuShi"
                    },
                    {
                        "id" : "411281",
                        "name" : "义马",
                        "pinyin" : "YiMa"
                    },
                    {
                        "id" : "411282",
                        "name" : "灵宝",
                        "pinyin" : "LingBao"
                    }
                ],
                "name" : "三门峡",
                "pinyin" : "SanMenXia"
            },
            {
                "id" : 411300,
                "list" : [
                    {
                        "id" : "411302",
                        "name" : "宛城",
                        "pinyin" : "WanCheng"
                    },
                    {
                        "id" : "411303",
                        "name" : "卧龙",
                        "pinyin" : "WoLong"
                    },
                    {
                        "id" : "411321",
                        "name" : "南召",
                        "pinyin" : "NanZhao"
                    },
                    {
                        "id" : "411322",
                        "name" : "方城",
                        "pinyin" : "FangCheng"
                    },
                    {
                        "id" : "411323",
                        "name" : "西峡",
                        "pinyin" : "XiXia"
                    },
                    {
                        "id" : "411324",
                        "name" : "镇平",
                        "pinyin" : "ZhenPing"
                    },
                    {
                        "id" : "411325",
                        "name" : "内乡",
                        "pinyin" : "NaXiang"
                    },
                    {
                        "id" : "411326",
                        "name" : "淅川",
                        "pinyin" : "XiChuan"
                    },
                    {
                        "id" : "411327",
                        "name" : "社旗",
                        "pinyin" : "SheQi"
                    },
                    {
                        "id" : "411328",
                        "name" : "唐河",
                        "pinyin" : "TangHe"
                    },
                    {
                        "id" : "411329",
                        "name" : "新野",
                        "pinyin" : "XinYe"
                    },
                    {
                        "id" : "411330",
                        "name" : "桐柏",
                        "pinyin" : "TongBai"
                    },
                    {
                        "id" : "411381",
                        "name" : "邓州",
                        "pinyin" : "DengZhou"
                    }
                ],
                "name" : "南阳",
                "pinyin" : "NanYang"
            },
            {
                "id" : 411400,
                "list" : [
                    {
                        "id" : "411402",
                        "name" : "梁园",
                        "pinyin" : "LiangYuan"
                    },
                    {
                        "id" : "411403",
                        "name" : "睢阳",
                        "pinyin" : "SuiYang"
                    },
                    {
                        "id" : "411421",
                        "name" : "民权",
                        "pinyin" : "MinQuan"
                    },
                    {
                        "id" : "411422",
                        "name" : "睢县",
                        "pinyin" : "SuiXian"
                    },
                    {
                        "id" : "411423",
                        "name" : "宁陵",
                        "pinyin" : "NingLing"
                    },
                    {
                        "id" : "411424",
                        "name" : "柘城",
                        "pinyin" : "ZheCheng"
                    },
                    {
                        "id" : "411425",
                        "name" : "虞城",
                        "pinyin" : "YuCheng"
                    },
                    {
                        "id" : "411426",
                        "name" : "夏邑",
                        "pinyin" : "XiaYi"
                    },
                    {
                        "id" : "411481",
                        "name" : "永城",
                        "pinyin" : "YongCheng"
                    }
                ],
                "name" : "商丘",
                "pinyin" : "ShangQiu"
            },
            {
                "id" : 411500,
                "list" : [
                    {
                        "id" : "411502",
                        "name" : "浉河",
                        "pinyin" : "ShiHe"
                    },
                    {
                        "id" : "411503",
                        "name" : "平桥",
                        "pinyin" : "PingQiao"
                    },
                    {
                        "id" : "411521",
                        "name" : "罗山",
                        "pinyin" : "LuoShan"
                    },
                    {
                        "id" : "411522",
                        "name" : "光山",
                        "pinyin" : "GuangShan"
                    },
                    {
                        "id" : "411523",
                        "name" : "新县",
                        "pinyin" : "XinXian"
                    },
                    {
                        "id" : "411524",
                        "name" : "商城",
                        "pinyin" : "ShangCheng"
                    },
                    {
                        "id" : "411525",
                        "name" : "固始",
                        "pinyin" : "GuShi"
                    },
                    {
                        "id" : "411526",
                        "name" : "潢川",
                        "pinyin" : "HuangChuan"
                    },
                    {
                        "id" : "411527",
                        "name" : "淮滨",
                        "pinyin" : "HuaiBin"
                    },
                    {
                        "id" : "411528",
                        "name" : "息县",
                        "pinyin" : "XiXian"
                    }
                ],
                "name" : "信阳",
                "pinyin" : "XinYang"
            },
            {
                "id" : 411600,
                "list" : [
                    {
                        "id" : "411602",
                        "name" : "川汇",
                        "pinyin" : "ChuanHui"
                    },
                    {
                        "id" : "411621",
                        "name" : "扶沟",
                        "pinyin" : "FuGou"
                    },
                    {
                        "id" : "411622",
                        "name" : "西华",
                        "pinyin" : "XiHua"
                    },
                    {
                        "id" : "411623",
                        "name" : "商水",
                        "pinyin" : "ShangShui"
                    },
                    {
                        "id" : "411624",
                        "name" : "沈丘",
                        "pinyin" : "ShenQiu"
                    },
                    {
                        "id" : "411625",
                        "name" : "郸城",
                        "pinyin" : "DanCheng"
                    },
                    {
                        "id" : "411626",
                        "name" : "淮阳",
                        "pinyin" : "HuaiYang"
                    },
                    {
                        "id" : "411627",
                        "name" : "太康",
                        "pinyin" : "TaiKang"
                    },
                    {
                        "id" : "411628",
                        "name" : "鹿邑",
                        "pinyin" : "LuYi"
                    },
                    {
                        "id" : "411681",
                        "name" : "项城",
                        "pinyin" : "XiangCheng"
                    }
                ],
                "name" : "周口",
                "pinyin" : "ZhouKou"
            },
            {
                "id" : 411700,
                "list" : [
                    {
                        "id" : "411702",
                        "name" : "驿城",
                        "pinyin" : "YiCheng"
                    },
                    {
                        "id" : "411721",
                        "name" : "西平",
                        "pinyin" : "XiPing"
                    },
                    {
                        "id" : "411722",
                        "name" : "上蔡",
                        "pinyin" : "ShangCai"
                    },
                    {
                        "id" : "411723",
                        "name" : "平舆",
                        "pinyin" : "PingYu"
                    },
                    {
                        "id" : "411724",
                        "name" : "正阳",
                        "pinyin" : "ZhengYang"
                    },
                    {
                        "id" : "411725",
                        "name" : "确山",
                        "pinyin" : "QueShan"
                    },
                    {
                        "id" : "411726",
                        "name" : "泌阳",
                        "pinyin" : "BiYang"
                    },
                    {
                        "id" : "411727",
                        "name" : "汝南",
                        "pinyin" : "RuNan"
                    },
                    {
                        "id" : "411728",
                        "name" : "遂平",
                        "pinyin" : "SuiPing"
                    },
                    {
                        "id" : "411729",
                        "name" : "新蔡",
                        "pinyin" : "XinCai"
                    }
                ],
                "name" : "驻马店",
                "pinyin" : "ZhuMaDian"
            },
            {
                "id" : 419001,
                "list" : [
                    {
                        "id" : "419001",
                        "name" : "济源",
                        "pinyin" : "JiYuan"
                    }
                ],
                "name" : "济源",
                "pinyin" : "JiYuan"
            }
        ],
        "name" : "河南",
        "pinyin" : "HeNan"
    },
    {
        "id" : 420000,
        "list" : [
            {
                "id" : 420100,
                "list" : [
                    {
                        "id" : "420102",
                        "name" : "江岸",
                        "pinyin" : "JiangAn"
                    },
                    {
                        "id" : "420103",
                        "name" : "江汉",
                        "pinyin" : "JiangHan"
                    },
                    {
                        "id" : "420104",
                        "name" : "硚口",
                        "pinyin" : "QiaoKou"
                    },
                    {
                        "id" : "420105",
                        "name" : "汉阳",
                        "pinyin" : "HanYang"
                    },
                    {
                        "id" : "420106",
                        "name" : "武昌",
                        "pinyin" : "WuChang"
                    },
                    {
                        "id" : "420107",
                        "name" : "青山",
                        "pinyin" : "QingShan"
                    },
                    {
                        "id" : "420111",
                        "name" : "洪山",
                        "pinyin" : "HongShan"
                    },
                    {
                        "id" : "420112",
                        "name" : "东西湖",
                        "pinyin" : "DongXiHu"
                    },
                    {
                        "id" : "420113",
                        "name" : "汉南",
                        "pinyin" : "HanNan"
                    },
                    {
                        "id" : "420114",
                        "name" : "蔡甸",
                        "pinyin" : "CaiDian"
                    },
                    {
                        "id" : "420115",
                        "name" : "江夏",
                        "pinyin" : "JiangXia"
                    },
                    {
                        "id" : "420116",
                        "name" : "黄陂",
                        "pinyin" : "HuangBei"
                    },
                    {
                        "id" : "420117",
                        "name" : "新洲",
                        "pinyin" : "XinZhou"
                    }
                ],
                "name" : "武汉",
                "pinyin" : "WuHan"
            },
            {
                "id" : 420200,
                "list" : [
                    {
                        "id" : "420202",
                        "name" : "黄石港",
                        "pinyin" : "HuangShiGang"
                    },
                    {
                        "id" : "420203",
                        "name" : "西塞山",
                        "pinyin" : "XiSaiShan"
                    },
                    {
                        "id" : "420204",
                        "name" : "下陆",
                        "pinyin" : "XiaLu"
                    },
                    {
                        "id" : "420205",
                        "name" : "铁山",
                        "pinyin" : "TieShan"
                    },
                    {
                        "id" : "420222",
                        "name" : "阳新",
                        "pinyin" : "YangXin"
                    },
                    {
                        "id" : "420281",
                        "name" : "大冶",
                        "pinyin" : "DaYe"
                    }
                ],
                "name" : "黄石",
                "pinyin" : "HuangShi"
            },
            {
                "id" : 420300,
                "list" : [
                    {
                        "id" : "420302",
                        "name" : "茅箭",
                        "pinyin" : "MaoJian"
                    },
                    {
                        "id" : "420303",
                        "name" : "张湾",
                        "pinyin" : "ZhangWan"
                    },
                    {
                        "id" : "420304",
                        "name" : "郧阳",
                        "pinyin" : "YunYang"
                    },
                    {
                        "id" : "420322",
                        "name" : "郧西",
                        "pinyin" : "YunXi"
                    },
                    {
                        "id" : "420323",
                        "name" : "竹山",
                        "pinyin" : "ZhuShan"
                    },
                    {
                        "id" : "420324",
                        "name" : "竹溪",
                        "pinyin" : "ZhuXi"
                    },
                    {
                        "id" : "420325",
                        "name" : "房县",
                        "pinyin" : "FangXian"
                    },
                    {
                        "id" : "420381",
                        "name" : "丹江口",
                        "pinyin" : "DanJiangKou"
                    }
                ],
                "name" : "十堰",
                "pinyin" : "ShiYan"
            },
            {
                "id" : 420500,
                "list" : [
                    {
                        "id" : "420502",
                        "name" : "西陵",
                        "pinyin" : "XiLing"
                    },
                    {
                        "id" : "420503",
                        "name" : "伍家岗",
                        "pinyin" : "WuJiaGang"
                    },
                    {
                        "id" : "420504",
                        "name" : "点军",
                        "pinyin" : "DianJun"
                    },
                    {
                        "id" : "420505",
                        "name" : "猇亭",
                        "pinyin" : "XiaoTing"
                    },
                    {
                        "id" : "420506",
                        "name" : "夷陵",
                        "pinyin" : "YiLing"
                    },
                    {
                        "id" : "420525",
                        "name" : "远安",
                        "pinyin" : "YuanAn"
                    },
                    {
                        "id" : "420526",
                        "name" : "兴山",
                        "pinyin" : "XingShan"
                    },
                    {
                        "id" : "420527",
                        "name" : "秭归",
                        "pinyin" : "ZiGui"
                    },
                    {
                        "id" : "420528",
                        "name" : "长阳",
                        "pinyin" : "ChangYang"
                    },
                    {
                        "id" : "420529",
                        "name" : "五峰",
                        "pinyin" : "WuFeng"
                    },
                    {
                        "id" : "420581",
                        "name" : "宜都",
                        "pinyin" : "YiDu"
                    },
                    {
                        "id" : "420582",
                        "name" : "当阳",
                        "pinyin" : "DangYang"
                    },
                    {
                        "id" : "420583",
                        "name" : "枝江",
                        "pinyin" : "ZhiJiang"
                    }
                ],
                "name" : "宜昌",
                "pinyin" : "YiChang"
            },
            {
                "id" : 420600,
                "list" : [
                    {
                        "id" : "420602",
                        "name" : "襄城",
                        "pinyin" : "XiangCheng"
                    },
                    {
                        "id" : "420606",
                        "name" : "樊城",
                        "pinyin" : "FanCheng"
                    },
                    {
                        "id" : "420607",
                        "name" : "襄州",
                        "pinyin" : "XiangZhou"
                    },
                    {
                        "id" : "420624",
                        "name" : "南漳",
                        "pinyin" : "NanZhang"
                    },
                    {
                        "id" : "420625",
                        "name" : "谷城",
                        "pinyin" : "GuCheng"
                    },
                    {
                        "id" : "420626",
                        "name" : "保康",
                        "pinyin" : "BaoKang"
                    },
                    {
                        "id" : "420682",
                        "name" : "老河口",
                        "pinyin" : "LaoHeKou"
                    },
                    {
                        "id" : "420683",
                        "name" : "枣阳",
                        "pinyin" : "ZaoYang"
                    },
                    {
                        "id" : "420684",
                        "name" : "宜城",
                        "pinyin" : "YiCheng"
                    }
                ],
                "name" : "襄阳",
                "pinyin" : "XiangYang"
            },
            {
                "id" : 420700,
                "list" : [
                    {
                        "id" : "420702",
                        "name" : "梁子湖",
                        "pinyin" : "LiangZiHu"
                    },
                    {
                        "id" : "420703",
                        "name" : "华容",
                        "pinyin" : "HuaRong"
                    },
                    {
                        "id" : "420704",
                        "name" : "鄂城",
                        "pinyin" : "ECheng"
                    }
                ],
                "name" : "鄂州",
                "pinyin" : "EZhou"
            },
            {
                "id" : 420800,
                "list" : [
                    {
                        "id" : "420802",
                        "name" : "东宝",
                        "pinyin" : "DongBao"
                    },
                    {
                        "id" : "420804",
                        "name" : "掇刀",
                        "pinyin" : "DuoDao"
                    },
                    {
                        "id" : "420822",
                        "name" : "沙洋",
                        "pinyin" : "ShaYang"
                    },
                    {
                        "id" : "420881",
                        "name" : "钟祥",
                        "pinyin" : "ZhongXiang"
                    },
                    {
                        "id" : "420882",
                        "name" : "京山",
                        "pinyin" : "JingShan"
                    }
                ],
                "name" : "荆门",
                "pinyin" : "JingMen"
            },
            {
                "id" : 420900,
                "list" : [
                    {
                        "id" : "420902",
                        "name" : "孝南",
                        "pinyin" : "XiaoNan"
                    },
                    {
                        "id" : "420921",
                        "name" : "孝昌",
                        "pinyin" : "XiaoChang"
                    },
                    {
                        "id" : "420922",
                        "name" : "大悟",
                        "pinyin" : "DaWu"
                    },
                    {
                        "id" : "420923",
                        "name" : "云梦",
                        "pinyin" : "YunMeng"
                    },
                    {
                        "id" : "420981",
                        "name" : "应城",
                        "pinyin" : "YingCheng"
                    },
                    {
                        "id" : "420982",
                        "name" : "安陆",
                        "pinyin" : "AnLu"
                    },
                    {
                        "id" : "420984",
                        "name" : "汉川",
                        "pinyin" : "HanChuan"
                    }
                ],
                "name" : "孝感",
                "pinyin" : "XiaoGan"
            },
            {
                "id" : 421000,
                "list" : [
                    {
                        "id" : "421002",
                        "name" : "沙市",
                        "pinyin" : "ShaShi"
                    },
                    {
                        "id" : "421003",
                        "name" : "荆州",
                        "pinyin" : "JingZhou"
                    },
                    {
                        "id" : "421022",
                        "name" : "公安",
                        "pinyin" : "GongAn"
                    },
                    {
                        "id" : "421023",
                        "name" : "监利",
                        "pinyin" : "JianLi"
                    },
                    {
                        "id" : "421024",
                        "name" : "江陵",
                        "pinyin" : "JiangLing"
                    },
                    {
                        "id" : "421081",
                        "name" : "石首",
                        "pinyin" : "ShiShou"
                    },
                    {
                        "id" : "421083",
                        "name" : "洪湖",
                        "pinyin" : "HongHu"
                    },
                    {
                        "id" : "421087",
                        "name" : "松滋",
                        "pinyin" : "SongZi"
                    }
                ],
                "name" : "荆州",
                "pinyin" : "JingZhou"
            },
            {
                "id" : 421100,
                "list" : [
                    {
                        "id" : "421102",
                        "name" : "黄州",
                        "pinyin" : "HuangZhou"
                    },
                    {
                        "id" : "421121",
                        "name" : "团风",
                        "pinyin" : "TuanFeng"
                    },
                    {
                        "id" : "421122",
                        "name" : "红安",
                        "pinyin" : "HongAn"
                    },
                    {
                        "id" : "421123",
                        "name" : "罗田",
                        "pinyin" : "LuoTian"
                    },
                    {
                        "id" : "421124",
                        "name" : "英山",
                        "pinyin" : "YingShan"
                    },
                    {
                        "id" : "421125",
                        "name" : "浠水",
                        "pinyin" : "XiShui"
                    },
                    {
                        "id" : "421126",
                        "name" : "蕲春",
                        "pinyin" : "QinChun"
                    },
                    {
                        "id" : "421127",
                        "name" : "黄梅",
                        "pinyin" : "HuangMei"
                    },
                    {
                        "id" : "421181",
                        "name" : "麻城",
                        "pinyin" : "MaCheng"
                    },
                    {
                        "id" : "421182",
                        "name" : "武穴",
                        "pinyin" : "WuXue"
                    }
                ],
                "name" : "黄冈",
                "pinyin" : "HuangGang"
            },
            {
                "id" : 421200,
                "list" : [
                    {
                        "id" : "421202",
                        "name" : "咸安",
                        "pinyin" : "XianAn"
                    },
                    {
                        "id" : "421221",
                        "name" : "嘉鱼",
                        "pinyin" : "JiaYu"
                    },
                    {
                        "id" : "421222",
                        "name" : "通城",
                        "pinyin" : "TongCheng"
                    },
                    {
                        "id" : "421223",
                        "name" : "崇阳",
                        "pinyin" : "ChongYang"
                    },
                    {
                        "id" : "421224",
                        "name" : "通山",
                        "pinyin" : "TongShan"
                    },
                    {
                        "id" : "421281",
                        "name" : "赤壁",
                        "pinyin" : "ChiBi"
                    }
                ],
                "name" : "咸宁",
                "pinyin" : "XianNing"
            },
            {
                "id" : 421300,
                "list" : [
                    {
                        "id" : "421303",
                        "name" : "曾都",
                        "pinyin" : "ZengDu"
                    },
                    {
                        "id" : "421321",
                        "name" : "随县",
                        "pinyin" : "SuiXian"
                    },
                    {
                        "id" : "421381",
                        "name" : "广水",
                        "pinyin" : "GuangShui"
                    }
                ],
                "name" : "随州",
                "pinyin" : "SuiZhou"
            },
            {
                "id" : 422800,
                "list" : [
                    {
                        "id" : "422801",
                        "name" : "恩施",
                        "pinyin" : "EnShi"
                    },
                    {
                        "id" : "422802",
                        "name" : "利川",
                        "pinyin" : "LiChuan"
                    },
                    {
                        "id" : "422822",
                        "name" : "建始",
                        "pinyin" : "JianShi"
                    },
                    {
                        "id" : "422823",
                        "name" : "巴东",
                        "pinyin" : "BaDong"
                    },
                    {
                        "id" : "422825",
                        "name" : "宣恩",
                        "pinyin" : "XuanEn"
                    },
                    {
                        "id" : "422826",
                        "name" : "咸丰",
                        "pinyin" : "XianFeng"
                    },
                    {
                        "id" : "422827",
                        "name" : "来凤",
                        "pinyin" : "LaiFeng"
                    },
                    {
                        "id" : "422828",
                        "name" : "鹤峰",
                        "pinyin" : "HeFeng"
                    }
                ],
                "name" : "恩施",
                "pinyin" : "EnShi"
            },
            {
                "id" : 429004,
                "list" : [
                    {
                        "id" : "429004",
                        "name" : "仙桃",
                        "pinyin" : "XianTao"
                    }
                ],
                "name" : "仙桃",
                "pinyin" : "XianTao"
            },
            {
                "id" : 429005,
                "list" : [
                    {
                        "id" : "429005",
                        "name" : "潜江",
                        "pinyin" : "QianJiang"
                    }
                ],
                "name" : "潜江",
                "pinyin" : "QianJiang"
            },
            {
                "id" : 429006,
                "list" : [
                    {
                        "id" : "429006",
                        "name" : "天门",
                        "pinyin" : "TianMen"
                    }
                ],
                "name" : "天门",
                "pinyin" : "TianMen"
            },
            {
                "id" : 429021,
                "list" : [
                    {
                        "id" : "429021",
                        "name" : "神农架",
                        "pinyin" : "ShenNongJia"
                    }
                ],
                "name" : "神农架",
                "pinyin" : "ShenNongJia"
            }
        ],
        "name" : "湖北",
        "pinyin" : "HuBei"
    },
    {
        "id" : 430000,
        "list" : [
            {
                "id" : 430100,
                "list" : [
                    {
                        "id" : "430102",
                        "name" : "芙蓉",
                        "pinyin" : "FuRong"
                    },
                    {
                        "id" : "430103",
                        "name" : "天心",
                        "pinyin" : "TianXin"
                    },
                    {
                        "id" : "430104",
                        "name" : "岳麓",
                        "pinyin" : "YueLu"
                    },
                    {
                        "id" : "430105",
                        "name" : "开福",
                        "pinyin" : "KaiFu"
                    },
                    {
                        "id" : "430111",
                        "name" : "雨花",
                        "pinyin" : "YuHua"
                    },
                    {
                        "id" : "430112",
                        "name" : "望城",
                        "pinyin" : "WangCheng"
                    },
                    {
                        "id" : "430121",
                        "name" : "长沙",
                        "pinyin" : "ChangSha"
                    },
                    {
                        "id" : "430181",
                        "name" : "浏阳",
                        "pinyin" : "LiuYang"
                    },
                    {
                        "id" : "430182",
                        "name" : "宁乡",
                        "pinyin" : "NingXiang"
                    }
                ],
                "name" : "长沙",
                "pinyin" : "ChangSha"
            },
            {
                "id" : 430200,
                "list" : [
                    {
                        "id" : "430202",
                        "name" : "荷塘",
                        "pinyin" : "HeTang"
                    },
                    {
                        "id" : "430203",
                        "name" : "芦淞",
                        "pinyin" : "LuSong"
                    },
                    {
                        "id" : "430204",
                        "name" : "石峰",
                        "pinyin" : "ShiFeng"
                    },
                    {
                        "id" : "430211",
                        "name" : "天元",
                        "pinyin" : "TianYuan"
                    },
                    {
                        "id" : "430212",
                        "name" : "渌口",
                        "pinyin" : "LuKou"
                    },
                    {
                        "id" : "430223",
                        "name" : "攸县",
                        "pinyin" : "YouXian"
                    },
                    {
                        "id" : "430224",
                        "name" : "茶陵",
                        "pinyin" : "ChaLing"
                    },
                    {
                        "id" : "430225",
                        "name" : "炎陵",
                        "pinyin" : "YanLing"
                    },
                    {
                        "id" : "430281",
                        "name" : "醴陵",
                        "pinyin" : "LiLing"
                    }
                ],
                "name" : "株洲",
                "pinyin" : "ZhuZhou"
            },
            {
                "id" : 430300,
                "list" : [
                    {
                        "id" : "430302",
                        "name" : "雨湖",
                        "pinyin" : "YuHu"
                    },
                    {
                        "id" : "430304",
                        "name" : "岳塘",
                        "pinyin" : "YueTang"
                    },
                    {
                        "id" : "430321",
                        "name" : "湘潭",
                        "pinyin" : "XiangTan"
                    },
                    {
                        "id" : "430381",
                        "name" : "湘乡",
                        "pinyin" : "XiangXiang"
                    },
                    {
                        "id" : "430382",
                        "name" : "韶山",
                        "pinyin" : "ShaoShan"
                    }
                ],
                "name" : "湘潭",
                "pinyin" : "XiangTan"
            },
            {
                "id" : 430400,
                "list" : [
                    {
                        "id" : "430405",
                        "name" : "珠晖",
                        "pinyin" : "ZhuHui"
                    },
                    {
                        "id" : "430406",
                        "name" : "雁峰",
                        "pinyin" : "YanFeng"
                    },
                    {
                        "id" : "430407",
                        "name" : "石鼓",
                        "pinyin" : "ShiGu"
                    },
                    {
                        "id" : "430408",
                        "name" : "蒸湘",
                        "pinyin" : "ZhengXiang"
                    },
                    {
                        "id" : "430412",
                        "name" : "南岳",
                        "pinyin" : "NanYue"
                    },
                    {
                        "id" : "430421",
                        "name" : "衡阳",
                        "pinyin" : "HengYang"
                    },
                    {
                        "id" : "430422",
                        "name" : "衡南",
                        "pinyin" : "HengNan"
                    },
                    {
                        "id" : "430423",
                        "name" : "衡山",
                        "pinyin" : "HengShan"
                    },
                    {
                        "id" : "430424",
                        "name" : "衡东",
                        "pinyin" : "HengDong"
                    },
                    {
                        "id" : "430426",
                        "name" : "祁东",
                        "pinyin" : "QiDong"
                    },
                    {
                        "id" : "430481",
                        "name" : "耒阳",
                        "pinyin" : "LeiYang"
                    },
                    {
                        "id" : "430482",
                        "name" : "常宁",
                        "pinyin" : "ChangNing"
                    }
                ],
                "name" : "衡阳",
                "pinyin" : "HengYang"
            },
            {
                "id" : 430500,
                "list" : [
                    {
                        "id" : "430502",
                        "name" : "双清",
                        "pinyin" : "ShuangQing"
                    },
                    {
                        "id" : "430503",
                        "name" : "大祥",
                        "pinyin" : "DaXiang"
                    },
                    {
                        "id" : "430511",
                        "name" : "北塔",
                        "pinyin" : "BeiTa"
                    },
                    {
                        "id" : "430521",
                        "name" : "邵东",
                        "pinyin" : "ShaoDong"
                    },
                    {
                        "id" : "430522",
                        "name" : "新邵",
                        "pinyin" : "XinShao"
                    },
                    {
                        "id" : "430523",
                        "name" : "邵阳",
                        "pinyin" : "ShaoYang"
                    },
                    {
                        "id" : "430524",
                        "name" : "隆回",
                        "pinyin" : "LongHui"
                    },
                    {
                        "id" : "430525",
                        "name" : "洞口",
                        "pinyin" : "DongKou"
                    },
                    {
                        "id" : "430527",
                        "name" : "绥宁",
                        "pinyin" : "SuiNing"
                    },
                    {
                        "id" : "430528",
                        "name" : "新宁",
                        "pinyin" : "XinNing"
                    },
                    {
                        "id" : "430529",
                        "name" : "城步",
                        "pinyin" : "ChengBu"
                    },
                    {
                        "id" : "430581",
                        "name" : "武冈",
                        "pinyin" : "WuGang"
                    }
                ],
                "name" : "邵阳",
                "pinyin" : "ShaoYang"
            },
            {
                "id" : 430600,
                "list" : [
                    {
                        "id" : "430602",
                        "name" : "岳阳楼",
                        "pinyin" : "YueYangLou"
                    },
                    {
                        "id" : "430603",
                        "name" : "云溪",
                        "pinyin" : "YunXi"
                    },
                    {
                        "id" : "430611",
                        "name" : "君山",
                        "pinyin" : "JunShan"
                    },
                    {
                        "id" : "430621",
                        "name" : "岳阳",
                        "pinyin" : "YueYang"
                    },
                    {
                        "id" : "430623",
                        "name" : "华容",
                        "pinyin" : "HuaRong"
                    },
                    {
                        "id" : "430624",
                        "name" : "湘阴",
                        "pinyin" : "XiangYin"
                    },
                    {
                        "id" : "430626",
                        "name" : "平江",
                        "pinyin" : "PingJiang"
                    },
                    {
                        "id" : "430681",
                        "name" : "汨罗",
                        "pinyin" : "MiLuo"
                    },
                    {
                        "id" : "430682",
                        "name" : "临湘",
                        "pinyin" : "LinXiang"
                    }
                ],
                "name" : "岳阳",
                "pinyin" : "YueYang"
            },
            {
                "id" : 430700,
                "list" : [
                    {
                        "id" : "430702",
                        "name" : "武陵",
                        "pinyin" : "WuLing"
                    },
                    {
                        "id" : "430703",
                        "name" : "鼎城",
                        "pinyin" : "DingCheng"
                    },
                    {
                        "id" : "430721",
                        "name" : "安乡",
                        "pinyin" : "AnXiang"
                    },
                    {
                        "id" : "430722",
                        "name" : "汉寿",
                        "pinyin" : "HanShou"
                    },
                    {
                        "id" : "430723",
                        "name" : "澧县",
                        "pinyin" : "LiXian"
                    },
                    {
                        "id" : "430724",
                        "name" : "临澧",
                        "pinyin" : "LinLi"
                    },
                    {
                        "id" : "430725",
                        "name" : "桃源",
                        "pinyin" : "TaoYuan"
                    },
                    {
                        "id" : "430726",
                        "name" : "石门",
                        "pinyin" : "ShiMen"
                    },
                    {
                        "id" : "430781",
                        "name" : "津市",
                        "pinyin" : "JinShi"
                    }
                ],
                "name" : "常德",
                "pinyin" : "ChangDe"
            },
            {
                "id" : 430800,
                "list" : [
                    {
                        "id" : "430802",
                        "name" : "永定",
                        "pinyin" : "YongDing"
                    },
                    {
                        "id" : "430811",
                        "name" : "武陵源",
                        "pinyin" : "WuLingYuan"
                    },
                    {
                        "id" : "430821",
                        "name" : "慈利",
                        "pinyin" : "CiLi"
                    },
                    {
                        "id" : "430822",
                        "name" : "桑植",
                        "pinyin" : "SangZhi"
                    }
                ],
                "name" : "张家界",
                "pinyin" : "ZhangJiaJie"
            },
            {
                "id" : 430900,
                "list" : [
                    {
                        "id" : "430902",
                        "name" : "资阳",
                        "pinyin" : "ZiYang"
                    },
                    {
                        "id" : "430903",
                        "name" : "赫山",
                        "pinyin" : "HeShan"
                    },
                    {
                        "id" : "430921",
                        "name" : "南县",
                        "pinyin" : "NanXian"
                    },
                    {
                        "id" : "430922",
                        "name" : "桃江",
                        "pinyin" : "TaoJiang"
                    },
                    {
                        "id" : "430923",
                        "name" : "安化",
                        "pinyin" : "AnHua"
                    },
                    {
                        "id" : "430981",
                        "name" : "沅江",
                        "pinyin" : "YuanJiang"
                    }
                ],
                "name" : "益阳",
                "pinyin" : "YiYang"
            },
            {
                "id" : 431000,
                "list" : [
                    {
                        "id" : "431002",
                        "name" : "北湖",
                        "pinyin" : "BeiHu"
                    },
                    {
                        "id" : "431003",
                        "name" : "苏仙",
                        "pinyin" : "SuXian"
                    },
                    {
                        "id" : "431021",
                        "name" : "桂阳",
                        "pinyin" : "GuiYang"
                    },
                    {
                        "id" : "431022",
                        "name" : "宜章",
                        "pinyin" : "YiZhang"
                    },
                    {
                        "id" : "431023",
                        "name" : "永兴",
                        "pinyin" : "YongXing"
                    },
                    {
                        "id" : "431024",
                        "name" : "嘉禾",
                        "pinyin" : "JiaHe"
                    },
                    {
                        "id" : "431025",
                        "name" : "临武",
                        "pinyin" : "LinWu"
                    },
                    {
                        "id" : "431026",
                        "name" : "汝城",
                        "pinyin" : "RuCheng"
                    },
                    {
                        "id" : "431027",
                        "name" : "桂东",
                        "pinyin" : "GuiDong"
                    },
                    {
                        "id" : "431028",
                        "name" : "安仁",
                        "pinyin" : "AnRen"
                    },
                    {
                        "id" : "431081",
                        "name" : "资兴",
                        "pinyin" : "ZiXing"
                    }
                ],
                "name" : "郴州",
                "pinyin" : "ChenZhou"
            },
            {
                "id" : 431100,
                "list" : [
                    {
                        "id" : "431102",
                        "name" : "零陵",
                        "pinyin" : "LingLing"
                    },
                    {
                        "id" : "431103",
                        "name" : "冷水滩",
                        "pinyin" : "LingShuiTan"
                    },
                    {
                        "id" : "431121",
                        "name" : "祁阳",
                        "pinyin" : "QiYang"
                    },
                    {
                        "id" : "431122",
                        "name" : "东安",
                        "pinyin" : "DongAn"
                    },
                    {
                        "id" : "431123",
                        "name" : "双牌",
                        "pinyin" : "ShuangPai"
                    },
                    {
                        "id" : "431124",
                        "name" : "道县",
                        "pinyin" : "DaoXian"
                    },
                    {
                        "id" : "431125",
                        "name" : "江永",
                        "pinyin" : "JiangYong"
                    },
                    {
                        "id" : "431126",
                        "name" : "宁远",
                        "pinyin" : "NingYuan"
                    },
                    {
                        "id" : "431127",
                        "name" : "蓝山",
                        "pinyin" : "LanShan"
                    },
                    {
                        "id" : "431128",
                        "name" : "新田",
                        "pinyin" : "XinTian"
                    },
                    {
                        "id" : "431129",
                        "name" : "江华",
                        "pinyin" : "JiangHua"
                    }
                ],
                "name" : "永州",
                "pinyin" : "YongZhou"
            },
            {
                "id" : 431200,
                "list" : [
                    {
                        "id" : "431202",
                        "name" : "鹤城",
                        "pinyin" : "HeCheng"
                    },
                    {
                        "id" : "431221",
                        "name" : "中方",
                        "pinyin" : "ZhongFang"
                    },
                    {
                        "id" : "431222",
                        "name" : "沅陵",
                        "pinyin" : "YuanLing"
                    },
                    {
                        "id" : "431223",
                        "name" : "辰溪",
                        "pinyin" : "ChenXi"
                    },
                    {
                        "id" : "431224",
                        "name" : "溆浦",
                        "pinyin" : "XuPu"
                    },
                    {
                        "id" : "431225",
                        "name" : "会同",
                        "pinyin" : "HuiTong"
                    },
                    {
                        "id" : "431226",
                        "name" : "麻阳",
                        "pinyin" : "MaYang"
                    },
                    {
                        "id" : "431227",
                        "name" : "新晃",
                        "pinyin" : "XinHuang"
                    },
                    {
                        "id" : "431228",
                        "name" : "芷江",
                        "pinyin" : "ZhiJiang"
                    },
                    {
                        "id" : "431229",
                        "name" : "靖州",
                        "pinyin" : "JingZhou"
                    },
                    {
                        "id" : "431230",
                        "name" : "通道",
                        "pinyin" : "TongDao"
                    },
                    {
                        "id" : "431281",
                        "name" : "洪江",
                        "pinyin" : "HongJiang"
                    }
                ],
                "name" : "怀化",
                "pinyin" : "HuaiHua"
            },
            {
                "id" : 431300,
                "list" : [
                    {
                        "id" : "431302",
                        "name" : "娄星",
                        "pinyin" : "LouXing"
                    },
                    {
                        "id" : "431321",
                        "name" : "双峰",
                        "pinyin" : "ShuangFeng"
                    },
                    {
                        "id" : "431322",
                        "name" : "新化",
                        "pinyin" : "XinHua"
                    },
                    {
                        "id" : "431381",
                        "name" : "冷水江",
                        "pinyin" : "LingShuiJiang"
                    },
                    {
                        "id" : "431382",
                        "name" : "涟源",
                        "pinyin" : "LianYuan"
                    }
                ],
                "name" : "娄底",
                "pinyin" : "LouDi"
            },
            {
                "id" : 433100,
                "list" : [
                    {
                        "id" : "433101",
                        "name" : "吉首",
                        "pinyin" : "JiShou"
                    },
                    {
                        "id" : "433122",
                        "name" : "泸溪",
                        "pinyin" : "LuXi"
                    },
                    {
                        "id" : "433123",
                        "name" : "凤凰",
                        "pinyin" : "FengHuang"
                    },
                    {
                        "id" : "433124",
                        "name" : "花垣",
                        "pinyin" : "HuaYuan"
                    },
                    {
                        "id" : "433125",
                        "name" : "保靖",
                        "pinyin" : "BaoJing"
                    },
                    {
                        "id" : "433126",
                        "name" : "古丈",
                        "pinyin" : "GuZhang"
                    },
                    {
                        "id" : "433127",
                        "name" : "永顺",
                        "pinyin" : "YongShun"
                    },
                    {
                        "id" : "433130",
                        "name" : "龙山",
                        "pinyin" : "LongShan"
                    }
                ],
                "name" : "湘西",
                "pinyin" : "XiangXi"
            }
        ],
        "name" : "湖南",
        "pinyin" : "HuNan"
    },
    {
        "id" : 440000,
        "list" : [
            {
                "id" : 440100,
                "list" : [
                    {
                        "id" : "440103",
                        "name" : "荔湾",
                        "pinyin" : "LiWan"
                    },
                    {
                        "id" : "440104",
                        "name" : "越秀",
                        "pinyin" : "YueXiu"
                    },
                    {
                        "id" : "440105",
                        "name" : "海珠",
                        "pinyin" : "HaiZhu"
                    },
                    {
                        "id" : "440106",
                        "name" : "天河",
                        "pinyin" : "TianHe"
                    },
                    {
                        "id" : "440111",
                        "name" : "白云",
                        "pinyin" : "BaiYun"
                    },
                    {
                        "id" : "440112",
                        "name" : "黄埔",
                        "pinyin" : "HuangPu"
                    },
                    {
                        "id" : "440113",
                        "name" : "番禺",
                        "pinyin" : "PanYu"
                    },
                    {
                        "id" : "440114",
                        "name" : "花都",
                        "pinyin" : "HuaDu"
                    },
                    {
                        "id" : "440115",
                        "name" : "南沙",
                        "pinyin" : "NanSha"
                    },
                    {
                        "id" : "440117",
                        "name" : "从化",
                        "pinyin" : "CongHua"
                    },
                    {
                        "id" : "440118",
                        "name" : "增城",
                        "pinyin" : "ZengCheng"
                    }
                ],
                "name" : "广州",
                "pinyin" : "GuangZhou"
            },
            {
                "id" : 440200,
                "list" : [
                    {
                        "id" : "440203",
                        "name" : "武江",
                        "pinyin" : "WuJiang"
                    },
                    {
                        "id" : "440204",
                        "name" : "浈江",
                        "pinyin" : "ChengJiang"
                    },
                    {
                        "id" : "440205",
                        "name" : "曲江",
                        "pinyin" : "QuJiang"
                    },
                    {
                        "id" : "440222",
                        "name" : "始兴",
                        "pinyin" : "ShiXing"
                    },
                    {
                        "id" : "440224",
                        "name" : "仁化",
                        "pinyin" : "RenHua"
                    },
                    {
                        "id" : "440229",
                        "name" : "翁源",
                        "pinyin" : "WengYuan"
                    },
                    {
                        "id" : "440232",
                        "name" : "乳源",
                        "pinyin" : "RuYuan"
                    },
                    {
                        "id" : "440233",
                        "name" : "新丰",
                        "pinyin" : "XinFeng"
                    },
                    {
                        "id" : "440281",
                        "name" : "乐昌",
                        "pinyin" : "LeChang"
                    },
                    {
                        "id" : "440282",
                        "name" : "南雄",
                        "pinyin" : "NanXiong"
                    }
                ],
                "name" : "韶关",
                "pinyin" : "ShaoGuan"
            },
            {
                "id" : 440300,
                "list" : [
                    {
                        "id" : "440303",
                        "name" : "罗湖",
                        "pinyin" : "LuoHu"
                    },
                    {
                        "id" : "440304",
                        "name" : "福田",
                        "pinyin" : "FuTian"
                    },
                    {
                        "id" : "440305",
                        "name" : "南山",
                        "pinyin" : "NanShan"
                    },
                    {
                        "id" : "440306",
                        "name" : "宝安",
                        "pinyin" : "BaoAn"
                    },
                    {
                        "id" : "440307",
                        "name" : "龙岗",
                        "pinyin" : "LongGang"
                    },
                    {
                        "id" : "440308",
                        "name" : "盐田",
                        "pinyin" : "YanTian"
                    },
                    {
                        "id" : "440309",
                        "name" : "龙华",
                        "pinyin" : "LongHua"
                    },
                    {
                        "id" : "440310",
                        "name" : "坪山",
                        "pinyin" : "PingShan"
                    },
                    {
                        "id" : "440311",
                        "name" : "光明",
                        "pinyin" : "GuangMing"
                    }
                ],
                "name" : "深圳",
                "pinyin" : "ShenChou"
            },
            {
                "id" : 440400,
                "list" : [
                    {
                        "id" : "440402",
                        "name" : "香洲",
                        "pinyin" : "XiangZhou"
                    },
                    {
                        "id" : "440403",
                        "name" : "斗门",
                        "pinyin" : "DouMen"
                    },
                    {
                        "id" : "440404",
                        "name" : "金湾",
                        "pinyin" : "JinWan"
                    }
                ],
                "name" : "珠海",
                "pinyin" : "ZhuHai"
            },
            {
                "id" : 440500,
                "list" : [
                    {
                        "id" : "440507",
                        "name" : "龙湖",
                        "pinyin" : "LongHu"
                    },
                    {
                        "id" : "440511",
                        "name" : "金平",
                        "pinyin" : "JinPing"
                    },
                    {
                        "id" : "440512",
                        "name" : "濠江",
                        "pinyin" : "HaoJiang"
                    },
                    {
                        "id" : "440513",
                        "name" : "潮阳",
                        "pinyin" : "ChaoYang"
                    },
                    {
                        "id" : "440514",
                        "name" : "潮南",
                        "pinyin" : "ChaoNan"
                    },
                    {
                        "id" : "440515",
                        "name" : "澄海",
                        "pinyin" : "ChengHai"
                    },
                    {
                        "id" : "440523",
                        "name" : "南澳",
                        "pinyin" : "NanAo"
                    }
                ],
                "name" : "汕头",
                "pinyin" : "ShanTou"
            },
            {
                "id" : 440600,
                "list" : [
                    {
                        "id" : "440604",
                        "name" : "禅城",
                        "pinyin" : "ChanCheng"
                    },
                    {
                        "id" : "440605",
                        "name" : "南海",
                        "pinyin" : "NanHai"
                    },
                    {
                        "id" : "440606",
                        "name" : "顺德",
                        "pinyin" : "ShunDe"
                    },
                    {
                        "id" : "440607",
                        "name" : "三水",
                        "pinyin" : "SanShui"
                    },
                    {
                        "id" : "440608",
                        "name" : "高明",
                        "pinyin" : "GaoMing"
                    }
                ],
                "name" : "佛山",
                "pinyin" : "FoShan"
            },
            {
                "id" : 440700,
                "list" : [
                    {
                        "id" : "440703",
                        "name" : "蓬江",
                        "pinyin" : "PengJiang"
                    },
                    {
                        "id" : "440704",
                        "name" : "江海",
                        "pinyin" : "JiangHai"
                    },
                    {
                        "id" : "440705",
                        "name" : "新会",
                        "pinyin" : "XinHui"
                    },
                    {
                        "id" : "440781",
                        "name" : "台山",
                        "pinyin" : "TaiShan"
                    },
                    {
                        "id" : "440783",
                        "name" : "开平",
                        "pinyin" : "KaiPing"
                    },
                    {
                        "id" : "440784",
                        "name" : "鹤山",
                        "pinyin" : "HeShan"
                    },
                    {
                        "id" : "440785",
                        "name" : "恩平",
                        "pinyin" : "EnPing"
                    }
                ],
                "name" : "江门",
                "pinyin" : "JiangMen"
            },
            {
                "id" : 440800,
                "list" : [
                    {
                        "id" : "440802",
                        "name" : "赤坎",
                        "pinyin" : "ChiKan"
                    },
                    {
                        "id" : "440803",
                        "name" : "霞山",
                        "pinyin" : "XiaShan"
                    },
                    {
                        "id" : "440804",
                        "name" : "坡头",
                        "pinyin" : "PoTou"
                    },
                    {
                        "id" : "440811",
                        "name" : "麻章",
                        "pinyin" : "MaZhang"
                    },
                    {
                        "id" : "440823",
                        "name" : "遂溪",
                        "pinyin" : "SuiXi"
                    },
                    {
                        "id" : "440825",
                        "name" : "徐闻",
                        "pinyin" : "XuWen"
                    },
                    {
                        "id" : "440881",
                        "name" : "廉江",
                        "pinyin" : "LianJiang"
                    },
                    {
                        "id" : "440882",
                        "name" : "雷州",
                        "pinyin" : "LeiZhou"
                    },
                    {
                        "id" : "440883",
                        "name" : "吴川",
                        "pinyin" : "WuChuan"
                    }
                ],
                "name" : "湛江",
                "pinyin" : "ZhanJiang"
            },
            {
                "id" : 440900,
                "list" : [
                    {
                        "id" : "440902",
                        "name" : "茂南",
                        "pinyin" : "MaoNan"
                    },
                    {
                        "id" : "440904",
                        "name" : "电白",
                        "pinyin" : "DianBai"
                    },
                    {
                        "id" : "440981",
                        "name" : "高州",
                        "pinyin" : "GaoZhou"
                    },
                    {
                        "id" : "440982",
                        "name" : "化州",
                        "pinyin" : "HuaZhou"
                    },
                    {
                        "id" : "440983",
                        "name" : "信宜",
                        "pinyin" : "XinYi"
                    }
                ],
                "name" : "茂名",
                "pinyin" : "MaoMing"
            },
            {
                "id" : 441200,
                "list" : [
                    {
                        "id" : "441202",
                        "name" : "端州",
                        "pinyin" : "DuanZhou"
                    },
                    {
                        "id" : "441203",
                        "name" : "鼎湖",
                        "pinyin" : "DingHu"
                    },
                    {
                        "id" : "441204",
                        "name" : "高要",
                        "pinyin" : "GaoYao"
                    },
                    {
                        "id" : "441223",
                        "name" : "广宁",
                        "pinyin" : "GuangNing"
                    },
                    {
                        "id" : "441224",
                        "name" : "怀集",
                        "pinyin" : "HuaiJi"
                    },
                    {
                        "id" : "441225",
                        "name" : "封开",
                        "pinyin" : "FengKai"
                    },
                    {
                        "id" : "441226",
                        "name" : "德庆",
                        "pinyin" : "DeQing"
                    },
                    {
                        "id" : "441284",
                        "name" : "四会",
                        "pinyin" : "SiHui"
                    }
                ],
                "name" : "肇庆",
                "pinyin" : "ZhaoQing"
            },
            {
                "id" : 441300,
                "list" : [
                    {
                        "id" : "441302",
                        "name" : "惠城",
                        "pinyin" : "HuiCheng"
                    },
                    {
                        "id" : "441303",
                        "name" : "惠阳",
                        "pinyin" : "HuiYang"
                    },
                    {
                        "id" : "441322",
                        "name" : "博罗",
                        "pinyin" : "BoLuo"
                    },
                    {
                        "id" : "441323",
                        "name" : "惠东",
                        "pinyin" : "HuiDong"
                    },
                    {
                        "id" : "441324",
                        "name" : "龙门",
                        "pinyin" : "LongMen"
                    }
                ],
                "name" : "惠州",
                "pinyin" : "HuiZhou"
            },
            {
                "id" : 441400,
                "list" : [
                    {
                        "id" : "441402",
                        "name" : "梅江",
                        "pinyin" : "MeiJiang"
                    },
                    {
                        "id" : "441403",
                        "name" : "梅县",
                        "pinyin" : "MeiXian"
                    },
                    {
                        "id" : "441422",
                        "name" : "大埔",
                        "pinyin" : "DaPu"
                    },
                    {
                        "id" : "441423",
                        "name" : "丰顺",
                        "pinyin" : "FengShun"
                    },
                    {
                        "id" : "441424",
                        "name" : "五华",
                        "pinyin" : "WuHua"
                    },
                    {
                        "id" : "441426",
                        "name" : "平远",
                        "pinyin" : "PingYuan"
                    },
                    {
                        "id" : "441427",
                        "name" : "蕉岭",
                        "pinyin" : "JiaoLing"
                    },
                    {
                        "id" : "441481",
                        "name" : "兴宁",
                        "pinyin" : "XingNing"
                    }
                ],
                "name" : "梅州",
                "pinyin" : "MeiZhou"
            },
            {
                "id" : 441500,
                "list" : [
                    {
                        "id" : "441502",
                        "name" : "城区",
                        "pinyin" : "ChengQu"
                    },
                    {
                        "id" : "441521",
                        "name" : "海丰",
                        "pinyin" : "HaiFeng"
                    },
                    {
                        "id" : "441523",
                        "name" : "陆河",
                        "pinyin" : "LuHe"
                    },
                    {
                        "id" : "441581",
                        "name" : "陆丰",
                        "pinyin" : "LuFeng"
                    }
                ],
                "name" : "汕尾",
                "pinyin" : "ShanWei"
            },
            {
                "id" : 441600,
                "list" : [
                    {
                        "id" : "441602",
                        "name" : "源城",
                        "pinyin" : "YuanCheng"
                    },
                    {
                        "id" : "441621",
                        "name" : "紫金",
                        "pinyin" : "ZiJin"
                    },
                    {
                        "id" : "441622",
                        "name" : "龙川",
                        "pinyin" : "LongChuan"
                    },
                    {
                        "id" : "441623",
                        "name" : "连平",
                        "pinyin" : "LianPing"
                    },
                    {
                        "id" : "441624",
                        "name" : "和平",
                        "pinyin" : "HePing"
                    },
                    {
                        "id" : "441625",
                        "name" : "东源",
                        "pinyin" : "DongYuan"
                    }
                ],
                "name" : "河源",
                "pinyin" : "HeYuan"
            },
            {
                "id" : 441700,
                "list" : [
                    {
                        "id" : "441702",
                        "name" : "江城",
                        "pinyin" : "JiangCheng"
                    },
                    {
                        "id" : "441704",
                        "name" : "阳东",
                        "pinyin" : "YangDong"
                    },
                    {
                        "id" : "441721",
                        "name" : "阳西",
                        "pinyin" : "YangXi"
                    },
                    {
                        "id" : "441781",
                        "name" : "阳春",
                        "pinyin" : "YangChun"
                    }
                ],
                "name" : "阳江",
                "pinyin" : "YangJiang"
            },
            {
                "id" : 441800,
                "list" : [
                    {
                        "id" : "441802",
                        "name" : "清城",
                        "pinyin" : "QingCheng"
                    },
                    {
                        "id" : "441803",
                        "name" : "清新",
                        "pinyin" : "QingXin"
                    },
                    {
                        "id" : "441821",
                        "name" : "佛冈",
                        "pinyin" : "FuGang"
                    },
                    {
                        "id" : "441823",
                        "name" : "阳山",
                        "pinyin" : "YangShan"
                    },
                    {
                        "id" : "441825",
                        "name" : "连山",
                        "pinyin" : "LianShan"
                    },
                    {
                        "id" : "441826",
                        "name" : "连南",
                        "pinyin" : "LianNan"
                    },
                    {
                        "id" : "441881",
                        "name" : "英德",
                        "pinyin" : "YingDe"
                    },
                    {
                        "id" : "441882",
                        "name" : "连州",
                        "pinyin" : "LianZhou"
                    }
                ],
                "name" : "清远",
                "pinyin" : "QingYuan"
            },
            {
                "id" : 441900,
                "list" : [
                    {
                        "id" : "441900",
                        "name" : "东莞",
                        "pinyin" : "DongGuan"
                    }
                ],
                "name" : "东莞",
                "pinyin" : "DongGuan"
            },
            {
                "id" : 442000,
                "list" : [
                    {
                        "id" : "442000",
                        "name" : "中山",
                        "pinyin" : "ZhongShan"
                    }
                ],
                "name" : "中山",
                "pinyin" : "ZhongShan"
            },
            {
                "id" : 445100,
                "list" : [
                    {
                        "id" : "445102",
                        "name" : "湘桥",
                        "pinyin" : "XiangQiao"
                    },
                    {
                        "id" : "445103",
                        "name" : "潮安",
                        "pinyin" : "ChaoAn"
                    },
                    {
                        "id" : "445122",
                        "name" : "饶平",
                        "pinyin" : "RaoPing"
                    }
                ],
                "name" : "潮州",
                "pinyin" : "ChaoZhou"
            },
            {
                "id" : 445200,
                "list" : [
                    {
                        "id" : "445202",
                        "name" : "榕城",
                        "pinyin" : "RongCheng"
                    },
                    {
                        "id" : "445203",
                        "name" : "揭东",
                        "pinyin" : "JieDong"
                    },
                    {
                        "id" : "445222",
                        "name" : "揭西",
                        "pinyin" : "JieXi"
                    },
                    {
                        "id" : "445224",
                        "name" : "惠来",
                        "pinyin" : "HuiLai"
                    },
                    {
                        "id" : "445281",
                        "name" : "普宁",
                        "pinyin" : "PuNing"
                    }
                ],
                "name" : "揭阳",
                "pinyin" : "JieYang"
            },
            {
                "id" : 445300,
                "list" : [
                    {
                        "id" : "445302",
                        "name" : "云城",
                        "pinyin" : "YunCheng"
                    },
                    {
                        "id" : "445303",
                        "name" : "云安",
                        "pinyin" : "YunAn"
                    },
                    {
                        "id" : "445321",
                        "name" : "新兴",
                        "pinyin" : "XinXing"
                    },
                    {
                        "id" : "445322",
                        "name" : "郁南",
                        "pinyin" : "YuNan"
                    },
                    {
                        "id" : "445381",
                        "name" : "罗定",
                        "pinyin" : "LuoDing"
                    }
                ],
                "name" : "云浮",
                "pinyin" : "YunFu"
            }
        ],
        "name" : "广东",
        "pinyin" : "GuangDong"
    },
    {
        "id" : 450000,
        "list" : [
            {
                "id" : 450100,
                "list" : [
                    {
                        "id" : "450102",
                        "name" : "兴宁",
                        "pinyin" : "XingNing"
                    },
                    {
                        "id" : "450103",
                        "name" : "青秀",
                        "pinyin" : "QingXiu"
                    },
                    {
                        "id" : "450105",
                        "name" : "江南",
                        "pinyin" : "JiangNan"
                    },
                    {
                        "id" : "450107",
                        "name" : "西乡塘",
                        "pinyin" : "XiXiangTang"
                    },
                    {
                        "id" : "450108",
                        "name" : "良庆",
                        "pinyin" : "LiangQing"
                    },
                    {
                        "id" : "450109",
                        "name" : "邕宁",
                        "pinyin" : "YongNing"
                    },
                    {
                        "id" : "450110",
                        "name" : "武鸣",
                        "pinyin" : "WuMing"
                    },
                    {
                        "id" : "450123",
                        "name" : "隆安",
                        "pinyin" : "LongAn"
                    },
                    {
                        "id" : "450124",
                        "name" : "马山",
                        "pinyin" : "MaShan"
                    },
                    {
                        "id" : "450125",
                        "name" : "上林",
                        "pinyin" : "ShangLin"
                    },
                    {
                        "id" : "450126",
                        "name" : "宾阳",
                        "pinyin" : "BinYang"
                    },
                    {
                        "id" : "450127",
                        "name" : "横县",
                        "pinyin" : "HengXian"
                    }
                ],
                "name" : "南宁",
                "pinyin" : "NanNing"
            },
            {
                "id" : 450200,
                "list" : [
                    {
                        "id" : "450202",
                        "name" : "城中",
                        "pinyin" : "ChengZhong"
                    },
                    {
                        "id" : "450203",
                        "name" : "鱼峰",
                        "pinyin" : "YuFeng"
                    },
                    {
                        "id" : "450204",
                        "name" : "柳南",
                        "pinyin" : "LiuNan"
                    },
                    {
                        "id" : "450205",
                        "name" : "柳北",
                        "pinyin" : "LiuBei"
                    },
                    {
                        "id" : "450206",
                        "name" : "柳江",
                        "pinyin" : "LiuJiang"
                    },
                    {
                        "id" : "450222",
                        "name" : "柳城",
                        "pinyin" : "LiuCheng"
                    },
                    {
                        "id" : "450223",
                        "name" : "鹿寨",
                        "pinyin" : "LuZhai"
                    },
                    {
                        "id" : "450224",
                        "name" : "融安",
                        "pinyin" : "RongAn"
                    },
                    {
                        "id" : "450225",
                        "name" : "融水",
                        "pinyin" : "RongShui"
                    },
                    {
                        "id" : "450226",
                        "name" : "三江",
                        "pinyin" : "SanJiang"
                    }
                ],
                "name" : "柳州",
                "pinyin" : "LiuZhou"
            },
            {
                "id" : 450300,
                "list" : [
                    {
                        "id" : "450302",
                        "name" : "秀峰",
                        "pinyin" : "XiuFeng"
                    },
                    {
                        "id" : "450303",
                        "name" : "叠彩",
                        "pinyin" : "DieCai"
                    },
                    {
                        "id" : "450304",
                        "name" : "象山",
                        "pinyin" : "XiangShan"
                    },
                    {
                        "id" : "450305",
                        "name" : "七星",
                        "pinyin" : "QiXing"
                    },
                    {
                        "id" : "450311",
                        "name" : "雁山",
                        "pinyin" : "YanShan"
                    },
                    {
                        "id" : "450312",
                        "name" : "临桂",
                        "pinyin" : "LinGui"
                    },
                    {
                        "id" : "450321",
                        "name" : "阳朔",
                        "pinyin" : "YangShuo"
                    },
                    {
                        "id" : "450323",
                        "name" : "灵川",
                        "pinyin" : "LingChuan"
                    },
                    {
                        "id" : "450324",
                        "name" : "全州",
                        "pinyin" : "QuanZhou"
                    },
                    {
                        "id" : "450325",
                        "name" : "兴安",
                        "pinyin" : "XingAn"
                    },
                    {
                        "id" : "450326",
                        "name" : "永福",
                        "pinyin" : "YongFu"
                    },
                    {
                        "id" : "450327",
                        "name" : "灌阳",
                        "pinyin" : "GuanYang"
                    },
                    {
                        "id" : "450328",
                        "name" : "龙胜",
                        "pinyin" : "LongSheng"
                    },
                    {
                        "id" : "450329",
                        "name" : "资源",
                        "pinyin" : "ZiYuan"
                    },
                    {
                        "id" : "450330",
                        "name" : "平乐",
                        "pinyin" : "PingLe"
                    },
                    {
                        "id" : "450332",
                        "name" : "恭城",
                        "pinyin" : "GongCheng"
                    },
                    {
                        "id" : "450381",
                        "name" : "荔浦",
                        "pinyin" : "LiPu"
                    }
                ],
                "name" : "桂林",
                "pinyin" : "GuiLin"
            },
            {
                "id" : 450400,
                "list" : [
                    {
                        "id" : "450403",
                        "name" : "万秀",
                        "pinyin" : "WanXiu"
                    },
                    {
                        "id" : "450405",
                        "name" : "长洲",
                        "pinyin" : "ChangZhou"
                    },
                    {
                        "id" : "450406",
                        "name" : "龙圩",
                        "pinyin" : "LongXu"
                    },
                    {
                        "id" : "450421",
                        "name" : "苍梧",
                        "pinyin" : "CangWu"
                    },
                    {
                        "id" : "450422",
                        "name" : "藤县",
                        "pinyin" : "TengXian"
                    },
                    {
                        "id" : "450423",
                        "name" : "蒙山",
                        "pinyin" : "MengShan"
                    },
                    {
                        "id" : "450481",
                        "name" : "岑溪",
                        "pinyin" : "CenXi"
                    }
                ],
                "name" : "梧州",
                "pinyin" : "WuZhou"
            },
            {
                "id" : 450500,
                "list" : [
                    {
                        "id" : "450502",
                        "name" : "海城",
                        "pinyin" : "HaiCheng"
                    },
                    {
                        "id" : "450503",
                        "name" : "银海",
                        "pinyin" : "YinHai"
                    },
                    {
                        "id" : "450512",
                        "name" : "铁山港",
                        "pinyin" : "TieShanGang"
                    },
                    {
                        "id" : "450521",
                        "name" : "合浦",
                        "pinyin" : "HePu"
                    }
                ],
                "name" : "北海",
                "pinyin" : "BeiHai"
            },
            {
                "id" : 450600,
                "list" : [
                    {
                        "id" : "450602",
                        "name" : "港口",
                        "pinyin" : "GangKou"
                    },
                    {
                        "id" : "450603",
                        "name" : "防城",
                        "pinyin" : "FangCheng"
                    },
                    {
                        "id" : "450621",
                        "name" : "上思",
                        "pinyin" : "ShangSi"
                    },
                    {
                        "id" : "450681",
                        "name" : "东兴",
                        "pinyin" : "DongXing"
                    }
                ],
                "name" : "防城港",
                "pinyin" : "FangChengGang"
            },
            {
                "id" : 450700,
                "list" : [
                    {
                        "id" : "450702",
                        "name" : "钦南",
                        "pinyin" : "QinNan"
                    },
                    {
                        "id" : "450703",
                        "name" : "钦北",
                        "pinyin" : "QinBei"
                    },
                    {
                        "id" : "450721",
                        "name" : "灵山",
                        "pinyin" : "LingShan"
                    },
                    {
                        "id" : "450722",
                        "name" : "浦北",
                        "pinyin" : "PuBei"
                    }
                ],
                "name" : "钦州",
                "pinyin" : "QinZhou"
            },
            {
                "id" : 450800,
                "list" : [
                    {
                        "id" : "450802",
                        "name" : "港北",
                        "pinyin" : "GangBei"
                    },
                    {
                        "id" : "450803",
                        "name" : "港南",
                        "pinyin" : "GangNan"
                    },
                    {
                        "id" : "450804",
                        "name" : "覃塘",
                        "pinyin" : "QinTang"
                    },
                    {
                        "id" : "450821",
                        "name" : "平南",
                        "pinyin" : "PingNan"
                    },
                    {
                        "id" : "450881",
                        "name" : "桂平",
                        "pinyin" : "GuiPing"
                    }
                ],
                "name" : "贵港",
                "pinyin" : "GuiGang"
            },
            {
                "id" : 450900,
                "list" : [
                    {
                        "id" : "450902",
                        "name" : "玉州",
                        "pinyin" : "YuZhou"
                    },
                    {
                        "id" : "450903",
                        "name" : "福绵",
                        "pinyin" : "FuMian"
                    },
                    {
                        "id" : "450921",
                        "name" : "容县",
                        "pinyin" : "RongXian"
                    },
                    {
                        "id" : "450922",
                        "name" : "陆川",
                        "pinyin" : "LuChuan"
                    },
                    {
                        "id" : "450923",
                        "name" : "博白",
                        "pinyin" : "BoBai"
                    },
                    {
                        "id" : "450924",
                        "name" : "兴业",
                        "pinyin" : "XingYe"
                    },
                    {
                        "id" : "450981",
                        "name" : "北流",
                        "pinyin" : "BeiLiu"
                    }
                ],
                "name" : "玉林",
                "pinyin" : "YuLin"
            },
            {
                "id" : 451000,
                "list" : [
                    {
                        "id" : "451002",
                        "name" : "右江",
                        "pinyin" : "YouJiang"
                    },
                    {
                        "id" : "451021",
                        "name" : "田阳",
                        "pinyin" : "TianYang"
                    },
                    {
                        "id" : "451022",
                        "name" : "田东",
                        "pinyin" : "TianDong"
                    },
                    {
                        "id" : "451023",
                        "name" : "平果",
                        "pinyin" : "PingGuo"
                    },
                    {
                        "id" : "451024",
                        "name" : "德保",
                        "pinyin" : "DeBao"
                    },
                    {
                        "id" : "451026",
                        "name" : "那坡",
                        "pinyin" : "NaPo"
                    },
                    {
                        "id" : "451027",
                        "name" : "凌云",
                        "pinyin" : "LingYun"
                    },
                    {
                        "id" : "451028",
                        "name" : "乐业",
                        "pinyin" : "LeYe"
                    },
                    {
                        "id" : "451029",
                        "name" : "田林",
                        "pinyin" : "TianLin"
                    },
                    {
                        "id" : "451030",
                        "name" : "西林",
                        "pinyin" : "XiLin"
                    },
                    {
                        "id" : "451031",
                        "name" : "隆林",
                        "pinyin" : "LongLin"
                    },
                    {
                        "id" : "451081",
                        "name" : "靖西",
                        "pinyin" : "JingXi"
                    }
                ],
                "name" : "百色",
                "pinyin" : "BaiSe"
            },
            {
                "id" : 451100,
                "list" : [
                    {
                        "id" : "451102",
                        "name" : "八步",
                        "pinyin" : "BaBu"
                    },
                    {
                        "id" : "451103",
                        "name" : "平桂",
                        "pinyin" : "PingGui"
                    },
                    {
                        "id" : "451121",
                        "name" : "昭平",
                        "pinyin" : "ZhaoPing"
                    },
                    {
                        "id" : "451122",
                        "name" : "钟山",
                        "pinyin" : "ZhongShan"
                    },
                    {
                        "id" : "451123",
                        "name" : "富川",
                        "pinyin" : "FuChuan"
                    }
                ],
                "name" : "贺州",
                "pinyin" : "HeZhou"
            },
            {
                "id" : 451200,
                "list" : [
                    {
                        "id" : "451202",
                        "name" : "金城江",
                        "pinyin" : "JinChengJiang"
                    },
                    {
                        "id" : "451203",
                        "name" : "宜州",
                        "pinyin" : "YiZhou"
                    },
                    {
                        "id" : "451221",
                        "name" : "南丹",
                        "pinyin" : "NanDan"
                    },
                    {
                        "id" : "451222",
                        "name" : "天峨",
                        "pinyin" : "TianE"
                    },
                    {
                        "id" : "451223",
                        "name" : "凤山",
                        "pinyin" : "FengShan"
                    },
                    {
                        "id" : "451224",
                        "name" : "东兰",
                        "pinyin" : "DongLan"
                    },
                    {
                        "id" : "451225",
                        "name" : "罗城",
                        "pinyin" : "LuoCheng"
                    },
                    {
                        "id" : "451226",
                        "name" : "环江",
                        "pinyin" : "HuanJiang"
                    },
                    {
                        "id" : "451227",
                        "name" : "巴马",
                        "pinyin" : "BaMa"
                    },
                    {
                        "id" : "451228",
                        "name" : "都安",
                        "pinyin" : "DuAn"
                    },
                    {
                        "id" : "451229",
                        "name" : "大化",
                        "pinyin" : "DaHua"
                    }
                ],
                "name" : "河池",
                "pinyin" : "HeChi"
            },
            {
                "id" : 451300,
                "list" : [
                    {
                        "id" : "451302",
                        "name" : "兴宾",
                        "pinyin" : "XingBin"
                    },
                    {
                        "id" : "451321",
                        "name" : "忻城",
                        "pinyin" : "XinCheng"
                    },
                    {
                        "id" : "451322",
                        "name" : "象州",
                        "pinyin" : "XiangZhou"
                    },
                    {
                        "id" : "451323",
                        "name" : "武宣",
                        "pinyin" : "WuXuan"
                    },
                    {
                        "id" : "451324",
                        "name" : "金秀",
                        "pinyin" : "JinXiu"
                    },
                    {
                        "id" : "451381",
                        "name" : "合山",
                        "pinyin" : "HeShan"
                    }
                ],
                "name" : "来宾",
                "pinyin" : "LaiBin"
            },
            {
                "id" : 451400,
                "list" : [
                    {
                        "id" : "451402",
                        "name" : "江州",
                        "pinyin" : "JiangZhou"
                    },
                    {
                        "id" : "451421",
                        "name" : "扶绥",
                        "pinyin" : "FuSui"
                    },
                    {
                        "id" : "451422",
                        "name" : "宁明",
                        "pinyin" : "NingMing"
                    },
                    {
                        "id" : "451423",
                        "name" : "龙州",
                        "pinyin" : "LongZhou"
                    },
                    {
                        "id" : "451424",
                        "name" : "大新",
                        "pinyin" : "DaXin"
                    },
                    {
                        "id" : "451425",
                        "name" : "天等",
                        "pinyin" : "TianDeng"
                    },
                    {
                        "id" : "451481",
                        "name" : "凭祥",
                        "pinyin" : "PingXiang"
                    }
                ],
                "name" : "崇左",
                "pinyin" : "ChongZuo"
            }
        ],
        "name" : "广西",
        "pinyin" : "GuangXi"
    },
    {
        "id" : 460000,
        "list" : [
            {
                "id" : 460100,
                "list" : [
                    {
                        "id" : "460105",
                        "name" : "秀英",
                        "pinyin" : "XiuYing"
                    },
                    {
                        "id" : "460106",
                        "name" : "龙华",
                        "pinyin" : "LongHua"
                    },
                    {
                        "id" : "460107",
                        "name" : "琼山",
                        "pinyin" : "QiongShan"
                    },
                    {
                        "id" : "460108",
                        "name" : "美兰",
                        "pinyin" : "MeiLan"
                    }
                ],
                "name" : "海口",
                "pinyin" : "HaiKou"
            },
            {
                "id" : 460200,
                "list" : [
                    {
                        "id" : "460202",
                        "name" : "海棠",
                        "pinyin" : "HaiTang"
                    },
                    {
                        "id" : "460203",
                        "name" : "吉阳",
                        "pinyin" : "JiYang"
                    },
                    {
                        "id" : "460204",
                        "name" : "天涯",
                        "pinyin" : "TianYa"
                    },
                    {
                        "id" : "460205",
                        "name" : "崖州",
                        "pinyin" : "YaZhou"
                    }
                ],
                "name" : "三亚",
                "pinyin" : "SanYa"
            },
            {
                "id" : 460300,
                "list" : [
                    {
                        "id" : "460321",
                        "name" : "西沙群岛",
                        "pinyin" : "XiShaQunDao"
                    },
                    {
                        "id" : "460322",
                        "name" : "南沙群岛",
                        "pinyin" : "NanShaQunDao"
                    },
                    {
                        "id" : "460323",
                        "name" : "中沙群岛",
                        "pinyin" : "ZhongShaQunDao"
                    }
                ],
                "name" : "三沙",
                "pinyin" : "SanSha"
            },
            {
                "id" : 460400,
                "list" : [
                    {
                        "id" : "460400",
                        "name" : "儋州",
                        "pinyin" : "DanZhou"
                    }
                ],
                "name" : "儋州",
                "pinyin" : "DanZhou"
            },
            {
                "id" : 469001,
                "list" : [
                    {
                        "id" : "469001",
                        "name" : "五指山",
                        "pinyin" : "WuZhiShan"
                    }
                ],
                "name" : "五指山",
                "pinyin" : "WuZhiShan"
            },
            {
                "id" : 469002,
                "list" : [
                    {
                        "id" : "469002",
                        "name" : "琼海",
                        "pinyin" : "QiongHai"
                    }
                ],
                "name" : "琼海",
                "pinyin" : "QiongHai"
            },
            {
                "id" : 469005,
                "list" : [
                    {
                        "id" : "469005",
                        "name" : "文昌",
                        "pinyin" : "WenChang"
                    }
                ],
                "name" : "文昌",
                "pinyin" : "WenChang"
            },
            {
                "id" : 469006,
                "list" : [
                    {
                        "id" : "469006",
                        "name" : "万宁",
                        "pinyin" : "WanNing"
                    }
                ],
                "name" : "万宁",
                "pinyin" : "WanNing"
            },
            {
                "id" : 469007,
                "list" : [
                    {
                        "id" : "469007",
                        "name" : "东方",
                        "pinyin" : "DongFang"
                    }
                ],
                "name" : "东方",
                "pinyin" : "DongFang"
            },
            {
                "id" : 469021,
                "list" : [
                    {
                        "id" : "469021",
                        "name" : "定安",
                        "pinyin" : "DingAn"
                    }
                ],
                "name" : "定安",
                "pinyin" : "DingAn"
            },
            {
                "id" : 469022,
                "list" : [
                    {
                        "id" : "469022",
                        "name" : "屯昌",
                        "pinyin" : "TunChang"
                    }
                ],
                "name" : "屯昌",
                "pinyin" : "TunChang"
            },
            {
                "id" : 469023,
                "list" : [
                    {
                        "id" : "469023",
                        "name" : "澄迈",
                        "pinyin" : "ChengMai"
                    }
                ],
                "name" : "澄迈",
                "pinyin" : "ChengMai"
            },
            {
                "id" : 469024,
                "list" : [
                    {
                        "id" : "469024",
                        "name" : "临高",
                        "pinyin" : "LinGao"
                    }
                ],
                "name" : "临高",
                "pinyin" : "LinGao"
            },
            {
                "id" : 469025,
                "list" : [
                    {
                        "id" : "469025",
                        "name" : "白沙",
                        "pinyin" : "BaiSha"
                    }
                ],
                "name" : "白沙",
                "pinyin" : "BaiSha"
            },
            {
                "id" : 469026,
                "list" : [
                    {
                        "id" : "469026",
                        "name" : "昌江",
                        "pinyin" : "ChangJiang"
                    }
                ],
                "name" : "昌江",
                "pinyin" : "ChangJiang"
            },
            {
                "id" : 469027,
                "list" : [
                    {
                        "id" : "469027",
                        "name" : "乐东",
                        "pinyin" : "LeDong"
                    }
                ],
                "name" : "乐东",
                "pinyin" : "LeDong"
            },
            {
                "id" : 469028,
                "list" : [
                    {
                        "id" : "469028",
                        "name" : "陵水",
                        "pinyin" : "LingShui"
                    }
                ],
                "name" : "陵水",
                "pinyin" : "LingShui"
            },
            {
                "id" : 469029,
                "list" : [
                    {
                        "id" : "469029",
                        "name" : "保亭",
                        "pinyin" : "BaoTing"
                    }
                ],
                "name" : "保亭",
                "pinyin" : "BaoTing"
            },
            {
                "id" : 469030,
                "list" : [
                    {
                        "id" : "469030",
                        "name" : "琼中",
                        "pinyin" : "QiongZhong"
                    }
                ],
                "name" : "琼中",
                "pinyin" : "QiongZhong"
            }
        ],
        "name" : "海南",
        "pinyin" : "HaiNan"
    },
    {
        "id" : 500000,
        "list" : [
            {
                "id" : 500100,
                "list" : [
                    {
                        "id" : "500101",
                        "name" : "万州",
                        "pinyin" : "WanZhou"
                    },
                    {
                        "id" : "500102",
                        "name" : "涪陵",
                        "pinyin" : "FuLing"
                    },
                    {
                        "id" : "500103",
                        "name" : "渝中",
                        "pinyin" : "YuZhong"
                    },
                    {
                        "id" : "500104",
                        "name" : "大渡口",
                        "pinyin" : "DaDuKou"
                    },
                    {
                        "id" : "500105",
                        "name" : "江北",
                        "pinyin" : "JiangBei"
                    },
                    {
                        "id" : "500106",
                        "name" : "沙坪坝",
                        "pinyin" : "ShaPingBa"
                    },
                    {
                        "id" : "500107",
                        "name" : "九龙坡",
                        "pinyin" : "JiuLongPo"
                    },
                    {
                        "id" : "500108",
                        "name" : "南岸",
                        "pinyin" : "NanAn"
                    },
                    {
                        "id" : "500109",
                        "name" : "北碚",
                        "pinyin" : "BeiBei"
                    },
                    {
                        "id" : "500110",
                        "name" : "綦江",
                        "pinyin" : "QiJiang"
                    },
                    {
                        "id" : "500111",
                        "name" : "大足",
                        "pinyin" : "DaZu"
                    },
                    {
                        "id" : "500112",
                        "name" : "渝北",
                        "pinyin" : "YuBei"
                    },
                    {
                        "id" : "500113",
                        "name" : "巴南",
                        "pinyin" : "BaNan"
                    },
                    {
                        "id" : "500114",
                        "name" : "黔江",
                        "pinyin" : "QianJiang"
                    },
                    {
                        "id" : "500115",
                        "name" : "长寿",
                        "pinyin" : "ChangShou"
                    },
                    {
                        "id" : "500116",
                        "name" : "江津",
                        "pinyin" : "JiangJin"
                    },
                    {
                        "id" : "500117",
                        "name" : "合川",
                        "pinyin" : "HeChuan"
                    },
                    {
                        "id" : "500118",
                        "name" : "永川",
                        "pinyin" : "YongChuan"
                    },
                    {
                        "id" : "500119",
                        "name" : "南川",
                        "pinyin" : "NanChuan"
                    },
                    {
                        "id" : "500120",
                        "name" : "璧山",
                        "pinyin" : "BiShan"
                    },
                    {
                        "id" : "500151",
                        "name" : "铜梁",
                        "pinyin" : "TongLiang"
                    },
                    {
                        "id" : "500152",
                        "name" : "潼南",
                        "pinyin" : "TongNan"
                    },
                    {
                        "id" : "500153",
                        "name" : "荣昌",
                        "pinyin" : "RongChang"
                    },
                    {
                        "id" : "500154",
                        "name" : "开州",
                        "pinyin" : "KaiZhou"
                    },
                    {
                        "id" : "500155",
                        "name" : "梁平",
                        "pinyin" : "LiangPing"
                    },
                    {
                        "id" : "500156",
                        "name" : "武隆",
                        "pinyin" : "WuLong"
                    }
                ],
                "name" : "重庆",
                "pinyin" : "ChongQing"
            },
            {
                "id" : 500200,
                "list" : [
                    {
                        "id" : "500229",
                        "name" : "城口",
                        "pinyin" : "ChengKou"
                    },
                    {
                        "id" : "500230",
                        "name" : "丰都",
                        "pinyin" : "FengDu"
                    },
                    {
                        "id" : "500231",
                        "name" : "垫江",
                        "pinyin" : "DianJiang"
                    },
                    {
                        "id" : "500233",
                        "name" : "忠县",
                        "pinyin" : "ZhongXian"
                    },
                    {
                        "id" : "500235",
                        "name" : "云阳",
                        "pinyin" : "YunYang"
                    },
                    {
                        "id" : "500236",
                        "name" : "奉节",
                        "pinyin" : "FengJie"
                    },
                    {
                        "id" : "500237",
                        "name" : "巫山",
                        "pinyin" : "WuShan"
                    },
                    {
                        "id" : "500238",
                        "name" : "巫溪",
                        "pinyin" : "WuXi"
                    },
                    {
                        "id" : "500240",
                        "name" : "石柱",
                        "pinyin" : "ShiZhu"
                    },
                    {
                        "id" : "500241",
                        "name" : "秀山",
                        "pinyin" : "XiuShan"
                    },
                    {
                        "id" : "500242",
                        "name" : "酉阳",
                        "pinyin" : "YouYang"
                    },
                    {
                        "id" : "500243",
                        "name" : "彭水",
                        "pinyin" : "PengShui"
                    }
                ],
                "name" : "县",
                "pinyin" : "Xian"
            }
        ],
        "name" : "重庆",
        "pinyin" : "ChongQing"
    },
    {
        "id" : 510000,
        "list" : [
            {
                "id" : 510100,
                "list" : [
                    {
                        "id" : "510104",
                        "name" : "锦江",
                        "pinyin" : "JinJiang"
                    },
                    {
                        "id" : "510105",
                        "name" : "青羊",
                        "pinyin" : "QingYang"
                    },
                    {
                        "id" : "510106",
                        "name" : "金牛",
                        "pinyin" : "JinNiu"
                    },
                    {
                        "id" : "510107",
                        "name" : "武侯",
                        "pinyin" : "WuHou"
                    },
                    {
                        "id" : "510108",
                        "name" : "成华",
                        "pinyin" : "ChengHua"
                    },
                    {
                        "id" : "510112",
                        "name" : "龙泉驿",
                        "pinyin" : "LongQuanYi"
                    },
                    {
                        "id" : "510113",
                        "name" : "青白江",
                        "pinyin" : "QingBaiJiang"
                    },
                    {
                        "id" : "510114",
                        "name" : "新都",
                        "pinyin" : "XinDu"
                    },
                    {
                        "id" : "510115",
                        "name" : "温江",
                        "pinyin" : "WenJiang"
                    },
                    {
                        "id" : "510116",
                        "name" : "双流",
                        "pinyin" : "ShuangLiu"
                    },
                    {
                        "id" : "510117",
                        "name" : "郫都",
                        "pinyin" : "PiDu"
                    },
                    {
                        "id" : "510121",
                        "name" : "金堂",
                        "pinyin" : "JinTang"
                    },
                    {
                        "id" : "510129",
                        "name" : "大邑",
                        "pinyin" : "DaYi"
                    },
                    {
                        "id" : "510131",
                        "name" : "蒲江",
                        "pinyin" : "PuJiang"
                    },
                    {
                        "id" : "510132",
                        "name" : "新津",
                        "pinyin" : "XinJin"
                    },
                    {
                        "id" : "510181",
                        "name" : "都江堰",
                        "pinyin" : "DuJiangYan"
                    },
                    {
                        "id" : "510182",
                        "name" : "彭州",
                        "pinyin" : "PengZhou"
                    },
                    {
                        "id" : "510183",
                        "name" : "邛崃",
                        "pinyin" : "QiongLai"
                    },
                    {
                        "id" : "510184",
                        "name" : "崇州",
                        "pinyin" : "ChongZhou"
                    },
                    {
                        "id" : "510185",
                        "name" : "简阳",
                        "pinyin" : "JianYang"
                    }
                ],
                "name" : "成都",
                "pinyin" : "ChengDu"
            },
            {
                "id" : 510300,
                "list" : [
                    {
                        "id" : "510302",
                        "name" : "自流井",
                        "pinyin" : "ZiLiuJing"
                    },
                    {
                        "id" : "510303",
                        "name" : "贡井",
                        "pinyin" : "GongJing"
                    },
                    {
                        "id" : "510304",
                        "name" : "大安",
                        "pinyin" : "DaAn"
                    },
                    {
                        "id" : "510311",
                        "name" : "沿滩",
                        "pinyin" : "YanTan"
                    },
                    {
                        "id" : "510321",
                        "name" : "荣县",
                        "pinyin" : "RongXian"
                    },
                    {
                        "id" : "510322",
                        "name" : "富顺",
                        "pinyin" : "FuShun"
                    }
                ],
                "name" : "自贡",
                "pinyin" : "ZiGong"
            },
            {
                "id" : 510400,
                "list" : [
                    {
                        "id" : "510402",
                        "name" : "东区",
                        "pinyin" : "DongQu"
                    },
                    {
                        "id" : "510403",
                        "name" : "西区",
                        "pinyin" : "XiQu"
                    },
                    {
                        "id" : "510411",
                        "name" : "仁和",
                        "pinyin" : "RenHe"
                    },
                    {
                        "id" : "510421",
                        "name" : "米易",
                        "pinyin" : "MiYi"
                    },
                    {
                        "id" : "510422",
                        "name" : "盐边",
                        "pinyin" : "YanBian"
                    }
                ],
                "name" : "攀枝花",
                "pinyin" : "PanZhiHua"
            },
            {
                "id" : 510500,
                "list" : [
                    {
                        "id" : "510502",
                        "name" : "江阳",
                        "pinyin" : "JiangYang"
                    },
                    {
                        "id" : "510503",
                        "name" : "纳溪",
                        "pinyin" : "NaXi"
                    },
                    {
                        "id" : "510504",
                        "name" : "龙马潭",
                        "pinyin" : "LongMaTan"
                    },
                    {
                        "id" : "510521",
                        "name" : "泸县",
                        "pinyin" : "LuXian"
                    },
                    {
                        "id" : "510522",
                        "name" : "合江",
                        "pinyin" : "HeJiang"
                    },
                    {
                        "id" : "510524",
                        "name" : "叙永",
                        "pinyin" : "XuYong"
                    },
                    {
                        "id" : "510525",
                        "name" : "古蔺",
                        "pinyin" : "GuLin"
                    }
                ],
                "name" : "泸州",
                "pinyin" : "LuZhou"
            },
            {
                "id" : 510600,
                "list" : [
                    {
                        "id" : "510603",
                        "name" : "旌阳",
                        "pinyin" : "JingYang"
                    },
                    {
                        "id" : "510604",
                        "name" : "罗江",
                        "pinyin" : "LuoJiang"
                    },
                    {
                        "id" : "510623",
                        "name" : "中江",
                        "pinyin" : "ZhongJiang"
                    },
                    {
                        "id" : "510681",
                        "name" : "广汉",
                        "pinyin" : "GuangHan"
                    },
                    {
                        "id" : "510682",
                        "name" : "什邡",
                        "pinyin" : "ShiFang"
                    },
                    {
                        "id" : "510683",
                        "name" : "绵竹",
                        "pinyin" : "MianZhu"
                    }
                ],
                "name" : "德阳",
                "pinyin" : "DeYang"
            },
            {
                "id" : 510700,
                "list" : [
                    {
                        "id" : "510703",
                        "name" : "涪城",
                        "pinyin" : "FuCheng"
                    },
                    {
                        "id" : "510704",
                        "name" : "游仙",
                        "pinyin" : "YouXian"
                    },
                    {
                        "id" : "510705",
                        "name" : "安州",
                        "pinyin" : "AnZhou"
                    },
                    {
                        "id" : "510722",
                        "name" : "三台",
                        "pinyin" : "SanTai"
                    },
                    {
                        "id" : "510723",
                        "name" : "盐亭",
                        "pinyin" : "YanTing"
                    },
                    {
                        "id" : "510725",
                        "name" : "梓潼",
                        "pinyin" : "ZiTong"
                    },
                    {
                        "id" : "510726",
                        "name" : "北川",
                        "pinyin" : "BeiChuan"
                    },
                    {
                        "id" : "510727",
                        "name" : "平武",
                        "pinyin" : "PingWu"
                    },
                    {
                        "id" : "510781",
                        "name" : "江油",
                        "pinyin" : "JiangYou"
                    }
                ],
                "name" : "绵阳",
                "pinyin" : "MianYang"
            },
            {
                "id" : 510800,
                "list" : [
                    {
                        "id" : "510802",
                        "name" : "利州",
                        "pinyin" : "LiZhou"
                    },
                    {
                        "id" : "510811",
                        "name" : "昭化",
                        "pinyin" : "ZhaoHua"
                    },
                    {
                        "id" : "510812",
                        "name" : "朝天",
                        "pinyin" : "ChaoTian"
                    },
                    {
                        "id" : "510821",
                        "name" : "旺苍",
                        "pinyin" : "WangCang"
                    },
                    {
                        "id" : "510822",
                        "name" : "青川",
                        "pinyin" : "QingChuan"
                    },
                    {
                        "id" : "510823",
                        "name" : "剑阁",
                        "pinyin" : "JianGe"
                    },
                    {
                        "id" : "510824",
                        "name" : "苍溪",
                        "pinyin" : "CangXi"
                    }
                ],
                "name" : "广元",
                "pinyin" : "GuangYuan"
            },
            {
                "id" : 510900,
                "list" : [
                    {
                        "id" : "510903",
                        "name" : "船山",
                        "pinyin" : "ChuanShan"
                    },
                    {
                        "id" : "510904",
                        "name" : "安居",
                        "pinyin" : "AnJi"
                    },
                    {
                        "id" : "510921",
                        "name" : "蓬溪",
                        "pinyin" : "PengXi"
                    },
                    {
                        "id" : "510922",
                        "name" : "射洪",
                        "pinyin" : "SheHong"
                    },
                    {
                        "id" : "510923",
                        "name" : "大英",
                        "pinyin" : "DaYing"
                    }
                ],
                "name" : "遂宁",
                "pinyin" : "SuiNing"
            },
            {
                "id" : 511000,
                "list" : [
                    {
                        "id" : "511002",
                        "name" : "市中",
                        "pinyin" : "ShiZhong"
                    },
                    {
                        "id" : "511011",
                        "name" : "东兴",
                        "pinyin" : "DongXing"
                    },
                    {
                        "id" : "511024",
                        "name" : "威远",
                        "pinyin" : "WeiYuan"
                    },
                    {
                        "id" : "511025",
                        "name" : "资中",
                        "pinyin" : "ZiZhong"
                    },
                    {
                        "id" : "511083",
                        "name" : "隆昌",
                        "pinyin" : "LongChang"
                    }
                ],
                "name" : "内江",
                "pinyin" : "NaJiang"
            },
            {
                "id" : 511100,
                "list" : [
                    {
                        "id" : "511102",
                        "name" : "市中",
                        "pinyin" : "ShiZhong"
                    },
                    {
                        "id" : "511111",
                        "name" : "沙湾",
                        "pinyin" : "ShaWan"
                    },
                    {
                        "id" : "511112",
                        "name" : "五通桥",
                        "pinyin" : "WuTongQiao"
                    },
                    {
                        "id" : "511113",
                        "name" : "金口河",
                        "pinyin" : "JinKouHe"
                    },
                    {
                        "id" : "511123",
                        "name" : "犍为",
                        "pinyin" : "QianWei"
                    },
                    {
                        "id" : "511124",
                        "name" : "井研",
                        "pinyin" : "JingYan"
                    },
                    {
                        "id" : "511126",
                        "name" : "夹江",
                        "pinyin" : "JiaJiang"
                    },
                    {
                        "id" : "511129",
                        "name" : "沐川",
                        "pinyin" : "MuChuan"
                    },
                    {
                        "id" : "511132",
                        "name" : "峨边",
                        "pinyin" : "EBian"
                    },
                    {
                        "id" : "511133",
                        "name" : "马边",
                        "pinyin" : "MaBian"
                    },
                    {
                        "id" : "511181",
                        "name" : "峨眉山",
                        "pinyin" : "EMeiShan"
                    }
                ],
                "name" : "乐山",
                "pinyin" : "LeShan"
            },
            {
                "id" : 511300,
                "list" : [
                    {
                        "id" : "511302",
                        "name" : "顺庆",
                        "pinyin" : "ShunQing"
                    },
                    {
                        "id" : "511303",
                        "name" : "高坪",
                        "pinyin" : "GaoPing"
                    },
                    {
                        "id" : "511304",
                        "name" : "嘉陵",
                        "pinyin" : "JiaLing"
                    },
                    {
                        "id" : "511321",
                        "name" : "南部",
                        "pinyin" : "NanBu"
                    },
                    {
                        "id" : "511322",
                        "name" : "营山",
                        "pinyin" : "YingShan"
                    },
                    {
                        "id" : "511323",
                        "name" : "蓬安",
                        "pinyin" : "PengAn"
                    },
                    {
                        "id" : "511324",
                        "name" : "仪陇",
                        "pinyin" : "YiLong"
                    },
                    {
                        "id" : "511325",
                        "name" : "西充",
                        "pinyin" : "XiChong"
                    },
                    {
                        "id" : "511381",
                        "name" : "阆中",
                        "pinyin" : "LangZhong"
                    }
                ],
                "name" : "南充",
                "pinyin" : "NanChong"
            },
            {
                "id" : 511400,
                "list" : [
                    {
                        "id" : "511402",
                        "name" : "东坡",
                        "pinyin" : "DongPo"
                    },
                    {
                        "id" : "511403",
                        "name" : "彭山",
                        "pinyin" : "PengShan"
                    },
                    {
                        "id" : "511421",
                        "name" : "仁寿",
                        "pinyin" : "RenShou"
                    },
                    {
                        "id" : "511423",
                        "name" : "洪雅",
                        "pinyin" : "HongYa"
                    },
                    {
                        "id" : "511424",
                        "name" : "丹棱",
                        "pinyin" : "DanLing"
                    },
                    {
                        "id" : "511425",
                        "name" : "青神",
                        "pinyin" : "QingShen"
                    }
                ],
                "name" : "眉山",
                "pinyin" : "MeiShan"
            },
            {
                "id" : 511500,
                "list" : [
                    {
                        "id" : "511502",
                        "name" : "翠屏",
                        "pinyin" : "CuiPing"
                    },
                    {
                        "id" : "511503",
                        "name" : "南溪",
                        "pinyin" : "NanXi"
                    },
                    {
                        "id" : "511504",
                        "name" : "叙州",
                        "pinyin" : "XuZhou"
                    },
                    {
                        "id" : "511523",
                        "name" : "江安",
                        "pinyin" : "JiangAn"
                    },
                    {
                        "id" : "511524",
                        "name" : "长宁",
                        "pinyin" : "ChangNing"
                    },
                    {
                        "id" : "511525",
                        "name" : "高县",
                        "pinyin" : "GaoXian"
                    },
                    {
                        "id" : "511526",
                        "name" : "珙县",
                        "pinyin" : "GongXian"
                    },
                    {
                        "id" : "511527",
                        "name" : "筠连",
                        "pinyin" : "JunLian"
                    },
                    {
                        "id" : "511528",
                        "name" : "兴文",
                        "pinyin" : "XingWen"
                    },
                    {
                        "id" : "511529",
                        "name" : "屏山",
                        "pinyin" : "PingShan"
                    }
                ],
                "name" : "宜宾",
                "pinyin" : "YiBin"
            },
            {
                "id" : 511600,
                "list" : [
                    {
                        "id" : "511602",
                        "name" : "广安",
                        "pinyin" : "GuangAn"
                    },
                    {
                        "id" : "511603",
                        "name" : "前锋",
                        "pinyin" : "QianFeng"
                    },
                    {
                        "id" : "511621",
                        "name" : "岳池",
                        "pinyin" : "YueChi"
                    },
                    {
                        "id" : "511622",
                        "name" : "武胜",
                        "pinyin" : "WuSheng"
                    },
                    {
                        "id" : "511623",
                        "name" : "邻水",
                        "pinyin" : "LinShui"
                    },
                    {
                        "id" : "511681",
                        "name" : "华蓥",
                        "pinyin" : "HuaNing"
                    }
                ],
                "name" : "广安",
                "pinyin" : "GuangAn"
            },
            {
                "id" : 511700,
                "list" : [
                    {
                        "id" : "511702",
                        "name" : "通川",
                        "pinyin" : "TongChuan"
                    },
                    {
                        "id" : "511703",
                        "name" : "达川",
                        "pinyin" : "DaChuan"
                    },
                    {
                        "id" : "511722",
                        "name" : "宣汉",
                        "pinyin" : "XuanHan"
                    },
                    {
                        "id" : "511723",
                        "name" : "开江",
                        "pinyin" : "KaiJiang"
                    },
                    {
                        "id" : "511724",
                        "name" : "大竹",
                        "pinyin" : "DaZhu"
                    },
                    {
                        "id" : "511725",
                        "name" : "渠县",
                        "pinyin" : "QuXian"
                    },
                    {
                        "id" : "511781",
                        "name" : "万源",
                        "pinyin" : "WanYuan"
                    }
                ],
                "name" : "达州",
                "pinyin" : "DaZhou"
            },
            {
                "id" : 511800,
                "list" : [
                    {
                        "id" : "511802",
                        "name" : "雨城",
                        "pinyin" : "YuCheng"
                    },
                    {
                        "id" : "511803",
                        "name" : "名山",
                        "pinyin" : "MingShan"
                    },
                    {
                        "id" : "511822",
                        "name" : "荥经",
                        "pinyin" : "XingJing"
                    },
                    {
                        "id" : "511823",
                        "name" : "汉源",
                        "pinyin" : "HanYuan"
                    },
                    {
                        "id" : "511824",
                        "name" : "石棉",
                        "pinyin" : "ShiMian"
                    },
                    {
                        "id" : "511825",
                        "name" : "天全",
                        "pinyin" : "TianQuan"
                    },
                    {
                        "id" : "511826",
                        "name" : "芦山",
                        "pinyin" : "LuShan"
                    },
                    {
                        "id" : "511827",
                        "name" : "宝兴",
                        "pinyin" : "BaoXing"
                    }
                ],
                "name" : "雅安",
                "pinyin" : "YaAn"
            },
            {
                "id" : 511900,
                "list" : [
                    {
                        "id" : "511902",
                        "name" : "巴州",
                        "pinyin" : "BaZhou"
                    },
                    {
                        "id" : "511903",
                        "name" : "恩阳",
                        "pinyin" : "EnYang"
                    },
                    {
                        "id" : "511921",
                        "name" : "通江",
                        "pinyin" : "TongJiang"
                    },
                    {
                        "id" : "511922",
                        "name" : "南江",
                        "pinyin" : "NanJiang"
                    },
                    {
                        "id" : "511923",
                        "name" : "平昌",
                        "pinyin" : "PingChang"
                    }
                ],
                "name" : "巴中",
                "pinyin" : "BaZhong"
            },
            {
                "id" : 512000,
                "list" : [
                    {
                        "id" : "512002",
                        "name" : "雁江",
                        "pinyin" : "YanJiang"
                    },
                    {
                        "id" : "512021",
                        "name" : "安岳",
                        "pinyin" : "AnYue"
                    },
                    {
                        "id" : "512022",
                        "name" : "乐至",
                        "pinyin" : "LeZhi"
                    }
                ],
                "name" : "资阳",
                "pinyin" : "ZiYang"
            },
            {
                "id" : 513200,
                "list" : [
                    {
                        "id" : "513201",
                        "name" : "马尔康",
                        "pinyin" : "MaErKang"
                    },
                    {
                        "id" : "513221",
                        "name" : "汶川",
                        "pinyin" : "WenChuan"
                    },
                    {
                        "id" : "513222",
                        "name" : "理县",
                        "pinyin" : "LiXian"
                    },
                    {
                        "id" : "513223",
                        "name" : "茂县",
                        "pinyin" : "MaoXian"
                    },
                    {
                        "id" : "513224",
                        "name" : "松潘",
                        "pinyin" : "SongPan"
                    },
                    {
                        "id" : "513225",
                        "name" : "九寨沟",
                        "pinyin" : "JiuZhaiGou"
                    },
                    {
                        "id" : "513226",
                        "name" : "金川",
                        "pinyin" : "JinChuan"
                    },
                    {
                        "id" : "513227",
                        "name" : "小金",
                        "pinyin" : "XiaoJin"
                    },
                    {
                        "id" : "513228",
                        "name" : "黑水",
                        "pinyin" : "HeiShui"
                    },
                    {
                        "id" : "513230",
                        "name" : "壤塘",
                        "pinyin" : "RangTang"
                    },
                    {
                        "id" : "513231",
                        "name" : "阿坝",
                        "pinyin" : "ABa"
                    },
                    {
                        "id" : "513232",
                        "name" : "若尔盖",
                        "pinyin" : "RuoErGai"
                    },
                    {
                        "id" : "513233",
                        "name" : "红原",
                        "pinyin" : "HongYuan"
                    }
                ],
                "name" : "阿坝",
                "pinyin" : "ABa"
            },
            {
                "id" : 513300,
                "list" : [
                    {
                        "id" : "513301",
                        "name" : "康定",
                        "pinyin" : "KangDing"
                    },
                    {
                        "id" : "513322",
                        "name" : "泸定",
                        "pinyin" : "LuDing"
                    },
                    {
                        "id" : "513323",
                        "name" : "丹巴",
                        "pinyin" : "DanBa"
                    },
                    {
                        "id" : "513324",
                        "name" : "九龙",
                        "pinyin" : "JiuLong"
                    },
                    {
                        "id" : "513325",
                        "name" : "雅江",
                        "pinyin" : "YaJiang"
                    },
                    {
                        "id" : "513326",
                        "name" : "道孚",
                        "pinyin" : "DaoFu"
                    },
                    {
                        "id" : "513327",
                        "name" : "炉霍",
                        "pinyin" : "LuHuo"
                    },
                    {
                        "id" : "513328",
                        "name" : "甘孜",
                        "pinyin" : "GanZi"
                    },
                    {
                        "id" : "513329",
                        "name" : "新龙",
                        "pinyin" : "XinLong"
                    },
                    {
                        "id" : "513330",
                        "name" : "德格",
                        "pinyin" : "DeGe"
                    },
                    {
                        "id" : "513331",
                        "name" : "白玉",
                        "pinyin" : "BaiYu"
                    },
                    {
                        "id" : "513332",
                        "name" : "石渠",
                        "pinyin" : "ShiQu"
                    },
                    {
                        "id" : "513333",
                        "name" : "色达",
                        "pinyin" : "SeDa"
                    },
                    {
                        "id" : "513334",
                        "name" : "理塘",
                        "pinyin" : "LiTang"
                    },
                    {
                        "id" : "513335",
                        "name" : "巴塘",
                        "pinyin" : "BaTang"
                    },
                    {
                        "id" : "513336",
                        "name" : "乡城",
                        "pinyin" : "XiangCheng"
                    },
                    {
                        "id" : "513337",
                        "name" : "稻城",
                        "pinyin" : "DaoCheng"
                    },
                    {
                        "id" : "513338",
                        "name" : "得荣",
                        "pinyin" : "DeRong"
                    }
                ],
                "name" : "甘孜",
                "pinyin" : "GanZi"
            },
            {
                "id" : 513400,
                "list" : [
                    {
                        "id" : "513401",
                        "name" : "西昌",
                        "pinyin" : "XiChang"
                    },
                    {
                        "id" : "513422",
                        "name" : "木里",
                        "pinyin" : "MuLi"
                    },
                    {
                        "id" : "513423",
                        "name" : "盐源",
                        "pinyin" : "YanYuan"
                    },
                    {
                        "id" : "513424",
                        "name" : "德昌",
                        "pinyin" : "DeChang"
                    },
                    {
                        "id" : "513425",
                        "name" : "会理",
                        "pinyin" : "HuiLi"
                    },
                    {
                        "id" : "513426",
                        "name" : "会东",
                        "pinyin" : "HuiDong"
                    },
                    {
                        "id" : "513427",
                        "name" : "宁南",
                        "pinyin" : "NingNan"
                    },
                    {
                        "id" : "513428",
                        "name" : "普格",
                        "pinyin" : "PuGe"
                    },
                    {
                        "id" : "513429",
                        "name" : "布拖",
                        "pinyin" : "BuTuo"
                    },
                    {
                        "id" : "513430",
                        "name" : "金阳",
                        "pinyin" : "JinYang"
                    },
                    {
                        "id" : "513431",
                        "name" : "昭觉",
                        "pinyin" : "ZhaoJue"
                    },
                    {
                        "id" : "513432",
                        "name" : "喜德",
                        "pinyin" : "XiDe"
                    },
                    {
                        "id" : "513433",
                        "name" : "冕宁",
                        "pinyin" : "MianNing"
                    },
                    {
                        "id" : "513434",
                        "name" : "越西",
                        "pinyin" : "YueXi"
                    },
                    {
                        "id" : "513435",
                        "name" : "甘洛",
                        "pinyin" : "GanLuo"
                    },
                    {
                        "id" : "513436",
                        "name" : "美姑",
                        "pinyin" : "MeiGu"
                    },
                    {
                        "id" : "513437",
                        "name" : "雷波",
                        "pinyin" : "LeiBo"
                    }
                ],
                "name" : "凉山",
                "pinyin" : "LiangShan"
            }
        ],
        "name" : "四川",
        "pinyin" : "SiChuan"
    },
    {
        "id" : 520000,
        "list" : [
            {
                "id" : 520100,
                "list" : [
                    {
                        "id" : "520102",
                        "name" : "南明",
                        "pinyin" : "NanMing"
                    },
                    {
                        "id" : "520103",
                        "name" : "云岩",
                        "pinyin" : "YunYan"
                    },
                    {
                        "id" : "520111",
                        "name" : "花溪",
                        "pinyin" : "HuaXi"
                    },
                    {
                        "id" : "520112",
                        "name" : "乌当",
                        "pinyin" : "WuDang"
                    },
                    {
                        "id" : "520113",
                        "name" : "白云",
                        "pinyin" : "BaiYun"
                    },
                    {
                        "id" : "520115",
                        "name" : "观山湖",
                        "pinyin" : "GuanShanHu"
                    },
                    {
                        "id" : "520121",
                        "name" : "开阳",
                        "pinyin" : "KaiYang"
                    },
                    {
                        "id" : "520122",
                        "name" : "息烽",
                        "pinyin" : "XiFeng"
                    },
                    {
                        "id" : "520123",
                        "name" : "修文",
                        "pinyin" : "XiuWen"
                    },
                    {
                        "id" : "520181",
                        "name" : "清镇",
                        "pinyin" : "QingZhen"
                    }
                ],
                "name" : "贵阳",
                "pinyin" : "GuiYang"
            },
            {
                "id" : 520200,
                "list" : [
                    {
                        "id" : "520201",
                        "name" : "钟山",
                        "pinyin" : "ZhongShan"
                    },
                    {
                        "id" : "520203",
                        "name" : "六枝特",
                        "pinyin" : "LiuZhiTe"
                    },
                    {
                        "id" : "520221",
                        "name" : "水城",
                        "pinyin" : "ShuiCheng"
                    },
                    {
                        "id" : "520281",
                        "name" : "盘州",
                        "pinyin" : "PanZhou"
                    }
                ],
                "name" : "六盘水",
                "pinyin" : "LiuPanShui"
            },
            {
                "id" : 520300,
                "list" : [
                    {
                        "id" : "520302",
                        "name" : "红花岗",
                        "pinyin" : "HongHuaGang"
                    },
                    {
                        "id" : "520303",
                        "name" : "汇川",
                        "pinyin" : "HuiChuan"
                    },
                    {
                        "id" : "520304",
                        "name" : "播州",
                        "pinyin" : "BoZhou"
                    },
                    {
                        "id" : "520322",
                        "name" : "桐梓",
                        "pinyin" : "TongZi"
                    },
                    {
                        "id" : "520323",
                        "name" : "绥阳",
                        "pinyin" : "SuiYang"
                    },
                    {
                        "id" : "520324",
                        "name" : "正安",
                        "pinyin" : "ZhengAn"
                    },
                    {
                        "id" : "520325",
                        "name" : "道真",
                        "pinyin" : "DaoZhen"
                    },
                    {
                        "id" : "520326",
                        "name" : "务川",
                        "pinyin" : "WuChuan"
                    },
                    {
                        "id" : "520327",
                        "name" : "凤冈",
                        "pinyin" : "FengGang"
                    },
                    {
                        "id" : "520328",
                        "name" : "湄潭",
                        "pinyin" : "MeiTan"
                    },
                    {
                        "id" : "520329",
                        "name" : "余庆",
                        "pinyin" : "YuQing"
                    },
                    {
                        "id" : "520330",
                        "name" : "习水",
                        "pinyin" : "XiShui"
                    },
                    {
                        "id" : "520381",
                        "name" : "赤水",
                        "pinyin" : "ChiShui"
                    },
                    {
                        "id" : "520382",
                        "name" : "仁怀",
                        "pinyin" : "RenHuai"
                    }
                ],
                "name" : "遵义",
                "pinyin" : "ZunYi"
            },
            {
                "id" : 520400,
                "list" : [
                    {
                        "id" : "520402",
                        "name" : "西秀",
                        "pinyin" : "XiXiu"
                    },
                    {
                        "id" : "520403",
                        "name" : "平坝",
                        "pinyin" : "PingBa"
                    },
                    {
                        "id" : "520422",
                        "name" : "普定",
                        "pinyin" : "PuDing"
                    },
                    {
                        "id" : "520423",
                        "name" : "镇宁",
                        "pinyin" : "ZhenNing"
                    },
                    {
                        "id" : "520424",
                        "name" : "关岭",
                        "pinyin" : "GuanLing"
                    },
                    {
                        "id" : "520425",
                        "name" : "紫云",
                        "pinyin" : "ZiYun"
                    }
                ],
                "name" : "安顺",
                "pinyin" : "AnShun"
            },
            {
                "id" : 520500,
                "list" : [
                    {
                        "id" : "520502",
                        "name" : "七星关",
                        "pinyin" : "QiXingGuan"
                    },
                    {
                        "id" : "520521",
                        "name" : "大方",
                        "pinyin" : "DaFang"
                    },
                    {
                        "id" : "520522",
                        "name" : "黔西",
                        "pinyin" : "QianXi"
                    },
                    {
                        "id" : "520523",
                        "name" : "金沙",
                        "pinyin" : "JinSha"
                    },
                    {
                        "id" : "520524",
                        "name" : "织金",
                        "pinyin" : "ZhiJin"
                    },
                    {
                        "id" : "520525",
                        "name" : "纳雍",
                        "pinyin" : "NaYong"
                    },
                    {
                        "id" : "520526",
                        "name" : "威宁",
                        "pinyin" : "WeiNing"
                    },
                    {
                        "id" : "520527",
                        "name" : "赫章",
                        "pinyin" : "HeZhang"
                    }
                ],
                "name" : "毕节",
                "pinyin" : "BiJie"
            },
            {
                "id" : 520600,
                "list" : [
                    {
                        "id" : "520602",
                        "name" : "碧江",
                        "pinyin" : "BiJiang"
                    },
                    {
                        "id" : "520603",
                        "name" : "万山",
                        "pinyin" : "WanShan"
                    },
                    {
                        "id" : "520621",
                        "name" : "江口",
                        "pinyin" : "JiangKou"
                    },
                    {
                        "id" : "520622",
                        "name" : "玉屏",
                        "pinyin" : "YuPing"
                    },
                    {
                        "id" : "520623",
                        "name" : "石阡",
                        "pinyin" : "ShiQian"
                    },
                    {
                        "id" : "520624",
                        "name" : "思南",
                        "pinyin" : "SiNan"
                    },
                    {
                        "id" : "520625",
                        "name" : "印江",
                        "pinyin" : "YinJiang"
                    },
                    {
                        "id" : "520626",
                        "name" : "德江",
                        "pinyin" : "DeJiang"
                    },
                    {
                        "id" : "520627",
                        "name" : "沿河",
                        "pinyin" : "YanHe"
                    },
                    {
                        "id" : "520628",
                        "name" : "松桃",
                        "pinyin" : "SongTao"
                    }
                ],
                "name" : "铜仁",
                "pinyin" : "TongRen"
            },
            {
                "id" : 522300,
                "list" : [
                    {
                        "id" : "522301",
                        "name" : "兴义",
                        "pinyin" : "XingYi"
                    },
                    {
                        "id" : "522302",
                        "name" : "兴仁",
                        "pinyin" : "XingRen"
                    },
                    {
                        "id" : "522323",
                        "name" : "普安",
                        "pinyin" : "PuAn"
                    },
                    {
                        "id" : "522324",
                        "name" : "晴隆",
                        "pinyin" : "QingLong"
                    },
                    {
                        "id" : "522325",
                        "name" : "贞丰",
                        "pinyin" : "ZhenFeng"
                    },
                    {
                        "id" : "522326",
                        "name" : "望谟",
                        "pinyin" : "WangMo"
                    },
                    {
                        "id" : "522327",
                        "name" : "册亨",
                        "pinyin" : "CeHeng"
                    },
                    {
                        "id" : "522328",
                        "name" : "安龙",
                        "pinyin" : "AnLong"
                    }
                ],
                "name" : "黔西南",
                "pinyin" : "QianXiNan"
            },
            {
                "id" : 522600,
                "list" : [
                    {
                        "id" : "522601",
                        "name" : "凯里",
                        "pinyin" : "KaiLi"
                    },
                    {
                        "id" : "522622",
                        "name" : "黄平",
                        "pinyin" : "HuangPing"
                    },
                    {
                        "id" : "522623",
                        "name" : "施秉",
                        "pinyin" : "ShiBing"
                    },
                    {
                        "id" : "522624",
                        "name" : "三穗",
                        "pinyin" : "SanSui"
                    },
                    {
                        "id" : "522625",
                        "name" : "镇远",
                        "pinyin" : "ZhenYuan"
                    },
                    {
                        "id" : "522626",
                        "name" : "岑巩",
                        "pinyin" : "CenGong"
                    },
                    {
                        "id" : "522627",
                        "name" : "天柱",
                        "pinyin" : "TianZhu"
                    },
                    {
                        "id" : "522628",
                        "name" : "锦屏",
                        "pinyin" : "JinPing"
                    },
                    {
                        "id" : "522629",
                        "name" : "剑河",
                        "pinyin" : "JianHe"
                    },
                    {
                        "id" : "522630",
                        "name" : "台江",
                        "pinyin" : "TaiJiang"
                    },
                    {
                        "id" : "522631",
                        "name" : "黎平",
                        "pinyin" : "LiPing"
                    },
                    {
                        "id" : "522632",
                        "name" : "榕江",
                        "pinyin" : "RongJiang"
                    },
                    {
                        "id" : "522633",
                        "name" : "从江",
                        "pinyin" : "CongJiang"
                    },
                    {
                        "id" : "522634",
                        "name" : "雷山",
                        "pinyin" : "LeiShan"
                    },
                    {
                        "id" : "522635",
                        "name" : "麻江",
                        "pinyin" : "MaJiang"
                    },
                    {
                        "id" : "522636",
                        "name" : "丹寨",
                        "pinyin" : "DanZhai"
                    }
                ],
                "name" : "黔东南",
                "pinyin" : "QianDongNan"
            },
            {
                "id" : 522700,
                "list" : [
                    {
                        "id" : "522701",
                        "name" : "都匀",
                        "pinyin" : "DuYun"
                    },
                    {
                        "id" : "522702",
                        "name" : "福泉",
                        "pinyin" : "FuQuan"
                    },
                    {
                        "id" : "522722",
                        "name" : "荔波",
                        "pinyin" : "LiBo"
                    },
                    {
                        "id" : "522723",
                        "name" : "贵定",
                        "pinyin" : "GuiDing"
                    },
                    {
                        "id" : "522725",
                        "name" : "瓮安",
                        "pinyin" : "WengAn"
                    },
                    {
                        "id" : "522726",
                        "name" : "独山",
                        "pinyin" : "DuShan"
                    },
                    {
                        "id" : "522727",
                        "name" : "平塘",
                        "pinyin" : "PingTang"
                    },
                    {
                        "id" : "522728",
                        "name" : "罗甸",
                        "pinyin" : "LuoDian"
                    },
                    {
                        "id" : "522729",
                        "name" : "长顺",
                        "pinyin" : "ChangShun"
                    },
                    {
                        "id" : "522730",
                        "name" : "龙里",
                        "pinyin" : "LongLi"
                    },
                    {
                        "id" : "522731",
                        "name" : "惠水",
                        "pinyin" : "HuiShui"
                    },
                    {
                        "id" : "522732",
                        "name" : "三都",
                        "pinyin" : "SanDu"
                    }
                ],
                "name" : "黔南",
                "pinyin" : "QianNan"
            }
        ],
        "name" : "贵州",
        "pinyin" : "GuiZhou"
    },
    {
        "id" : 530000,
        "list" : [
            {
                "id" : 530100,
                "list" : [
                    {
                        "id" : "530102",
                        "name" : "五华",
                        "pinyin" : "WuHua"
                    },
                    {
                        "id" : "530103",
                        "name" : "盘龙",
                        "pinyin" : "PanLong"
                    },
                    {
                        "id" : "530111",
                        "name" : "官渡",
                        "pinyin" : "GuanDu"
                    },
                    {
                        "id" : "530112",
                        "name" : "西山",
                        "pinyin" : "XiShan"
                    },
                    {
                        "id" : "530113",
                        "name" : "东川",
                        "pinyin" : "DongChuan"
                    },
                    {
                        "id" : "530114",
                        "name" : "呈贡",
                        "pinyin" : "ChengGong"
                    },
                    {
                        "id" : "530115",
                        "name" : "晋宁",
                        "pinyin" : "JinNing"
                    },
                    {
                        "id" : "530124",
                        "name" : "富民",
                        "pinyin" : "FuMin"
                    },
                    {
                        "id" : "530125",
                        "name" : "宜良",
                        "pinyin" : "YiLiang"
                    },
                    {
                        "id" : "530126",
                        "name" : "石林",
                        "pinyin" : "ShiLin"
                    },
                    {
                        "id" : "530127",
                        "name" : "嵩明",
                        "pinyin" : "SongMing"
                    },
                    {
                        "id" : "530128",
                        "name" : "禄劝",
                        "pinyin" : "LuQuan"
                    },
                    {
                        "id" : "530129",
                        "name" : "寻甸",
                        "pinyin" : "XunDian"
                    },
                    {
                        "id" : "530181",
                        "name" : "安宁",
                        "pinyin" : "AnNing"
                    }
                ],
                "name" : "昆明",
                "pinyin" : "KunMing"
            },
            {
                "id" : 530300,
                "list" : [
                    {
                        "id" : "530302",
                        "name" : "麒麟",
                        "pinyin" : "QiLin"
                    },
                    {
                        "id" : "530303",
                        "name" : "沾益",
                        "pinyin" : "ZhanYi"
                    },
                    {
                        "id" : "530304",
                        "name" : "马龙",
                        "pinyin" : "MaLong"
                    },
                    {
                        "id" : "530322",
                        "name" : "陆良",
                        "pinyin" : "LuLiang"
                    },
                    {
                        "id" : "530323",
                        "name" : "师宗",
                        "pinyin" : "ShiZong"
                    },
                    {
                        "id" : "530324",
                        "name" : "罗平",
                        "pinyin" : "LuoPing"
                    },
                    {
                        "id" : "530325",
                        "name" : "富源",
                        "pinyin" : "FuYuan"
                    },
                    {
                        "id" : "530326",
                        "name" : "会泽",
                        "pinyin" : "HuiZe"
                    },
                    {
                        "id" : "530381",
                        "name" : "宣威",
                        "pinyin" : "XuanWei"
                    }
                ],
                "name" : "曲靖",
                "pinyin" : "QuJing"
            },
            {
                "id" : 530400,
                "list" : [
                    {
                        "id" : "530402",
                        "name" : "红塔",
                        "pinyin" : "HongTa"
                    },
                    {
                        "id" : "530403",
                        "name" : "江川",
                        "pinyin" : "JiangChuan"
                    },
                    {
                        "id" : "530422",
                        "name" : "澄江",
                        "pinyin" : "ChengJiang"
                    },
                    {
                        "id" : "530423",
                        "name" : "通海",
                        "pinyin" : "TongHai"
                    },
                    {
                        "id" : "530424",
                        "name" : "华宁",
                        "pinyin" : "HuaNing"
                    },
                    {
                        "id" : "530425",
                        "name" : "易门",
                        "pinyin" : "YiMen"
                    },
                    {
                        "id" : "530426",
                        "name" : "峨山",
                        "pinyin" : "EShan"
                    },
                    {
                        "id" : "530427",
                        "name" : "新平",
                        "pinyin" : "XinPing"
                    },
                    {
                        "id" : "530428",
                        "name" : "元江",
                        "pinyin" : "YuanJiang"
                    }
                ],
                "name" : "玉溪",
                "pinyin" : "YuXi"
            },
            {
                "id" : 530500,
                "list" : [
                    {
                        "id" : "530502",
                        "name" : "隆阳",
                        "pinyin" : "LongYang"
                    },
                    {
                        "id" : "530521",
                        "name" : "施甸",
                        "pinyin" : "ShiDian"
                    },
                    {
                        "id" : "530523",
                        "name" : "龙陵",
                        "pinyin" : "LongLing"
                    },
                    {
                        "id" : "530524",
                        "name" : "昌宁",
                        "pinyin" : "ChangNing"
                    },
                    {
                        "id" : "530581",
                        "name" : "腾冲",
                        "pinyin" : "TengChong"
                    }
                ],
                "name" : "保山",
                "pinyin" : "BaoShan"
            },
            {
                "id" : 530600,
                "list" : [
                    {
                        "id" : "530602",
                        "name" : "昭阳",
                        "pinyin" : "ZhaoYang"
                    },
                    {
                        "id" : "530621",
                        "name" : "鲁甸",
                        "pinyin" : "LuDian"
                    },
                    {
                        "id" : "530622",
                        "name" : "巧家",
                        "pinyin" : "QiaoJia"
                    },
                    {
                        "id" : "530623",
                        "name" : "盐津",
                        "pinyin" : "YanJin"
                    },
                    {
                        "id" : "530624",
                        "name" : "大关",
                        "pinyin" : "DaGuan"
                    },
                    {
                        "id" : "530625",
                        "name" : "永善",
                        "pinyin" : "YongShan"
                    },
                    {
                        "id" : "530626",
                        "name" : "绥江",
                        "pinyin" : "SuiJiang"
                    },
                    {
                        "id" : "530627",
                        "name" : "镇雄",
                        "pinyin" : "ZhenXiong"
                    },
                    {
                        "id" : "530628",
                        "name" : "彝良",
                        "pinyin" : "YiLiang"
                    },
                    {
                        "id" : "530629",
                        "name" : "威信",
                        "pinyin" : "WeiXin"
                    },
                    {
                        "id" : "530681",
                        "name" : "水富",
                        "pinyin" : "ShuiFu"
                    }
                ],
                "name" : "昭通",
                "pinyin" : "ZhaoTong"
            },
            {
                "id" : 530700,
                "list" : [
                    {
                        "id" : "530702",
                        "name" : "古城",
                        "pinyin" : "GuCheng"
                    },
                    {
                        "id" : "530721",
                        "name" : "玉龙",
                        "pinyin" : "YuLong"
                    },
                    {
                        "id" : "530722",
                        "name" : "永胜",
                        "pinyin" : "YongSheng"
                    },
                    {
                        "id" : "530723",
                        "name" : "华坪",
                        "pinyin" : "HuaPing"
                    },
                    {
                        "id" : "530724",
                        "name" : "宁蒗",
                        "pinyin" : "NingLang"
                    }
                ],
                "name" : "丽江",
                "pinyin" : "LiJiang"
            },
            {
                "id" : 530800,
                "list" : [
                    {
                        "id" : "530802",
                        "name" : "思茅",
                        "pinyin" : "SiMao"
                    },
                    {
                        "id" : "530821",
                        "name" : "宁洱",
                        "pinyin" : "NingEr"
                    },
                    {
                        "id" : "530822",
                        "name" : "墨江",
                        "pinyin" : "MoJiang"
                    },
                    {
                        "id" : "530823",
                        "name" : "景东",
                        "pinyin" : "JingDong"
                    },
                    {
                        "id" : "530824",
                        "name" : "景谷",
                        "pinyin" : "JingGu"
                    },
                    {
                        "id" : "530825",
                        "name" : "镇沅",
                        "pinyin" : "ZhenYuan"
                    },
                    {
                        "id" : "530826",
                        "name" : "江城",
                        "pinyin" : "JiangCheng"
                    },
                    {
                        "id" : "530827",
                        "name" : "孟连",
                        "pinyin" : "MengLian"
                    },
                    {
                        "id" : "530828",
                        "name" : "澜沧",
                        "pinyin" : "LanCang"
                    },
                    {
                        "id" : "530829",
                        "name" : "西盟",
                        "pinyin" : "XiMeng"
                    }
                ],
                "name" : "普洱",
                "pinyin" : "PuEr"
            },
            {
                "id" : 530900,
                "list" : [
                    {
                        "id" : "530902",
                        "name" : "临翔",
                        "pinyin" : "LinXiang"
                    },
                    {
                        "id" : "530921",
                        "name" : "凤庆",
                        "pinyin" : "FengQing"
                    },
                    {
                        "id" : "530922",
                        "name" : "云县",
                        "pinyin" : "YunXian"
                    },
                    {
                        "id" : "530923",
                        "name" : "永德",
                        "pinyin" : "YongDe"
                    },
                    {
                        "id" : "530924",
                        "name" : "镇康",
                        "pinyin" : "ZhenKang"
                    },
                    {
                        "id" : "530925",
                        "name" : "双江",
                        "pinyin" : "ShuangJiang"
                    },
                    {
                        "id" : "530926",
                        "name" : "耿马",
                        "pinyin" : "GengMa"
                    },
                    {
                        "id" : "530927",
                        "name" : "沧源",
                        "pinyin" : "CangYuan"
                    }
                ],
                "name" : "临沧",
                "pinyin" : "LinCang"
            },
            {
                "id" : 532300,
                "list" : [
                    {
                        "id" : "532301",
                        "name" : "楚雄",
                        "pinyin" : "ChuXiong"
                    },
                    {
                        "id" : "532322",
                        "name" : "双柏",
                        "pinyin" : "ShuangBai"
                    },
                    {
                        "id" : "532323",
                        "name" : "牟定",
                        "pinyin" : "MouDing"
                    },
                    {
                        "id" : "532324",
                        "name" : "南华",
                        "pinyin" : "NanHua"
                    },
                    {
                        "id" : "532325",
                        "name" : "姚安",
                        "pinyin" : "YaoAn"
                    },
                    {
                        "id" : "532326",
                        "name" : "大姚",
                        "pinyin" : "DaYao"
                    },
                    {
                        "id" : "532327",
                        "name" : "永仁",
                        "pinyin" : "YongRen"
                    },
                    {
                        "id" : "532328",
                        "name" : "元谋",
                        "pinyin" : "YuanMou"
                    },
                    {
                        "id" : "532329",
                        "name" : "武定",
                        "pinyin" : "WuDing"
                    },
                    {
                        "id" : "532331",
                        "name" : "禄丰",
                        "pinyin" : "LuFeng"
                    }
                ],
                "name" : "楚雄",
                "pinyin" : "ChuXiong"
            },
            {
                "id" : 532500,
                "list" : [
                    {
                        "id" : "532501",
                        "name" : "个旧",
                        "pinyin" : "GeJiu"
                    },
                    {
                        "id" : "532502",
                        "name" : "开远",
                        "pinyin" : "KaiYuan"
                    },
                    {
                        "id" : "532503",
                        "name" : "蒙自",
                        "pinyin" : "MengZi"
                    },
                    {
                        "id" : "532504",
                        "name" : "弥勒",
                        "pinyin" : "MiLe"
                    },
                    {
                        "id" : "532523",
                        "name" : "屏边",
                        "pinyin" : "PingBian"
                    },
                    {
                        "id" : "532524",
                        "name" : "建水",
                        "pinyin" : "JianShui"
                    },
                    {
                        "id" : "532525",
                        "name" : "石屏",
                        "pinyin" : "ShiPing"
                    },
                    {
                        "id" : "532527",
                        "name" : "泸西",
                        "pinyin" : "LuXi"
                    },
                    {
                        "id" : "532528",
                        "name" : "元阳",
                        "pinyin" : "YuanYang"
                    },
                    {
                        "id" : "532529",
                        "name" : "红河",
                        "pinyin" : "HongHe"
                    },
                    {
                        "id" : "532530",
                        "name" : "金平",
                        "pinyin" : "JinPing"
                    },
                    {
                        "id" : "532531",
                        "name" : "绿春",
                        "pinyin" : "LvChun"
                    },
                    {
                        "id" : "532532",
                        "name" : "河口",
                        "pinyin" : "HeKou"
                    }
                ],
                "name" : "红河",
                "pinyin" : "HongHe"
            },
            {
                "id" : 532600,
                "list" : [
                    {
                        "id" : "532601",
                        "name" : "文山",
                        "pinyin" : "WenShan"
                    },
                    {
                        "id" : "532622",
                        "name" : "砚山",
                        "pinyin" : "YanShan"
                    },
                    {
                        "id" : "532623",
                        "name" : "西畴",
                        "pinyin" : "XiChou"
                    },
                    {
                        "id" : "532624",
                        "name" : "麻栗坡",
                        "pinyin" : "MaLiPo"
                    },
                    {
                        "id" : "532625",
                        "name" : "马关",
                        "pinyin" : "MaGuan"
                    },
                    {
                        "id" : "532626",
                        "name" : "丘北",
                        "pinyin" : "QiuBei"
                    },
                    {
                        "id" : "532627",
                        "name" : "广南",
                        "pinyin" : "GuangNan"
                    },
                    {
                        "id" : "532628",
                        "name" : "富宁",
                        "pinyin" : "FuNing"
                    }
                ],
                "name" : "文山",
                "pinyin" : "WenShan"
            },
            {
                "id" : 532800,
                "list" : [
                    {
                        "id" : "532801",
                        "name" : "景洪",
                        "pinyin" : "JingHong"
                    },
                    {
                        "id" : "532822",
                        "name" : "勐海",
                        "pinyin" : "MengHai"
                    },
                    {
                        "id" : "532823",
                        "name" : "勐腊",
                        "pinyin" : "MengLa"
                    }
                ],
                "name" : "西双版纳",
                "pinyin" : "XiShuangBanNa"
            },
            {
                "id" : 532900,
                "list" : [
                    {
                        "id" : "532901",
                        "name" : "大理",
                        "pinyin" : "DaLi"
                    },
                    {
                        "id" : "532922",
                        "name" : "漾濞",
                        "pinyin" : "YangBi"
                    },
                    {
                        "id" : "532923",
                        "name" : "祥云",
                        "pinyin" : "XiangYun"
                    },
                    {
                        "id" : "532924",
                        "name" : "宾川",
                        "pinyin" : "BinChuan"
                    },
                    {
                        "id" : "532925",
                        "name" : "弥渡",
                        "pinyin" : "MiDu"
                    },
                    {
                        "id" : "532926",
                        "name" : "南涧",
                        "pinyin" : "NanJian"
                    },
                    {
                        "id" : "532927",
                        "name" : "巍山",
                        "pinyin" : "WeiShan"
                    },
                    {
                        "id" : "532928",
                        "name" : "永平",
                        "pinyin" : "YongPing"
                    },
                    {
                        "id" : "532929",
                        "name" : "云龙",
                        "pinyin" : "YunLong"
                    },
                    {
                        "id" : "532930",
                        "name" : "洱源",
                        "pinyin" : "ErYuan"
                    },
                    {
                        "id" : "532931",
                        "name" : "剑川",
                        "pinyin" : "JianChuan"
                    },
                    {
                        "id" : "532932",
                        "name" : "鹤庆",
                        "pinyin" : "HeQing"
                    }
                ],
                "name" : "大理",
                "pinyin" : "DaLi"
            },
            {
                "id" : 533100,
                "list" : [
                    {
                        "id" : "533102",
                        "name" : "瑞丽",
                        "pinyin" : "RuiLi"
                    },
                    {
                        "id" : "533103",
                        "name" : "芒市",
                        "pinyin" : "Mang"
                    },
                    {
                        "id" : "533122",
                        "name" : "梁河",
                        "pinyin" : "LiangHe"
                    },
                    {
                        "id" : "533123",
                        "name" : "盈江",
                        "pinyin" : "YingJiang"
                    },
                    {
                        "id" : "533124",
                        "name" : "陇川",
                        "pinyin" : "LongChuan"
                    }
                ],
                "name" : "德宏",
                "pinyin" : "DeHong"
            },
            {
                "id" : 533300,
                "list" : [
                    {
                        "id" : "533301",
                        "name" : "泸水",
                        "pinyin" : "LuShui"
                    },
                    {
                        "id" : "533323",
                        "name" : "福贡",
                        "pinyin" : "FuGong"
                    },
                    {
                        "id" : "533324",
                        "name" : "贡山",
                        "pinyin" : "GongShan"
                    },
                    {
                        "id" : "533325",
                        "name" : "兰坪",
                        "pinyin" : "LanPing"
                    }
                ],
                "name" : "怒江",
                "pinyin" : "NuJiang"
            },
            {
                "id" : 533400,
                "list" : [
                    {
                        "id" : "533401",
                        "name" : "香格里拉",
                        "pinyin" : "XiangGeLiLa"
                    },
                    {
                        "id" : "533422",
                        "name" : "德钦",
                        "pinyin" : "DeQin"
                    },
                    {
                        "id" : "533423",
                        "name" : "维西",
                        "pinyin" : "WeiXi"
                    }
                ],
                "name" : "迪庆",
                "pinyin" : "DiQing"
            }
        ],
        "name" : "云南",
        "pinyin" : "YunNan"
    },
    {
        "id" : 540000,
        "list" : [
            {
                "id" : 540100,
                "list" : [
                    {
                        "id" : "540102",
                        "name" : "城关",
                        "pinyin" : "ChengGuan"
                    },
                    {
                        "id" : "540103",
                        "name" : "堆龙德庆",
                        "pinyin" : "DuiLongDeQing"
                    },
                    {
                        "id" : "540104",
                        "name" : "达孜",
                        "pinyin" : "DaZi"
                    },
                    {
                        "id" : "540121",
                        "name" : "林周",
                        "pinyin" : "LinZhou"
                    },
                    {
                        "id" : "540122",
                        "name" : "当雄",
                        "pinyin" : "DangXiong"
                    },
                    {
                        "id" : "540123",
                        "name" : "尼木",
                        "pinyin" : "NiMu"
                    },
                    {
                        "id" : "540124",
                        "name" : "曲水",
                        "pinyin" : "QuShui"
                    },
                    {
                        "id" : "540127",
                        "name" : "墨竹工卡",
                        "pinyin" : "MoZhuGongKa"
                    }
                ],
                "name" : "拉萨",
                "pinyin" : "LaSa"
            },
            {
                "id" : 540200,
                "list" : [
                    {
                        "id" : "540202",
                        "name" : "桑珠孜",
                        "pinyin" : "SangZhuZi"
                    },
                    {
                        "id" : "540221",
                        "name" : "南木林",
                        "pinyin" : "NanMuLin"
                    },
                    {
                        "id" : "540222",
                        "name" : "江孜",
                        "pinyin" : "JiangZi"
                    },
                    {
                        "id" : "540223",
                        "name" : "定日",
                        "pinyin" : "DingRi"
                    },
                    {
                        "id" : "540224",
                        "name" : "萨迦",
                        "pinyin" : "SaJia"
                    },
                    {
                        "id" : "540225",
                        "name" : "拉孜",
                        "pinyin" : "LaZi"
                    },
                    {
                        "id" : "540226",
                        "name" : "昂仁",
                        "pinyin" : "AngRen"
                    },
                    {
                        "id" : "540227",
                        "name" : "谢通门",
                        "pinyin" : "XieTongMen"
                    },
                    {
                        "id" : "540228",
                        "name" : "白朗",
                        "pinyin" : "BaiLang"
                    },
                    {
                        "id" : "540229",
                        "name" : "仁布",
                        "pinyin" : "RenBu"
                    },
                    {
                        "id" : "540230",
                        "name" : "康马",
                        "pinyin" : "KangMa"
                    },
                    {
                        "id" : "540231",
                        "name" : "定结",
                        "pinyin" : "DingJie"
                    },
                    {
                        "id" : "540232",
                        "name" : "仲巴",
                        "pinyin" : "ZhongBa"
                    },
                    {
                        "id" : "540233",
                        "name" : "亚东",
                        "pinyin" : "YaDong"
                    },
                    {
                        "id" : "540234",
                        "name" : "吉隆",
                        "pinyin" : "JiLong"
                    },
                    {
                        "id" : "540235",
                        "name" : "聂拉木",
                        "pinyin" : "NieLaMu"
                    },
                    {
                        "id" : "540236",
                        "name" : "萨嘎",
                        "pinyin" : "SaGa"
                    },
                    {
                        "id" : "540237",
                        "name" : "岗巴",
                        "pinyin" : "GangBa"
                    }
                ],
                "name" : "日喀则",
                "pinyin" : "RiKaZe"
            },
            {
                "id" : 540300,
                "list" : [
                    {
                        "id" : "540302",
                        "name" : "卡若",
                        "pinyin" : "KaRuo"
                    },
                    {
                        "id" : "540321",
                        "name" : "江达",
                        "pinyin" : "JiangDa"
                    },
                    {
                        "id" : "540322",
                        "name" : "贡觉",
                        "pinyin" : "GongJue"
                    },
                    {
                        "id" : "540323",
                        "name" : "类乌齐",
                        "pinyin" : "LeiWuQi"
                    },
                    {
                        "id" : "540324",
                        "name" : "丁青",
                        "pinyin" : "DingQing"
                    },
                    {
                        "id" : "540325",
                        "name" : "察雅",
                        "pinyin" : "ChaYa"
                    },
                    {
                        "id" : "540326",
                        "name" : "八宿",
                        "pinyin" : "BaSu"
                    },
                    {
                        "id" : "540327",
                        "name" : "左贡",
                        "pinyin" : "ZuoGong"
                    },
                    {
                        "id" : "540328",
                        "name" : "芒康",
                        "pinyin" : "MangKang"
                    },
                    {
                        "id" : "540329",
                        "name" : "洛隆",
                        "pinyin" : "LuoLong"
                    },
                    {
                        "id" : "540330",
                        "name" : "边坝",
                        "pinyin" : "BianBa"
                    }
                ],
                "name" : "昌都",
                "pinyin" : "ChangDu"
            },
            {
                "id" : 540400,
                "list" : [
                    {
                        "id" : "540402",
                        "name" : "巴宜",
                        "pinyin" : "BaYi"
                    },
                    {
                        "id" : "540421",
                        "name" : "工布江达",
                        "pinyin" : "GongBuJiangDa"
                    },
                    {
                        "id" : "540422",
                        "name" : "米林",
                        "pinyin" : "MiLin"
                    },
                    {
                        "id" : "540423",
                        "name" : "墨脱",
                        "pinyin" : "MoTuo"
                    },
                    {
                        "id" : "540424",
                        "name" : "波密",
                        "pinyin" : "BoMi"
                    },
                    {
                        "id" : "540425",
                        "name" : "察隅",
                        "pinyin" : "ChaYu"
                    },
                    {
                        "id" : "540426",
                        "name" : "朗县",
                        "pinyin" : "Lang"
                    }
                ],
                "name" : "林芝",
                "pinyin" : "LinZhi"
            },
            {
                "id" : 540500,
                "list" : [
                    {
                        "id" : "540502",
                        "name" : "乃东",
                        "pinyin" : "NaiDong"
                    },
                    {
                        "id" : "540521",
                        "name" : "扎囊",
                        "pinyin" : "ZhaNan"
                    },
                    {
                        "id" : "540522",
                        "name" : "贡嘎",
                        "pinyin" : "GongGa"
                    },
                    {
                        "id" : "540523",
                        "name" : "桑日",
                        "pinyin" : "SangRi"
                    },
                    {
                        "id" : "540524",
                        "name" : "琼结",
                        "pinyin" : "QiongJie"
                    },
                    {
                        "id" : "540525",
                        "name" : "曲松",
                        "pinyin" : "QuSong"
                    },
                    {
                        "id" : "540526",
                        "name" : "措美",
                        "pinyin" : "CuoMei"
                    },
                    {
                        "id" : "540527",
                        "name" : "洛扎",
                        "pinyin" : "LuoZha"
                    },
                    {
                        "id" : "540528",
                        "name" : "加查",
                        "pinyin" : "JiaCha"
                    },
                    {
                        "id" : "540529",
                        "name" : "隆子",
                        "pinyin" : "LongZi"
                    },
                    {
                        "id" : "540530",
                        "name" : "错那",
                        "pinyin" : "CuoNa"
                    },
                    {
                        "id" : "540531",
                        "name" : "浪卡子",
                        "pinyin" : "LangKaZi"
                    }
                ],
                "name" : "山南",
                "pinyin" : "ShanNan"
            },
            {
                "id" : 540600,
                "list" : [
                    {
                        "id" : "540602",
                        "name" : "色尼",
                        "pinyin" : "SeNi"
                    },
                    {
                        "id" : "540621",
                        "name" : "嘉黎",
                        "pinyin" : "JiaLi"
                    },
                    {
                        "id" : "540622",
                        "name" : "比如",
                        "pinyin" : "BiRu"
                    },
                    {
                        "id" : "540623",
                        "name" : "聂荣",
                        "pinyin" : "NieRong"
                    },
                    {
                        "id" : "540624",
                        "name" : "安多",
                        "pinyin" : "AnDuo"
                    },
                    {
                        "id" : "540625",
                        "name" : "申扎",
                        "pinyin" : "ShenZha"
                    },
                    {
                        "id" : "540626",
                        "name" : "索县",
                        "pinyin" : "SuoXian"
                    },
                    {
                        "id" : "540627",
                        "name" : "班戈",
                        "pinyin" : "BanGe"
                    },
                    {
                        "id" : "540628",
                        "name" : "巴青",
                        "pinyin" : "BaQing"
                    },
                    {
                        "id" : "540629",
                        "name" : "尼玛",
                        "pinyin" : "NiMa"
                    },
                    {
                        "id" : "540630",
                        "name" : "双湖",
                        "pinyin" : "ShuangHu"
                    }
                ],
                "name" : "那曲",
                "pinyin" : "NaQu"
            },
            {
                "id" : 542500,
                "list" : [
                    {
                        "id" : "542521",
                        "name" : "普兰",
                        "pinyin" : "PuLan"
                    },
                    {
                        "id" : "542522",
                        "name" : "札达",
                        "pinyin" : "ZhaDa"
                    },
                    {
                        "id" : "542523",
                        "name" : "噶尔",
                        "pinyin" : "GaEr"
                    },
                    {
                        "id" : "542524",
                        "name" : "日土",
                        "pinyin" : "RiTu"
                    },
                    {
                        "id" : "542525",
                        "name" : "革吉",
                        "pinyin" : "GeJi"
                    },
                    {
                        "id" : "542526",
                        "name" : "改则",
                        "pinyin" : "GaiZe"
                    },
                    {
                        "id" : "542527",
                        "name" : "措勤",
                        "pinyin" : "CuoQin"
                    }
                ],
                "name" : "阿里地区",
                "pinyin" : "ALiDiQu"
            }
        ],
        "name" : "西藏",
        "pinyin" : "XiCang"
    },
    {
        "id" : 610000,
        "list" : [
            {
                "id" : 610100,
                "list" : [
                    {
                        "id" : "610102",
                        "name" : "新城",
                        "pinyin" : "XinCheng"
                    },
                    {
                        "id" : "610103",
                        "name" : "碑林",
                        "pinyin" : "BeiLin"
                    },
                    {
                        "id" : "610104",
                        "name" : "莲湖",
                        "pinyin" : "LianHu"
                    },
                    {
                        "id" : "610111",
                        "name" : "灞桥",
                        "pinyin" : "BaQiao"
                    },
                    {
                        "id" : "610112",
                        "name" : "未央",
                        "pinyin" : "WeiYang"
                    },
                    {
                        "id" : "610113",
                        "name" : "雁塔",
                        "pinyin" : "YanTa"
                    },
                    {
                        "id" : "610114",
                        "name" : "阎良",
                        "pinyin" : "YanLiang"
                    },
                    {
                        "id" : "610115",
                        "name" : "临潼",
                        "pinyin" : "LinTong"
                    },
                    {
                        "id" : "610116",
                        "name" : "长安",
                        "pinyin" : "ChangAn"
                    },
                    {
                        "id" : "610117",
                        "name" : "高陵",
                        "pinyin" : "GaoLing"
                    },
                    {
                        "id" : "610118",
                        "name" : "鄠邑",
                        "pinyin" : "HuYi"
                    },
                    {
                        "id" : "610122",
                        "name" : "蓝田",
                        "pinyin" : "LanTian"
                    },
                    {
                        "id" : "610124",
                        "name" : "周至",
                        "pinyin" : "ZhouZhi"
                    }
                ],
                "name" : "西安",
                "pinyin" : "XiAn"
            },
            {
                "id" : 610200,
                "list" : [
                    {
                        "id" : "610202",
                        "name" : "王益",
                        "pinyin" : "WangYi"
                    },
                    {
                        "id" : "610203",
                        "name" : "印台",
                        "pinyin" : "YinTai"
                    },
                    {
                        "id" : "610204",
                        "name" : "耀州",
                        "pinyin" : "YaoZhou"
                    },
                    {
                        "id" : "610222",
                        "name" : "宜君",
                        "pinyin" : "YiJun"
                    }
                ],
                "name" : "铜川",
                "pinyin" : "TongChuan"
            },
            {
                "id" : 610300,
                "list" : [
                    {
                        "id" : "610302",
                        "name" : "渭滨",
                        "pinyin" : "WeiBin"
                    },
                    {
                        "id" : "610303",
                        "name" : "金台",
                        "pinyin" : "JinTai"
                    },
                    {
                        "id" : "610304",
                        "name" : "陈仓",
                        "pinyin" : "ChenCang"
                    },
                    {
                        "id" : "610322",
                        "name" : "凤翔",
                        "pinyin" : "FengXiang"
                    },
                    {
                        "id" : "610323",
                        "name" : "岐山",
                        "pinyin" : "QiShan"
                    },
                    {
                        "id" : "610324",
                        "name" : "扶风",
                        "pinyin" : "FuFeng"
                    },
                    {
                        "id" : "610326",
                        "name" : "眉县",
                        "pinyin" : "MeiXian"
                    },
                    {
                        "id" : "610327",
                        "name" : "陇县",
                        "pinyin" : "LongXian"
                    },
                    {
                        "id" : "610328",
                        "name" : "千阳",
                        "pinyin" : "QianYang"
                    },
                    {
                        "id" : "610329",
                        "name" : "麟游",
                        "pinyin" : "LinYou"
                    },
                    {
                        "id" : "610330",
                        "name" : "凤县",
                        "pinyin" : "FengXian"
                    },
                    {
                        "id" : "610331",
                        "name" : "太白",
                        "pinyin" : "TaiBai"
                    }
                ],
                "name" : "宝鸡",
                "pinyin" : "BaoJi"
            },
            {
                "id" : 610400,
                "list" : [
                    {
                        "id" : "610402",
                        "name" : "秦都",
                        "pinyin" : "QinDu"
                    },
                    {
                        "id" : "610403",
                        "name" : "杨陵",
                        "pinyin" : "YangLing"
                    },
                    {
                        "id" : "610404",
                        "name" : "渭城",
                        "pinyin" : "WeiCheng"
                    },
                    {
                        "id" : "610422",
                        "name" : "三原",
                        "pinyin" : "SanYuan"
                    },
                    {
                        "id" : "610423",
                        "name" : "泾阳",
                        "pinyin" : "JingYang"
                    },
                    {
                        "id" : "610424",
                        "name" : "乾县",
                        "pinyin" : "QianXian"
                    },
                    {
                        "id" : "610425",
                        "name" : "礼泉",
                        "pinyin" : "LiQuan"
                    },
                    {
                        "id" : "610426",
                        "name" : "永寿",
                        "pinyin" : "YongShou"
                    },
                    {
                        "id" : "610428",
                        "name" : "长武",
                        "pinyin" : "ChangWu"
                    },
                    {
                        "id" : "610429",
                        "name" : "旬邑",
                        "pinyin" : "XunYi"
                    },
                    {
                        "id" : "610430",
                        "name" : "淳化",
                        "pinyin" : "ChunHua"
                    },
                    {
                        "id" : "610431",
                        "name" : "武功",
                        "pinyin" : "WuGong"
                    },
                    {
                        "id" : "610481",
                        "name" : "兴平",
                        "pinyin" : "XingPing"
                    },
                    {
                        "id" : "610482",
                        "name" : "彬州",
                        "pinyin" : "BinZhou"
                    }
                ],
                "name" : "咸阳",
                "pinyin" : "XianYang"
            },
            {
                "id" : 610500,
                "list" : [
                    {
                        "id" : "610502",
                        "name" : "临渭",
                        "pinyin" : "LinWei"
                    },
                    {
                        "id" : "610503",
                        "name" : "华州",
                        "pinyin" : "HuaZhou"
                    },
                    {
                        "id" : "610522",
                        "name" : "潼关",
                        "pinyin" : "TongGuan"
                    },
                    {
                        "id" : "610523",
                        "name" : "大荔",
                        "pinyin" : "DaLi"
                    },
                    {
                        "id" : "610524",
                        "name" : "合阳",
                        "pinyin" : "HeYang"
                    },
                    {
                        "id" : "610525",
                        "name" : "澄城",
                        "pinyin" : "ChengCheng"
                    },
                    {
                        "id" : "610526",
                        "name" : "蒲城",
                        "pinyin" : "PuCheng"
                    },
                    {
                        "id" : "610527",
                        "name" : "白水",
                        "pinyin" : "BaiShui"
                    },
                    {
                        "id" : "610528",
                        "name" : "富平",
                        "pinyin" : "FuPing"
                    },
                    {
                        "id" : "610581",
                        "name" : "韩城",
                        "pinyin" : "HanCheng"
                    },
                    {
                        "id" : "610582",
                        "name" : "华阴",
                        "pinyin" : "HuaYin"
                    }
                ],
                "name" : "渭南",
                "pinyin" : "WeiNan"
            },
            {
                "id" : 610600,
                "list" : [
                    {
                        "id" : "610602",
                        "name" : "宝塔",
                        "pinyin" : "BaoTa"
                    },
                    {
                        "id" : "610603",
                        "name" : "安塞",
                        "pinyin" : "AnSai"
                    },
                    {
                        "id" : "610621",
                        "name" : "延长",
                        "pinyin" : "YanChang"
                    },
                    {
                        "id" : "610622",
                        "name" : "延川",
                        "pinyin" : "YanChuan"
                    },
                    {
                        "id" : "610623",
                        "name" : "子长",
                        "pinyin" : "ZiChang"
                    },
                    {
                        "id" : "610625",
                        "name" : "志丹",
                        "pinyin" : "ZhiDan"
                    },
                    {
                        "id" : "610626",
                        "name" : "吴起",
                        "pinyin" : "WuQi"
                    },
                    {
                        "id" : "610627",
                        "name" : "甘泉",
                        "pinyin" : "GanQuan"
                    },
                    {
                        "id" : "610628",
                        "name" : "富县",
                        "pinyin" : "FuXian"
                    },
                    {
                        "id" : "610629",
                        "name" : "洛川",
                        "pinyin" : "LuoChuan"
                    },
                    {
                        "id" : "610630",
                        "name" : "宜川",
                        "pinyin" : "YiChuan"
                    },
                    {
                        "id" : "610631",
                        "name" : "黄龙",
                        "pinyin" : "HuangLong"
                    },
                    {
                        "id" : "610632",
                        "name" : "黄陵",
                        "pinyin" : "HuangLing"
                    }
                ],
                "name" : "延安",
                "pinyin" : "YanAn"
            },
            {
                "id" : 610700,
                "list" : [
                    {
                        "id" : "610702",
                        "name" : "汉台",
                        "pinyin" : "HanTai"
                    },
                    {
                        "id" : "610703",
                        "name" : "南郑",
                        "pinyin" : "NanZheng"
                    },
                    {
                        "id" : "610722",
                        "name" : "城固",
                        "pinyin" : "ChengGu"
                    },
                    {
                        "id" : "610723",
                        "name" : "洋县",
                        "pinyin" : "YangXian"
                    },
                    {
                        "id" : "610724",
                        "name" : "西乡",
                        "pinyin" : "XiXiang"
                    },
                    {
                        "id" : "610725",
                        "name" : "勉县",
                        "pinyin" : "MianXian"
                    },
                    {
                        "id" : "610726",
                        "name" : "宁强",
                        "pinyin" : "NingQiang"
                    },
                    {
                        "id" : "610727",
                        "name" : "略阳",
                        "pinyin" : "LueYang"
                    },
                    {
                        "id" : "610728",
                        "name" : "镇巴",
                        "pinyin" : "ZhenBa"
                    },
                    {
                        "id" : "610729",
                        "name" : "留坝",
                        "pinyin" : "LiuBa"
                    },
                    {
                        "id" : "610730",
                        "name" : "佛坪",
                        "pinyin" : "FuPing"
                    }
                ],
                "name" : "汉中",
                "pinyin" : "HanZhong"
            },
            {
                "id" : 610800,
                "list" : [
                    {
                        "id" : "610802",
                        "name" : "榆阳",
                        "pinyin" : "YuYang"
                    },
                    {
                        "id" : "610803",
                        "name" : "横山",
                        "pinyin" : "HengShan"
                    },
                    {
                        "id" : "610822",
                        "name" : "府谷",
                        "pinyin" : "FuGu"
                    },
                    {
                        "id" : "610824",
                        "name" : "靖边",
                        "pinyin" : "JingBian"
                    },
                    {
                        "id" : "610825",
                        "name" : "定边",
                        "pinyin" : "DingBian"
                    },
                    {
                        "id" : "610826",
                        "name" : "绥德",
                        "pinyin" : "SuiDe"
                    },
                    {
                        "id" : "610827",
                        "name" : "米脂",
                        "pinyin" : "MiZhi"
                    },
                    {
                        "id" : "610828",
                        "name" : "佳县",
                        "pinyin" : "JiaXian"
                    },
                    {
                        "id" : "610829",
                        "name" : "吴堡",
                        "pinyin" : "WuBao"
                    },
                    {
                        "id" : "610830",
                        "name" : "清涧",
                        "pinyin" : "QingJian"
                    },
                    {
                        "id" : "610831",
                        "name" : "子洲",
                        "pinyin" : "ZiZhou"
                    },
                    {
                        "id" : "610881",
                        "name" : "神木",
                        "pinyin" : "ShenMu"
                    }
                ],
                "name" : "榆林",
                "pinyin" : "YuLin"
            },
            {
                "id" : 610900,
                "list" : [
                    {
                        "id" : "610902",
                        "name" : "汉滨",
                        "pinyin" : "HanBin"
                    },
                    {
                        "id" : "610921",
                        "name" : "汉阴",
                        "pinyin" : "HanYin"
                    },
                    {
                        "id" : "610922",
                        "name" : "石泉",
                        "pinyin" : "ShiQuan"
                    },
                    {
                        "id" : "610923",
                        "name" : "宁陕",
                        "pinyin" : "NingShan"
                    },
                    {
                        "id" : "610924",
                        "name" : "紫阳",
                        "pinyin" : "ZiYang"
                    },
                    {
                        "id" : "610925",
                        "name" : "岚皋",
                        "pinyin" : "LanGao"
                    },
                    {
                        "id" : "610926",
                        "name" : "平利",
                        "pinyin" : "PingLi"
                    },
                    {
                        "id" : "610927",
                        "name" : "镇坪",
                        "pinyin" : "ZhenPing"
                    },
                    {
                        "id" : "610928",
                        "name" : "旬阳",
                        "pinyin" : "XunYang"
                    },
                    {
                        "id" : "610929",
                        "name" : "白河",
                        "pinyin" : "BaiHe"
                    }
                ],
                "name" : "安康",
                "pinyin" : "AnKang"
            },
            {
                "id" : 611000,
                "list" : [
                    {
                        "id" : "611002",
                        "name" : "商州",
                        "pinyin" : "ShangZhou"
                    },
                    {
                        "id" : "611021",
                        "name" : "洛南",
                        "pinyin" : "LuoNan"
                    },
                    {
                        "id" : "611022",
                        "name" : "丹凤",
                        "pinyin" : "DanFeng"
                    },
                    {
                        "id" : "611023",
                        "name" : "商南",
                        "pinyin" : "ShangNan"
                    },
                    {
                        "id" : "611024",
                        "name" : "山阳",
                        "pinyin" : "ShanYang"
                    },
                    {
                        "id" : "611025",
                        "name" : "镇安",
                        "pinyin" : "ZhenAn"
                    },
                    {
                        "id" : "611026",
                        "name" : "柞水",
                        "pinyin" : "ZuoShui"
                    }
                ],
                "name" : "商洛",
                "pinyin" : "ShangLuo"
            }
        ],
        "name" : "陕西",
        "pinyin" : "ShanXi"
    },
    {
        "id" : 620000,
        "list" : [
            {
                "id" : 620100,
                "list" : [
                    {
                        "id" : "620102",
                        "name" : "城关",
                        "pinyin" : "ChengGuan"
                    },
                    {
                        "id" : "620103",
                        "name" : "七里河",
                        "pinyin" : "QiLiHe"
                    },
                    {
                        "id" : "620104",
                        "name" : "西固",
                        "pinyin" : "XiGu"
                    },
                    {
                        "id" : "620105",
                        "name" : "安宁",
                        "pinyin" : "AnNing"
                    },
                    {
                        "id" : "620111",
                        "name" : "红古",
                        "pinyin" : "HongGu"
                    },
                    {
                        "id" : "620121",
                        "name" : "永登",
                        "pinyin" : "YongDeng"
                    },
                    {
                        "id" : "620122",
                        "name" : "皋兰",
                        "pinyin" : "GaoLan"
                    },
                    {
                        "id" : "620123",
                        "name" : "榆中",
                        "pinyin" : "YuZhong"
                    }
                ],
                "name" : "兰州",
                "pinyin" : "LanZhou"
            },
            {
                "id" : 620200,
                "list" : [
                    {
                        "id" : "620200",
                        "name" : "嘉峪关",
                        "pinyin" : "JiaYuGuan"
                    }
                ],
                "name" : "嘉峪关",
                "pinyin" : "JiaYuGuan"
            },
            {
                "id" : 620300,
                "list" : [
                    {
                        "id" : "620302",
                        "name" : "金川",
                        "pinyin" : "JinChuan"
                    },
                    {
                        "id" : "620321",
                        "name" : "永昌",
                        "pinyin" : "YongChang"
                    }
                ],
                "name" : "金昌",
                "pinyin" : "JinChang"
            },
            {
                "id" : 620400,
                "list" : [
                    {
                        "id" : "620402",
                        "name" : "白银",
                        "pinyin" : "BaiYin"
                    },
                    {
                        "id" : "620403",
                        "name" : "平川",
                        "pinyin" : "PingChuan"
                    },
                    {
                        "id" : "620421",
                        "name" : "靖远",
                        "pinyin" : "JingYuan"
                    },
                    {
                        "id" : "620422",
                        "name" : "会宁",
                        "pinyin" : "HuiNing"
                    },
                    {
                        "id" : "620423",
                        "name" : "景泰",
                        "pinyin" : "JingTai"
                    }
                ],
                "name" : "白银",
                "pinyin" : "BaiYin"
            },
            {
                "id" : 620500,
                "list" : [
                    {
                        "id" : "620502",
                        "name" : "秦州",
                        "pinyin" : "QinZhou"
                    },
                    {
                        "id" : "620503",
                        "name" : "麦积",
                        "pinyin" : "MaiJi"
                    },
                    {
                        "id" : "620521",
                        "name" : "清水",
                        "pinyin" : "QingShui"
                    },
                    {
                        "id" : "620522",
                        "name" : "秦安",
                        "pinyin" : "QinAn"
                    },
                    {
                        "id" : "620523",
                        "name" : "甘谷",
                        "pinyin" : "GanGu"
                    },
                    {
                        "id" : "620524",
                        "name" : "武山",
                        "pinyin" : "WuShan"
                    },
                    {
                        "id" : "620525",
                        "name" : "张家川",
                        "pinyin" : "ZhangJiaChuan"
                    }
                ],
                "name" : "天水",
                "pinyin" : "TianShui"
            },
            {
                "id" : 620600,
                "list" : [
                    {
                        "id" : "620602",
                        "name" : "凉州",
                        "pinyin" : "LiangZhou"
                    },
                    {
                        "id" : "620621",
                        "name" : "民勤",
                        "pinyin" : "MinQin"
                    },
                    {
                        "id" : "620622",
                        "name" : "古浪",
                        "pinyin" : "GuLang"
                    },
                    {
                        "id" : "620623",
                        "name" : "天祝",
                        "pinyin" : "TianZhu"
                    }
                ],
                "name" : "武威",
                "pinyin" : "WuWei"
            },
            {
                "id" : 620700,
                "list" : [
                    {
                        "id" : "620702",
                        "name" : "甘州",
                        "pinyin" : "GanZhou"
                    },
                    {
                        "id" : "620721",
                        "name" : "肃南",
                        "pinyin" : "SuNan"
                    },
                    {
                        "id" : "620722",
                        "name" : "民乐",
                        "pinyin" : "MinLe"
                    },
                    {
                        "id" : "620723",
                        "name" : "临泽",
                        "pinyin" : "LinZe"
                    },
                    {
                        "id" : "620724",
                        "name" : "高台",
                        "pinyin" : "GaoTai"
                    },
                    {
                        "id" : "620725",
                        "name" : "山丹",
                        "pinyin" : "ShanDan"
                    }
                ],
                "name" : "张掖",
                "pinyin" : "ZhangYe"
            },
            {
                "id" : 620800,
                "list" : [
                    {
                        "id" : "620802",
                        "name" : "崆峒",
                        "pinyin" : "KongDong"
                    },
                    {
                        "id" : "620821",
                        "name" : "泾川",
                        "pinyin" : "JingChuan"
                    },
                    {
                        "id" : "620822",
                        "name" : "灵台",
                        "pinyin" : "LingTai"
                    },
                    {
                        "id" : "620823",
                        "name" : "崇信",
                        "pinyin" : "ChongXin"
                    },
                    {
                        "id" : "620825",
                        "name" : "庄浪",
                        "pinyin" : "ZhuangLang"
                    },
                    {
                        "id" : "620826",
                        "name" : "静宁",
                        "pinyin" : "JingNing"
                    },
                    {
                        "id" : "620881",
                        "name" : "华亭",
                        "pinyin" : "HuaTing"
                    }
                ],
                "name" : "平凉",
                "pinyin" : "PingLiang"
            },
            {
                "id" : 620900,
                "list" : [
                    {
                        "id" : "620902",
                        "name" : "肃州",
                        "pinyin" : "SuZhou"
                    },
                    {
                        "id" : "620921",
                        "name" : "金塔",
                        "pinyin" : "JinTa"
                    },
                    {
                        "id" : "620922",
                        "name" : "瓜州",
                        "pinyin" : "GuaZhou"
                    },
                    {
                        "id" : "620923",
                        "name" : "肃北",
                        "pinyin" : "SuBei"
                    },
                    {
                        "id" : "620924",
                        "name" : "阿克塞",
                        "pinyin" : "AKeSai"
                    },
                    {
                        "id" : "620981",
                        "name" : "玉门",
                        "pinyin" : "YuMen"
                    },
                    {
                        "id" : "620982",
                        "name" : "敦煌",
                        "pinyin" : "DunHuang"
                    }
                ],
                "name" : "酒泉",
                "pinyin" : "JiuQuan"
            },
            {
                "id" : 621000,
                "list" : [
                    {
                        "id" : "621002",
                        "name" : "西峰",
                        "pinyin" : "XiFeng"
                    },
                    {
                        "id" : "621021",
                        "name" : "庆城",
                        "pinyin" : "QingCheng"
                    },
                    {
                        "id" : "621022",
                        "name" : "环县",
                        "pinyin" : "HuanXian"
                    },
                    {
                        "id" : "621023",
                        "name" : "华池",
                        "pinyin" : "HuaChi"
                    },
                    {
                        "id" : "621024",
                        "name" : "合水",
                        "pinyin" : "HeShui"
                    },
                    {
                        "id" : "621025",
                        "name" : "正宁",
                        "pinyin" : "ZhengNing"
                    },
                    {
                        "id" : "621026",
                        "name" : "宁县",
                        "pinyin" : "NingXian"
                    },
                    {
                        "id" : "621027",
                        "name" : "镇原",
                        "pinyin" : "ZhenYuan"
                    }
                ],
                "name" : "庆阳",
                "pinyin" : "QingYang"
            },
            {
                "id" : 621100,
                "list" : [
                    {
                        "id" : "621102",
                        "name" : "安定",
                        "pinyin" : "AnDing"
                    },
                    {
                        "id" : "621121",
                        "name" : "通渭",
                        "pinyin" : "TongWei"
                    },
                    {
                        "id" : "621122",
                        "name" : "陇西",
                        "pinyin" : "LongXi"
                    },
                    {
                        "id" : "621123",
                        "name" : "渭源",
                        "pinyin" : "WeiYuan"
                    },
                    {
                        "id" : "621124",
                        "name" : "临洮",
                        "pinyin" : "LinDao"
                    },
                    {
                        "id" : "621125",
                        "name" : "漳县",
                        "pinyin" : "ZhangXian"
                    },
                    {
                        "id" : "621126",
                        "name" : "岷县",
                        "pinyin" : "MinXian"
                    }
                ],
                "name" : "定西",
                "pinyin" : "DingXi"
            },
            {
                "id" : 621200,
                "list" : [
                    {
                        "id" : "621202",
                        "name" : "武都",
                        "pinyin" : "WuDu"
                    },
                    {
                        "id" : "621221",
                        "name" : "成县",
                        "pinyin" : "ChengXian"
                    },
                    {
                        "id" : "621222",
                        "name" : "文县",
                        "pinyin" : "WenXian"
                    },
                    {
                        "id" : "621223",
                        "name" : "宕昌",
                        "pinyin" : "DangChang"
                    },
                    {
                        "id" : "621224",
                        "name" : "康县",
                        "pinyin" : "KangXian"
                    },
                    {
                        "id" : "621225",
                        "name" : "西和",
                        "pinyin" : "XiHe"
                    },
                    {
                        "id" : "621226",
                        "name" : "礼县",
                        "pinyin" : "LiXian"
                    },
                    {
                        "id" : "621227",
                        "name" : "徽县",
                        "pinyin" : "HuiXian"
                    },
                    {
                        "id" : "621228",
                        "name" : "两当",
                        "pinyin" : "LiangDang"
                    }
                ],
                "name" : "陇南",
                "pinyin" : "LongNan"
            },
            {
                "id" : 622900,
                "list" : [
                    {
                        "id" : "622901",
                        "name" : "临夏",
                        "pinyin" : "LinXia"
                    },
                    {
                        "id" : "622922",
                        "name" : "康乐",
                        "pinyin" : "KangLe"
                    },
                    {
                        "id" : "622923",
                        "name" : "永靖",
                        "pinyin" : "YongJing"
                    },
                    {
                        "id" : "622924",
                        "name" : "广河",
                        "pinyin" : "GuangHe"
                    },
                    {
                        "id" : "622925",
                        "name" : "和政",
                        "pinyin" : "HeZheng"
                    },
                    {
                        "id" : "622926",
                        "name" : "东乡族",
                        "pinyin" : "DongXiangZu"
                    },
                    {
                        "id" : "622927",
                        "name" : "积石山",
                        "pinyin" : "JiShiShan"
                    }
                ],
                "name" : "临夏",
                "pinyin" : "LinXia"
            },
            {
                "id" : 623000,
                "list" : [
                    {
                        "id" : "623001",
                        "name" : "合作",
                        "pinyin" : "HeZuo"
                    },
                    {
                        "id" : "623021",
                        "name" : "临潭",
                        "pinyin" : "LinTan"
                    },
                    {
                        "id" : "623022",
                        "name" : "卓尼",
                        "pinyin" : "ZhuoNi"
                    },
                    {
                        "id" : "623023",
                        "name" : "舟曲",
                        "pinyin" : "ZhouQu"
                    },
                    {
                        "id" : "623024",
                        "name" : "迭部",
                        "pinyin" : "DieBu"
                    },
                    {
                        "id" : "623025",
                        "name" : "玛曲",
                        "pinyin" : "MaQu"
                    },
                    {
                        "id" : "623026",
                        "name" : "碌曲",
                        "pinyin" : "LuQu"
                    },
                    {
                        "id" : "623027",
                        "name" : "夏河",
                        "pinyin" : "XiaHe"
                    }
                ],
                "name" : "甘南",
                "pinyin" : "GanNan"
            }
        ],
        "name" : "甘肃",
        "pinyin" : "GanSu"
    },
    {
        "id" : 630000,
        "list" : [
            {
                "id" : 630100,
                "list" : [
                    {
                        "id" : "630102",
                        "name" : "城东",
                        "pinyin" : "ChengDong"
                    },
                    {
                        "id" : "630103",
                        "name" : "城中",
                        "pinyin" : "ChengZhong"
                    },
                    {
                        "id" : "630104",
                        "name" : "城西",
                        "pinyin" : "ChengXi"
                    },
                    {
                        "id" : "630105",
                        "name" : "城北",
                        "pinyin" : "ChengBei"
                    },
                    {
                        "id" : "630121",
                        "name" : "大通",
                        "pinyin" : "DaTong"
                    },
                    {
                        "id" : "630122",
                        "name" : "湟中",
                        "pinyin" : "HuangZhong"
                    },
                    {
                        "id" : "630123",
                        "name" : "湟源",
                        "pinyin" : "HuangYuan"
                    }
                ],
                "name" : "西宁",
                "pinyin" : "XiNing"
            },
            {
                "id" : 630200,
                "list" : [
                    {
                        "id" : "630202",
                        "name" : "乐都",
                        "pinyin" : "LeDu"
                    },
                    {
                        "id" : "630203",
                        "name" : "平安",
                        "pinyin" : "PingAn"
                    },
                    {
                        "id" : "630222",
                        "name" : "民和",
                        "pinyin" : "MinHe"
                    },
                    {
                        "id" : "630223",
                        "name" : "互助",
                        "pinyin" : "HuZhu"
                    },
                    {
                        "id" : "630224",
                        "name" : "化隆",
                        "pinyin" : "HuaLong"
                    },
                    {
                        "id" : "630225",
                        "name" : "循化",
                        "pinyin" : "XunHua"
                    }
                ],
                "name" : "海东",
                "pinyin" : "HaiDong"
            },
            {
                "id" : 632200,
                "list" : [
                    {
                        "id" : "632221",
                        "name" : "门源",
                        "pinyin" : "MenYuan"
                    },
                    {
                        "id" : "632222",
                        "name" : "祁连",
                        "pinyin" : "QiLian"
                    },
                    {
                        "id" : "632223",
                        "name" : "海晏",
                        "pinyin" : "HaiYan"
                    },
                    {
                        "id" : "632224",
                        "name" : "刚察",
                        "pinyin" : "GangCha"
                    }
                ],
                "name" : "海北",
                "pinyin" : "HaiBei"
            },
            {
                "id" : 632300,
                "list" : [
                    {
                        "id" : "632321",
                        "name" : "同仁",
                        "pinyin" : "TongRen"
                    },
                    {
                        "id" : "632322",
                        "name" : "尖扎",
                        "pinyin" : "JianZha"
                    },
                    {
                        "id" : "632323",
                        "name" : "泽库",
                        "pinyin" : "ZeKu"
                    },
                    {
                        "id" : "632324",
                        "name" : "河南",
                        "pinyin" : "HeNan"
                    }
                ],
                "name" : "黄南",
                "pinyin" : "HuangNan"
            },
            {
                "id" : 632500,
                "list" : [
                    {
                        "id" : "632521",
                        "name" : "共和",
                        "pinyin" : "GongHe"
                    },
                    {
                        "id" : "632522",
                        "name" : "同德",
                        "pinyin" : "TongDe"
                    },
                    {
                        "id" : "632523",
                        "name" : "贵德",
                        "pinyin" : "GuiDe"
                    },
                    {
                        "id" : "632524",
                        "name" : "兴海",
                        "pinyin" : "XingHai"
                    },
                    {
                        "id" : "632525",
                        "name" : "贵南",
                        "pinyin" : "GuiNan"
                    }
                ],
                "name" : "海南",
                "pinyin" : "HaiNan"
            },
            {
                "id" : 632600,
                "list" : [
                    {
                        "id" : "632621",
                        "name" : "玛沁",
                        "pinyin" : "MaQin"
                    },
                    {
                        "id" : "632622",
                        "name" : "班玛",
                        "pinyin" : "BanMa"
                    },
                    {
                        "id" : "632623",
                        "name" : "甘德",
                        "pinyin" : "GanDe"
                    },
                    {
                        "id" : "632624",
                        "name" : "达日",
                        "pinyin" : "DaRi"
                    },
                    {
                        "id" : "632625",
                        "name" : "久治",
                        "pinyin" : "JiuZhi"
                    },
                    {
                        "id" : "632626",
                        "name" : "玛多",
                        "pinyin" : "MaDuo"
                    }
                ],
                "name" : "果洛",
                "pinyin" : "GuoLuo"
            },
            {
                "id" : 632700,
                "list" : [
                    {
                        "id" : "632701",
                        "name" : "玉树",
                        "pinyin" : "YuShu"
                    },
                    {
                        "id" : "632722",
                        "name" : "杂多",
                        "pinyin" : "ZaDuo"
                    },
                    {
                        "id" : "632723",
                        "name" : "称多",
                        "pinyin" : "ChenDuo"
                    },
                    {
                        "id" : "632724",
                        "name" : "治多",
                        "pinyin" : "ZhiDuo"
                    },
                    {
                        "id" : "632725",
                        "name" : "囊谦",
                        "pinyin" : "NanQian"
                    },
                    {
                        "id" : "632726",
                        "name" : "曲麻莱",
                        "pinyin" : "QuMaLai"
                    }
                ],
                "name" : "玉树",
                "pinyin" : "YuShu"
            },
            {
                "id" : 632800,
                "list" : [
                    {
                        "id" : "632801",
                        "name" : "格尔木",
                        "pinyin" : "GeErMu"
                    },
                    {
                        "id" : "632802",
                        "name" : "德令哈",
                        "pinyin" : "DeLingHa"
                    },
                    {
                        "id" : "632803",
                        "name" : "茫崖",
                        "pinyin" : "MangYa"
                    },
                    {
                        "id" : "632821",
                        "name" : "乌兰",
                        "pinyin" : "WuLan"
                    },
                    {
                        "id" : "632822",
                        "name" : "都兰",
                        "pinyin" : "DuLan"
                    },
                    {
                        "id" : "632823",
                        "name" : "天峻",
                        "pinyin" : "TianJun"
                    },
                    {
                        "id" : "632824",
                        "name" : "海西",
                        "pinyin" : "HaiXi"
                    }
                ],
                "name" : "海西",
                "pinyin" : "HaiXi"
            }
        ],
        "name" : "青海",
        "pinyin" : "QingHai"
    },
    {
        "id" : 640000,
        "list" : [
            {
                "id" : 640100,
                "list" : [
                    {
                        "id" : "640104",
                        "name" : "兴庆",
                        "pinyin" : "XingQing"
                    },
                    {
                        "id" : "640105",
                        "name" : "西夏",
                        "pinyin" : "XiXia"
                    },
                    {
                        "id" : "640106",
                        "name" : "金凤",
                        "pinyin" : "JinFeng"
                    },
                    {
                        "id" : "640121",
                        "name" : "永宁",
                        "pinyin" : "YongNing"
                    },
                    {
                        "id" : "640122",
                        "name" : "贺兰",
                        "pinyin" : "HeLan"
                    },
                    {
                        "id" : "640181",
                        "name" : "灵武",
                        "pinyin" : "LingWu"
                    }
                ],
                "name" : "银川",
                "pinyin" : "YinChuan"
            },
            {
                "id" : 640200,
                "list" : [
                    {
                        "id" : "640202",
                        "name" : "大武口",
                        "pinyin" : "DaWuKou"
                    },
                    {
                        "id" : "640205",
                        "name" : "惠农",
                        "pinyin" : "HuiNong"
                    },
                    {
                        "id" : "640221",
                        "name" : "平罗",
                        "pinyin" : "PingLuo"
                    }
                ],
                "name" : "石嘴山",
                "pinyin" : "ShiZuiShan"
            },
            {
                "id" : 640300,
                "list" : [
                    {
                        "id" : "640302",
                        "name" : "利通",
                        "pinyin" : "LiTong"
                    },
                    {
                        "id" : "640303",
                        "name" : "红寺堡",
                        "pinyin" : "HongSiBao"
                    },
                    {
                        "id" : "640323",
                        "name" : "盐池",
                        "pinyin" : "YanChi"
                    },
                    {
                        "id" : "640324",
                        "name" : "同心",
                        "pinyin" : "TongXin"
                    },
                    {
                        "id" : "640381",
                        "name" : "青铜峡",
                        "pinyin" : "QingTongXia"
                    }
                ],
                "name" : "吴忠",
                "pinyin" : "WuZhong"
            },
            {
                "id" : 640400,
                "list" : [
                    {
                        "id" : "640402",
                        "name" : "原州",
                        "pinyin" : "YuanZhou"
                    },
                    {
                        "id" : "640422",
                        "name" : "西吉",
                        "pinyin" : "XiJi"
                    },
                    {
                        "id" : "640423",
                        "name" : "隆德",
                        "pinyin" : "LongDe"
                    },
                    {
                        "id" : "640424",
                        "name" : "泾源",
                        "pinyin" : "JingYuan"
                    },
                    {
                        "id" : "640425",
                        "name" : "彭阳",
                        "pinyin" : "PengYang"
                    }
                ],
                "name" : "固原",
                "pinyin" : "GuYuan"
            },
            {
                "id" : 640500,
                "list" : [
                    {
                        "id" : "640502",
                        "name" : "沙坡头",
                        "pinyin" : "ShaPoTou"
                    },
                    {
                        "id" : "640521",
                        "name" : "中宁",
                        "pinyin" : "ZhongNing"
                    },
                    {
                        "id" : "640522",
                        "name" : "海原",
                        "pinyin" : "HaiYuan"
                    }
                ],
                "name" : "中卫",
                "pinyin" : "ZhongWei"
            }
        ],
        "name" : "宁夏",
        "pinyin" : "NingXia"
    },
    {
        "id" : 650000,
        "list" : [
            {
                "id" : 650100,
                "list" : [
                    {
                        "id" : "650102",
                        "name" : "天山",
                        "pinyin" : "TianShan"
                    },
                    {
                        "id" : "650103",
                        "name" : "沙依巴克",
                        "pinyin" : "ShaYiBaKe"
                    },
                    {
                        "id" : "650104",
                        "name" : "新市",
                        "pinyin" : "XinShi"
                    },
                    {
                        "id" : "650105",
                        "name" : "水磨沟",
                        "pinyin" : "ShuiMoGou"
                    },
                    {
                        "id" : "650106",
                        "name" : "头屯河",
                        "pinyin" : "TouTunHe"
                    },
                    {
                        "id" : "650107",
                        "name" : "达坂城",
                        "pinyin" : "DaBanCheng"
                    },
                    {
                        "id" : "650109",
                        "name" : "米东",
                        "pinyin" : "MiDong"
                    },
                    {
                        "id" : "650121",
                        "name" : "乌鲁木齐",
                        "pinyin" : "WuLuMuQi"
                    }
                ],
                "name" : "乌鲁木齐",
                "pinyin" : "WuLuMuQi"
            },
            {
                "id" : 650200,
                "list" : [
                    {
                        "id" : "650202",
                        "name" : "独山子",
                        "pinyin" : "DuShanZi"
                    },
                    {
                        "id" : "650203",
                        "name" : "克拉玛依",
                        "pinyin" : "KeLaMaYi"
                    },
                    {
                        "id" : "650204",
                        "name" : "白碱滩",
                        "pinyin" : "BaiJianTan"
                    },
                    {
                        "id" : "650205",
                        "name" : "乌尔禾",
                        "pinyin" : "WuErHe"
                    }
                ],
                "name" : "克拉玛依",
                "pinyin" : "KeLaMaYi"
            },
            {
                "id" : 650400,
                "list" : [
                    {
                        "id" : "650402",
                        "name" : "高昌",
                        "pinyin" : "GaoChang"
                    },
                    {
                        "id" : "650421",
                        "name" : "鄯善",
                        "pinyin" : "ShanShan"
                    },
                    {
                        "id" : "650422",
                        "name" : "托克逊",
                        "pinyin" : "TuoKeXun"
                    }
                ],
                "name" : "吐鲁番",
                "pinyin" : "TuLuFan"
            },
            {
                "id" : 650500,
                "list" : [
                    {
                        "id" : "650502",
                        "name" : "伊州",
                        "pinyin" : "YiZhou"
                    },
                    {
                        "id" : "650521",
                        "name" : "巴里坤",
                        "pinyin" : "BaLiKun"
                    },
                    {
                        "id" : "650522",
                        "name" : "伊吾",
                        "pinyin" : "YiWu"
                    }
                ],
                "name" : "哈密",
                "pinyin" : "HaMi"
            },
            {
                "id" : 652300,
                "list" : [
                    {
                        "id" : "652301",
                        "name" : "昌吉",
                        "pinyin" : "ChangJi"
                    },
                    {
                        "id" : "652302",
                        "name" : "阜康",
                        "pinyin" : "FuKang"
                    },
                    {
                        "id" : "652323",
                        "name" : "呼图壁",
                        "pinyin" : "HuTuBi"
                    },
                    {
                        "id" : "652324",
                        "name" : "玛纳斯",
                        "pinyin" : "MaNaSi"
                    },
                    {
                        "id" : "652325",
                        "name" : "奇台",
                        "pinyin" : "QiTai"
                    },
                    {
                        "id" : "652327",
                        "name" : "吉木萨尔",
                        "pinyin" : "JiMuSaEr"
                    },
                    {
                        "id" : "652328",
                        "name" : "木垒",
                        "pinyin" : "MuLei"
                    }
                ],
                "name" : "昌吉",
                "pinyin" : "ChangJi"
            },
            {
                "id" : 652700,
                "list" : [
                    {
                        "id" : "652701",
                        "name" : "博乐",
                        "pinyin" : "BoLe"
                    },
                    {
                        "id" : "652702",
                        "name" : "阿拉山口",
                        "pinyin" : "ALaShanKou"
                    },
                    {
                        "id" : "652722",
                        "name" : "精河",
                        "pinyin" : "JingHe"
                    },
                    {
                        "id" : "652723",
                        "name" : "温泉",
                        "pinyin" : "WenQuan"
                    }
                ],
                "name" : "博尔塔拉",
                "pinyin" : "BoErTaLa"
            },
            {
                "id" : 652800,
                "list" : [
                    {
                        "id" : "652801",
                        "name" : "库尔勒",
                        "pinyin" : "KuErLe"
                    },
                    {
                        "id" : "652822",
                        "name" : "轮台",
                        "pinyin" : "LunTai"
                    },
                    {
                        "id" : "652823",
                        "name" : "尉犁",
                        "pinyin" : "YuLi"
                    },
                    {
                        "id" : "652824",
                        "name" : "若羌",
                        "pinyin" : "RuoQiang"
                    },
                    {
                        "id" : "652825",
                        "name" : "且末",
                        "pinyin" : "QieMo"
                    },
                    {
                        "id" : "652826",
                        "name" : "焉耆",
                        "pinyin" : "YanShi"
                    },
                    {
                        "id" : "652827",
                        "name" : "和静",
                        "pinyin" : "HeJing"
                    },
                    {
                        "id" : "652828",
                        "name" : "和硕",
                        "pinyin" : "HeShuo"
                    },
                    {
                        "id" : "652829",
                        "name" : "博湖",
                        "pinyin" : "BoHu"
                    }
                ],
                "name" : "巴音郭楞",
                "pinyin" : "BaYinGuoLeng"
            },
            {
                "id" : 652900,
                "list" : [
                    {
                        "id" : "652901",
                        "name" : "阿克苏市",
                        "pinyin" : "AKeSuShi"
                    },
                    {
                        "id" : "652922",
                        "name" : "温宿",
                        "pinyin" : "WenSu"
                    },
                    {
                        "id" : "652923",
                        "name" : "库车",
                        "pinyin" : "KuChe"
                    },
                    {
                        "id" : "652924",
                        "name" : "沙雅",
                        "pinyin" : "ShaYa"
                    },
                    {
                        "id" : "652925",
                        "name" : "新和",
                        "pinyin" : "XinHe"
                    },
                    {
                        "id" : "652926",
                        "name" : "拜城",
                        "pinyin" : "BaiCheng"
                    },
                    {
                        "id" : "652927",
                        "name" : "乌什",
                        "pinyin" : "WuShi"
                    },
                    {
                        "id" : "652928",
                        "name" : "阿瓦提",
                        "pinyin" : "AWaTi"
                    },
                    {
                        "id" : "652929",
                        "name" : "柯坪",
                        "pinyin" : "KePing"
                    }
                ],
                "name" : "阿克苏",
                "pinyin" : "AKeSu"
            },
            {
                "id" : 653000,
                "list" : [
                    {
                        "id" : "653001",
                        "name" : "阿图什",
                        "pinyin" : "ATuShi"
                    },
                    {
                        "id" : "653022",
                        "name" : "阿克陶",
                        "pinyin" : "AKeTao"
                    },
                    {
                        "id" : "653023",
                        "name" : "阿合奇",
                        "pinyin" : "AHeQi"
                    },
                    {
                        "id" : "653024",
                        "name" : "乌恰",
                        "pinyin" : "WuQia"
                    }
                ],
                "name" : "克孜勒苏",
                "pinyin" : "KeZiLeSu"
            },
            {
                "id" : 653100,
                "list" : [
                    {
                        "id" : "653101",
                        "name" : "喀什市",
                        "pinyin" : "KaShiShi"
                    },
                    {
                        "id" : "653121",
                        "name" : "疏附",
                        "pinyin" : "ShuFu"
                    },
                    {
                        "id" : "653122",
                        "name" : "疏勒",
                        "pinyin" : "ShuLe"
                    },
                    {
                        "id" : "653123",
                        "name" : "英吉沙",
                        "pinyin" : "YingJiSha"
                    },
                    {
                        "id" : "653124",
                        "name" : "泽普",
                        "pinyin" : "ZePu"
                    },
                    {
                        "id" : "653125",
                        "name" : "莎车",
                        "pinyin" : "ShaChe"
                    },
                    {
                        "id" : "653126",
                        "name" : "叶城",
                        "pinyin" : "YeCheng"
                    },
                    {
                        "id" : "653127",
                        "name" : "麦盖提",
                        "pinyin" : "MaiGaiTi"
                    },
                    {
                        "id" : "653128",
                        "name" : "岳普湖",
                        "pinyin" : "YuePuHu"
                    },
                    {
                        "id" : "653129",
                        "name" : "伽师",
                        "pinyin" : "JiaShi"
                    },
                    {
                        "id" : "653130",
                        "name" : "巴楚",
                        "pinyin" : "BaChu"
                    },
                    {
                        "id" : "653131",
                        "name" : "塔什库尔干塔吉克",
                        "pinyin" : "TaShiKuErGanTaJiKe"
                    }
                ],
                "name" : "喀什",
                "pinyin" : "KaShiDiQu"
            },
            {
                "id" : 653200,
                "list" : [
                    {
                        "id" : "653201市",
                        "name" : "和田市",
                        "pinyin" : "HeTianShi"
                    },
                    {
                        "id" : "653221",
                        "name" : "和田县",
                        "pinyin" : "HeTianXian"
                    },
                    {
                        "id" : "653222",
                        "name" : "墨玉",
                        "pinyin" : "MoYu"
                    },
                    {
                        "id" : "653223",
                        "name" : "皮山",
                        "pinyin" : "PiShan"
                    },
                    {
                        "id" : "653224",
                        "name" : "洛浦",
                        "pinyin" : "LuoPu"
                    },
                    {
                        "id" : "653225",
                        "name" : "策勒",
                        "pinyin" : "CeLe"
                    },
                    {
                        "id" : "653226",
                        "name" : "于田",
                        "pinyin" : "YuTian"
                    },
                    {
                        "id" : "653227",
                        "name" : "民丰",
                        "pinyin" : "MinFeng"
                    }
                ],
                "name" : "和田",
                "pinyin" : "HeTian"
            },
            {
                "id" : 654000,
                "list" : [
                    {
                        "id" : "654002",
                        "name" : "伊宁市",
                        "pinyin" : "YiNingShi"
                    },
                    {
                        "id" : "654003",
                        "name" : "奎屯",
                        "pinyin" : "KuiTun"
                    },
                    {
                        "id" : "654004",
                        "name" : "霍尔果斯",
                        "pinyin" : "HuoErGuoSi"
                    },
                    {
                        "id" : "654021",
                        "name" : "伊宁县",
                        "pinyin" : "YiNingXian"
                    },
                    {
                        "id" : "654022",
                        "name" : "察布查尔",
                        "pinyin" : "ChaBuChaEr"
                    },
                    {
                        "id" : "654023",
                        "name" : "霍城",
                        "pinyin" : "HuoCheng"
                    },
                    {
                        "id" : "654024",
                        "name" : "巩留",
                        "pinyin" : "GongLiu"
                    },
                    {
                        "id" : "654025",
                        "name" : "新源",
                        "pinyin" : "XinYuan"
                    },
                    {
                        "id" : "654026",
                        "name" : "昭苏",
                        "pinyin" : "ZhaoSu"
                    },
                    {
                        "id" : "654027",
                        "name" : "特克斯",
                        "pinyin" : "TeKeSi"
                    },
                    {
                        "id" : "654028",
                        "name" : "尼勒克",
                        "pinyin" : "NiLeKe"
                    }
                ],
                "name" : "伊犁",
                "pinyin" : "YiLi"
            },
            {
                "id" : 654200,
                "list" : [
                    {
                        "id" : "654201",
                        "name" : "塔城市",
                        "pinyin" : "TaChengShi"
                    },
                    {
                        "id" : "654202",
                        "name" : "乌苏",
                        "pinyin" : "WuSu"
                    },
                    {
                        "id" : "654221",
                        "name" : "额敏",
                        "pinyin" : "EMin"
                    },
                    {
                        "id" : "654223",
                        "name" : "沙湾",
                        "pinyin" : "ShaWan"
                    },
                    {
                        "id" : "654224",
                        "name" : "托里",
                        "pinyin" : "TuoLi"
                    },
                    {
                        "id" : "654225",
                        "name" : "裕民",
                        "pinyin" : "YuMin"
                    },
                    {
                        "id" : "654226",
                        "name" : "和布克赛尔",
                        "pinyin" : "HeBuKeSaiEr"
                    }
                ],
                "name" : "塔城",
                "pinyin" : "TaCheng"
            },
            {
                "id" : 654300,
                "list" : [
                    {
                        "id" : "654301",
                        "name" : "阿勒泰市",
                        "pinyin" : "ALeTaiShi"
                    },
                    {
                        "id" : "654321",
                        "name" : "布尔津",
                        "pinyin" : "BuErJin"
                    },
                    {
                        "id" : "654322",
                        "name" : "富蕴",
                        "pinyin" : "FuYun"
                    },
                    {
                        "id" : "654323",
                        "name" : "福海",
                        "pinyin" : "FuHai"
                    },
                    {
                        "id" : "654324",
                        "name" : "哈巴河",
                        "pinyin" : "HaBaHe"
                    },
                    {
                        "id" : "654325",
                        "name" : "青河",
                        "pinyin" : "QingHe"
                    },
                    {
                        "id" : "654326",
                        "name" : "吉木乃",
                        "pinyin" : "JiMuNai"
                    }
                ],
                "name" : "阿勒泰",
                "pinyin" : "ALeTai"
            },
            {
                "id" : 659001,
                "list" : [
                    {
                        "id" : "659001",
                        "name" : "石河子",
                        "pinyin" : "ShiHeZi"
                    }
                ],
                "name" : "石河子",
                "pinyin" : "ShiHeZi"
            },
            {
                "id" : 659002,
                "list" : [
                    {
                        "id" : "659002",
                        "name" : "阿拉尔",
                        "pinyin" : "ALaEr"
                    }
                ],
                "name" : "阿拉尔",
                "pinyin" : "ALaEr"
            },
            {
                "id" : 659003,
                "list" : [
                    {
                        "id" : "659003",
                        "name" : "图木舒克",
                        "pinyin" : "TuMuShuKe"
                    }
                ],
                "name" : "图木舒克",
                "pinyin" : "TuMuShuKe"
            },
            {
                "id" : 659004,
                "list" : [
                    {
                        "id" : "659004",
                        "name" : "五家渠",
                        "pinyin" : "WuJiaQu"
                    }
                ],
                "name" : "五家渠",
                "pinyin" : "WuJiaQu"
            },
            {
                "id" : 659005,
                "list" : [
                    {
                        "id" : "659005",
                        "name" : "北屯",
                        "pinyin" : "BeiTun"
                    }
                ],
                "name" : "北屯",
                "pinyin" : "BeiTun"
            },
            {
                "id" : 659006,
                "list" : [
                    {
                        "id" : "659006",
                        "name" : "铁门关",
                        "pinyin" : "TieMenGuan"
                    }
                ],
                "name" : "铁门关",
                "pinyin" : "TieMenGuan"
            },
            {
                "id" : 659007,
                "list" : [
                    {
                        "id" : "659007",
                        "name" : "双河",
                        "pinyin" : "ShuangHe"
                    }
                ],
                "name" : "双河",
                "pinyin" : "ShuangHe"
            },
            {
                "id" : 659008,
                "list" : [
                    {
                        "id" : "659008",
                        "name" : "可克达拉",
                        "pinyin" : "KeKeDaLa"
                    }
                ],
                "name" : "可克达拉",
                "pinyin" : "KeKeDaLa"
            },
            {
                "id" : 659009,
                "list" : [

                ],
                "name" : "昆玉",
                "pinyin" : "KunYu"
            }
        ],
        "name" : "新疆",
        "pinyin" : "XinJiang"
    },
    {
        "id" : 710000,
        "list" : [
            {
                "id" : 710100,
                "list" : [
                    {
                        "id" : "710102",
                        "name" : "松山区",
                        "pinyin" : "SongShanQu"
                    },
                    {
                        "id" : "710103",
                        "name" : "大同区",
                        "pinyin" : "DaTongQu"
                    },
                    {
                        "id" : "710104",
                        "name" : "文山区",
                        "pinyin" : "WenShanQu"
                    },
                    {
                        "id" : "710105",
                        "name" : "信义区",
                        "pinyin" : "XinYiQu"
                    },
                    {
                        "id" : "710106",
                        "name" : "内湖区",
                        "pinyin" : "NaHuQu"
                    },
                    {
                        "id" : "710107",
                        "name" : "中正区",
                        "pinyin" : "ZhongZhengQu"
                    },
                    {
                        "id" : "710108",
                        "name" : "万华区",
                        "pinyin" : "WanHuaQu"
                    },
                    {
                        "id" : "710109",
                        "name" : "中山区",
                        "pinyin" : "ZhongShanQu"
                    },
                    {
                        "id" : "710110",
                        "name" : "士林区",
                        "pinyin" : "ShiLinQu"
                    },
                    {
                        "id" : "710111",
                        "name" : "南港区",
                        "pinyin" : "NanGangQu"
                    }
                ],
                "name" : "台北市",
                "pinyin" : "TaiBeiShi"
            },
            {
                "id" : 710200,
                "list" : [
                    {
                        "id" : "710201",
                        "name" : "小港区",
                        "pinyin" : "XiaoGangQu"
                    },
                    {
                        "id" : "710202",
                        "name" : "左营区",
                        "pinyin" : "ZuoYingQu"
                    },
                    {
                        "id" : "710203",
                        "name" : "大寮区",
                        "pinyin" : "DaLiaoQu"
                    },
                    {
                        "id" : "710204",
                        "name" : "那玛夏区",
                        "pinyin" : "NaMaXiaQu"
                    },
                    {
                        "id" : "710205",
                        "name" : "旗津区",
                        "pinyin" : "QiJinQu"
                    },
                    {
                        "id" : "710206",
                        "name" : "弥陀区",
                        "pinyin" : "MiTuoQu"
                    },
                    {
                        "id" : "710207",
                        "name" : "燕巢区",
                        "pinyin" : "YanChaoQu"
                    },
                    {
                        "id" : "710208",
                        "name" : "大树区",
                        "pinyin" : "DaShuQu"
                    },
                    {
                        "id" : "710210",
                        "name" : "楠梓区",
                        "pinyin" : "NanZiQu"
                    },
                    {
                        "id" : "710211",
                        "name" : "鸟松区",
                        "pinyin" : "NiaoSongQu"
                    },
                    {
                        "id" : "710212",
                        "name" : "苓雅区",
                        "pinyin" : "LingYaQu"
                    },
                    {
                        "id" : "710213",
                        "name" : "桥头区",
                        "pinyin" : "QiaoTouQu"
                    },
                    {
                        "id" : "710214",
                        "name" : "梓官区",
                        "pinyin" : "ZiGuanQu"
                    },
                    {
                        "id" : "710215",
                        "name" : "美浓区",
                        "pinyin" : "MeiNongQu"
                    },
                    {
                        "id" : "710216",
                        "name" : "仁武区",
                        "pinyin" : "RenWuQu"
                    },
                    {
                        "id" : "710217",
                        "name" : "凤山区",
                        "pinyin" : "FengShanQu"
                    },
                    {
                        "id" : "710218",
                        "name" : "甲仙区",
                        "pinyin" : "JiaXianQu"
                    },
                    {
                        "id" : "710219",
                        "name" : "茄萣区",
                        "pinyin" : "QieQu"
                    },
                    {
                        "id" : "710220",
                        "name" : "大社区",
                        "pinyin" : "DaSheQu"
                    },
                    {
                        "id" : "710221",
                        "name" : "前镇区",
                        "pinyin" : "QianZhenQu"
                    },
                    {
                        "id" : "710222",
                        "name" : "茂林区",
                        "pinyin" : "MaoLinQu"
                    },
                    {
                        "id" : "710223",
                        "name" : "湖内区",
                        "pinyin" : "HuNaQu"
                    },
                    {
                        "id" : "710224",
                        "name" : "六龟区",
                        "pinyin" : "LiuGuiQu"
                    },
                    {
                        "id" : "710225",
                        "name" : "林园区",
                        "pinyin" : "LinYuanQu"
                    },
                    {
                        "id" : "710226",
                        "name" : "内门区",
                        "pinyin" : "NaMenQu"
                    },
                    {
                        "id" : "710227",
                        "name" : "鼓山区",
                        "pinyin" : "GuShanQu"
                    },
                    {
                        "id" : "710228",
                        "name" : "杉林区",
                        "pinyin" : "ShanLinQu"
                    },
                    {
                        "id" : "710229",
                        "name" : "三民区",
                        "pinyin" : "SanMinQu"
                    },
                    {
                        "id" : "710230",
                        "name" : "前金区",
                        "pinyin" : "QianJinQu"
                    },
                    {
                        "id" : "710231",
                        "name" : "冈山区",
                        "pinyin" : "GangShanQu"
                    },
                    {
                        "id" : "710232",
                        "name" : "田寮区",
                        "pinyin" : "TianLiaoQu"
                    },
                    {
                        "id" : "710233",
                        "name" : "桃源区",
                        "pinyin" : "TaoYuanQu"
                    },
                    {
                        "id" : "710234",
                        "name" : "盐埕区",
                        "pinyin" : "YanChengQu"
                    },
                    {
                        "id" : "710235",
                        "name" : "旗山区",
                        "pinyin" : "QiShanQu"
                    },
                    {
                        "id" : "710236",
                        "name" : "永安区",
                        "pinyin" : "YongAnQu"
                    },
                    {
                        "id" : "710237",
                        "name" : "路竹区",
                        "pinyin" : "LuZhuQu"
                    },
                    {
                        "id" : "710238",
                        "name" : "新兴区",
                        "pinyin" : "XinXingQu"
                    }
                ],
                "name" : "高雄市",
                "pinyin" : "GaoXiongShi"
            },
            {
                "id" : 710300,
                "list" : [
                    {
                        "id" : "710302",
                        "name" : "三峡区",
                        "pinyin" : "SanXiaQu"
                    },
                    {
                        "id" : "710303",
                        "name" : "新庄区",
                        "pinyin" : "XinZhuangQu"
                    },
                    {
                        "id" : "710304",
                        "name" : "坪林区",
                        "pinyin" : "PingLinQu"
                    },
                    {
                        "id" : "710305",
                        "name" : "莺歌区",
                        "pinyin" : "YingGeQu"
                    },
                    {
                        "id" : "710306",
                        "name" : "汐止区",
                        "pinyin" : "XiZhiQu"
                    },
                    {
                        "id" : "710307",
                        "name" : "深坑区",
                        "pinyin" : "ShenKangQu"
                    },
                    {
                        "id" : "710308",
                        "name" : "芦洲区",
                        "pinyin" : "LuZhouQu"
                    },
                    {
                        "id" : "710309",
                        "name" : "三芝区",
                        "pinyin" : "SanZhiQu"
                    },
                    {
                        "id" : "710310",
                        "name" : "三重区",
                        "pinyin" : "SanZhongQu"
                    },
                    {
                        "id" : "710311",
                        "name" : "平溪区",
                        "pinyin" : "PingXiQu"
                    },
                    {
                        "id" : "710312",
                        "name" : "中和区",
                        "pinyin" : "ZhongHeQu"
                    },
                    {
                        "id" : "710313",
                        "name" : "永和区",
                        "pinyin" : "YongHeQu"
                    },
                    {
                        "id" : "710314",
                        "name" : "泰山区",
                        "pinyin" : "TaiShanQu"
                    },
                    {
                        "id" : "710315",
                        "name" : "土城区",
                        "pinyin" : "TuChengQu"
                    },
                    {
                        "id" : "710316",
                        "name" : "石碇区",
                        "pinyin" : "ShiDingQu"
                    },
                    {
                        "id" : "710317",
                        "name" : "五股区",
                        "pinyin" : "WuGuQu"
                    },
                    {
                        "id" : "710318",
                        "name" : "林口区",
                        "pinyin" : "LinKouQu"
                    },
                    {
                        "id" : "710319",
                        "name" : "双溪区",
                        "pinyin" : "ShuangXiQu"
                    },
                    {
                        "id" : "710320",
                        "name" : "树林区",
                        "pinyin" : "ShuLinQu"
                    },
                    {
                        "id" : "710321",
                        "name" : "板桥区",
                        "pinyin" : "BanQiaoQu"
                    },
                    {
                        "id" : "710322",
                        "name" : "新店区",
                        "pinyin" : "XinDianQu"
                    },
                    {
                        "id" : "710323",
                        "name" : "乌来区",
                        "pinyin" : "WuLaiQu"
                    },
                    {
                        "id" : "710324",
                        "name" : "贡寮区",
                        "pinyin" : "GongLiaoQu"
                    },
                    {
                        "id" : "710325",
                        "name" : "金山区",
                        "pinyin" : "JinShanQu"
                    },
                    {
                        "id" : "710326",
                        "name" : "万里区",
                        "pinyin" : "WanLiQu"
                    },
                    {
                        "id" : "710327",
                        "name" : "淡水区",
                        "pinyin" : "DanShuiQu"
                    },
                    {
                        "id" : "710328",
                        "name" : "瑞芳区",
                        "pinyin" : "RuiFangQu"
                    },
                    {
                        "id" : "710329",
                        "name" : "石门区",
                        "pinyin" : "ShiMenQu"
                    }
                ],
                "name" : "新北市",
                "pinyin" : "XinBeiShi"
            },
            {
                "id" : 710400,
                "list" : [
                    {
                        "id" : "710401",
                        "name" : "东区",
                        "pinyin" : "DongQu"
                    },
                    {
                        "id" : "710402",
                        "name" : "新社区",
                        "pinyin" : "XinSheQu"
                    },
                    {
                        "id" : "710403",
                        "name" : "中区",
                        "pinyin" : "ZhongQu"
                    },
                    {
                        "id" : "710404",
                        "name" : "南屯区",
                        "pinyin" : "NanTunQu"
                    },
                    {
                        "id" : "710405",
                        "name" : "神冈区",
                        "pinyin" : "ShenGangQu"
                    },
                    {
                        "id" : "710406",
                        "name" : "沙鹿区",
                        "pinyin" : "ShaLuQu"
                    },
                    {
                        "id" : "710407",
                        "name" : "丰原区",
                        "pinyin" : "FengYuanQu"
                    },
                    {
                        "id" : "710408",
                        "name" : "大里区",
                        "pinyin" : "DaLiQu"
                    },
                    {
                        "id" : "710409",
                        "name" : "龙井区",
                        "pinyin" : "LongJingQu"
                    },
                    {
                        "id" : "710410",
                        "name" : "西区",
                        "pinyin" : "XiQu"
                    },
                    {
                        "id" : "710411",
                        "name" : "南区",
                        "pinyin" : "NanQu"
                    },
                    {
                        "id" : "710412",
                        "name" : "西屯区",
                        "pinyin" : "XiTunQu"
                    },
                    {
                        "id" : "710413",
                        "name" : "太平区",
                        "pinyin" : "TaiPingQu"
                    },
                    {
                        "id" : "710414",
                        "name" : "北屯区",
                        "pinyin" : "BeiTunQu"
                    },
                    {
                        "id" : "710415",
                        "name" : "大雅区",
                        "pinyin" : "DaYaQu"
                    },
                    {
                        "id" : "710416",
                        "name" : "石冈区",
                        "pinyin" : "ShiGangQu"
                    },
                    {
                        "id" : "710418",
                        "name" : "大甲区",
                        "pinyin" : "DaJiaQu"
                    },
                    {
                        "id" : "710419",
                        "name" : "梧栖区",
                        "pinyin" : "WuQiQu"
                    },
                    {
                        "id" : "710420",
                        "name" : "东势区",
                        "pinyin" : "DongShiQu"
                    },
                    {
                        "id" : "710421",
                        "name" : "和平区",
                        "pinyin" : "HePingQu"
                    },
                    {
                        "id" : "710422",
                        "name" : "雾峰区",
                        "pinyin" : "WuFengQu"
                    },
                    {
                        "id" : "710423",
                        "name" : "乌日区",
                        "pinyin" : "WuRiQu"
                    },
                    {
                        "id" : "710424",
                        "name" : "后里区",
                        "pinyin" : "HouLiQu"
                    },
                    {
                        "id" : "710425",
                        "name" : "潭子区",
                        "pinyin" : "TanZiQu"
                    },
                    {
                        "id" : "710426",
                        "name" : "大肚区",
                        "pinyin" : "DaDuQu"
                    },
                    {
                        "id" : "710427",
                        "name" : "外埔区",
                        "pinyin" : "WaiPuQu"
                    },
                    {
                        "id" : "710428",
                        "name" : "清水区",
                        "pinyin" : "QingShuiQu"
                    },
                    {
                        "id" : "710429",
                        "name" : "大安区",
                        "pinyin" : "DaAnQu"
                    }
                ],
                "name" : "台中市",
                "pinyin" : "TaiZhongShi"
            },
            {
                "id" : 710500,
                "list" : [
                    {
                        "id" : "710502",
                        "name" : "大内区",
                        "pinyin" : "DaNaQu"
                    },
                    {
                        "id" : "710503",
                        "name" : "东山区",
                        "pinyin" : "DongShanQu"
                    },
                    {
                        "id" : "710504",
                        "name" : "下营区",
                        "pinyin" : "XiaYingQu"
                    },
                    {
                        "id" : "710505",
                        "name" : "山上区",
                        "pinyin" : "ShanShangQu"
                    },
                    {
                        "id" : "710506",
                        "name" : "永康区",
                        "pinyin" : "YongKangQu"
                    },
                    {
                        "id" : "710507",
                        "name" : "新营区",
                        "pinyin" : "XinYingQu"
                    },
                    {
                        "id" : "710508",
                        "name" : "白河区",
                        "pinyin" : "BaiHeQu"
                    },
                    {
                        "id" : "710509",
                        "name" : "盐水区",
                        "pinyin" : "YanShuiQu"
                    },
                    {
                        "id" : "710510",
                        "name" : "归仁区",
                        "pinyin" : "GuiRenQu"
                    },
                    {
                        "id" : "710511",
                        "name" : "龙崎区",
                        "pinyin" : "LongQiQu"
                    },
                    {
                        "id" : "710512",
                        "name" : "关庙区",
                        "pinyin" : "GuanMiaoQu"
                    },
                    {
                        "id" : "710513",
                        "name" : "仁德区",
                        "pinyin" : "RenDeQu"
                    },
                    {
                        "id" : "710514",
                        "name" : "学甲区",
                        "pinyin" : "XueJiaQu"
                    },
                    {
                        "id" : "710515",
                        "name" : "善化区",
                        "pinyin" : "ShanHuaQu"
                    },
                    {
                        "id" : "710516",
                        "name" : "新市区",
                        "pinyin" : "XinShiQu"
                    },
                    {
                        "id" : "710517",
                        "name" : "佳里区",
                        "pinyin" : "JiaLiQu"
                    },
                    {
                        "id" : "710518",
                        "name" : "新化区",
                        "pinyin" : "XinHuaQu"
                    },
                    {
                        "id" : "710519",
                        "name" : "西港区",
                        "pinyin" : "XiGangQu"
                    },
                    {
                        "id" : "710520",
                        "name" : "麻豆区",
                        "pinyin" : "MaDouQu"
                    },
                    {
                        "id" : "710521",
                        "name" : "安南区",
                        "pinyin" : "AnNanQu"
                    },
                    {
                        "id" : "710522",
                        "name" : "后壁区",
                        "pinyin" : "HouBiQu"
                    },
                    {
                        "id" : "710523",
                        "name" : "柳营区",
                        "pinyin" : "LiuYingQu"
                    },
                    {
                        "id" : "710524",
                        "name" : "玉井区",
                        "pinyin" : "YuJingQu"
                    },
                    {
                        "id" : "710525",
                        "name" : "官田区",
                        "pinyin" : "GuanTianQu"
                    },
                    {
                        "id" : "710526",
                        "name" : "东区",
                        "pinyin" : "DongQu"
                    },
                    {
                        "id" : "710527",
                        "name" : "六甲区",
                        "pinyin" : "LiuJiaQu"
                    },
                    {
                        "id" : "710528",
                        "name" : "中西区",
                        "pinyin" : "ZhongXiQu"
                    },
                    {
                        "id" : "710529",
                        "name" : "北区",
                        "pinyin" : "BeiQu"
                    },
                    {
                        "id" : "710530",
                        "name" : "楠西区",
                        "pinyin" : "NanXiQu"
                    },
                    {
                        "id" : "710531",
                        "name" : "南化区",
                        "pinyin" : "NanHuaQu"
                    },
                    {
                        "id" : "710532",
                        "name" : "左镇区",
                        "pinyin" : "ZuoZhenQu"
                    },
                    {
                        "id" : "710533",
                        "name" : "南区",
                        "pinyin" : "NanQu"
                    },
                    {
                        "id" : "710534",
                        "name" : "安平区",
                        "pinyin" : "AnPingQu"
                    },
                    {
                        "id" : "710535",
                        "name" : "北门区",
                        "pinyin" : "BeiMenQu"
                    },
                    {
                        "id" : "710536",
                        "name" : "七股区",
                        "pinyin" : "QiGuQu"
                    },
                    {
                        "id" : "710537",
                        "name" : "将军区",
                        "pinyin" : "JiangJunQu"
                    }
                ],
                "name" : "台南市",
                "pinyin" : "TaiNanShi"
            },
            {
                "id" : 710600,
                "list" : [
                    {
                        "id" : "710601",
                        "name" : "平镇市",
                        "pinyin" : "PingZhenShi"
                    },
                    {
                        "id" : "710602",
                        "name" : "杨梅市",
                        "pinyin" : "YangMeiShi"
                    },
                    {
                        "id" : "710604",
                        "name" : "桃园区",
                        "pinyin" : "TaoYuanQu"
                    },
                    {
                        "id" : "710605",
                        "name" : "龟山乡",
                        "pinyin" : "GuiShanXiang"
                    },
                    {
                        "id" : "710606",
                        "name" : "复兴乡",
                        "pinyin" : "FuXingXiang"
                    },
                    {
                        "id" : "710607",
                        "name" : "芦竹乡",
                        "pinyin" : "LuZhuXiang"
                    },
                    {
                        "id" : "710608",
                        "name" : "观音乡",
                        "pinyin" : "GuanYinXiang"
                    },
                    {
                        "id" : "710609",
                        "name" : "龙潭乡",
                        "pinyin" : "LongTanXiang"
                    },
                    {
                        "id" : "710610",
                        "name" : "大溪镇",
                        "pinyin" : "DaXiZhen"
                    },
                    {
                        "id" : "710611",
                        "name" : "大园乡",
                        "pinyin" : "DaYuanXiang"
                    },
                    {
                        "id" : "710612",
                        "name" : "新屋乡",
                        "pinyin" : "XinWuXiang"
                    },
                    {
                        "id" : "710613",
                        "name" : "中坜市",
                        "pinyin" : "ZhongLiShi"
                    }
                ],
                "name" : "桃园市",
                "pinyin" : "TaoYuanShi"
            },
            {
                "id" : 719001,
                "list" : [
                    {
                        "id" : "719001",
                        "name" : "基隆市",
                        "pinyin" : "JiLongShi"
                    }
                ],
                "name" : "基隆市",
                "pinyin" : "JiLongShi"
            },
            {
                "id" : 719002,
                "list" : [
                    {
                        "id" : "719002",
                        "name" : "新竹市",
                        "pinyin" : "XinZhuShi"
                    }
                ],
                "name" : "新竹市",
                "pinyin" : "XinZhuShi"
            },
            {
                "id" : 719003,
                "list" : [
                    {
                        "id" : "719003",
                        "name" : "嘉义市",
                        "pinyin" : "JiaYiShi"
                    }
                ],
                "name" : "嘉义市",
                "pinyin" : "JiaYiShi"
            },
            {
                "id" : 719004,
                "list" : [
                    {
                        "id" : "719004",
                        "name" : "新竹县",
                        "pinyin" : "XinZhuXian"
                    }
                ],
                "name" : "新竹县",
                "pinyin" : "XinZhuXian"
            },
            {
                "id" : 719005,
                "list" : [
                    {
                        "id" : "719005",
                        "name" : "宜兰县",
                        "pinyin" : "YiLanXian"
                    }
                ],
                "name" : "宜兰县",
                "pinyin" : "YiLanXian"
            },
            {
                "id" : 719006,
                "list" : [
                    {
                        "id" : "719006",
                        "name" : "苗栗县",
                        "pinyin" : "MiaoLiXian"
                    }
                ],
                "name" : "苗栗县",
                "pinyin" : "MiaoLiXian"
            },
            {
                "id" : 719007,
                "list" : [
                    {
                        "id" : "719007",
                        "name" : "彰化县",
                        "pinyin" : "ZhangHuaXian"
                    }
                ],
                "name" : "彰化县",
                "pinyin" : "ZhangHuaXian"
            },
            {
                "id" : 719008,
                "list" : [
                    {
                        "id" : "719008",
                        "name" : "云林县",
                        "pinyin" : "YunLinXian"
                    }
                ],
                "name" : "云林县",
                "pinyin" : "YunLinXian"
            },
            {
                "id" : 719009,
                "list" : [
                    {
                        "id" : "719009",
                        "name" : "南投县",
                        "pinyin" : "NanTouXian"
                    }
                ],
                "name" : "南投县",
                "pinyin" : "NanTouXian"
            },
            {
                "id" : 719010,
                "list" : [
                    {
                        "id" : "719010",
                        "name" : "嘉义县",
                        "pinyin" : "JiaYiXian"
                    }
                ],
                "name" : "嘉义县",
                "pinyin" : "JiaYiXian"
            },
            {
                "id" : 719011,
                "list" : [
                    {
                        "id" : "719011",
                        "name" : "屏东县",
                        "pinyin" : "PingDongXian"
                    }
                ],
                "name" : "屏东县",
                "pinyin" : "PingDongXian"
            },
            {
                "id" : 719012,
                "list" : [
                    {
                        "id" : "719012",
                        "name" : "台东县",
                        "pinyin" : "TaiDongXian"
                    }
                ],
                "name" : "台东县",
                "pinyin" : "TaiDongXian"
            },
            {
                "id" : 719013,
                "list" : [
                    {
                        "id" : "719013",
                        "name" : "花莲县",
                        "pinyin" : "HuaLianXian"
                    }
                ],
                "name" : "花莲县",
                "pinyin" : "HuaLianXian"
            },
            {
                "id" : 719014,
                "list" : [
                    {
                        "id" : "719014",
                        "name" : "澎湖县",
                        "pinyin" : "PengHuXian"
                    }
                ],
                "name" : "澎湖县",
                "pinyin" : "PengHuXian"
            }
        ],
        "name" : "台湾省",
        "pinyin" : "TaiWanSheng"
    },
    {
        "id" : 810000,
        "list" : [
            {
                "id" : 810000,
                "list" : [
                    {
                        "id" : "810101",
                        "name" : "中西区",
                        "pinyin" : "ZhongXiQu"
                    },
                    {
                        "id" : "810102",
                        "name" : "东区",
                        "pinyin" : "DongQu"
                    },
                    {
                        "id" : "810103",
                        "name" : "九龙城区",
                        "pinyin" : "JiuLongChengQu"
                    },
                    {
                        "id" : "810104",
                        "name" : "观塘区",
                        "pinyin" : "GuanTangQu"
                    },
                    {
                        "id" : "810105",
                        "name" : "南区",
                        "pinyin" : "NanQu"
                    },
                    {
                        "id" : "810106",
                        "name" : "深水埗区",
                        "pinyin" : "ShenShuiQu"
                    },
                    {
                        "id" : "810107",
                        "name" : "湾仔区",
                        "pinyin" : "WanZiQu"
                    },
                    {
                        "id" : "810108",
                        "name" : "黄大仙区",
                        "pinyin" : "HuangDaXianQu"
                    },
                    {
                        "id" : "810109",
                        "name" : "油尖旺区",
                        "pinyin" : "YouJianWangQu"
                    },
                    {
                        "id" : "810110",
                        "name" : "离岛区",
                        "pinyin" : "LiDaoQu"
                    },
                    {
                        "id" : "810111",
                        "name" : "葵青区",
                        "pinyin" : "KuiQingQu"
                    },
                    {
                        "id" : "810113",
                        "name" : "西贡区",
                        "pinyin" : "XiGongQu"
                    },
                    {
                        "id" : "810114",
                        "name" : "沙田区",
                        "pinyin" : "ShaTianQu"
                    },
                    {
                        "id" : "810115",
                        "name" : "屯门区",
                        "pinyin" : "TunMenQu"
                    },
                    {
                        "id" : "810117",
                        "name" : "荃湾区",
                        "pinyin" : "QuanWanQu"
                    },
                    {
                        "id" : "810118",
                        "name" : "元朗区",
                        "pinyin" : "YuanLangQu"
                    }
                ],
                "name" : "香港",
                "pinyin" : "XiangGang"
            }
        ],
        "name" : "香港",
        "pinyin" : "XiangGang"
    },
    {
        "id" : 820000,
        "list" : [
            {
                "id" : 820000,
                "list" : [
                    {
                        "id" : "820001",
                        "name" : "花王堂区",
                        "pinyin" : "HuaWangTangQu"
                    },
                    {
                        "id" : "820002",
                        "name" : "望德堂区",
                        "pinyin" : "WangDeTangQu"
                    },
                    {
                        "id" : "820003",
                        "name" : "风顺堂区",
                        "pinyin" : "FengShunTangQu"
                    },
                    {
                        "id" : "820004",
                        "name" : "圣方济各堂区",
                        "pinyin" : "ShengFangJiGeTangQu"
                    },
                    {
                        "id" : "820005",
                        "name" : "嘉模堂区",
                        "pinyin" : "JiaMoTangQu"
                    },
                    {
                        "id" : "820007",
                        "name" : "路氹填海区",
                        "pinyin" : "LuTianHaiQu"
                    },
                    {
                        "id" : "820008",
                        "name" : "花地玛堂区",
                        "pinyin" : "HuaDiMaTangQu"
                    }
                ],
                "name" : "澳门",
                "pinyin" : "AoMen"
            }
        ],
        "name" : "澳门",
        "pinyin" : "AoMen"
    },
    {
        "id" : 999999,
        "list" : [
            {
                "id" : 990100,
                "list" : [

                ],
                "name" : "海外",
                "pinyin" : "HaiWai"
            }
        ],
        "name" : "海外",
        "pinyin" : "HaiWai"
    }
]
export default {
    city
}